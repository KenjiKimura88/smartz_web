import buildSchema from 'redux-form-schema'
import R from 'ramda'

export const getTotalShareForBeneficiariesType = R.compose(
  R.sum,
  R.map(R.prop('share'))
)

export default ({ types }) => {
  const schema = buildSchema({
    'beneficiaries[].id': {
      label: 'ID'
    },
    'beneficiaries[].name': {
      label: 'Name'
    },
    'beneficiaries[].relationship': {
      label: 'Relationship'
    },
    'beneficiaries[].birthdate': {
      label: 'Birthdate'
    },
    'beneficiaries[].share': {
      label: 'Share'
    },
    'beneficiaries[].type': {
      label: 'Primary or Contingent'
    },
    // These two are used only for storing errors
    primary: {
      label: 'Primary Beneficiaries'
    },
    contingent: {
      label: 'Contingent Beneficiaries'
    }
  })

  const getBeneficiariesByType = (typeLabel) => R.filter(R.propEq('type', types[typeLabel]))

  const validateBeneficiariesType = (beneficiaries, typeLabel) => {
    const beneficiariesOfType = getBeneficiariesByType(typeLabel)(beneficiaries)
    const totalShareOfType = getTotalShareForBeneficiariesType(beneficiariesOfType)
    if (R.length(beneficiariesOfType) < 1) {
      return `You should enter at least 1 ${typeLabel} beneficiary.`
    } else if (!R.equals(totalShareOfType, 100)) {
      return `Total shares for ${typeLabel} beneficiaries should equal 100%.`
    }
  }

  const validate = ({ beneficiaries }) =>
    R.reject(R.isEmpty, {
      primary: validateBeneficiariesType(beneficiaries, 'primary'),
      contingent: validateBeneficiariesType(beneficiaries, 'contingent')
    })

  return R.merge(schema, { validate })
}
