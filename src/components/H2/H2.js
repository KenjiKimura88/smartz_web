import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './H2.scss'
import Text from 'components/Text'

export default class H2 extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    noMargin: PropTypes.bool
  };

  render () {
    const { children, className, noMargin, ...props } = this.props
    const finalClassName = classNames(classes.h2, {
      [classes.noMargin]: noMargin,
      [className]: !!className
    })

    return (
      <Text className={finalClassName} size='large' tagName='div' {...props}>
        {children}
      </Text>
    )
  }
}
