import React, { Component, PropTypes } from 'react'
import { debounce } from 'lodash'
import { LinkContainer } from 'react-router-bootstrap'
import { Col, Nav, NavItem, Row } from 'react-bootstrap'
import { WindowResizeListener } from 'react-window-resize-listener'
import R from 'ramda'
import ReactDOM from 'react-dom'
import { BzArrowRight } from 'icons'
import { isMarried, getPeerGroupExpenseRate, serializeRetirementIncome,
  serializeRetirementPlan, sumAmount, sumArrayBy, sumContributionsAmount,
  sumTaxesAmount, toExpenseAmount, toMonthlyIncome } from '../../helpers'
import { requestIsPending } from 'helpers/requests'
import AssumptionsAndSettings from '../AssumptionsAndSettings'
import Button from 'components/Button'
import CashFlow from '../CashFlow'
import classes from './RetiresmartzWizard.scss'
import config from 'config'
import ConfirmSaveModal from '../ConfirmSaveModal'
import ContentPane from 'components/ContentPane'
import Finances from '../Finances'
import MainFrame from 'components/MainFrame'
import PersonalDetails from '../PersonalDetails'
import Sidebar from 'components/Sidebar'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import YourRetirement from '../YourRetirement'
import Well from 'components/Well'
import WizardPage from '../WizardPage'

const { expensesCategories } = config

const pages = [
  {
    label: 'Personal Details',
    id: 'personal-details',
    component: PersonalDetails
  },
  {
    label: 'Your Retirement',
    id: 'your-retirement',
    component: YourRetirement
  },
  {
    label: 'Cash Flow',
    id: 'cash-flow',
    component: CashFlow
  },
  {
    label: 'Finances',
    id: 'finances',
    component: Finances
  },
  {
    label: 'Assumptions and Settings',
    id: 'assumptions-and-settings',
    component: AssumptionsAndSettings
  }
]

const getVisibleHeight = R.curry((container, element) => {
  if (!element) {
    return 0
  }
  const { offsetHeight: elHeight, offsetTop: elOffset } = element
  const { offsetHeight: containerHeight, scrollTop } = container

  if ((scrollTop > elOffset) && (scrollTop <= elOffset + elHeight)) {
    return elHeight - (scrollTop - elOffset)
  } else if ((scrollTop > elOffset + elHeight - containerHeight)
    && (scrollTop <= elOffset)) {
    return elHeight
  } else if ((scrollTop > elOffset - containerHeight)
    && (scrollTop <= elOffset + elHeight - containerHeight)) {
    return scrollTop + containerHeight - elOffset
  } else {
    return 0
  }
})

const taxFieldsDirty = R.compose(
  R.reduce((acc, item) => (acc || R.path(['cat', 'dirty'])(item)), false),
  R.filter(R.pathEq(['cat', 'value'], expensesCategories.TAXES))
)

const sumExceedsText = (
  `Total value of monthly expenses and retirement contributions
  should not exceed monthly client income.`
)

export default class RetiresmartzWizard extends Component {
  static propTypes = {
    client: PropTypes.object,
    createIncome: PropTypes.func.isRequired,
    dirty: PropTypes.bool.isRequired,
    error: PropTypes.string,
    externalAccounts: PropTypes.array.isRequired,
    fields: PropTypes.object.isRequired,
    goals: PropTypes.array.isRequired,
    goalTypes: PropTypes.array.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    jointResendEmail: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    peerGroupExpenses: PropTypes.array.isRequired,
    reduxFormInitialize: PropTypes.func.isRequired,
    refreshClient: PropTypes.func.isRequired,
    reloadPeerGroupExpenses: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    retirementPlans: PropTypes.array.isRequired,
    router: PropTypes.object.isRequired,
    saveFile: PropTypes.func.isRequired,
    saveRetirementPlan: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    settings: PropTypes.object,
    show: PropTypes.func.isRequired,
    user: PropTypes.object,
    valid: PropTypes.bool.isRequired,
    values: PropTypes.object
  };

  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)
    this.state = {
      contentHeight: 0,
      clickedSave: false
    }
    this.handleScroll = debounce(this.handleScroll, 50)
  }

  componentDidMount() {
    this.handleScroll()
  }

  handleResize = ({ windowHeight }) => {
    const { wizard } = this.refs
    const { top } = wizard.getBoundingClientRect()
    this.setState({
      // 50px is the distance from the footer
      contentHeight: windowHeight - top - 50
    })
  }

  handleScroll = () => {
    const { context, props, refs } = this
    const { router: { replace } } = context
    const { params: { clientId, id: currentId, retirementPlanId } } = props
    const { scrollContainer } = refs
    const basePath = `/${clientId}/retiresmartz`

    // Division of visible-height/total-height of page.
    // The page with the largest ratio is declared as active.
    const ratios = R.compose(
      R.map(R.converge(R.divide, [
        getVisibleHeight(scrollContainer),
        R.prop('offsetHeight')
      ])),
      R.reject(R.isNil),
      R.map(R.compose(ReactDOM.findDOMNode, R.prop(R.__, refs))),
      R.pluck('id')
    )(pages)

    const maxRatio = R.reduce(R.max, -1, ratios)
    const pageWithMaxRatio = R.compose(
      R.nth(R.__, pages),
      R.indexOf(maxRatio)
    )(ratios)

    if (pageWithMaxRatio && !R.equals(pageWithMaxRatio.id, currentId)) {
      const pagePath = `wizard/${pageWithMaxRatio.id}`
      const newPath = retirementPlanId
        ? `${basePath}/${retirementPlanId}/${pagePath}`
        : `${basePath}/${pagePath}`
      replace(newPath)
    }
  }

  save = R.curry((callback, values) => {
    return new Promise((resolve, reject) => {
      const { createIncome, dirty, params: { clientId, retirementPlanId },
        refreshClient, saveRetirementPlan, set } = this.props
      const { clientIncome, expenses, maritalStatus, partnerIncome, retirementAccounts } = values
      const calculatedIncome = sumAmount(expenses) + sumArrayBy('contrib_amt')(retirementAccounts)
      const hasPartner = isMarried(maritalStatus)
      const totalIncome = clientIncome + (hasPartner ? R.defaultTo(0, partnerIncome) : 0)
      const totalMonthlyIncome = toMonthlyIncome(totalIncome)
      if (totalMonthlyIncome <= calculatedIncome) {
        return reject({ _error: sumExceedsText })
      }

      const clientBtc = sumContributionsAmount(retirementAccounts, 'self')
      const partnerBtc = isMarried(maritalStatus)
        ? sumContributionsAmount(retirementAccounts, 'partner')
        : 0

      const body = serializeRetirementPlan(R.merge(values, { clientBtc, partnerBtc }))
      set({ shouldCalculate: dirty && !!retirementPlanId })

      saveRetirementPlan({
        body,
        success: ({ value: { id } }) => {
          R.forEach(income => {
            createIncome({
              body: serializeRetirementIncome(income, id)
            })
          }, R.reject(R.prop('id'), values.incomes))
          refreshClient(clientId, () => callback && callback(id))
        }
      })
      resolve()
    })
  })

  setClickedSave = () => {
    this.setState({
      clickedSave: true
    })
  }

  updateExpensesFields = (prevExpenses, newExpenses) => {
    const { props } = this
    const { fields: { expenses }, reduxFormInitialize, peerGroupExpenses,
      values: { expenses: expensesValues } } = props
    const pgeTaxItem = R.find(R.propEq('cat', expensesCategories.TAXES))(peerGroupExpenses)
    const taxRate = taxFieldsDirty(expenses)
      ? sumTaxesAmount(expensesValues) / prevExpenses
      : pgeTaxItem.rate
    const prevPostTaxIncome = prevExpenses * (1 - taxRate)
    const expenseRate = (item) => (item.amt.dirty
      ? item.amt.value / prevPostTaxIncome
      : getPeerGroupExpenseRate(item.cat.value)(peerGroupExpenses)
    )
    const newExpenseAmts = R.map(item => ({
      amt: R.equals(item.who.value, 'self')
        ? R.equals(item.cat.value, expensesCategories.TAXES)
          ? item.amt.value / prevExpenses * newExpenses
          : toExpenseAmount(newExpenses, taxRate, expenseRate(item))
        : item.amt.value
    }), expenses)

    reduxFormInitialize('retirementPlan', { expenses: newExpenseAmts }, ['expenses[].amt'])
  }

  handleExitClick = () => {
    const { dirty, show } = this.props
    dirty ? show('confirmSaveModal') : this.exit()
  }

  next = (id) => {
    const { params: { clientId }, router: { replace } } = this.props
    replace(`/${clientId}/retiresmartz/${id}/plan`)
  }

  exit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  render () {
    const { handleResize, props, state } = this
    const { accounts, client, externalAccounts, fields, goals, goalTypes, handleSubmit,
      jointResendEmail, params, params: { clientId, retirementPlanId }, reloadPeerGroupExpenses,
      requests, saveFile, settings, show, user, valid, values, error } = props
    const { civil_statuses: civilStatuses, retirement_expense_categories: expenseCategories = [],
      retirement_lifestyles: lifestyles = [],
      retirement_lifestyle_categories: lifestyleCategories = [],
      retirement_account_categories: retirementAccountCategories = [] } = R.defaultTo({}, settings)
    const { clickedSave, contentHeight } = state
    const { scrollContainer } = this.refs
    const showErrorMessage = clickedSave && !valid
    const isPending = requestIsPending('saveRetirementPlan')(props)

    return (
      <div ref='wizard' className={classes.retiresmartzWizard}>
        <MainFrame rounded>
          <WindowResizeListener onResize={handleResize} />
          <Sidebar xs={3} className={classes.sidebar}>
            <Nav bsStyle='pills' stacked>
              {R.map(({ id, label }) =>
                <LinkContainer key={id} onClick={this.refs[id] && this.refs[id].scroll}
                  to={retirementPlanId
                    ? `/${clientId}/retiresmartz/${retirementPlanId}/wizard/${id}`
                    : `/${clientId}/retiresmartz/wizard/${id}`}>
                  <NavItem>
                    <Text>{label}</Text>
                  </NavItem>
                </LinkContainer>
              , pages)}
            </Nav>
          </Sidebar>
          <ContentPane xs={9}>
            <div ref='scrollContainer' className={classes.content}
              style={{ height: contentHeight }} onScroll={this.handleScroll}>
              <form onSubmit={handleSubmit(this.save(this.next))}>
                {R.map(({ component: PageComponent, id }) =>
                  <WizardPage key={id} ref={id} scrollContainer={scrollContainer}>
                    <PageComponent {...fields} {...params} accounts={accounts}
                      civilStatuses={civilStatuses} client={client}
                      expenseCategories={expenseCategories} externalAccounts={externalAccounts}
                      goals={goals} goalTypes={goalTypes} jointResendEmail={jointResendEmail}
                      lifestyles={lifestyles} lifestyleCategories={lifestyleCategories}
                      reloadPeerGroupExpenses={reloadPeerGroupExpenses} requests={requests}
                      retirementAccountCategories={retirementAccountCategories}
                      saveFile={saveFile} show={show} user={user}
                      updateExpensesFields={this.updateExpensesFields} values={values} />
                  </WizardPage>
                , pages)}
                {showErrorMessage && error && <Well bsStyle='danger'>{error}</Well>}
                <Row>
                  <Col xs={5}>
                    <Button onClick={this.handleExitClick}>Exit</Button>
                  </Col>
                  {showErrorMessage && (
                    <Col xs={4} className='text-right'>
                      <Well bsStyle='danger' className={classes.errorWell}>
                        Please complete required fields above.
                      </Well>
                    </Col>)}
                  <Col xs={showErrorMessage ? 3 : 7} className='text-right'>
                    <Button bsStyle={showErrorMessage ? 'danger' : 'default'}
                      type='submit' onClick={this.setClickedSave}>
                      Save and Continue&nbsp;&nbsp;
                      <BzArrowRight size={9} />
                    </Button>
                  </Col>
                </Row>
              </form>
            </div>
            <ConfirmSaveModal onCancel={this.exit} onSave={handleSubmit(this.save(this.exit))} />
            {isPending && <div className={classes.spinnerWrapper}>
              <Spinner className={classes.spinner}>
                <Text size='large' bold primary>Saving ...</Text>
              </Spinner>
            </div>}
          </ContentPane>
        </MainFrame>
      </div>
    )
  }
}
