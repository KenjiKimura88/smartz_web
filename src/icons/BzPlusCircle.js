/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzPlusCircle extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <g>
            <path d='M100,200C44.9,200,0,155.1,0,100S44.9,0,100,0s100,44.9,100,100S155.1,200,100,200z M100,11.8
c-48.7,0-88.2,39.6-88.2,88.2s39.6,88.2,88.2,88.2s88.2-39.6,88.2-88.2S148.7,11.8,100,11.8z' />
          </g>
          <g>
            <path d='M111.5,90.7h19.6c6.1,0,11.1,5,11.1,11.1v0c0,6.1-5,11.1-11.1,11.1h-19.6v23.6c0,6.1-5,11.1-11.1,11.1
h-1.2c-6.1,0-11.1-5-11.1-11.1v-23.6H68.5c-6.1,0-11.1-5-11.1-11.1v0c0-6.1,5-11.1,11.1-11.1h19.7V68.6c0-6.1,5-11.1,11.1-11.1
h1.2c6.1,0,11.1,5,11.1,11.1V90.7z' />
          </g>
        </g>
      </IconBase>
    )
  }
}
