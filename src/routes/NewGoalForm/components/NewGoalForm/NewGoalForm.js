import React, { Component, PropTypes } from 'react'
import { Col, Form, Row } from 'react-bootstrap'
import moment from 'moment'
import R from 'ramda'
import { BzArrowRight } from 'icons'
import { isAnyTrue } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import BuildWealthFields from '../BuildWealthFields'
import Button from 'components/Button'
import classes from './NewGoalForm.scss'
import Notification from 'containers/Notification'
import MajorPurchaseFields from '../MajorPurchaseFields'
import ReserveFields from '../ReserveFields'
import Spinner from 'components/Spinner'

const formMapping = {
  'build-wealth': BuildWealthFields,
  'major-purchase': MajorPurchaseFields,
  'reserve': ReserveFields
}

// getIsPending :: Props -> Boolean
const getIsPending = R.converge(isAnyTrue, [
  requestIsPending('createGoal'),
  requestIsPending('goals'),
  requestIsPending('goalTypes')
])

const getPortfolioOptions = R.compose(
  R.map(item => ({
    label: item.name,
    value: item.id
  })),
  R.defaultTo([]),
  R.prop('portfolioProviders')
)

export default class NewGoal extends Component {
  static propTypes = {
    createGoal: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    portfolioProviders: PropTypes.array.isRequired,
    router: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.checkRedirect(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.checkRedirect(nextProps)
  }

  checkRedirect(props) {
    const { params: { clientId, goalType }, router: { replace } } = props
    if (R.isNil(formMapping[goalType])) {
      replace(`/${clientId}/new-goal`)
    }
  }

  handleCreateGoal = (values) => {
    const { account, selectedGoalType, name, amount, duration, initialDeposit,
      ethicalInvestments, portfolio } = values
    const { createGoal, params: { accountId, clientId }, router: { replace } } = this.props
    const completion = moment().add(duration, 'years').format('YYYY-MM-DD')
    const body = {
      account,
      type: selectedGoalType.id,
      name,
      target: R.is(Number, amount) ? parseFloat(amount) : 0,
      completion,
      initial_deposit: R.is(Number, initialDeposit) ? parseFloat(initialDeposit) : undefined,
      ethical_investments: ethicalInvestments,
      portfolio_provider: portfolio
    }
    createGoal({
      body,
      success: ({ value }) => {
        replace(`/${clientId}/new-goal/account/${accountId}/complete/${value.id}`)
      }
    })
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  render () {
    const { props } = this
    const { fields, handleSubmit, invalid, params: { goalType },
      requests: { createGoal } } = props
    const FieldsComponent = formMapping[goalType]
    const isLoading = getIsPending(props)
    return (
      <Form onSubmit={handleSubmit(this.handleCreateGoal)} className={classes.wrapper}>
        <Notification isRetrying={isLoading} request={createGoal} />
        {FieldsComponent && <FieldsComponent fields={fields}
          portfolioOptions={getPortfolioOptions(props)} />}
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button bsStyle='primary' type='submit' disabled={isLoading || invalid}>
              Continue <BzArrowRight />
            </Button>
          </Col>
        </Row>
        {isLoading && <div className={classes.spinner}>
          <div className={classes.vhcenter}>
            <Spinner />
          </div>
        </div>}
      </Form>
    )
  }
}
