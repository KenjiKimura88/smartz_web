import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup } from 'react-bootstrap'
import R from 'ramda'
import AdvisorEmail from 'components/AdvisorEmail'
import AdvisorName from 'components/AdvisorName'
import AdvisorPhone from 'components/AdvisorPhone'

const renderAdvisorRow = (advisor, key) =>
  <FormControl.Static componentClass='div' key={key}>
    <AdvisorName advisor={advisor} /><br />
    <AdvisorPhone advisor={advisor} /><br />
    <AdvisorEmail advisor={advisor} />
  </FormControl.Static>

const getSecondaryAdvisors = R.path(['client', 'secondary_advisors'])

export default class AdvisorPanel extends Component {
  static propTypes = {
    client: PropTypes.object.isRequired
  };

  render() {
    const { props } = this
    const { client } = props
    const advisor = client && client.advisor
    const secondaryAdvisors = getSecondaryAdvisors(props)

    return (
      <div className='form-horizontal'>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>
            Primary Advisor
          </Col>
          <Col xs={9}>
            {advisor && renderAdvisorRow(advisor)}
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>
            Secondary Advisor
          </Col>
          <Col xs={9}>
            {R.length(secondaryAdvisors) > 0
              ? R.map(advisor =>
                renderAdvisorRow(advisor, advisor.id)
              , secondaryAdvisors)
              : <FormControl.Static>N/A</FormControl.Static>
            }
          </Col>
        </FormGroup>
      </div>
    )
  }
}
