import React, { PropTypes, Component } from 'react'
import { Col, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import classNames from 'classnames'
import moment from 'moment'
import R from 'ramda'
import { BzRefresh } from 'icons'
import { MdHighlightRemove } from 'helpers/icons'
import { mapIndexed, prettyPrint } from 'helpers/pureFunctions'
import { STATUS_FULFILLED, STATUS_REJECTED, touchRequest } from 'redux/api/modules/requests'
import classes from './Notification.scss'
import InlineList from 'components/InlineList'
import Text from 'components/Text'

const renderErrorValues = (errors) => (
  R.gt(R.length(errors), 1)
    ? <ul>
      {mapIndexed((error, index) => (
        <li key={index}>{error}</li>
      ), errors)}
    </ul>
    : <span className={classes.singleError}>{errors}</span>
)

const renderErrors = (errors) => (
  R.is(Array, errors)
  ? renderErrorValues(errors)
  : <ul className={classes.errorsList}>
    {R.map(key => (
      <li key={key}>
        <Text bold>{prettyPrint(key)}</Text>:{' '}
        {R.is(Object, errors[key]) && !R.is(Array, errors[key]) && renderErrors(errors[key])}
        {R.is(Array, errors[key]) && renderErrorValues(errors[key])}
      </li>
    ), R.keys(errors))}
  </ul>
)

const defaultErrorMessage = (error) => (
  error
  ? error.reason
    ? <div>
      <Text bold>{error.reason}</Text>
      {error.errors
        ? renderErrors(error.errors)
        : `: ${error.message || error}`}
    </div>
    : renderErrors(error)
  : 'Internal Server Error'
)

const errorValue = R.path(['request', 'error'])

const notifyError = (_props) => {
  const { errorMessage } = _props
  const error = errorValue(_props)
  return typeof errorMessage === 'function'
    ? errorMessage(error)
    : errorMessage
}

const successValue = R.path(['request', 'value'])

const notifySuccess = (_props) => {
  const { successMessage } = _props
  const value = successValue(_props)
  return typeof successMessage === 'function'
    ? successMessage(value)
    : successMessage
}

class Notification extends Component {
  static propTypes = {
    className: PropTypes.string,
    errorDisabled: PropTypes.bool,
    errorMessage: PropTypes.oneOfType([
      PropTypes.func, PropTypes.node
    ]),
    hideDisabled: PropTypes.bool,
    isRetrying: PropTypes.bool,
    onRetry: PropTypes.func,
    request: PropTypes.object,
    showTimestamp: PropTypes.bool,
    successMessage: PropTypes.oneOfType([
      PropTypes.func, PropTypes.node
    ]),
    touchRequest: PropTypes.func
  };

  static defaultProps = {
    errorDisabled: false,
    errorMessage: defaultErrorMessage,
    hideDisabled: false
  }

  handleHide = () => {
    const { request, touchRequest } = this.props
    request && touchRequest(request)
  }

  render() {
    const { props } = this
    const { className, errorDisabled, hideDisabled, isRetrying, request, onRetry,
      showTimestamp, successMessage } = props
    const status = request && request.status
    const finalClassName = classNames(classes.notification, classes[status], className)
    const retryActionClassName = classNames({
      [classes.action]: true,
      [classes.spin]: isRetrying
    })
    const actionCol = (!!onRetry && 1) + (!hideDisabled && 1)
    const shouldShowError = (status === STATUS_REJECTED) && !errorDisabled
    const shouldShowSuccess = (status === STATUS_FULFILLED) && !!successMessage
    const touched = request && request.touched
    const shouldShowNotification = (shouldShowError || shouldShowSuccess) && !touched

    return shouldShowNotification && (
      <div className={finalClassName}>
        <Row>
          <Col xs={12 - actionCol} className={classes.content}>
            {showTimestamp &&
              <Text className={classes.timestamp}>
                {moment().format('HH:mm:ss')}{' '}
              </Text>
            }
            {shouldShowError && notifyError(props)}
            {shouldShowSuccess && notifySuccess(props)}
          </Col>
          <Col xs={actionCol}>
            <InlineList className={classes.actionsList}>
              {onRetry && <a className={retryActionClassName} onClick={!isRetrying && onRetry}>
                <BzRefresh />
              </a>}
              {!hideDisabled && <a className={classes.action} onClick={this.handleHide}>
                <MdHighlightRemove size={18} />
              </a>}
            </InlineList>
          </Col>
        </Row>
      </div>
    )
  }
}

const actions = {
  touchRequest
}

export default connect(null, actions)(Notification)
