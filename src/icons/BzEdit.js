/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzEdit extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 200 200' {...this.props}>
        <g>
          <path d='M195.12,4.88a16.67,16.67,0,0,0-23.58,0L154.87,21.56h0l-128,128h0L1.16,189.3h0c-4,6.25,3.3,13.58,9.55,9.55h0L50.4,173.18h0l128-128h0l16.67-16.67A16.67,16.67,0,0,0,195.12,4.88ZM11.61,190.32l-1.92-1.92,18.26-28.24,11.91,11.91ZM50.4,163.75,36.25,149.6,154.87,31,169,45.13Zm140-140-12,12L164.3,21.56l12-12A10,10,0,0,1,190.4,23.75Z' />
        </g>
      </IconBase>
    )
  }
}
