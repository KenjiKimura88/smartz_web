import React, { Component } from 'react'
import { FormControl } from 'react-bootstrap'
import MaskedInput from 'react-input-mask'

export default class PhoneNumber extends Component {
  render () {
    return <FormControl componentClass={MaskedInput} mask='+1 (999) 999-9999'
      placeholder='e.g +1 (415) 555-2671' {...this.props} />
  }
}
