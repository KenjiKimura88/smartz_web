import React, { Component, PropTypes } from 'react'
import { Form, InputGroup, Row } from 'react-bootstrap'
import { connect } from 'react-redux'
import { FormattedNumber } from 'react-intl'
import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import Button from 'components/Button/Button'
import Col from 'components/Col'
import ConfirmDepositModal from 'containers/ConfirmDepositModal'
import CurrencyInput from 'components/CurrencyInput'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import RecommendedValue from '../../components/RecommendedValue'
import ResetValueButton from '../../components/ResetValueButton'
import schema from 'schemas/oneTimeDeposit'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

const header = ({ fields: { amount } }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle}>
      <Col xs={8}>
        <Label>One-Time Deposit</Label>
      </Col>
      <Col xs={2} className='text-right'>
        {isExpanded && <ResetValueButton field={amount} savedValue={0} />}
      </Col>
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const showConfirmModal = R.curry(({ resetForm, show }, { amount }) => {
  show('confirmDeposit', { amount, resetForm })
})

const body = (_props) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { fields: { amount, amount: { onChange } }, recommendedValue, handleSubmit } = _props

  return isExpanded
    ? (
      <div>
        <Form onSubmit={handleSubmit(showConfirmModal(_props))}>
          <InputGroup>
            <CurrencyInput precision={2} {...amount} />
            <InputGroup.Button>
              <Button bsStyle='primary' type='submit'>
                Deposit
              </Button>
            </InputGroup.Button>
          </InputGroup>
        </Form>
        {R.is(Number, recommendedValue) &&
          <RecommendedValue onChange={onChange} value={recommendedValue}
            display={<FormattedNumber value={recommendedValue} format='currency' />} />}
      </div>
    )
    : recommendedValue && (
      <Value>
        Recommended: <FormattedNumber value={recommendedValue}
          format='currency' />
      </Value>
    )
}

export class OneTimeDeposit extends Component {
  static propTypes = {
    fields: PropTypes.object,
    goal: PropTypes.object,
    handleSubmit: PropTypes.func,
    recommendedValue: PropTypes.number,
    resetForm: PropTypes.func,
    show: PropTypes.func
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['fields', 'goal', 'recommendedValue'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { goal } = props

    return (
      <div>
        <Panel id='oneTimeDepositPanel' header={header(props)} collapsible>
          {body(props)}
        </Panel>
        <ConfirmDepositModal goal={goal} />
      </div>
    )
  }
}

const actions = {
  show
}

export default R.compose(
  connect(null, actions),
  reduxForm({
    form: 'oneTimeDeposit',
    ...schema
  })
)(OneTimeDeposit)
