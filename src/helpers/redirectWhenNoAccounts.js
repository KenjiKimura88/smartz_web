import R from 'ramda'
import { findAll, findOne } from 'redux/api/modules/requests'
import config from 'config'

const { INVITE_REASON } = config.onboarding

const redirect = (nextState, replace, callback, { accounts, client }) => {
  const { clientId } = nextState.params
  const hasAccounts = R.length(accounts) > 0
  if (!hasAccounts) {
    if (R.equals(client.reason, INVITE_REASON.RETIREMENT)) {
      replace(`/${clientId}/retiresmartz`)
    } else if (R.equals(client.reason, INVITE_REASON.PERSONAL_INVESTING)) {
      replace(`/${clientId}/accounts/new`)
    }
  }

  callback()
}

export default R.curry(({ store }, nextState, replace, callback) => {
  const { clientId } = nextState.params
  store.dispatch(
    findAll({
      type: 'accounts',
      url: `/clients/${clientId}/accounts`,
      success: ({ value: accounts }) => store.dispatch(
        findOne({
          type: 'clients',
          id: clientId,
          success: ({ value: client }) =>
            redirect(nextState, replace, callback, { accounts, client }),
          fail: () => callback()
        })
      ),
      // FIXME: On fail it should probably redirect to some error page
      fail: () => callback()
    })
  )
})
