import React, { Component, PropTypes } from 'react'
import { Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import { getMonthlyTransactionAmount } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import Col from 'components/Col'
import CurrencyInput from 'components/CurrencyInput'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import RecommendedValue from '../RecommendedValue'
import ResetValueButton from '../ResetValueButton'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

const header = ({ monthlyTransactionAmount, settings }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle}>
      <Col xs={8}>
        <Label>Monthly Auto Deposit</Label>
      </Col>
      <Col xs={2} className='text-right'>
        {isExpanded && <ResetValueButton field={monthlyTransactionAmount}
          savedValue={getMonthlyTransactionAmount(settings)} />}
      </Col>
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const body = (_props) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { monthlyTransactionAmount, recommendedValue } = _props
  const { onChange } = monthlyTransactionAmount

  return isExpanded
    ? <div>
      <CurrencyInput {...monthlyTransactionAmount} />
      {R.is(Number, recommendedValue) &&
        <RecommendedValue onChange={onChange} value={recommendedValue}
          display={<FormattedNumber value={recommendedValue} format='currencyWithoutCents' />} />}
    </div>
    : <Value>
      <FormattedNumber value={monthlyTransactionAmount.value} format='currencyWithoutCents' />
    </Value>
}

export default class MonthlyDeposit extends Component {
  static propTypes = {
    monthlyTransactionAmount: PropTypes.object,
    recommendedValue: PropTypes.number,
    settings: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['monthlyTransactionAmount', 'recommendedValue'], this.props, nextProps)
  }

  render () {
    const { props } = this

    return (
      <Panel id='monthlyDepositPanel' header={header(props)} collapsible>
        {body(props)}
      </Panel>
    )
  }
}
