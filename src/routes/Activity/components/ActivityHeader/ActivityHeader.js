import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, MenuItem, Row } from 'react-bootstrap'
import { withRouter } from 'react-router'
import DateTime from 'react-datetime'
import moment from 'moment'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import GoalNavigation from 'components/GoalNavigation'
import SelectActivityType from '../SelectActivityType'
import SelectDateRange from '../SelectDateRange'
import Text from 'components/Text'

export class ActivityHeader extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    activeDateRange: PropTypes.object,
    activeType: PropTypes.object.isRequired,
    activityTypes: PropTypes.array.isRequired,
    endDate: PropTypes.number,
    goals: PropTypes.array.isRequired,
    router: PropTypes.object.isRequired,
    set: PropTypes.func.isRequired,
    startDate: PropTypes.number
  };

  shouldComponentUpdate(nextProps) {
    return propsChanged(['activeDateRange', 'activeType', 'activityTypes', 'goals', 'router'],
      this.props, nextProps)
  }

  handleGoalSelect = (goal) => {
    const { params: { clientId }, push } = this.props.router
    push(`/${clientId}/activity/goal/${goal.id}`)
  }

  handleAccountSelect = (account) => {
    const { params: { clientId }, push } = this.props.router
    push(`/${clientId}/activity/account/${account.id}`)
  }

  render () {
    const { props } = this
    const { accounts, activeType, activityTypes, activeDateRange, endDate,
      router: { params: { accountId, clientId, goalId }, push }, set, startDate } = props
    const selectedItem = (accountId && { type: 'account', id: accountId }) ||
      (goalId && { type: 'goal', id: goalId })
    const label = selectedItem ? null : 'All Accounts'
    const validStartDate = (current) => current.isSameOrBefore(moment())
    const validEndDate = (current) =>
      current.isSameOrBefore(moment()) && current.isSameOrAfter(startDate)

    return (
      <Row>
        <Col xs={3}>
          <GoalNavigation accounts={accounts} onSelectGoal={this.handleGoalSelect}
            onSelectAccount={this.handleAccountSelect} selectedItem={selectedItem} label={label}>
            <MenuItem onClick={() => push(`/${clientId}/activity`)} active={!selectedItem}>
              <Text bold>
                All Accounts
              </Text>
            </MenuItem>
          </GoalNavigation>
        </Col>
        <Col xs={3}>
          <SelectActivityType activeType={activeType} activityTypes={activityTypes} set={set} />
        </Col>
        <Col mdOffset={1} md={5} className='text-right'>
          <SelectDateRange activeDateRange={activeDateRange} endDate={endDate}
            set={set} startDate={startDate} />
          {activeDateRange && R.equals(activeDateRange.label, 'Custom') &&
            <Row className='form-horizontal'>
              <Col xs={6}>
                <Row>
                  <Col componentClass={ControlLabel} xs={4}>
                    From:
                  </Col>
                  <Col xs={8}>
                    <DateTime isValidDate={validStartDate} value={startDate} timeFormat={false}
                      onChange={function (value) { set({ startDate: value.valueOf() }) }} />
                  </Col>
                </Row>
              </Col>
              <Col xs={6}>
                <Row>
                  <Col componentClass={ControlLabel} xs={4}>
                    To:
                  </Col>
                  <Col xs={8}>
                    <DateTime isValidDate={validEndDate} value={endDate} timeFormat={false}
                      onChange={function (value) { set({ endDate: value.valueOf() }) }} />
                  </Col>
                </Row>
              </Col>
            </Row>
          }
        </Col>
      </Row>
    )
  }
}

export default R.compose(
  withRouter
)(ActivityHeader)
