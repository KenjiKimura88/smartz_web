import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import R from 'ramda'

export class ConfirmReplaceRetirementPlan extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, onClick, show } = this.props

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to replace your existing plan?
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={onClick}>
            Replace
          </Button>
          <Button onClick={handleHide}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'confirmReplaceRetirementPlan' })
)(ConfirmReplaceRetirementPlan)
