import { createStructuredSelector } from 'reselect'
import { initialize as reduxFormInitialize, reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import R from 'ramda'
import { connect } from 'redux/api'
import { findAllSelector } from 'redux/api/selectors'
import { update } from 'redux/api/modules/requests'
import { getClient } from 'helpers/requests'
import { initialize } from 'routes/RetiresmartzWizard/helpers'
import { retiresmartzSelector } from 'redux/selectors'
import { set } from 'redux/modules/retiresmartz'
import { updateApiData } from 'redux/api/modules/data'
import RetiresmartzPlan from '../components/RetiresmartzPlan'
import schema from 'schemas/retirementPlan'

const deserializeAdviceFeeds = (data, getState) => R.unionWith(
  R.eqBy(R.prop('id')),
  findAllSelector({ type: 'retirementAdviceFeeds' })(getState()),
  R.prop('results')(data)
)

const requests = ({ params: { clientId, retirementPlanId } }) => ({
  adviceFeeds: ({ findAll }) => findAll({
    type: 'retirementAdviceFeeds',
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/advice-feed`,
    deserialize: deserializeAdviceFeeds
  }),
  client: getClient(clientId),
  calculatedData: ({ findSingle }) => findSingle({
    type: 'retirementPlanCalculate',
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/calculated-data`,
    propKey: 'calculatedData'
  }),
  calculate: ({ findSingle }) => findSingle({
    type: 'retirementPlanCalculate',
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/calculate`,
    force: true,
    lazy: true,
    propKey: 'calculatedData'
  }),
  incomes: ({ findQuery }) => findQuery({
    type: 'retirementIncomes',
    url: `/clients/${clientId}/retirement-incomes`,
    query: {
      plan: parseInt(retirementPlanId, 10)
    }
  }),
  refreshAdviceFeeds: ({ findAll }) => findAll({
    type: 'retirementAdviceFeeds',
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/advice-feed`,
    deserialize: deserializeAdviceFeeds,
    force: true,
    lazy: true
  }),
  retirementPlan: ({ findOne }) => findOne({
    type: 'retirementPlans',
    id: retirementPlanId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
  }),
  settings: (({ findSingle }) => findSingle({
    type: 'globalSettings',
    url: '/settings'
  })),
  updateRetirementPlan: ({ update }) => update({
    type: 'retirementPlans',
    id: retirementPlanId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
  })
})

const selector = createStructuredSelector({
  retiresmartz: retiresmartzSelector
})

const actions = {
  updateApiData,
  reduxFormInitialize,
  readAdviceFeed: (clientId, retirementPlanId, feedId, read) => update({
    type: 'retirementAdviceFeeds',
    id: feedId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/advice-feed/${feedId}`,
    body: { read }
  }),
  set,
  show
}

export default R.compose(
  connect(requests, selector, actions),
  reduxForm({
    form: 'retirementPlan',
    destroyOnUnmount: false,
    ...schema
  }, (state, props) => ({
    initialValues: initialize(props)
  }))
)(RetiresmartzPlan)
