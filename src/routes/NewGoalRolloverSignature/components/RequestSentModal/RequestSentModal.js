import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import classes from './RequestSentModal.scss'
import H2 from 'components/H2'
import P from 'components/P'

class RequestSentModal extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    handleHide: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired
  };

  handleDone = () => {
    const { clientId, handleHide, router: { push } } = this.props
    handleHide()
    push(`/${clientId}`)
  }

  render () {
    const { show } = this.props
    return (
      <Modal show={show} backdrop='static' className={classes.wrapper}>
        <Modal.Body>
          <H2 className='text-center'>Thank you</H2>
          <P className='text-center'>
            Your transfer request has been submitted and we will notify you when the
            rollover has taken place.
          </P>
          <div className='text-center'>
            <Button bsStyle='primary' onClick={this.handleDone}>
              Done
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'rolloverRequestSentModal' }),
  withRouter
)(RequestSentModal)
