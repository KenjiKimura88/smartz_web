import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormGroup, FormControl, Row }
  from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { TRADING_INFO_STEP_ID } from '../../helpers'
import bgImage from './onboarding_personal-info-icon.svg'
import Card from '../Card'
import classes from './TradingInfo.scss'
import FieldError from 'components/FieldError'
import NavHeader from '../NavHeader'
import Option from '../TradingOption'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Sidebar from '../Sidebar'
import Slider from 'components/Slider/Slider'

export default class TradingInfo extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  handleChange = value => () => {
    const { fields } = this.props
    fields.regulatoryQuestion5.onChange(value)
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitTradingInfo(values, callback, callback)
  }

  handleSubmitTradingInfo = (values) => {
    const { push } = this.props
    const callback = () => {
      push(`risk`)
    }
    this.submitTradingInfo(values, callback, callback)
  }

  submitTradingInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['info', 'steps', TRADING_INFO_STEP_ID], finalValues, (onboardingValue) => {
      save({
        body: {
          onboarding_data: R.assocPath(['info', 'completed'], true, onboardingValue)
        },
        success,
        fail
      })
    })
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    return (
      <Sidebar title='Personal Information'>
        <p>(3/3)</p>
        <p>
          Provide us with your level of experience
          in trading investment products and
          answer some regulatory questions.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { fields, handleSubmit, isLoading } = this.props
    return (
      <Form onSubmit={handleSubmit(this.handleSubmitTradingInfo)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
          isLoading={isLoading}>
          <strong>Stocks</strong>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Years Trading</ControlLabel>
                <FormControl type='number'
                  {...domOnlyProps(fields.stockYearsTrading)} />
                <FieldError for={fields.stockYearsTrading} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Trades Per Year</ControlLabel>
                <FormControl type='number'
                  {...domOnlyProps(fields.stockTradesPerYear)} />
                <FieldError for={fields.stockTradesPerYear} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              <ControlLabel>Knowledge Level</ControlLabel>
              <Slider className={classes.level} min={0} max={10}
                getDisplayedValue={function (value) { return value }}
                {...domOnlyProps(fields.stockKnowledgeLevel)} />
            </Col>
          </Row>
          <strong>Mutual Funds</strong>
          <Row>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Years Trading</ControlLabel>
                <FormControl type='number'
                  {...domOnlyProps(fields.fundYearsTrading)} />
                <FieldError for={fields.fundYearsTrading} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Trades Per Year</ControlLabel>
                <FormControl type='number'
                  {...domOnlyProps(fields.fundTradesPerYear)} />
                <FieldError for={fields.fundTradesPerYear} />
              </FormGroup>
            </Col>
            <Col xs={4}>
              <ControlLabel>Knowledge Level</ControlLabel>
              <Slider className={classes.level} min={0} max={10}
                getDisplayedValue={function (value) { return value }}
                {...domOnlyProps(fields.fundKnowledgeLevel)} />
            </Col>
          </Row>
          <p><strong>Requlatory questions</strong></p>
          <Row>
            <Col xs={8}>
              <FormGroup>
                <ControlLabel>
                  Are you or any of your immediate family that live with you
                  registered as a broker-dealer or an employee, director or
                  owner of a securities or commodities brokerage firm?
                </ControlLabel>
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                  {...domOnlyProps(fields.regulatoryQuestion1)}>
                  <Button className={classes.radioButton} value>Yes</Button>
                  <Button className={classes.radioButton} value={false}>No</Button>
                </RadioButtonGroup>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <FormGroup>
                <ControlLabel>
                  Are you member of an exchange or a regulatory or a self-
                  regulatory ogranization, or an associated person, affiliated
                  person or employee of an exchange member?
                </ControlLabel>
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                  {...domOnlyProps(fields.regulatoryQuestion2)}>
                  <Button className={classes.radioButton} value>Yes</Button>
                  <Button className={classes.radioButton} value={false}>No</Button>
                </RadioButtonGroup>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <FormGroup>
                <ControlLabel>
                  Have you ever been the subject of, or initiated, litigation,
                  arbitration or any other type of dispute or settlement
                  procedure with another broker or dealer?
                </ControlLabel>
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                  {...domOnlyProps(fields.regulatoryQuestion3)}>
                  <Button className={classes.radioButton} value>Yes</Button>
                  <Button className={classes.radioButton} value={false}>No</Button>
                </RadioButtonGroup>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <FormGroup>
                <ControlLabel>
                  Have you ever been the subject of an investigation or
                  proceeding by any commodities or securities exchange or
                  regulatory authority or self-regulatory authority?
                </ControlLabel>
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
                  {...domOnlyProps(fields.regulatoryQuestion4)}>
                  <Button className={classes.radioButton} value>Yes</Button>
                  <Button className={classes.radioButton} value={false}>No</Button>
                </RadioButtonGroup>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={8}>
              <FormGroup>
                <ControlLabel>
                  In any publicly traded company do any of the following
                  positions apply to you?
                </ControlLabel>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={11}>
              <Option
                key={1}
                value={1}
                label='A Director'
                onClick={this.handleChange}
                selected={fields.regulatoryQuestion5.value==1} />
              <Option
                key={2}
                value={2}
                label='A 10% Shareholder'
                onClick={this.handleChange}
                selected={fields.regulatoryQuestion5.value==2} />
              <Option
                key={3}
                value={3}
                label='A Policy-Making Officer'
                onClick={this.handleChange}
                selected={fields.regulatoryQuestion5.value==3} />
            </Col>
          </Row>

          <div className={classes.submitWrapper}>
            <Button disabled={isLoading} type='submit' bsStyle='thick-outline'>
              Continue
            </Button>
          </div>
        </Card>
      </Form>
    )
  }
}
