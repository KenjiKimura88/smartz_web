import React, { Component, PropTypes } from 'react'
import R from 'ramda'

// getClientName :: Props -> String | undefined
const getClientName = R.compose(
  R.converge(R.compose(
    R.join(' '),
    R.reject(R.isNil),
    R.reject(R.isEmpty),
    R.unapply(R.identity)
  ), [
    R.prop('first_name'),
    R.prop('middle_name'),
    R.prop('last_name')
  ]),
  R.defaultTo({}),
  R.path(['client', 'user'])
)

export default class ClientName extends Component {
  static propTypes = {
    defaultTo: PropTypes.string.isRequired,
    client: PropTypes.object
  };

  static defaultProps = {
    defaultTo: ''
  }

  render () {
    const { props } = this
    const { defaultTo } = props

    return (
      <span>
        {getClientName(props) || defaultTo}
      </span>
    )
  }
}
