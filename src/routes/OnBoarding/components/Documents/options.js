import config from 'config'

export const OPTIONS = [
  {
    value: config.maritalStatuses.MARRIED_FILING_JOINTLY,
    label: 'Married filing jointly'
  },
  {
    value: config.maritalStatuses.SINGLE,
    label: 'Single'
  },
  {
    value: config.maritalStatuses.HEAD_OF_HOUSEHOLD,
    label: 'Head of household'
  },
  {
    value: config.maritalStatuses.MARRIED_FILING_SEPARATELY_NOT_LIVED_TOGETHER,
    label: 'Married filing separately'
  },
  {
    value: config.maritalStatuses.QUALIFYING_WIDOWER,
    label: 'Qualified widow(er)'
  }
]

export const exemptions = fields => [
  {
    type: 0,
    field: fields.dependents,
    label: 'Dependents',
    max: 99
  },
  {
    type: 0,
    field: fields.dependentsQualifying,
    label: 'Dependents qualifying for child tax credit',
    max: 99
  },
  {
    type: 1,
    field: fields.isPersonalExemption,
    label: 'Personal exemption',
    description: 'Check here'
  },
  {
    type: 1,
    field: fields.isSpouseExemption,
    label: 'Spouse exemption',
    description: 'Check here'
  }
]

export const income = fields => [
  {
    type: 2,
    field: fields.wages,
    label: 'Wages, salaries, tips, etc',
    max: 500000
  },
  {
    type: 3,
    filed: fields.spouseWages,
    label: 'Spouse wages, salaries, tips, etc'
  },
  {
    type: 2,
    field: fields.taxableInterest,
    label: 'Taxable interest',
    max: 500000
  },
  {
    type: 2,
    field: fields.taxExempt,
    label: 'Tax-exempt interest',
    max: 500000
  },
  {
    type: 2,
    field: fields.ordinaryDividends,
    label: 'Ordinary dividends (this includes any qualified dividends)',
    max: 500000
  },
  {
    type: 2,
    field: fields.qualifiedDividends,
    label: 'Qualified dividends (included in ordinary dividends above)',
    max: 500000
  },
  {
    type: 2,
    field: fields.taxableRefunds,
    label: 'Taxable refunds of state and local income taxes',
    max: 500000
  },
  {
    type: 2,
    field: fields.alimonyReceived,
    label: 'Alimony received', value: 0
  },
  {
    type: 2,
    field: fields.businessIncomeOrLoss,
    label: 'Business income or loss (Schedule C & E subject to self-employment taxes)',
    max: 500000
  },
  {
    type: 3,
    field: fields.spouseBusinessIncomeOrLoss,
    label: 'Spouse\'s business income or loss (Schedule C & E subject to self-employment taxes)'
  },
  {
    type: 2,
    field: fields.shortTermCapitalGainOrLoss,
    label: 'Short term capital gain or loss',
    max: 500000
  },
  {
    type: 2,
    field: fields.longTermCapitalGainOrLoss,
    label: 'Long term Capital gain or loss',
    max: 500000
  },
  {
    type: 2,
    field: fields.otherGainsOrLosses,
    label: 'Other gains or losses',
    max: 500000
  },
  {
    type: 2,
    field: fields.taxableIRADistributions,
    label: 'Taxable IRA distributions',
    max: 500000
  },
  {
    type: 2,
    field: fields.taxablePensions,
    label: 'Taxable pensions and annuity distributions',
    max: 500000
  },
  {
    type: 2,
    field: fields.incomeFromRentals,
    label: 'Income from rentals, royalties, S Corporations and Schedule E (not included above and subject to NIIT)', // eslint-disable-line
    max: 500000
  },
  {
    type: 2,
    field: fields.incomeFromRentalsNot,
    label: 'Income from rentals, royalties, S Corporations and Schedule E (not included above and not subject to NIIT)', // eslint-disable-line
    max: 500000
  },
  {
    type: 2,
    field: fields.farmIncome,
    label: 'Farm income or loss (Schedule F)',
    max: 500000
  },
  {
    type: 2,
    field: fields.totalUnemploymentCompensation,
    label: 'Total unemployment compensation',
    max: 500000
  },
  {
    type: 2,
    field: fields.taxableSSB,
    label: 'Taxable Social Security benefits',
    max: 500000
  },
  {
    type: 2,
    field: fields.otherIncome,
    label: 'Other income',
    max: 500000
  }
]

export const gross = fields => [
  {
    type: 2,
    field: fields.educatorExpenses,
    label: 'Educator expenses',
    max: 1000
  },
  {
    type: 2,
    field: fields.certainBusinessExpenses,
    label: 'Certain business expenses (form 2106)',
    max: 500000
  },
  {
    type: 2,
    field: fields.hsaDeduction,
    label: 'Health Savings Account (HSA) deduction (form 8889)',
    max: 500000
  },
  {
    type: 2,
    field: fields.movingExpenses,
    label: 'Moving expenses (form 3903)',
    max: 500000
  },
  {
    type: 3,
    field: fields.oneHalfOfSelfEmploymentTax,
    label: 'One-half of self-employment tax (Schedule SE)'
  },
  {
    type: 2,
    field: fields.selfEmployedSEP,
    label: 'Self-employed SEP, SIMPLE and qualified plans',
    max: 500000
  },
  {
    type: 2,
    field: fields.selfEmployedHealthInsuranceDeduction,
    label: 'Self-employed health insurance deduction',
    max: 500000
  },
  {
    type: 2,
    field: fields.penalty,
    label: 'Penalty on early withdrawal of savings',
    max: 500000
  },
  {
    type: 2,
    field: fields.alimonyPaid,
    label: 'Alimony paid',
    max: 500000
  },
  {
    type: 2,
    field: fields.iraDeduction,
    label: 'IRA deduction',
    max: 500000
  },
  {
    type: 2,
    field: fields.studentLoanInterestDeduction,
    label: 'Student loan interest deduction',
    max: 500000
  },
  {
    type: 2,
    field: fields.tuitionAndFeesDeduction,
    label: 'Tuition and fees deduction (Form 8917)',
    max: 500000
  },
  {
    type: 2,
    field: fields.domesticProductionActivitiesDeduction,
    label: 'Domestic production activities deduction (form 8903)',
    max: 500000
  },
  {
    type: 3,
    field: fields.totalAdjustments,
    label: 'Total adjustments'
  }
]

export const deduction = fields => [
  {
    type: 1,
    field: fields.isOlder65,
    label: 'Check if',
    description: 'You are 65 or older'
  },
  {
    type: 1,
    field: fields.isBlind,
    label: 'Check if',
    description: 'You are blind'
  },
  {
    type: 1,
    field: fields.isSpuseOlder65,
    label: 'Check if',
    description: 'Spouse is 65 or older'
  },
  {
    type: 1,
    field: fields.isSpouseBlind,
    label: 'Check if',
    description: 'Spouse is blind'
  },
  {
    type: 2,
    field: fields.itemizedDeductions,
    label: 'Itemized deductions',
    max: 500000
  },
  {
    type: 1,
    field: fields.isSomeoneCanClaim,
    label: 'Check if',
    description: 'Someone can claim you as a dependent'
  },
  {
    type: 3,
    field: fields.standardDeduction,
    label: 'Standard deduction'
  }
]

export const taxable = fields => [
  { type: 3, field: fields.adjustedGrossIncome, label: 'Adjusted gross income' },
  { type: 3, field: fields.deductionForExemptions, label: 'Deduction for exemptions' },
  { type: 3, field: fields.standardOrItemizedDeduction, label: 'Standard or itemized deduction' },
  { type: 3, field: fields.taxableIncome, label: 'Taxable income' }
]

export const totalTaxCredits = fields => [
  {
    type: 2,
    field: fields.foreignTaxCredit,
    label: 'Foreign tax credit (form 1116)',
    max: 10000
  },
  {
    type: 2,
    field: fields.creditForChild,
    label: 'Credit for child and dependent care expenses (form 2441)',
    max: 5000
  },
  {
    type: 2,
    field: fields.educationCredits,
    label: 'Education credits (form 8863, line 23)',
    max: 10000
  },
  {
    type: 2,
    field: fields.retirementSavings,
    label: 'Retirement savings contributions credit (form 8880)',
    max: 1000
  },
  {
    type: 3,
    field: fields.childTaxCredit,
    label: 'Child tax credit'
  },
  {
    type: 2,
    field: fields.residentialEnergyCredits,
    label: 'Residential energy credits (Form 5695)',
    max: 1000
  },
  {
    type: 2,
    field: fields.americanOpportunityCredit,
    label: 'American opportunity credit (Form 8863, line 14)',
    max: 5000
  },
  {
    type: 3,
    field: fields.americanOpportunityCreditNonRefundable,
    label: 'American opportunity credit non-refundable'
  },
  {
    type: 2,
    field: fields.otherCredits,
    label: 'Other credits that are not refundable',
    max: 500
   }
]

export const totalTax = fields => [
  {
    type: 2,
    field: fields.incomeTax,
    label: 'Income tax',
    max: 100000
  },
  {
    type: 2,
    field: fields.alternativeMinimumTax,
    label: 'Alternative minimum tax',
    max: 100000
  },
  {
    type: 2,
    field: fields.selfEmploymentTax,
    label: 'Self-employment tax',
    max: 100000
  },
  {
    type: 2,
    field: fields.unreportedTax,
    label: 'Unreported Social Security and Medicare tax (forms 4137 & 5329)',
    max: 100000
  },
  {
    type: 2,
    field: fields.additionalTax,
    label: 'Additional tax on IRAs and other retirement plans (Form 5329)',
    max: 100000
  },
  {
    type: 2,
    field: fields.houseHoldEmploymentTaxes,
    label: 'Household employment taxes (Schedule H), Advanced EIC (W-2 box 9)',
    max: 100000
  },
  {
    type: 2,
    field: fields.firstTimeHomebuyerCreditRepayment,
    label: 'First time homebuyer credit repayment (Form 5405)',
    max: 100000
  },
  {
    type: 2,
    field: fields.healthCare,
    label: 'Health care: individual responsibility',
    max: 100000
  },
  {
    type: 3,
    field: fields.medicareTax,
    label: 'Medicare tax sur-tax on earned income (Form 8959)'
  },
  {
    type: 3,
    field: fields.netInvestmentIncomeTax,
    label: 'Net Investment Income Tax (NIIT) (Form 8960)'
  }
]

export const payment = fields => [
  {
    type: 2,
    field: fields.federalIncomeTax,
    label: 'Federal income tax withheld on Forms W-2 and 1099',
    max: 500000
  },
  {
    type: 2,
    field: fields.estimatedTaxPayments,
    label: 'Estimated tax payments',
    max: 500000
  },
  {
    type: 3,
    field: fields.additionalChildTaxCredit,
    label: 'Additional child tax credit (Form 8812)'
  },
  {
    type: 2,
    field: fields.excessSocialSecurity,
    label: 'Excess Social Security and RRTA tax withheld',
    max: 10000
  },
  {
    type: 2,
    field: fields.anyOtherPayments,
    label: 'Any other payments including amount paid with request for extension',
    max: 500000
  },
  {
    type: 3,
    field: fields.americanOpportunityCreditRefundable,
    label: 'American opportunity credit refundable'
  },
  {
    type: 2,
    field: fields.netPremiumCredit,
    label: 'Net premium credit (form 8962)',
    max: 100000
  },
  {
    type: 2,
    field: fields.otherRefundableCredits,
    label: 'Other refundable credits',
    max: 500000
  },
  {
    type: 3,
    field: fields.earnedIncomeCredit,
    label: 'Earned income credit (EIC)'
  },
  {
    type: 0,
    field: fields.qualifyingChildren,
    label: 'Qualifying children for EIC',
    max: 3
  },
  {
    type: 2,
    field: fields.scholarships,
    label: 'Scholarships, penal income and retirement income',
    max: 20000
  },
  {
    type: 2,
    field: fields.nonTaxableCombatPay,
    label: 'Non-taxable combat pay',
    max: 20000
  },
  {
    type: 1,
    field: fields.isYourAge,
    label: 'Your age',
    description: 'Are you (or spouse) between the age of 25 and 65?'
  },
  {
    type: 1,
    field: fields.isResidencyStatus,
    label: 'Residency status',
    description: 'Have you (and spouse) lived in the U.S. for at least six months?'
  },
  {
    type: 1,
    field: fields.isQualifyingChildStatus,
    label: 'Qualifying child status',
    description: 'Can you (or spouse) be claimed as a qualifying child of someone else?'
  }
]
