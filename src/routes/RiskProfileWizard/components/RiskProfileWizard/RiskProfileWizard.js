import React, { Component, PropTypes } from 'react'
import { Button, Form, Grid } from 'react-bootstrap'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import bgImage from 'routes/OnBoarding/components/Risk/risk-profile-icon.svg'
import Card from 'routes/OnBoarding/components/Card'
import classes from './RiskProfileWizard.scss'
import Option from 'routes/OnBoarding/components/Option'
import Sidebar from 'routes/OnBoarding/components/Sidebar'
import WelcomeModal from '../WelcomeModal'

const isLastStep = (questionIndex, questions) =>
  !questions || parseInt(questionIndex, 10) + 1 >= questions.length

export default class RiskProfileWizard extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    questions: PropTypes.array,
    refreshProfile: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired,
    updateRiskProfileResponses: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired
  };

  componentWillMount () {
    this.props.show('welcomeModal')
  }

  handleChange = (value) => () => {
    const { fields: { answers }, params } = this.props
    const questionIndex = parseInt(params.questionIndex, 10)
    answers[questionIndex].onChange(value)
  }

  handleSuccess = () => {
    const { params: { clientId }, refreshProfile, router: { push } } = this.props
    refreshProfile()
    push(`/${clientId}/profile`)
  }

  handleContinue = (values) => {
    const { fields: { answers }, params, router: { push }, questions,
      updateRiskProfileResponses } = this.props
    const { clientId } = params
    const questionIndex = parseInt(params.questionIndex, 10)
    if (isLastStep(questionIndex, questions)) {
      updateRiskProfileResponses({
        body: values.answers,
        success: this.handleSuccess
      })
    } else {
      const nextIndex = parseInt(questionIndex, 10) + 1
      !answers[nextIndex] && answers.addField()
      push(`/${clientId}/risk-profile/${nextIndex}`)
    }
  }

  renderSidebar() {
    const { params, questions } = this.props
    const questionIndex = parseInt(params.questionIndex, 10)
    const question = questions ? questions[questionIndex] : {}
    return (
      <Sidebar title={question.text}>
        <p>
          <strong>
            Why we ask:
          </strong>
        </p>
        <p>
          {question.explanation}
        </p>
      </Sidebar>
    )
  }

  render () {
    const { values: { answers }, handleSubmit, params, questions } = this.props
    const questionIndex = parseInt(params.questionIndex, 10)
    const currentValue = !R.isEmpty(answers) && parseInt(answers[questionIndex], 10)
    const question = questions ? questions[questionIndex] : {}
    const answersList = question.answers || []

    return (
      <Form onSubmit={handleSubmit(this.handleContinue)}>
        <Grid className={classes.wrapper}>
          <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage}>
            {question.figure &&
              <div className={classes.figure}
                dangerouslySetInnerHTML={{ __html: question.figure }} />
            }
            <div>
              {mapIndexed((item, index) => (
                <Option key={index} value={item.id} label={item.text}
                  onClick={this.handleChange} selected={R.equals(currentValue, item.id)} />
              ), answersList)}
            </div>
            {R.is(Number, currentValue) &&
              <div className='text-right'>
                <Button bsStyle='thick-outline' type='submit'>
                  {isLastStep(questionIndex, questions) ? 'Save' : 'Continue'}
                </Button>
              </div>}
          </Card>
          <WelcomeModal />
        </Grid>
      </Form>
    )
  }
}
