import { createStructuredSelector } from 'reselect'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import uuid from 'uuid'
import { setActiveHolding, resetActiveHolding } from 'redux/modules/portfolio'
import { activeHolding, questions, isAuthenticatedSelector } from 'redux/selectors'
import { getAccounts, getGoal } from 'helpers/requests'
import { connect } from 'redux/api'
import { findQuerySelector } from 'redux/api/selectors'
import { set } from 'redux/modules/goals'
import { viewedSettingsToApiPath } from 'routes/Allocation/helpers'
import Portfolio from '../components/Portfolio'

const requests = ({ params: { clientId, goalId, viewedSettings } }) => ({
  accounts: getAccounts(clientId),
  assetsClasses: ({ findAll }) => findAll({
    type: 'assetsClasses',
    url: '/settings/asset-classes'
  }),
  tickers: ({ findAll }) => findAll({
    type: 'tickers',
    url: '/settings/tickers'
  }),
  goal: getGoal({ clientId, goalId }),
  positions: ({ findAll }) => findAll({
    type: 'positions',
    url: `/goals/${goalId}/positions`,
    mergeParams: () => ({
      goal: parseInt(goalId, 10),
      id: uuid.v4()
    }),
    selector: findQuerySelector({
      type: 'positions',
      query: { goal: parseInt(goalId, 10) }
    })
  }),
  recommended: (({ findSingle }) => findSingle({
    url: `/goals/${goalId}/risk-score-data`,
    type: 'recommendedRiskScores',
    footprint: R.pick(['goal']),
    deserialize: R.merge({ goal: goalId })
  })),
  settings: goalId && viewedSettingsToApiPath[viewedSettings] && (({ findSingleByURL }) =>
    findSingleByURL({
      type: 'settings',
      url: `/goals/${goalId}/${viewedSettingsToApiPath[viewedSettings]}-settings`
    })
  )
})

const selector = createStructuredSelector({
  activeHolding,
  isAuthenticated: isAuthenticatedSelector,
  questions
})

const actions = {
  resetActiveHolding,
  set,
  setActiveHolding,
  show
}

export default R.compose(
  connect(requests, selector, actions),
  withRouter
)(Portfolio)
