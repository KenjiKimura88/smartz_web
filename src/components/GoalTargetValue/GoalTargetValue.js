import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'

// getTarget :: Props -> Number
const getTarget = R.compose(
  R.defaultTo(0),
  R.path(['goal', 'settings', 'target'])
)

export default class GoalTargetValue extends Component {
  static propTypes = {
    goal: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goal'], this.props, nextProps)
  }

  render () {
    const target = getTarget(this.props)
    return (
      <FormattedNumber value={target} format='currency' />
    )
  }
}
