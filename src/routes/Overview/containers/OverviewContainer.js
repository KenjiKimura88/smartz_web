import { createStructuredSelector } from 'reselect'
import { DragDropContext, DropTarget } from 'react-dnd'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import HTML5Backend from 'react-dnd-html5-backend'
import R from 'ramda'
import { connect } from 'redux/api'
import { getAccounts, getClient, getGoals } from 'helpers/requests'
import { getProfile } from 'redux/modules/auth'
import { isAuthenticatedSelector, goalsSelector } from 'redux/selectors'
import { moveGoal, set, toggle } from 'redux/modules/goals'
import Overview from '../components/Overview'

const requests = ({ params: { clientId }, set }) => ({
  accounts: getAccounts(clientId),
  client: getClient(clientId),
  fetchExternalAccounts: ({ findAll }) => findAll({
    type: 'externalAccounts',
    url: '/quovo/get-accounts',
    lazy: true,
    force: true,
    propKey: 'externalAccounts'
  }),
  goals: getGoals(clientId),
  updateGoal: ({ update }) => update({ type: 'goals' }),
  user: getProfile
})

const selector = createStructuredSelector({
  isAuthenticated: isAuthenticatedSelector,
  goalsState: goalsSelector
})

const actions = {
  moveGoal,
  set,
  show,
  toggle
}

const boxTarget = {
  drop () {
  }
}

export default R.compose(
  connect(requests, selector, actions),
  withRouter,
  DragDropContext(HTML5Backend),
  DropTarget('goal', boxTarget, connect => ({
    connectDropTarget: connect.dropTarget()
  }))
)(Overview)
