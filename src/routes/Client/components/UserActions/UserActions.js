import React, { Component, PropTypes } from 'react'
import { Col, Dropdown, MenuItem, Row } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import R from 'ramda'
import { MdAdd, MdPerson, MdPowerSettingsNew, MdSettings, MdMenu } from 'helpers/icons'
import Button from 'components/Button'
import classes from './UserActions.scss'
import config from 'config'

const { accountTypes } = config

// getFirstName :: Props -> String | undefined
const getFirstName = R.ifElse(
  R.pathEq(['user', 'role'], 'advisor'),
  R.path(['client', 'user', 'first_name']),
  R.path(['user', 'first_name'])
)

// getSortedAccounts :: Props -> Array
const getSortedAccounts = R.compose(
  R.sortBy(R.prop('account_name')),
  R.prop('accounts')
)

const needsAccountOfType = (accounts, type) => R.compose(
  R.isNil,
  R.find(R.propEq('account_type', type)),
  R.defaultTo([])
)(accounts)

export default class UserActions extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    client: PropTypes.object,
    clientId: PropTypes.string.isRequired,
    logout: PropTypes.func.isRequired,
    user: PropTypes.object
  };

  render () {
    const { props } = this
    const { accounts, clientId, logout } = props
    const firstName = getFirstName(props)

    return (
      <div className={classes.userActions}>
        <Dropdown id='userActions' pullRight>
          <div className={classes.welcomeWrapper}>
            <span className={classes.welcomeText}>
              Welcome, {firstName}
            </span>
          </div>
          <Dropdown.Toggle className={classes.dropdownToggle} noCaret>
            <MdMenu size={28} />
          </Dropdown.Toggle>
          <Dropdown.Menu className={classes.dropdown}>
            {R.map(account =>
              <LinkContainer key={account.id} to={`/${clientId}/account/${account.id}/settings`}>
                <MenuItem>
                  <Row className={classes.row}>
                    <Col xs={9}>
                      <span>
                        {account.account_name}
                      </span>
                    </Col>
                    <Col xs={3}>
                      <Button bsStyle='link' componentClass='span'>
                        <MdSettings size={18} />
                      </Button>
                    </Col>
                  </Row>
                </MenuItem>
              </LinkContainer>
            , getSortedAccounts(props))}
            {needsAccountOfType(accounts, accountTypes.ACCOUNT_TYPE_JOINT) &&
              <LinkContainer to={`${clientId}/accounts/new/joint`}>
                <MenuItem>
                  <div>
                    <Row className={classes.row}>
                      <Col xs={9}>
                        <span>Open a Joint Account</span>
                      </Col>
                      <Col xs={3}>
                        <Button bsStyle='link' componentClass='span'>
                          <MdAdd size={18} />
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </MenuItem>
              </LinkContainer>
            }
            {needsAccountOfType(accounts, accountTypes.ACCOUNT_TYPE_TRUST) &&
              <LinkContainer to={`${clientId}/accounts/new/trust`}>
                <MenuItem>
                  <div>
                    <Row className={classes.row}>
                      <Col xs={9}>
                        <span>Add a Trust Account</span>
                      </Col>
                      <Col xs={3}>
                        <Button bsStyle='link' componentClass='span'>
                          <MdAdd size={18} />
                        </Button>
                      </Col>
                    </Row>
                  </div>
                </MenuItem>
              </LinkContainer>
            }
            <LinkContainer to={`/${clientId}/profile`}>
              <Col xs={6} componentClass={MenuItem} className={classes.halfMenuItem}>
                <span>
                  <MdPerson size='28' />
                  <div>Settings</div>
                </span>
              </Col>
            </LinkContainer>
            <Col xs={6} componentClass={MenuItem} className={classes.halfMenuItem}>
              <span onClick={logout}>
                <MdPowerSettingsNew size='28' />
                <div>Logout</div>
              </span>
            </Col>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    )
  }
}
