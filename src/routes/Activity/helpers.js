import moment from 'moment'
import R from 'ramda'

// ------------------------------------
// Activity Types
// ------------------------------------
const allActivityType = {
  id: -1,
  name: 'All Activity',
  filter: R.always(true)
}

const filterActivity = (activityType) =>
  (activity) => R.equals(activity.type, activityType.id)

const getActiveTypeId = ({ activityState }) =>
  activityState && activityState.activeTypeId || allActivityType.id

const assocFilterActivity = (activityType) =>
  R.assoc('filter', filterActivity(activityType), activityType)

export const getActivityTypes = R.compose(
  R.prepend(allActivityType),
  R.map(assocFilterActivity),
  R.prop('activityTypes')
)

export const getActiveType = (props) =>
  R.find(
    R.propEq('id', getActiveTypeId(props)),
    getActivityTypes(props)
  )

// ------------------------------------
// Date Ranges
// ------------------------------------
export const dateRanges = ({ startDate, endDate }) => ([
  {
    label: 'All',
    startDate: () => undefined,
    endDate: () => moment().endOf('day').valueOf(),
    filter: R.always(true)
  },
  {
    label: '1 yr',
    startDate: () => moment().subtract(1, 'years').valueOf(),
    endDate: () => moment().endOf('day').valueOf(),
    filter: ({ time }) =>
      R.gte(time * 1000, moment().subtract(1, 'years').valueOf())
  },
  {
    label: '1 mo',
    startDate: () => moment().subtract(1, 'months').valueOf(),
    endDate: () => moment().endOf('day').valueOf(),
    filter: ({ time }) =>
      R.gte(time * 1000, moment().subtract(1, 'months').valueOf())
  },
  {
    label: 'Custom',
    startDate: () => startDate,
    endDate: () => endDate,
    filter: ({ time }) =>
      R.gte(time * 1000, R.defaultTo(0, startDate)) &&
      R.lte(time * 1000, R.defaultTo(moment().valueOf(), endDate))
  }
])

export const getActiveDateRange = (props) => R.compose(
  R.flip(R.find)(dateRanges(R.pick(['startDate', 'endDate'], props.activityState))),
  R.propEq('label'),
  R.defaultTo('All'),
  R.path(['activityState', 'activeDateRangeLabel'])
)(props)

// ------------------------------------
// Activity Description, Goal, Account
// ------------------------------------
const getDescriptionTemplate = ({ activity, activityTypes }) => R.compose(
  R.defaultTo(''),
  R.prop('format_str'),
  R.defaultTo({}),
  R.find(R.propEq('id', activity.type))
)(activityTypes)

const getNumberOfSubstitutions = R.compose(
  R.length,
  R.match(/\{\}/g)
)

export const getDescription = (activity, { activityTypes }) => {
  const { data } = activity
  const template = getDescriptionTemplate({ activity, activityTypes })
  const numberOfSubstitutions = getNumberOfSubstitutions(template)

  return R.reduce((str, index) =>
    R.replace('{}', data[index], str)
  , template, R.range(0, numberOfSubstitutions))
}

export const getGoalForActivity = ({ goal }, { goals }) => R.find(R.propEq('id', goal), goals)

export const getAccountForActivity = ({ account, goal }, { accounts, goals }) => {
  const goalForActivity = getGoalForActivity({ goal }, { goals })
  return goalForActivity && goalForActivity.account || R.find(R.propEq('id', account), accounts)
}

// ------------------------------------
// Filter
// ------------------------------------
const getActivity = (props) => {
  const { accountActivity, clientActivity, goalActivity, params: { accountId, goalId } } = props
  return (accountId && accountActivity) ||
   (goalId && goalActivity) ||
   clientActivity
}

export const getFilteredActivity = (activeType, activeDateRange, props) =>
  R.filter(
    R.converge(R.and, [activeType.filter, activeDateRange.filter]),
    getActivity(props)
  )
