import React, { Component, PropTypes } from 'react'
import { ControlLabel, FormControl, FormGroup, HelpBlock, Row } from 'react-bootstrap'
import MaskedInput from 'react-input-mask'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { MdEdit } from 'helpers/icons'
import AllCaps from 'components/AllCaps'
import Button from 'components/Button'
import Col from 'components/Col'
import config from 'config'
import Convert from 'components/Convert'
import classes from './LifeExpectancyForm.scss'
import FieldError from 'components/FieldError'
import InlineEdit from 'components/InlineEdit'
import RadioButtonGroup from 'components/RadioButtonGroup'
import SyncHealthDeviceButton from '../../containers/SyncHealthDeviceButton'
import Text from 'components/Text'

const kgToLbsMultiplier = 2.20462
const mToFtMultiplier = 0.0328084

const sanitizedNumberValue = ({ value }) =>
  R.isEmpty(value) || R.isNil(value) ? 0 : value

const handleFeetChange = R.curry((onChange, event) => {
  const { value } = event.target
  const splitted = R.split(' ', value)
  const feet = parseInt(splitted[0], 10)
  const inches = parseInt(splitted[2], 10)
  const finalFeet = isNaN(feet) ? 0 : feet
  const finalInches = isNaN(inches) ? 0 : inches

  if (finalFeet > 9 || finalInches > 11) {
    return
  }

  const finalTarget = R.merge(event.target, { value: finalFeet + finalInches / 12 })
  const finalEvent = R.merge(event, { target: finalTarget })
  onChange(finalEvent)
})

const formatFeet = (totalFeet) => {
  const feet = Math.floor(totalFeet)
  const remainder = totalFeet - feet
  const inches = Math.round(remainder * 12)
  if (inches === 12) {
    return `${feet + 1} ft 0 in`
  } else {
    return `${feet} ft ${inches} in`
  }
}

export default class LifeExpectancyForm extends Component {
  static propTypes = {
    calculatedLifeExpectancy: PropTypes.number,
    client: PropTypes.object,
    fields: PropTypes.object.isRequired,
    healthDevicesConfig: PropTypes.object,
    lifeExpectancyValue: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    metricSystem: PropTypes.bool.isRequired,
    setMetricSystem: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired
  };

  constructor (props) {
    super(props)
    this.state = {
      isEditing: false
    }
  }

  setIsEditing = (isEditing) => {
    this.setState({ isEditing })
  }

  render () {
    const { props, setIsEditing } = this
    const { calculatedLifeExpectancy, client, fields, fields: { dailyExercise, drinks, height,
      lifeExpectancy, smoker, weight }, healthDevicesConfig, lifeExpectancyValue, metricSystem,
      setMetricSystem, title } = props
    const { isEditing } = this.state
    const canSetToCalculated = R.is(Number, calculatedLifeExpectancy) &&
      !R.equals(calculatedLifeExpectancy, lifeExpectancyValue)

    return (
      <div>
        <div className='form-horizontal'>
          <FormGroup>
            <Col xs={6} className={classes.markerTooltipTitle} noGutterRight>
              <Text size='small'>
                <AllCaps>{title}</AllCaps>
              </Text>
            </Col>
            <Col xs={6} className='text-right' noGutterLeft>
              <InlineEdit buttonLabel='Set' isEditing={isEditing} type='number'
                min={config.retirementPlan.MIN_LIFE_EXPECTANCY}
                max={config.retirementPlan.MAX_LIFE_EXPECTANCY}
                onEnd={function (value) { lifeExpectancy.onChange(value); setIsEditing(false) }}
                onCancel={function () { setIsEditing(false) }} value={lifeExpectancyValue}>
                <span className={classes.lifeExpectancyValue}
                  onClick={function () { setIsEditing(true) }}>
                  {lifeExpectancyValue}
                  <MdEdit size={16} className={classes.editIcon} />
                </span>
              </InlineEdit>
              <FieldError for={lifeExpectancy} />
            </Col>
          </FormGroup>
        </div>
        <FormGroup>
          {client &&
            <SyncHealthDeviceButton client={client} fields={fields}
              healthDevicesConfig={healthDevicesConfig} />}
        </FormGroup>
        <FormGroup className={classes.formGroup}>
          <ControlLabel className={classes.controlLabel}>
            Have you smoked in the last 12 months?
          </ControlLabel>
          <RadioButtonGroup type='radio' className={classes.radioButtonGroup}
            {...domOnlyProps(smoker)}>
            <Button bsStyle='wide' className={classes.radioButton} value>Yes</Button>
            <Button bsStyle='wide' className={classes.radioButton} value={false}>No</Button>
          </RadioButtonGroup>
        </FormGroup>
        <FormGroup className={classes.formGroup}>
          <ControlLabel className={classes.controlLabel}>
            Alcoholic drink consumption (per day)
          </ControlLabel>
          <FormControl type='number' step={1} min={0} {...domOnlyProps(drinks)} />
          <FieldError for={drinks} />
        </FormGroup>
        <FormGroup className={classes.formGroup}>
          <ControlLabel className={classes.controlLabel}>
            On average, how many minutes do you exercise per day?
          </ControlLabel>
          <Row>
            <Col xs={6}>
              <FormControl type='number' step={1} min={0} max={1440}
                {...domOnlyProps(dailyExercise)} />
              <FieldError for={dailyExercise} />
            </Col>
            <Col xs={6} className='text-right'>
              <div className={classes.help}>mins/day</div>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup className={classes.formGroup}>
          <ControlLabel className={classes.controlLabel}>
            What is your weight?
          </ControlLabel>
          <Row>
            <Col xs={6}>
              <Convert value={sanitizedNumberValue(weight)} onChange={weight.onChange}
                convert={!metricSystem} multiplier={kgToLbsMultiplier}>
                {({ onChange, value }) =>
                  <FormControl type='number' min={0} value={value} onChange={onChange} />}
              </Convert>
              <FieldError for={weight} />
            </Col>
            <Col xs={6} className='text-right'>
              <RadioButtonGroup className={classes.radioButtonGroup} value={metricSystem}
                onChange={setMetricSystem}>
                <Button className={classes.radioButton} value>Kg</Button>
                <Button className={classes.radioButton} value={false}>lbs</Button>
              </RadioButtonGroup>
            </Col>
          </Row>
        </FormGroup>
        <FormGroup className={classes.formGroup}>
          <ControlLabel className={classes.controlLabel}>
            What is your height?
          </ControlLabel>
          <Row>
            <Col xs={6}>
              <Convert value={sanitizedNumberValue(height)} onChange={height.onChange}
                convert={!metricSystem} multiplier={mToFtMultiplier}>
                {({ onChange, value }) =>
                  metricSystem
                    ? <FormControl type='number' min={0} step={1} value={value}
                      onChange={onChange} />
                    : <div>
                      <FormControl componentClass={MaskedInput} mask='9 ft 99 in'
                        placeholder='_ ft __ in' value={formatFeet(value)}
                        onChange={handleFeetChange(onChange)} />
                    </div>}
              </Convert>
              <FieldError for={height} />
            </Col>
            <Col xs={6} className='text-right'>
              <RadioButtonGroup className={classes.radioButtonGroup} value={metricSystem}
                onChange={setMetricSystem}>
                <Button className={classes.radioButton} value>cm</Button>
                <Button className={classes.radioButton} value={false}>ft</Button>
              </RadioButtonGroup>
            </Col>
          </Row>
        </FormGroup>
        {canSetToCalculated &&
        <FormGroup>
          <Button className={classes.setToCalculatedButton} bsStyle='primary'
            onClick={() => lifeExpectancy.onChange(calculatedLifeExpectancy)}>
            Set life expectancy to {calculatedLifeExpectancy} *
          </Button>
          <HelpBlock>
            <Text size='small' light>*calculated based on your input above</Text>
          </HelpBlock>
        </FormGroup>}
      </div>
    )
  }
}
