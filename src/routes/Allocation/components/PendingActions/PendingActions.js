import React, { Component, PropTypes } from 'react'
import { ControlLabel } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import { getUniqueSettingsKeys } from '../../helpers'
import Button from 'components/Button'
import InlineList from 'components/InlineList'
import OverlayTooltip from 'components/OverlayTooltip'
import PendingStatus from '../PendingStatus'
import Text from 'components/Text'

export default class PendingActions extends Component {
  static propTypes = {
    approve: PropTypes.func.isRequired,
    goal: PropTypes.object.isRequired,
    revert: PropTypes.func.isRequired
  };

  render () {
    const { approve, goal, goal: { active_settings, approved_settings }, revert } = this.props
    const uniqueSettings = getUniqueSettingsKeys(goal)
    const isPendingApproval = R.contains('selected_settings', uniqueSettings)
    const canRevert = R.or(active_settings, approved_settings)
    const overlayTrigger =
      <Button bsStyle='danger'
        className={classNames('btn-xlarge-thin', { disabled: !canRevert })}
        onClick={canRevert && revert}>
        Revert
      </Button>

    return isPendingApproval ? (
      <div>
        <ControlLabel>
          <Text size='small' light>
            Approve or Revert
          </Text>
        </ControlLabel>
        <div>
          <InlineList>
            <Button bsStyle='primary' className='btn-xlarge-thin' onClick={approve}>
              Approve
            </Button>
            <OverlayTooltip placement='top' id='cannotRevertTooltip' trigger={overlayTrigger}>
              {!canRevert
                ? <span>
                  No settings have yet been approved for this goal, cannot revert to last approved.
                </span>
                : false}
            </OverlayTooltip>
          </InlineList>
        </div>
      </div>
    ) : <PendingStatus goal={goal} isAdvisor />
  }
}
