import React, { Component, PropTypes } from 'react'
import classes from './Value.scss'

export default class Value extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  render () {
    const { children } = this.props

    return (
      <dd className={classes.value}>{children}</dd>
    )
  }
}
