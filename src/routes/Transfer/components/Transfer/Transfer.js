import React, { Component, PropTypes } from 'react'
import { Col, Panel, Row } from 'react-bootstrap'
import { FormattedDate, FormattedNumber } from 'react-intl'
import R from 'ramda'
import { MdExpandLess, MdExpandMore } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import AllCaps from 'components/AllCaps'
import classes from './Transfer.scss'
import CashManagementModal from '../CashManagementModal'
import ConfirmDepositModal from 'containers/ConfirmDepositModal'
import ConfirmWithdrawModal from '../../containers/ConfirmWithdrawModal'
import Deposit from '../../containers/Deposit'
import GoalBalanceValue from 'components/GoalBalanceValue'
import GoalNavigation from 'components/GoalNavigation'
import InlineList from 'components/InlineList'
import PageTitle from 'components/PageTitle'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Withdraw from '../../containers/Withdraw'

const getPendingDeposit = R.compose(
  R.when(R.gt(0), R.always(0)),
  R.defaultTo(0),
  R.path(['goal', 'invested', 'net_pending'])
)

const pageHeader = (_props) => {
  const { accounts, goal, params: { clientId, goalId }, router: { push } } = _props

  return (
    <Row>
      <Col xs={3}>
        <GoalNavigation accounts={accounts} selectedItem={{ id: goalId, type: 'goal' }}
          onSelectGoal={function (goal) { push(`/${clientId}/transfer/${goal.id}`) }} />
      </Col>
      <Col xs={4}>
        <AllCaps tagName='div' value='Balance' />
        <Text size='large'>
          <GoalBalanceValue goal={goal} />
        </Text>
      </Col>
      <Col xs={5} className='text-right'>
        {goal && goal.account && R.gt(goal.account.cash_balance, 0) && (
          <InlineList>
            <AllCaps tagName='div' value='Unallocated Account Balance' />
            <Text size='large'>
              <FormattedNumber value={goal.account.cash_balance} format='currency' />
            </Text>
          </InlineList>)}
      </Col>
    </Row>
  )
}

const subHeader = (_props) =>
  <Row>
    <Col xs={3}>
      <Text size='medium' primary>Pending Transfers</Text>
    </Col>
    <Col xs={9}>
      <Text size='medium'>
        <FormattedNumber value={getPendingDeposit(_props)} format='currency' />
        <span onClick={_props.togglePendingPanel}>
          {_props.pendingPanelExpanded ? <MdExpandLess /> : <MdExpandMore />}
        </span>
      </Text>
    </Col>
  </Row>

export default class Transfer extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    goal: PropTypes.object,
    params: PropTypes.object.isRequired,
    pendingPanelExpanded: PropTypes.bool.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired,
    togglePendingPanel: PropTypes.func.isRequired,
    transfers: PropTypes.array.isRequired,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['goal', 'params', 'pendingPanelExpanded', 'transfers'],
      this.props, nextProps)
  }

  render () {
    const { props } = this
    const { goal, pendingPanelExpanded, show, transfers, user } = props
    const pendingDeposit = getPendingDeposit(props)

    return (
      <div className='container'>
        <PageTitle title='Deposit or withdraw money' />
        <Panel header={goal && pageHeader(props)} className={classes.panel}>
          <Panel collapsible header={subHeader(props)} expanded={pendingPanelExpanded}
            className={classes.pendingPanel}>
            {R.map(({ amount, id, time }) =>
              <Row key={id}>
                <Col xs={3}>
                  <FormattedDate value={time * 1000} format='dayMonthAndYear' />
                </Col>
                <Col xs={9}>
                  <FormattedNumber value={amount} format='currency' />
                </Col>
              </Row>
            , transfers)}
          </Panel>
          {user && goal
            ? (
              <div>
                <div className={classes.section}>
                  <Col xs={12}>
                    <Deposit goal={goal} pendingDeposit={pendingDeposit} show={show} />
                  </Col>
                </div>
                <div className={classes.section}>
                  <Col xs={12}>
                    <Withdraw goal={goal} show={show} />
                  </Col>
                </div>
              </div>
            )
            : <Spinner />}
        </Panel>
        <ConfirmDepositModal goal={goal} />
        <ConfirmWithdrawModal goal={goal} />
        <CashManagementModal goal={goal} />
      </div>
    )
  }
}
