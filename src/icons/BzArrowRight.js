/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzArrowRight extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 9 18' {...this.props}>
        <g>
          <path d='M1.53,18a1.54,1.54,0,0,1-1-.34,1.48,1.48,0,0,1-.22-2.11L5.79,9,.34,2.45A1.48,1.48,0,0,1,.56.34,1.55,1.55,0,0,1,2.72.55L8.43,7.43a2.46,2.46,0,0,1,0,3.15L2.72,17.45A1.54,1.54,0,0,1,1.53,18ZM6.06,9.32h0Z' />
        </g>
      </IconBase>
    )
  }
}
