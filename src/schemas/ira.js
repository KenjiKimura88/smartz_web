import buildSchema from 'redux-form-schema'
import R from 'ramda'

const schemaProperties = {
  'ira': {
    label: 'Select an IRA',
    required: true
  },
  'confirmSep': {
    label: 'I confirm',
    validate: {
      validateConfirmSep: ({ ira }, fieldValue) =>
        !R.equals(ira, 'sep') || R.equals(ira, 'sep') && fieldValue
    }
  },
}

export default buildSchema(schemaProperties)
