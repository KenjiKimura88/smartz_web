import React from 'react'
import { shallow } from 'enzyme'
import Allocation from 'routes/Allocation/components/Allocation'
import MainFrame from 'components/MainFrame'

describe('(Route) Allocation', () => {
  describe('(Component) Allocation', () => {
    const goals = [
      {
        id: 1,
        name: 'AAA',
        drift_score: 0.3,
        selected_settings: {}
      }
    ]

    const accounts = [
      {
        id: 22,
        account_name: 'Account X',
        goals
      }
    ]

    const params = {
      goalId: 1,
      viewedSettings: 'pending'
    }

    const oneTimeDepositForm = {
      fields: {
        amount: {}
      },
      values: {}
    }

    const values = {
      constraints: []
    }

    const fields = {
      constraints: [],
      duration: {},
      monthlyTransactionAmount: {},
      performance: {},
      risk: {},
      target: {}
    }

    const getComponent = (props) =>
      <Allocation allocationState={{}} approve={function () {}} dirty={false} fields={fields}
        goal={goals[0]} goals={goals} accounts={accounts} router={{ push: function () {} }}
        oneTimeDepositForm={oneTimeDepositForm} params={params} resetForm={function () {}}
        revert={function () {}} setAllocationVar={function () {}} setGoalsVar={function () {}}
        settings={{}} show={function () {}} toggleAllocationVar={function () {}} values={values}
        {...props} />

    it('renders as a MainFrame', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(MainFrame)
    })
  })
})
