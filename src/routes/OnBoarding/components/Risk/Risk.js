import React, { Component, PropTypes } from 'react'
import { Button } from 'react-bootstrap'
import R from 'ramda'
import bgImage from './risk-profile-icon.svg'
import Card from '../Card'
import classes from './Risk.scss'
import NavHeader from '../NavHeader'
import Option from '../Option'
import Sidebar from '../Sidebar'

const isLastStep = (stepId, questions) => stepId + 1 >= questions.length

export default class Risk extends Component {
  static propTypes = {
    getValue: PropTypes.func.isRequired,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    questions: PropTypes.array,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string.isRequired,
    stepId: PropTypes.number.isRequired,
    save: PropTypes.func.isRequired,
  };

  submitAgreement(value, success, fail) {
    const { save } = this.props
    save({
      body: {
        onboarding_data: value
      },
      success,
      fail
    })
  }

  handleChange = value => () => {
    const { setValue, stepId } = this.props
    setValue(['risk', 'steps', stepId], {
      value,
      completed: true
    })
  }

  handleContinue = () => {
    const { onboarding, push, setValue, stepId, questions } = this.props
    const callback = () => {
      const url = isLastStep(stepId, questions)
        ? `/agreements`
        : `/risk/${stepId + 1}`
      push(url)
    }
    isLastStep(stepId, questions)
    ? setValue(['risk', 'completed'], true, (onboardingValue) => {
        this.submitAgreement(onboardingValue, callback, callback)
      })
    : this.submitAgreement(onboarding.value, callback, callback)
  }

  handleSaveAndQuit = (fields) => {
    const { push, onboarding } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitAgreement(onboarding.value, callback, callback)
  }

  renderHeader() {
    return (
      <NavHeader {...this.props} onSaveAndQuit={this.handleSaveAndQuit} />
    )
  }

  renderSidebar() {
    const { questions, stepId } = this.props
    const question = questions ? questions[stepId] : {}

    return (
      <Sidebar title={question.text}>
        <p><strong>Why we ask:</strong></p>
        <p>{question.explanation}</p>
      </Sidebar>
    )
  }

  render () {
    const { getValue, isLoading, questions, stepId } = this.props
    const question = questions ? questions[stepId] : {}
    const answers = question.answers || []
    const currentValue = getValue(['risk', 'steps', stepId, 'value'])
    return (
      <div>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} isLoading={isLoading}>
          {question.figure &&
            <div className={classes.figure}
              dangerouslySetInnerHTML={{ __html: question.figure }} />
          }
          <div>
            {answers.map(o => (
              <Option
                key={o.id}
                value={o.id}
                label={o.text}
                onClick={this.handleChange}
                selected={currentValue === o.id} />))}
          </div>

          {R.is(Number, currentValue) &&
            <div className='text-right'>
              <Button disabled={isLoading} bsStyle='thick-outline' onClick={this.handleContinue}>
                Continue
              </Button>
            </div>}
        </Card>
      </div>
    )
  }
}
