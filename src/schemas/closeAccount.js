import buildSchema from 'redux-form-schema'
import R from 'ramda'

export const LIQUIDATE = 0
export const TRANSFER_TO_ACCOUNT = 1
export const TRANSFER_TO_CUSTODIAN = 2
export const TAKE_CUSTODY = 3

export const closeOptions = [
  { label: 'Liquidate assets', value: LIQUIDATE },
  { label: 'Transfer assets to another account', value: TRANSFER_TO_ACCOUNT },
  { label: 'Transfer assets to another custodian', value: TRANSFER_TO_CUSTODIAN },
  { label: 'Take direct custody of your assets', value: TAKE_CUSTODY }
]

export default buildSchema({
  closeOption: {
    label: 'Option',
    required: true
  },
  accountTransferForm: {
    label: 'Internal Account Transfer Form',
    required: R.propEq('closeOption', TRANSFER_TO_ACCOUNT)
  }
})
