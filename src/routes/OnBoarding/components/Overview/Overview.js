import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { BzCheck } from 'icons'
import bgImage from './onboarding_steps-screen-bg-icon.svg'
import classes from './Overview.scss'
import Card from '../Card'
import LogoHeader from '../LogoHeader'

const steps = [
  'login',
  'info',
  'risk',
  'agreements'
]

const stepLabels = [
  'Create Login',
  'Personal Details',
  'Risk Profile',
  'Sign Agreements'
]

const getArrows = (step, push, invitationKey) => {
  return steps.map((s, index) => {
    const liProps = { className: classes.arrow }
    const sIndex = steps.indexOf(step)
    const isActive = index < sIndex
    const isDisabled = index > sIndex
    if (isActive) {
      liProps.className += ` ${classes.active}`
    } else if (isDisabled) {
      liProps.className += ` ${classes.disabled}`
    } else {
      liProps.onClick = () => {
        push(`${s}/0`)
      }
    }

    return (
      <li key={s} {...liProps}>
        <div className={classes.stepInner}>
          <span className={classes.number}>{isActive ? <BzCheck /> : index + 1}</span>
          <span className={classes.label}>{stepLabels[index]}</span>
        </div>
      </li>
    )
  })
}

export default class Overview extends Component {
  static propTypes = {
    invitation: PropTypes.object,
    push: PropTypes.func.isRequired,
    step: PropTypes.string.isRequired,
    user: PropTypes.object
  };

  get firstName() {
    const { invitation, user } = this.props
    const userFirstName = user && user.first_name
    const invitationFirstName = invitation && invitation.first_name
    return userFirstName || invitationFirstName
  }

  render () {
    const { props } = this
    const { step, push, invitation, invitationKey } = props

    return (
      <div className={classes.overview}>
        <LogoHeader invitation={invitation} />
        <Card bgImage={bgImage}>
          <div className={classes.overviewContent}>
            <h2 className={classes.title}>
              Welcome <span className={classes.name}>{this.firstName}</span>
            </h2>
            <div className={classes.descriptionWrapper}>
              <Row>
                <Col xs={10} xsOffset={1}>
                  <div className={classes.description}>
                    Thank you for choosing{' '}
                    <span className={classes.name}>{invitation && invitation.firm_name} </span>
                    to manage your finances. To get started, we need some details about you.
                    Complete the steps below to get started with your financial dreams. We know this
                    stuff can be tedious, so we have broken it up into a few steps.
                  </div>
                </Col>
              </Row>
            </div>
            <ul className={classes.arrowContainer}>
              {getArrows(step, push, invitationKey)}
            </ul>
            <Row>
              <Col xs={10} xsOffset={1}>
                <div className={classes.bottomText}>
                  Use the arrows to navigate the sign-up process. You can leave any time
                  and we will save your progress.
                </div>
              </Col>
            </Row>
          </div>
        </Card>
      </div>
    )
  }
}
