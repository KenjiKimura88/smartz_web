/* eslint-disable max-len */
import { BzRollover401k, BzRolloverIra, BzRolloverOther } from 'icons'
import config from 'config'
const { accountTypes } = config

const rolloverTypesOptions = [
  {
    accountName: 'Individual Retirement Account',
    title: 'IRA',
    icon: BzRolloverIra,
    text: 'Roll over a Traditional, Roth, SEP or SIMPLE IRA',
    value: accountTypes.ACCOUNT_TYPE_IRA
  },
  {
    accountName: '401(K) Account',
    title: '401(k)',
    icon: BzRollover401k,
    text: 'Roll over a Roth or Traditional 401(k) from a previous employer',
    value: accountTypes.ACCOUNT_TYPE_401K
  },
  {
    accountName: 'Other Account',
    title: 'Other',
    icon: BzRolloverOther,
    text: 'Roll over a less common retirement account',
    value: accountTypes.ACCOUNT_TYPE_OTHER
  }
]

export default rolloverTypesOptions
