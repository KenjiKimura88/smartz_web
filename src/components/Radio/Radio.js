import React, { Component, PropTypes } from 'react'
import { Radio as BsRadio } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './Radio.scss'

export default class Radio extends Component {
  static propTypes = {
    checked: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
  };

  static defaultProps = {
    checked: false
  }

  render () {
    const { props } = this
    const { children, checked, disabled, className, inline } = props
    const radioClass = classNames(classes.radio, {
      [className]: !!className,
      [classes.disabled]: disabled,
      [classes.inline]: inline
    })
    const radioIconClass = classNames(classes.dot, {
      ['hide']: !checked
    })

    return (
      <BsRadio {...props} className={radioClass}>
        <span className={classes.radioInner}>
          <span className={radioIconClass} />
        </span>
        {children}
      </BsRadio>
    )
  }
}
