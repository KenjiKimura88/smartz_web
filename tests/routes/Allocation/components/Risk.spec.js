import React from 'react'
import { shallow } from 'enzyme'
import Risk from 'routes/Allocation/components/Risk/Risk'
import Panel from 'containers/Panel'

describe('(Route) Allocation', () => {
  describe('(Component) Risk', () => {

    const getComponent = (props) =>
      <Risk goalId={1} recommended={{}} risk={{}} settings={{}} viewedSettings='pending'
        years={1} {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Panel)
    })
  })
})
