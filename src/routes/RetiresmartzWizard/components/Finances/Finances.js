import React, { Component, PropTypes } from 'react'
import { FormGroup } from 'react-bootstrap'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import { BzPlusCircle } from 'icons'
import { sumContributionsAmount } from '../../helpers'
import Assets from '../../containers/Assets'
import Button from 'components/Button'
import classes from './Finances.scss'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Incomes from '../../containers/Incomes'
import RetirementAccount from '../RetirementAccount'
import RetirementGoals from '../RetirementGoals'
import SocialSecurityStatement from '../SocialSecurityStatement'

// getRetirementAccountCategoryOptions :: Props -> [{ label: String, value: Number }]
const getRetirementAccountCategoryOptions = R.compose(
  R.sortBy(R.prop('label')),
  R.map(({ id, title }) => ({
    label: title,
    value: id
  })),
  R.prop('retirementAccountCategories')
)

export default class Finances extends Component {
  static propTypes = {
    client: PropTypes.object,
    clientId: PropTypes.string.isRequired,
    expenses: PropTypes.array.isRequired,
    goalTypes: PropTypes.array.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    homeValue: PropTypes.object.isRequired,
    incomes: PropTypes.array.isRequired,
    partnerName: PropTypes.object.isRequired,
    requests: PropTypes.object.isRequired,
    retirementAccountCategories: PropTypes.array.isRequired,
    retirementAccounts: PropTypes.array.isRequired,
    retirementGoals: PropTypes.object.isRequired,
    retirementPlanId: PropTypes.string,
    saveFile: PropTypes.func,
    socialSecurityClient: PropTypes.object.isRequired,
    socialSecurityPartner: PropTypes.object.isRequired,
    ssFraTodays: PropTypes.object.isRequired,
    estimateDate: PropTypes.object.isRequired,
    updateExpensesFields: PropTypes.func.isRequired,
    user: PropTypes.object,
    values: PropTypes.object
  };

  addRetirementAccount = () => {
    const { retirementAccounts } = this.props
    const lastId = R.compose(
      R.reduce(R.max, 0),
      R.map(R.path(['id', 'value'])),
      R.defaultTo([])
    )(retirementAccounts) + 1
    retirementAccounts.addField({ id: lastId, contrib_amt: 0, contrib_period: 'monthly' })
  }

  removeRetirementAccount = (retirementAccount) => {
    const { retirementAccounts, updateExpensesFields,
      values: { clientIncome, retirementAccounts: retirementAccountsValues } } = this.props
    const prevMonthlyBtc = sumContributionsAmount(retirementAccountsValues, 'self')
    const nextMonthlyBtc = sumContributionsAmount(R.reject(
      R.propEq('id', retirementAccount.id.value),
      retirementAccountsValues
    ), 'self')
    const monthlyClientIncome = clientIncome / 12
    const prevAvailableExpenses = monthlyClientIncome - prevMonthlyBtc
    const newAvailableExpenses = monthlyClientIncome - nextMonthlyBtc
    retirementAccounts.removeField(R.findIndex(R.equals(retirementAccount), retirementAccounts))
    updateExpensesFields(prevAvailableExpenses, newAvailableExpenses)
  }

  render () {
    const { props, removeRetirementAccount } = this
    const { client, clientId, expenses, goals, goalTypes, homeValue, incomes, maritalStatus,
      partnerName, requests, retirementAccounts, retirementGoals, retirementPlanId, saveFile,
      socialSecurityClient, socialSecurityPartner, ssFraTodays, estimateDate,
      updateExpensesFields, user, values } = props
    const retirementAccountCategoryOptions = getRetirementAccountCategoryOptions(props)

    return (
      <div>
        <Assets clientId={clientId} homeValue={homeValue} retirementPlanId={retirementPlanId} />
        <Incomes clientId={clientId} incomes={incomes} retirementPlanId={retirementPlanId} />
        <FormGroup>
          <H2 noMargin>Retirement Accounts</H2>
          {mapIndexed((retirementAccount, index) =>
            <RetirementAccount key={index} categoryOptions={retirementAccountCategoryOptions}
              expenses={expenses} maritalStatus={maritalStatus} partnerName={partnerName}
              remove={function () { removeRetirementAccount(retirementAccount) }} user={user}
              retirementAccount={retirementAccount} updateExpensesFields={updateExpensesFields}
              values={values} />
          , retirementAccounts)}
          <div>
            <Button className={classes.addButton} onClick={this.addRetirementAccount}>
              <BzPlusCircle size={18} />{' '}
              <span>Add a retirement account</span>
            </Button>
          </div>
        </FormGroup>
        <SocialSecurityStatement client={client} estimateDate={estimateDate}
          maritalStatus={maritalStatus} partnerName={partnerName} requests={requests}
          saveFile={saveFile} socialSecurityClient={socialSecurityClient}
          socialSecurityPartner={socialSecurityPartner} ssFraTodays={ssFraTodays} />
        {R.length(goals) > 0 &&
          <RetirementGoals goals={goals} goalTypes={goalTypes} retirementGoals={retirementGoals} />}
        <Hr />
      </div>
    )
  }
}
