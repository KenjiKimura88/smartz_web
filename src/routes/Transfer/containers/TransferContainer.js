import { createStructuredSelector } from 'reselect'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getAccounts, getGoal } from 'helpers/requests'
import { getProfile } from 'redux/modules/auth'
import { pendingPanelExpandedSelector } from 'redux/selectors'
import { togglePendingPanel } from 'redux/modules/transfer'
import Transfer from '../components/Transfer'

const selector = createStructuredSelector({
  pendingPanelExpanded: pendingPanelExpandedSelector
})

const requests = ({ params: { clientId, goalId } }) => ({
  accounts: getAccounts(clientId),
  goal: getGoal({ clientId, goalId }),
  transfers: goalId && (({ findQuery }) => findQuery({
    type: 'transfers',
    url: `/goals/${goalId}/pending-transfers`,
    query: {
      goal: parseInt(goalId, 10)
    },
    deserialize: R.map(R.assoc('goal', parseInt(goalId, 10)))
  })),
  user: getProfile
})

const actions = {
  show,
  togglePendingPanel
}

export default R.compose(
  connect(requests, selector, actions),
  withRouter
)(Transfer)
