import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './GoalReturnsRow.scss'

export default class GoalReturnsRow extends Component {
  static propTypes = {
    label: PropTypes.node.isRequired,
    value: PropTypes.node.isRequired,
    footer: PropTypes.bool
  };

  static defaultProps = {
    footer: false
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['label', 'value', 'footer'], this.props, nextProps)
  }

  render () {
    const { footer, label, value } = this.props
    const wrapperClass = classNames(classes.row, {
      [classes.footer]: footer
    })
    return (
      <div className={wrapperClass}>
        <Row>
          <Col xs={8}>
            {label}
          </Col>
          <Col xs={4} className='text-right'>
            {(R.is(Number, value) || R.is(String, value))
              ? <FormattedNumber value={value} format='currency' />
              : value}
          </Col>
        </Row>
      </div>
    )
  }
}
