import React from 'react'
import { shallow } from 'enzyme'
import { Table } from 'react-bootstrap'
import moment from 'moment'
import R from 'ramda'
import ActivityList from 'routes/Activity/components/ActivityList'
import ActivityListItem from 'routes/Activity/components/ActivityListItem'
import mountWithIntl from '../../../test-helpers/mountWithIntl'

describe('(Route) Activity', () => {
  describe('(Component) ActivityList', () => {
    const params = {
      clientId: '3',
      goalId: '34'
    }
    const activityTypes = [
      {
        id: 1,
        name: 'Foo',
        format_str: '1 + {} = {}'
      },
      {
        id: 2,
        name: 'Bar',
        format_str: 'Baz'
      }
    ]
    const accounts = [
      {
        id: 14,
        account_name: 'Account X'
      }
    ]
    const goals = [
      {
        id: 1,
        name: 'Foo',
        account: accounts[0]
      },
      {
        id: 2,
        name: 'Bar',
        account: accounts[0]
      }
    ]
    const items = [
      {
        amount: 20,
        data: [1, 2],
        goal: 2,
        time: parseInt(moment('2012', 'YYYY').valueOf() / 1000, 10),
        type: 1
      },
      {
        amount: 30,
        goal: 1,
        time: parseInt(moment('2013', 'YYYY').valueOf() / 1000, 10),
        type: 2
      }
    ]

    const getComponent = (props) =>
      <ActivityList activityTypes={activityTypes} params={params} goals={goals} accounts={accounts}
        items={items} {...props} />

    it('renders as a <Table />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Table)
    })

    it('renders table header with the correct columns', () => {
      const wrapper = mountWithIntl(getComponent())
      const columns = wrapper.find('table > thead > tr > th')
      const columnsText = R.map((el) => el.text(), columns)
      expect(columns).to.have.length(4)
      expect(columnsText).to.deep.equal(['Date', 'Account', 'Description', 'Change'])
    })

    it('renders 2 <ActivityListItem />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(ActivityListItem)).to.have.length(2)
    })

    it('passes description to each <ActivityListItem />', () => {
      const wrapper = shallow(getComponent())
      const descriptions = R.map(item => item.prop('description') , wrapper.find(ActivityListItem))
      expect(descriptions).to.deep.equal(['1 + 1 = 2', 'Baz'])
    })

    it('passes the goal to each <ActivityListItem />', () => {
      const wrapper = shallow(getComponent())
      const descriptions = R.map(item => item.prop('goal') , wrapper.find(ActivityListItem))
      expect(descriptions).to.deep.equal([goals[1], goals[0]])
    })
  })
})
