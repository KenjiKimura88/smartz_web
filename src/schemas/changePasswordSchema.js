import buildSchema from 'redux-form-schema'

export default buildSchema({
  'old_password': {
    label: 'Current password',
    required: true
  },
  'new_password': {
    label: 'New password',
    required: true
  }
})
