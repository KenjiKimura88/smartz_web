import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './P.scss'

export default class P extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    doubleMargin: PropTypes.bool
  };

  render () {
    const { children, className, doubleMargin } = this.props

    const finalClassName = classNames(classes.p, {
      [className]: !!className,
      [classes.doubleMargin]: doubleMargin
    })

    return (
      <div className={finalClassName}>
        {children}
      </div>
    )
  }
}
