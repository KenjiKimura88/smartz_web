import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, Form, FormControl, FormGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import Button from 'components/Button'
import Notification from 'containers/Notification'
import FieldError from 'components/FieldError'
import PhoneInput from 'react-telephone-input'
import Select from 'components/Select'
import USStateOptions from 'helpers/states'
import CanadaStateOptions from 'helpers/canadaStates'
import IndiaStateOptions from 'helpers/indiaStates'
import countriesOptions from 'helpers/countries'

const userFields = ['first_name', 'middle_name', 'last_name']

const clientFields = ['email', 'phone_num', 'civil_status']

const civilStatusOptions = (settings) => (
  settings && R.compose(
    R.map(({ id, name }) => ({
      value: id,
      label: name
    })),
    R.defaultTo([])
  )(settings.civil_statuses)
)

export default class PersonalProfile extends Component {
  static propTypes = {
    client: PropTypes.object,
    errors: PropTypes.object.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    refreshProfile: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    settings: PropTypes.object,
    show: PropTypes.func.isRequired,
    updateClient: PropTypes.func,
    updateUser: PropTypes.func,
    values: PropTypes.object
  };

  handleCivilStatusChange = (option) => {
    const { fields } = this.props
    fields.civil_status.onChange(option.value)
  }

  showSecurityQuestionsModal = () => {
    const { show } = this.props
    show('securityQuestions', {
      onSubmit: this.save,
      operation: 'update your personal profile',
      submitButtonLabel: 'Update Personal Profile'
    })
  }

  save = (securityQuestions) => {
    const { refreshProfile, updateUser, updateClient, values } = this.props
    const processUpdateClient = () => updateClient({
      body: {
        ...R.pick(clientFields, values),
        residential_address: {
          address: R.join('\n', [
            values.address1,
            values.address2,
            values.city,
          ]),
          post_code: values.post_code,
          region: {
            country: values.country,
            name: values.state,
            code: values.state
          }
        },
        ...securityQuestions
      },
      success: () => refreshProfile()
    })

    updateUser({
      body: {
        ...R.pick(userFields, values),
        ...securityQuestions
      },
      success: processUpdateClient
    })
  }

  stateField() {
    const { fields } = this.props
    if (fields.country.value == 'US') {
      return (
        <Select options={USStateOptions} clearable={false} value={fields.state.value}
          onChange={fields.state.onChange} />
      )
    } else if (fields.country.value == 'CA') {
      return (
        <Select options={CanadaStateOptions} clearable={false} value={fields.state.value}
          onChange={fields.state.onChange} />
      )
    } else if (fields.country.value == 'IN') {
      return (
        <Select options={IndiaStateOptions} clearable={false} value={fields.state.value}
          onChange={fields.state.onChange} />
      )
    } else {
      return (
        <FormControl type='text' {...domOnlyProps(fields.state)} />
      )
    }
  }

  render () {
    const { fields, errors, handleSubmit, requests: { updateClient, updateUser },
      settings } = this.props
    const hasErrors = !R.compose(R.equals(0), R.length, R.keys)(errors)

    return (
      <Form horizontal onSubmit={handleSubmit(this.showSecurityQuestionsModal)}>
        <Notification request={updateUser} />
        <Notification successMessage='Personal Profile has been updated'
          request={updateClient} />
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Name</Col>
          <Col xs={9}>
            <Row>
              <Col xs={4}>
                <FormControl placeholder='First Name' {...domOnlyProps(fields.first_name)} />
                <FieldError for={fields.first_name} />
              </Col>
              <Col xs={4}>
                <FormControl placeholder='Mid' {...domOnlyProps(fields.middle_name)} />
                <FieldError for={fields.middle_name} />
              </Col>
              <Col xs={4}>
                <FormControl type='text' placeholder='Last Name'
                  {...domOnlyProps(fields.last_name)} />
                <FieldError for={fields.last_name} />
              </Col>
            </Row>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Address 1</Col>
          <Col xs={9}>
            <FormControl {...domOnlyProps(fields.address1)} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Address 2</Col>
          <Col xs={9}>
            <FormControl {...domOnlyProps(fields.address2)} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>City/State</Col>
          <Col xs={9}>
            <Row>
              <Col xs={6}>
                <FormControl placeholder='City' {...domOnlyProps(fields.city)} />
              </Col>
              <Col xs={6}>
                {this.stateField()}
              </Col>
            </Row>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Zipcode/Country</Col>
          <Col xs={9}>
            <Row>
              <Col xs={6}>
                <FormControl placeholder='Zip Code' {...domOnlyProps(fields.post_code)} />
              </Col>
              <Col xs={6}>
                <Select options={countriesOptions} clearable={false} placeholder='Country'
                  value={fields.country.value} onChange={fields.country.onChange} />
              </Col>
            </Row>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Contact</Col>
          <Col xs={9}>
            <Row>
              <Col xs={6}>
                <PhoneInput
                  value={fields.phone_num.value}
                  defaultCountry={fields.country.value.toLowerCase()}
                  onChange={fields.phone_num.onChange} />
                <FieldError for={fields.phone_num} />
              </Col>
              <Col xs={6}>
                <FormControl type='email' placeholder='Email' {...domOnlyProps(fields.email)} />
                <FieldError for={fields.email} />
              </Col>
            </Row>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Marital Status</Col>
          <Col xs={9}>
            <Select name={fields.civil_status.name} clearable={false} searchable={false}
              placeholder='Not Selected' options={civilStatusOptions(settings)}
              value={fields.civil_status.value} onChange={this.handleCivilStatusChange} />
            <FieldError for={fields.civil_status} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Gender</Col>
          <Col xs={9}>
            <FormControl.Static>
              {fields.gender.value}
            </FormControl.Static>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Date of Birth</Col>
          <Col xs={9}>
            <FormControl.Static>
              {fields.date_of_birth.value}
            </FormControl.Static>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>SSN</Col>
          <Col xs={9}>
            <FormControl.Static>
              {fields.ssn.value}
            </FormControl.Static>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={9} xsOffset={3} className='text-right'>
            <Button type='submit' bsStyle='primary' disabled={hasErrors}>
              Update
            </Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }
}
