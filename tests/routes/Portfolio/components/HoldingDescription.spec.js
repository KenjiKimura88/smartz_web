import React from 'react'
import { mount, shallow } from 'enzyme'
import { Collapse } from 'react-bootstrap'
import HoldingDescription from 'routes/Portfolio/components/HoldingDescription'

describe('(Route) Portfolio', () => {
  describe('(Component) HoldingDescription', () => {
    const assetClassExplanation = 'explanation 1'
    const tickersExplanation = 'explanation 2'
    const getComponent = (props) =>
      <HoldingDescription assetClassExplanation={assetClassExplanation}
        tickersExplanation={tickersExplanation} {...props} />

    it('renders as a <div>', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('div')
    })

    it('renders all description', () => {
      const wrapper = mount(getComponent())
      expect(wrapper.containsMatchingElement(<div>{assetClassExplanation}</div>)).to.equal(true)
      expect(wrapper.containsMatchingElement(<div>{assetClassExplanation}</div>)).to.equal(true)
    })

    it('toggles description section when link is clicked', () => {
      const wrapper = mount(getComponent())
      wrapper.find('a').simulate('click')
      expect(wrapper.find(Collapse).prop('in')).to.equal(true)
      wrapper.find('a').simulate('click')
      expect(wrapper.find(Collapse).prop('in')).to.equal(false)
    })
  })
})
