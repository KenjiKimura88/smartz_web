import React, { Component, PropTypes } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { NavItem } from 'react-bootstrap'
import R from 'ramda'

const getPathWithoutGoalId = ({ params: { clientId }, path }) => `/${clientId}/${path}`

const getPathWithGoalId = ({ params: { clientId, goalId }, path }) =>
  `/${clientId}/${path}/${goalId}`

const getActive = (props) => {
  const { currentPath } = props
  return R.equals(currentPath.indexOf(getPathWithoutGoalId(props)), 0) ||
    R.equals(currentPath.indexOf(getPathWithGoalId(props)), 0)
}

export default class SiteNavLink extends Component {
  static propTypes = {
    currentPath: PropTypes.string.isRequired,
    ignoreGoalId: PropTypes.bool,
    label: PropTypes.string.isRequired,
    params: PropTypes.object.isRequired,
    path: PropTypes.string.isRequired,
  };

  render () {
    const { props } = this
    const { ignoreGoalId, label, params: { goalId } } = props
    const path = (ignoreGoalId || !goalId)
      ? getPathWithoutGoalId(props)
      : getPathWithGoalId(props)
    return (
      <LinkContainer active={getActive(props)} to={path}>
        <NavItem>
          {label}
        </NavItem>
      </LinkContainer>
    )
  }
}
