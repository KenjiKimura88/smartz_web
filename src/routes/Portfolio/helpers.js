import R from 'ramda'
import uuid from 'uuid'
import { round } from 'helpers/pureFunctions'

export const getHoldingName = R.pathOr('', ['assetClass', 'display_name'])

export const sortHoldings = R.sortBy(
  R.compose(R.toLower, getHoldingName)
)

const allocatedValue = R.compose(
  R.sum,
  R.pluck('value')
)

const allocatedPercent = R.compose(
  R.sum,
  R.pluck('percent')
)

export const getGoalBalance = R.propOr(0, 'balance')

export const unallocatedForGoal = (goal, holdings) => ({
  assetClass: { display_name: 'Unallocated Funds' },
  value: round(getGoalBalance(goal) - allocatedValue(holdings)),
  percent: 100 - allocatedPercent(holdings),
  type: 'unallocatedItem'
})

const emptyAssetClass = {
  id: uuid.v4()
}

const getAssetClass = ({ assetsClasses, ticker: { asset_class } }) => R.compose(
  R.defaultTo(emptyAssetClass),
  R.find(R.propEq('id', asset_class))
)(assetsClasses)

const emptyTicker = () => ({
  id: uuid.v4(),
  unit_price: 0
})

const getTickerFromPosition = ({ position: { ticker }, tickers }) => R.compose(
  R.defaultTo(emptyTicker()),
  R.find(R.propEq('id', ticker))
)(tickers)

const getTickerFromPortfolioItem = ({ portfolioItem: { asset }, tickers }) => R.compose(
  R.defaultTo(emptyTicker()),
  R.find(R.propEq('id', asset))
)(tickers)

const getPortfolioItems = R.compose(
  R.defaultTo([]),
  R.path(['portfolio', 'items'])
)

const getTotalWeight = R.compose(
  R.sum,
  R.map(R.prop('weight')),
  getPortfolioItems
)

const getHoldingFromPosition = (props) => (position) => {
  const { assetsClasses, goal, goal: { balance }, tickers } = props
  const { quantity } = position
  const ticker = getTickerFromPosition({ position, tickers })
  const { unit_price } = ticker
  const value = R.gt(unit_price, 0) ? quantity * unit_price : 0
  const weight = R.gt(unit_price, 0) ? quantity * unit_price / balance : 0
  const finalTicker = R.merge(ticker, {
    percent: weight * 100,
    shares: quantity,
    value
  })

  return {
    assetClass: getAssetClass({ assetsClasses, ticker }),
    goal,
    ticker: finalTicker
  }
}

const getHoldingsFromPortfolio = (props) => (portfolioItem) => {
  const { assetsClasses, goal, goal: { balance }, tickers } = props
  const totalWeight = getTotalWeight(props)
  const ticker = getTickerFromPortfolioItem({ portfolioItem, tickers })
  const { unit_price } = ticker
  const weight = portfolioItem.weight / totalWeight
  const shares = R.gt(unit_price, 0) ? balance * weight / unit_price : 0
  const value = R.gt(unit_price, 0) ? shares * unit_price : 0
  const finalTicker = R.merge(ticker, {
    percent: weight * 100,
    shares,
    value
  })

  return {
    assetClass: getAssetClass({ assetsClasses, ticker }),
    goal,
    ticker: finalTicker
  }
}

const groupByAssetClass = R.compose(
  R.values,
  R.reduceBy(({ percent, shares, tickers, value }, { assetClass, goal, ticker }) => ({
    assetClass,
    id: `${goal.id}-${assetClass.id}`,
    percent: percent + ticker.percent,
    shares: shares + ticker.shares,
    tickers: R.compose(
      R.uniqBy(R.prop('id')),
      R.append(ticker)
    )(tickers),
    value: value + ticker.value,
  }), {
    percent: 0,
    shares: 0,
    tickers: [],
    value: 0,
  }, R.path(['assetClass', 'id']))
)

export const getCurrentHoldings = R.compose(
  groupByAssetClass,
  R.converge(R.map, [
    getHoldingFromPosition,
    R.prop('positions')
  ])
)

export const getPortfolioHoldings = R.compose(
  groupByAssetClass,
  R.converge(R.map, [
    getHoldingsFromPortfolio,
    getPortfolioItems
  ])
)

export const getHoldings = R.ifElse(
  R.propEq('viewedSettings', 'current'),
  getCurrentHoldings,
  getPortfolioHoldings
)
