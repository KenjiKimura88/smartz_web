import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { Motion, presets, spring } from 'react-motion'
import R from 'ramda'
import classes from './NewGoal.scss'
import image from './assets/add_new_goal.jpg'

export default class NewGoal extends Component {
  static propTypes = {
    button: PropTypes.bool,
    clientId: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props)
    this.state = {
      imageLoaded: false,
      imageErrored: false
    }
  }

  handleImageLoaded = () => {
    this.setState({
      imageLoaded: true,
      imageErrored: false
    })
  }

  handleImageErrored = () => {
    this.setState({
      imageLoaded: false,
      imageErrored: true
    })
  }

  render() {
    const { props, state } = this
    const { clientId } = props
    const { imageLoaded, imageErrored } = state

    return (
      <div className={classes.wrapper}>
        {!imageErrored &&
          <img className={classes.background} onLoad={this.handleImageLoaded}
            onError={this.handleImageErrored} src={image} />}
        {R.or(imageLoaded, imageErrored) &&
          <Motion defaultStyle={{ opacity: 0 }}
            style={{ opacity: spring(1, presets.gentle) }}>
            {value =>
              <div className={classes.content} style={{ opacity: value.opacity }}>
                <h2 className={classes.heading}>
                  Add a <span className={classes.primary}>New Goal</span>
                </h2>
                <div className={classes.link}>
                  <Link to={`${clientId}/new-goal/account-type`}>
                    Get started now &gt;
                  </Link>
                </div>
              </div>}
          </Motion>}
      </div>
    )
  }
}
