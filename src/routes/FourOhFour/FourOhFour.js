import React, { Component } from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'
import classes from './FourOhFour.scss'

export default class FourOhFour extends Component {
  render () {
    return (
      <div className={classNames('text-center', classes.fourOhFour)}>
        <h1 className={classes.fourOhFourText}>404</h1>
        <span className={classes.notFoundText}>Page not found</span>
        <hr />
        <Link to='/'>Back To Home View</Link>
      </div>
    )
  }
}
