import React, { Component, PropTypes } from 'react'
import { Col, Form, FormGroup, Row } from 'react-bootstrap'
import { FormattedDate } from 'react-intl'
import R from 'ramda'
import { BzArrowLeft, BzArrowRight } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import AllCaps from 'components/AllCaps'
import Button from 'components/Button'
import classes from './NewGoalRolloverSignature.scss'
import ClientName from 'components/ClientName'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import P from 'components/P'
import RequestSentModal from '../RequestSentModal'
import SignaturePad from '../SignaturePad'
import Text from 'components/Text'
import Well from 'components/Well'

const formattedTansferType = ({ value }) => R.equals(value, 'full')
  ? 'Full Amount'
  : 'Partial Amount'

const accountTypeName = ({ accountTypes, params: { rolloverType } }) => R.compose(
  R.prop('name'),
  R.defaultTo({}),
  R.find(R.propEq('id', parseInt(rolloverType, 10))),
  R.defaultTo([])
)(accountTypes)

export default class NewGoalRolloverSignature extends Component {
  static propTypes = {
    accountTypes: PropTypes.array,
    addAccount: PropTypes.func,
    client: PropTypes.object,
    params: PropTypes.object.isRequired,
    rolloverApprovals: PropTypes.object.isRequired,
    rolloverRequest: PropTypes.object.isRequired,
    rolloverSignature: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { params: { clientId, rolloverType }, router: { replace }, rolloverRequest,
      rolloverApprovals } = this.props
    if (rolloverRequest.invalid || rolloverApprovals.invalid) {
      replace(`/${clientId}/new-goal/rollover/${rolloverType}`)
    }
  }

  handleBack = () => {
    const { params: { clientId, rolloverType }, router: { replace } } = this.props
    replace(`/${clientId}/new-goal/rollover/${rolloverType}/approvals`)
  }

  handleSignSubmit = ({ signature }) => {
    const { addAccount, rolloverRequest: { values },
      params: { rolloverType }, show } = this.props

    addAccount({
      body: {
        account_type: parseInt(rolloverType, 10),
        provider: values.provider,
        account_number: values.accountNumber,
        amount: values.balance,
        signature
      },
      success: () => {
        show('rolloverRequestSentModal')
      },
      fail: ({ error }) => {
        this.setState({
          notification: {
            error,
            show: true
          }
        })
      }
    })
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  handleNotificationHide = () => {
    this.setState({ notification: null })
  }

  resizeSignaturePad() {
    const { signaturePad, signatureWrapper } = this.refs
    const signatureCanvas = signaturePad.refs.cv
    if (signatureCanvas.width !== signatureWrapper.offsetWidth) {
      signatureCanvas.width = signatureWrapper.offsetWidth
      signatureCanvas.height = signatureWrapper.offsetHeight
    }
  }

  render () {
    const { props } = this
    const { rolloverSignature: { fields: { signature }, handleSubmit, invalid },
      rolloverRequest: { fields: { accountNumber, provider, transferType } },
      params: { clientId }, client } = props
    return (
      <Form className={classes.wrapper} onSubmit={handleSubmit(this.handleSignSubmit)}>
        <Text tagName='p' primary bold>TRUSTEE-TO-TRUSTEE</Text>
        <H2 bold>IRA Transfer Request</H2>
        <Well className={classes.well}>
          <Row>
            <Col xs={6}>
              <P>
                <AllCaps>Customer Name: </AllCaps>
                <ClientName client={client} defaultTo='Client' />
              </P>
              <P>
                <AllCaps>Current IRA Provider: </AllCaps>
                <Text>{provider.value}</Text>
              </P>
              <P className={classes.lastP}>
                <AllCaps>Current IRA Type: </AllCaps>
                <Text>{accountTypeName(props)}</Text>
              </P>
            </Col>
            <Col xs={6}>
              <P>
                <AllCaps>Date of Birth: </AllCaps>
                {client && <FormattedDate value={client.date_of_birth} />}
              </P>
              <P>
                <AllCaps>Current IRA Account #: </AllCaps>
                <Text>{accountNumber.value}</Text>
              </P>
              <P className={classes.lastP}>
                <AllCaps>Amount to be transferred: </AllCaps>
                <Text>{formattedTansferType(transferType)}</Text>
              </P>
            </Col>
          </Row>
        </Well>
        <P><Text bold>Authorization and Signature</Text></P>
        <P>
          Please accept this letter as authorization to liquidate and transfer the assets in
          the account specified above to BetaSmartz, as a custodian for my IRA.
        </P>
        <P>
          <AllCaps>Amount to be transfered: </AllCaps>
          <Text>{formattedTansferType(transferType)}</Text>
        </P>
        <P>
          Sign the transfer request below using your mouse, stylus pen or by hand.
        </P>
        <FormGroup>
          <SignaturePad {...domOnlyProps(signature)} />
          <FieldError for={signature} />
        </FormGroup>

        <P>
          I understand that attaching my e-signature is the legal equivalent of submitting a
          document signed by hand, and that clicking the "Sign & Submit" button manifests my
          desire and intent to transfer my IRA to BetaSmartz.
        </P>
        <P>
          BetaSmartz may use a third-party service to send communications including emails and
          faxes on your behalf. BetaSmartz will ensure that this third-party service keeps
          communications and information confidential. By submitting this form you acknowledge
          and accept the use of any third-party service.
        </P>
        <Hr />
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button onClick={this.handleBack}>
              <BzArrowLeft /> Back
            </Button>
            {' '}
            <Button bsStyle='primary' type='submit' disabled={invalid}>
              Sign & Submit <BzArrowRight />
            </Button>
          </Col>
        </Row>
        <RequestSentModal clientId={clientId} />
      </Form>
    )
  }
}
