import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { DOCUMENTS_STEP_ID, maritalStatusFromTaxTranscript, MISC_INFO_STEP_ID } from '../helpers'
import { miscInfoSchema } from 'schemas/onboarding'
import config from 'config'
import MiscInfo from '../components/MiscInfo'

const { ibEnabled } = config

const defaultFromTaxTranscriptData = (getValue) => {
  const documents = getValue(['info', 'steps', DOCUMENTS_STEP_ID])
  const ssn = R.path(['taxTranscriptData', 'SSN'], documents)
  const filingStatus = maritalStatusFromTaxTranscript(
    R.path(['taxTranscriptData', 'filing_status'], documents)
  )
  return (ssn || filingStatus) ? { ssn, filingStatus } : {}
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const asyncValidate = (values) => {
  let birthDate = new Date(values.dateOfBirth)
  let ageDifMs = Date.now() - birthDate.getTime()
  let ageDate = new Date(ageDifMs)
  let result = Math.abs(ageDate.getUTCFullYear() - 1970)
  return sleep(1)
    .then(() => {
      if (result < 18) {
        throw { dateOfBirth: 'Customer must be over 18' }
      }
    })
}

export default R.compose(
  withRouter,
  reduxForm({
    form: 'onboardingMiscInfo',
    ...miscInfoSchema(ibEnabled),
    asyncValidate,
    asyncBlurFields: ['dateOfBirth'],
  }, (state, { getValue }) => ({
    initialValues: R.merge(
      defaultFromTaxTranscriptData(getValue),
      R.defaultTo({}, getValue(['info', 'steps', MISC_INFO_STEP_ID]))
    )
  }))
)(MiscInfo)
