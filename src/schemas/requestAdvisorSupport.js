import buildSchema from 'redux-form-schema'

const schemaProperties = {
  'message': {
    label: 'Message',
    required: true
  }
}

export default buildSchema(schemaProperties)
