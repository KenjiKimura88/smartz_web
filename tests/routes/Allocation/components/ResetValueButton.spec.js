import React from 'react'
import { shallow } from 'enzyme'
import ResetValueButton from 'routes/Allocation/components/ResetValueButton/ResetValueButton'

describe('(Component) ResetValueButton', () => {
  const field = {
    dirty: true
  }
  const getComponent = (props) =>
    <ResetValueButton field={field} savedValue={0} {...props} />

  it('renders as a <a />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('a')
  })

  it('renders nothing when dirty=false', () => {
    const wrapper = shallow(getComponent({
      field: {
        dirty: false
      }
    }))
    expect(wrapper.type()).to.equal(null)
  })

  it('calls onChange with savedValue, on click', () => {
    const onChange = sinon.spy()
    const wrapper = shallow(getComponent({
      field: {
        dirty: true,
        onChange
      },
      savedValue: 933
    }))
    onChange.should.not.have.been.called
    wrapper.find('a').simulate('click')
    onChange.should.have.been.calledOnce
    onChange.should.have.been.calledWith(933)
  })
})
