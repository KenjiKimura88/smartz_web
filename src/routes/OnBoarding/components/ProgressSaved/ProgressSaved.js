import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { MdChevronRight } from 'helpers/icons'
import Button from 'components/Button'
import Card from '../Card'
import classes from './ProgressSaved.scss'
import LogoHeader from '../LogoHeader'
import Notification from 'containers/Notification'

export default class ProgressSaved extends Component {
  static propTypes = {
    invitation: PropTypes.object.isRequired,
    requests: PropTypes.object,
    resendInvitation: PropTypes.func
  }

  handleDone = () => {
    location.replace('https://www.betasmartz.com')
  }

  render () {
    const { invitation, requests, resendInvitation } = this.props
    return (
      <div>
        <LogoHeader invitation={invitation} />
        <Card>
          {requests && <Notification request={requests.resendInvitation}
            successMessage='Invitation link resent successfully.' />}
          <div className={classes.wrapper}>
            <div className={classes.wrapperInner}>
              <h2 className={classes.title}>
                Progress Saved!
              </h2>
              <div className={classes.description}>
                <p>Thanks for getting started with RetireSmartz.</p>
                <p>
                  You can continue the signup process any time by following
                  the original link on the invitation email.<br />
                  <Link onClick={resendInvitation}>Resend invitation email</Link>
                </p>
                <p>
                  <Button bsSize='large' bsStyle='primary' onClick={this.handleDone}>
                    Done, for now <MdChevronRight />
                  </Button>
                </p>
              </div>
            </div>
          </div>
        </Card>
      </div>
    )
  }
}
