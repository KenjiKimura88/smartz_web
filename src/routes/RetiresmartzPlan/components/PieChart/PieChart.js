import React, { PropTypes, Component } from 'react'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import d3 from 'd3'
import R from 'ramda'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import { BzSliceSlider } from 'icons'
import { ControlLabel, Fade, FormControl, FormGroup } from 'react-bootstrap'
import { angleInValidRange, buildTooltipItems, findAngle,
  initDonutChart, MV_DONUT_RATIO, rotateAngle } from './helpers'
import { getExpenditureItems, sumExpendituresAmount, sumIPSAmount, sumSavingsAmount,
  sumTaxesAmount, toMonthlyIncome } from 'routes/RetiresmartzWizard/helpers'
import { domOnlyProps, formatCurrency, mapIndexed, propsChanged, round }
  from 'helpers/pureFunctions'
import classes from './PieChart.scss'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import Spinner from 'components/Spinner'
import Text from 'components/Text'

const PIE_LABEL_LINE_HEIGHT = 12

const toFixedNumber = (value) => Math.round(value)

const hasContributionOf = (who, retirementAccounts) => R.compose(
  R.length(),
  R.defaultTo([]),
  R.filter(R.propEq('owner', who))
)(retirementAccounts)

const savingsInvestments = {
  label: 'Savings &  Investments',
  value : 500,
  type: 'simple',
  color: '#83ad5d',
  excludeLabel: true,
  showLabelThreshold: 0.1
}

const expenditures = {
  label: 'Expenditures',
  value: 500,
  type: 'simple',
  color: '#d47877',
  showLabelThreshold: 0.1
}

const insurancesPensions = {
  label: 'Social security &  Medicare (FCIA)',
  value: 700,
  type: 'simple',
  color: '#8a6d3b',
  excludeLabel: true,
  showLabelThreshold: 0.1
}

const taxes = {
  label: 'Tax',
  value: 700,
  type: 'simple',
  color: '#5f9ea0',
  excludeLabel: true,
  showLabelThreshold: 0.05
}

const retirementContributions = {
  label: 'Retirement  contributions',
  value: 700,
  type: 'complex',
  color: '#add5ff',
  showLabelThreshold: 0.1
}

const yTickFormat = (value, context) => {
  const { formatNumber } = context.intl
  return formatCurrency(value, formatNumber)
}

const hasLoaded = props => {
  const { isLoading, values: { clientIncome } } = props
  return !isLoading && !!clientIncome
}

const getContributionsAmount = (props, state) => {
  const { hasPartner } = props
  const { clientBtcValue, partnerBtcValue } = state
  return R.defaultTo(0, clientBtcValue + (hasPartner ? R.defaultTo(0, partnerBtcValue) : 0))
}

const getTotalAmount = (props) => {
  const { hasPartner, values: { clientIncome, partnerIncome } } = props
  return toMonthlyIncome(clientIncome) + (hasPartner ? toMonthlyIncome(partnerIncome) : 0)
}

const getAvailableAmount = (props) => {
  const { values: { expenses } } = props
  return getTotalAmount(props) - (
    sumIPSAmount(expenses) + sumSavingsAmount(expenses) + sumTaxesAmount(expenses)
  )
}

const tooltip = (d, context) => {
  return R.isNil(d.data.tooltip)
    ? false
    : <div className={classes.tooltip}>
      {!d.data.excludeLabel && <Text bold>{d.data.label}</Text>}
      {mapIndexed((item, index) => (
        <div className={classes.tooltipRow} key={index}>
          <span className={classes.tooltipLabel}>{item.label}</span>
          <span className={classes.tooltipValue}>
            {yTickFormat(item.value, context)}
          </span>
        </div>
      ), d.data.tooltip)}
    </div>
}

const getDatum = (props, state) => {
  const { client, expenseCategories, hasPartner, values: { partnerName, expenses } } = props
  const { clientBtcValue, partnerBtcValue } = state
  const ipsAmount = sumIPSAmount(expenses)
  const savingsAmount = sumSavingsAmount(expenses)
  const taxAmount = sumTaxesAmount(expenses)
  const contributionsAmount = getContributionsAmount(props, state)
  const expendituresAmount = getAvailableAmount(props) - contributionsAmount
  const rcItems = hasPartner && [
    {
      label: client.user.first_name,
      value: R.defaultTo(0, clientBtcValue),
    },
    {
      label: partnerName,
      value: R.defaultTo(0, partnerBtcValue)
    }
  ]

  let datum = []

  if (savingsAmount) {
    datum = R.append(
      R.merge(savingsInvestments, {
        value: savingsAmount,
        tooltip: [{
          label: savingsInvestments.label,
          value: savingsAmount
        }]
      }),
      datum
    )
  }

  datum = R.append(
    R.merge(insurancesPensions, {
      value: ipsAmount,
      tooltip: [{
        label: insurancesPensions.label,
        value: ipsAmount
      }]
    }),
    datum
  )

  datum = R.append(
    R.merge(taxes, {
      value: taxAmount,
      tooltip: [{
        label: taxes.label,
        value: taxAmount
      }]
    }),
    datum
  )

  datum = R.append(
    R.merge(retirementContributions, {
      value: contributionsAmount,
      items: rcItems,
      tooltip: rcItems || [{
        label: retirementContributions.label,
        value: contributionsAmount,
      }]
    }),
    datum
  )

  if (expendituresAmount) {
    datum = R.append(
      R.merge(expenditures, {
        value: expendituresAmount,
        tooltip: R.append({
          label: 'Other',
          value: expendituresAmount - sumExpendituresAmount(expenses)
        }, buildTooltipItems(getExpenditureItems(expenses), expenseCategories))
      }),
      datum
    )
  }

  return datum
}

export default class PieChart extends Component {
  static propTypes = {
    client: PropTypes.object,
    expenseCategories: PropTypes.array.isRequired,
    fields: PropTypes.object.isRequired,
    hasPartner: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool,
    rcFormVisible: PropTypes.bool.isRequired,
    reduxFormInitialize: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)

    this.state = {
      currentAngle: null,
      dragging: false,
      dragOffset: null,
      clientBtcValue: props.values.clientBtc,
      partnerBtcValue: props.values.partnerBtc
    }
  }

  componentDidMount () {
    this.pieChart = initDonutChart(this.renderPieChart)
    window.addEventListener('click', this.handleHideForm)
    window.addEventListener('resize', this.handleResize)
  }

  componentWillReceiveProps (nextProps) {
    !this.state.dragging && this.setState({
      clientBtcValue: nextProps.values.clientBtc,
      partnerBtcValue: nextProps.values.partnerBtc
    })
  }

  shouldComponentUpdate (nextProps, nextState) {
    return propsChanged([
      'client', 'expenseCategories', 'hasPartner', 'isLoading', 'rcFormVisible','values'
    ], this.props, nextProps) || propsChanged([
      'clientBtcValue', 'dragging', 'partnerBtcValue'
    ], this.state, nextState)
  }

  componentDidUpdate () {
    this.renderPieChart()
  }

  componentWillUnmount () {
    this.pieChart.resizeHandler.clear()
    this.removeTooltips()
    window.removeEventListener('click', this.handleHideForm)
    window.removeEventListener('resize', this.handleResize)
  }

  removeTooltips () {
    // Remove ghost tooltips
    d3.selectAll('.nvtooltip').remove()
  }

  handleToggleForm = (event) => {
    const { set, rcFormVisible } = this.props
    set({ rcFormVisible: !rcFormVisible })
    event.stopPropagation()
  }

  handleHideForm = () => {
    const { set } = this.props
    set({ rcFormVisible: false })
  }

  handleResize = (event) => {
    this.renderPieChart()
  }

  handleDragStart = () => {
    this.setState({ dragging: true })
    d3.event.sourceEvent.stopPropagation() // silence other listeners
  }

  handleDragEnd = () => {
    this.setState({
      dragging: false,
      dragOffset: null
    }, () => {
      const { clientBtcValue, partnerBtcValue } = this.state
      this.updateBtcValues(clientBtcValue, partnerBtcValue)
    })
  }

  handleClientBtcChange = (clientBtcValue) => {
    const { values: { partnerBtc: partnerBtcValue } } = this.props
    this.updateBtcValues(clientBtcValue, partnerBtcValue)
  }

  handlePartnerBtcChange = (partnerBtcValue) => {
    const { values: { clientBtc: clientBtcValue } } = this.props
    this.updateBtcValues(clientBtcValue, partnerBtcValue, true)
  }

  updateBtcValues(clientBtcValue, partnerBtcValue, withPartner = false) {
    const { props } = this
    const { fields: { clientBtc, partnerBtc }, hasPartner, reduxFormInitialize,
      values: { expenses: expensesValues, retirementAccounts: retirementAccountsValues } } = props
    const expenditureItems = getExpenditureItems(expensesValues)
    const expendituresAmount = sumExpendituresAmount(expensesValues)
    const availableAmount = getAvailableAmount(props)
    const contributionsAmount = clientBtcValue + partnerBtcValue
    const newExpendituresAmount = availableAmount - contributionsAmount
    const hasClientRC = hasContributionOf('self', retirementAccountsValues)
    const hasPartnerRC = hasContributionOf('partner', retirementAccountsValues)
    if ((hasClientRC && R.lte(clientBtcValue, 0)) ||
      (hasPartnerRC && R.lte(partnerBtcValue, 0)) ||
      R.lte(newExpendituresAmount, 0)) {
      return // clientBtc or partnerBtc or expenditures shouldn't be zero
    }
    if (!R.equals(clientBtc.value, clientBtcValue) || (
      hasPartner && !R.equals(partnerBtc.value, partnerBtcValue)
    )) {
      const newExpenseAmts = R.map(item => ({
        amt: R.contains(item, expenditureItems)
          ? round(newExpendituresAmount * item.amt / expendituresAmount, 0)
          : item.amt
      }), expensesValues)
      const newRetirementAccountAmts = R.map(item => ({
        contrib_amt: R.equals(item.owner, 'partner')
          ? round(partnerBtcValue * item.contrib_amt / partnerBtc.value, 0)
          : round(clientBtcValue * item.contrib_amt / clientBtc.value, 0)
      }), retirementAccountsValues)
      if (withPartner) {
        reduxFormInitialize('retirementPlan', {
          clientBtc: clientBtcValue,
          expenses: newExpenseAmts,
          retirementAccounts: newRetirementAccountAmts
        }, ['clientBtc', 'expenses[].amt', 'retirementAccounts[].contrib_amt'])
        partnerBtc.onChange(partnerBtcValue)
      } else {
        reduxFormInitialize('retirementPlan', {
          partnerBtc: partnerBtcValue,
          expenses: newExpenseAmts,
          retirementAccounts: newRetirementAccountAmts
        }, ['partnerBtc', 'expenses[].amt', 'retirementAccounts[].contrib_amt'])
        clientBtc.onChange(clientBtcValue)
      }
    }
  }

  renderPieChart = () => {
    const { context, props, refs } = this
    const { pieChart, handle1, handle2, rcForm } = refs
    const { hasPartner } = props

    if (!hasLoaded(props)) return

    const that = this
    const totalAmount = getTotalAmount(props)
    const availableAmount = getAvailableAmount(props)

    const rcFormDom = ReactDOM.findDOMNode(rcForm)
    const handle1Dom = ReactDOM.findDOMNode(handle1)
    const handle2Dom = ReactDOM.findDOMNode(handle2)

    const d3PieChart = d3.select(pieChart)
      .datum(getDatum(this.props, this.state))
      .transition().duration(350)
      .call(this.pieChart)

    d3PieChart.selectAll('.nv-pieLabels .nv-label')
      .each(function(d,i) {
        let group = d3.select(this)
        const labelCtrl = group.select('text').text('')
        if (d.data.value / totalAmount < d.data.showLabelThreshold) return
        let offsetY = 0

        R.forEach(label => {
          labelCtrl.append('tspan')
            .text(label)
            .attr('x', 0)
            .attr('y', offsetY)
          offsetY += PIE_LABEL_LINE_HEIGHT
        }, R.split('  ', d.data.label))

        if (R.equals(d.data.type, 'complex') && d.data.items) {
          R.forEach(item => {
            labelCtrl
              .append('tspan')
              .text(`${item.label}: ${yTickFormat(item.value, context)}`)
              .attr('x', 0)
              .attr('y', offsetY)
            offsetY += PIE_LABEL_LINE_HEIGHT
          }, d.data.items)
        } else {
          labelCtrl.append('tspan')
            .text(yTickFormat(d.value, context))
            .attr('x', 0)
            .attr('y', offsetY + 2)
            .attr('class', classes.labelValue)
        }
      })

    // Draws retirement contribution section, reposition handles.
    d3PieChart.select('.nv-pie').selectAll('.nv-slice')
      .each(function(d, i) {
        if (R.equals(d.data.type, 'complex')) {
          // now is retirement contribution section,
          let group = d3.select(this)

          const radius = Math.min(
            pieChart.clientWidth,
            pieChart.clientHeight
          ) / 2
          const outerRadius = radius * 4 / 5
          const innerRadius = MV_DONUT_RATIO * radius
          const r1 = outerRadius - 10
          const r2 = outerRadius + 10

          const arc = d3.svg.arc().outerRadius(outerRadius)
          const startAngle = rotateAngle(d.startAngle)
          const endAngle = rotateAngle(d.endAngle)
          const partValue = d.data.items ? d.data.items[0].value : d.data.value
          const partEndAngle = rotateAngle(d.startAngle + (d.endAngle - d.startAngle) *
            partValue / (d.data.value || 1))

          // Draw pie if partner exists
          if (d.data.items) {
            arc.startAngle(startAngle)
            arc.endAngle(partEndAngle)

            arc.innerRadius(innerRadius)
            let path2 = group.select('path:nth-of-type(2)')
            if (path2.empty()) path2 = group.append('path')

            path2
              .attr('d', arc)
              .attr('fill', 'rgba(0,0,0,.1)')
          }

          const x1 = - Math.cos(endAngle - Math.PI * 1.5) * r1
          const y1 = - Math.sin(endAngle - Math.PI * 1.5) * r1

          // Reposition retirement contribution form
          if (rcFormDom) {
            rcFormDom.style.left = `${x1}px`
            rcFormDom.style.top = `${y1 - 20}px`
          }

          // Reposition handles
          if (handle1Dom) {
            handle1Dom.style.left = `${x1}px`
            handle1Dom.style.top = `${y1}px`

            const d3Handle1 = d3.select(handle1Dom)
            d3Handle1
              .style('visibility', 'visible')
              .select('svg')
              .style('transform', `rotate(${endAngle / Math.PI * 180}deg)`)
              .call(d3.behavior.drag()
                .on('drag', () => {
                  const { clientBtcValue, currentAngle, dragOffset } = that.state
                  const contributionsAmount = getContributionsAmount(props, that.state)
                  const offsetX = handle1Dom.offsetLeft - handle1Dom.clientWidth / 2 + d3.event.x
                  const offsetY = handle1Dom.offsetTop - handle1Dom.clientHeight / 2 + d3.event.y
                  if (!dragOffset) { // dragging just started, need to calculate offset
                    that.setState({
                      dragOffset: {
                        x: offsetX - handle1Dom.offsetLeft,
                        y: offsetY - handle1Dom.offsetTop,
                      }
                    })
                    return
                  }
                  const handleX = offsetX - dragOffset.x
                  const handleY = offsetY - dragOffset.y
                  const angle = angleInValidRange(
                    findAngle(handleX, handleY, currentAngle),
                    startAngle,
                    rotateAngle(Math.PI * 2)
                  )
                  const availableAngle = Math.PI * 2 - d.startAngle

                  const newContribAmount = toFixedNumber(
                    (angle - startAngle) / availableAngle * availableAmount
                  )
                  const newBtcValue = !hasPartner || (!clientBtcValue && !contributionsAmount)
                    ? newContribAmount
                    : toFixedNumber(newContribAmount * clientBtcValue / (contributionsAmount || 1))

                  that.setState({
                    currentAngle: angle,
                    clientBtcValue: newBtcValue,
                    partnerBtcValue: newContribAmount - newBtcValue
                  })
                })
                .on('dragstart', that.handleDragStart)
                .on('dragend', that.handleDragEnd)
              )
          }

          if (hasPartner && handle2Dom) {
            const x2 = - Math.cos(partEndAngle - Math.PI * 1.5) * r2
            const y2 = - Math.sin(partEndAngle - Math.PI * 1.5) * r2
            handle2Dom.style.left = `${x2}px`
            handle2Dom.style.top = `${y2}px`
            const d3Handle2 = d3.select(handle2Dom)
            d3Handle2
              .style('visibility', 'visible')
              .select('svg')
              .style('transform', `rotate(${partEndAngle / Math.PI * 180}deg)`)
              .call(d3.behavior.drag()
                .on('drag', () => {
                  const { currentAngle, dragOffset } = that.state
                  const contributionsAmount = getContributionsAmount(props, that.state)
                  const offsetX = handle2Dom.offsetLeft - handle2Dom.clientWidth / 2 + d3.event.x
                  const offsetY = handle2Dom.offsetTop - handle2Dom.clientHeight / 2 + d3.event.y
                  if (!dragOffset) { // dragging just started, need to calculate offset
                    that.setState({
                      dragOffset: {
                        x: offsetX - handle2Dom.offsetLeft,
                        y: offsetY - handle2Dom.offsetTop,
                      }
                    })
                    return
                  }
                  const handleX = offsetX - dragOffset.x
                  const handleY = offsetY - dragOffset.y
                  const angle = angleInValidRange(
                    findAngle(handleX, handleY, currentAngle),
                    startAngle,
                    endAngle
                  )
                  const availableAngle = d.endAngle - d.startAngle

                  const newBtcValue = toFixedNumber(
                    (angle - startAngle) / availableAngle * contributionsAmount
                  )

                  that.setState({
                    clientBtcValue: newBtcValue,
                    partnerBtcValue: contributionsAmount - newBtcValue
                  })
                })
                .on('dragstart', that.handleDragStart)
                .on('dragend', that.handleDragEnd)
              )
          }
        }
      })
    // Re-generate tooltip
    this.pieChart.tooltip
      .enabled(!this.state.dragging)
      .gravity('w')
      .contentGenerator(
        R.compose(
          ReactDOMServer.renderToStaticMarkup,
          d => tooltip(d, context)
        )
      )
  }

  renderChart() {
    const { props, state } = this
    const { client, fields: { clientBtc, partnerBtc }, hasPartner, rcFormVisible,
      values: { partnerName } } = props
    const wrapperClass = classNames(classes.pieChartWrapper, {
      [classes.dragging]: state.dragging
    })
    const hasRendered = hasLoaded(props)
    return (
      <div className={wrapperClass}>
        <svg ref='pieChart' />
        <div className={classes.handlesWrapper}>
          {hasRendered &&
            <span ref='handle1' className={classes.handle}
              onClick={this.handleToggleForm}>
              <BzSliceSlider size={20} />
            </span>}
          {hasRendered && hasPartner &&
            <span ref='handle2' className={classes.handlePartner}>
              <BzSliceSlider size={20} />
            </span>}
          <Fade in={!state.dragging && rcFormVisible} unmountOnExit>
            <div className={classes.rcForm} ref='rcForm'
              onClick={function (e) { e.stopPropagation() }}>
              <FormGroup>
                <ControlLabel>Retirement contribution:</ControlLabel>
                <FormControl.Static>
                  <FormattedNumber value={getContributionsAmount(props, state)}
                    format='currencyWithoutCents' />
                </FormControl.Static>
              </FormGroup>
              <FormGroup>
                <ControlLabel>{client && client.user.first_name}</ControlLabel>
                <CurrencyInput {...domOnlyProps(clientBtc)}
                  onChange={this.handleClientBtcChange} />
                <FieldError for={clientBtc} />
              </FormGroup>
              {hasPartner &&
                <FormGroup>
                  <ControlLabel>{partnerName}</ControlLabel>
                  <CurrencyInput {...domOnlyProps(partnerBtc)}
                    onChange={this.handlePartnerBtcChange} />
                  <FieldError for={partnerBtc} />
                </FormGroup>}
            </div>
          </Fade>
        </div>
      </div>
    )
  }

  renderLoading() {
    return (
      <div className={classes.spinnerWrapper}>
        <Spinner>Loading data...</Spinner>
      </div>
    )
  }

  render() {
    const { isLoading } = this.props
    return (
      <div className={classes.wrapper}>
        {!isLoading && this.renderChart()}
      </div>
    )
  }
}
