import React, { Component, PropTypes } from 'react'
import { Form, FormControl, FormGroup, InputGroup, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import R from 'ramda'
import { BzTrash } from 'icons'
import { propsChanged, round } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from './Constraint.scss'
import Col from 'components/Col'
import InlineList from 'components/InlineList'
import Panel from 'containers/Panel'
import Select from 'components/Select/Select'
import Slider from 'components/Slider/Slider'
import Text from 'components/Text'
import TogglePanelButton from 'components/TogglePanelButton'

const emptyText = '––'

// getFeatureOptions :: Props -> [Object]
const getFeatureOptions = R.compose(
  R.map(({ name, upper_limit, values }) => ({
    label: name,
    options: R.map(({ id, name }) => ({
      value: id,
      label: name,
      upperLimit: upper_limit
    }), values)
  })),
  R.reject(R.compose(
    R.isNil,
    R.prop('values'))
  ),
  R.prop('features')
)

// getFlattenFeatureOptions :: Props -> [Object]
const getFlattenFeatureOptions = R.compose(
  R.reduce(R.concat, []),
  R.map(R.prop('options')),
  getFeatureOptions
)

// getSelectedFeature :: Props -> Object
const getSelectedFeature = R.converge(R.find, [
  R.compose(
    R.propEq('value'),
    R.path(['constraint', 'feature', 'value'])
  ),
  getFlattenFeatureOptions
])

// getSelectedFeatureLabel :: Props -> String
const getSelectedFeatureLabel = R.compose(
  R.defaultTo(emptyText),
  R.prop('label'),
  R.defaultTo({}),
  getSelectedFeature
)

// getSelectedFeatureMax :: Props -> Number
const getSelectedFeatureMax = R.compose(
  R.defaultTo(1),
  R.prop('upperLimit'),
  R.defaultTo({}),
  getSelectedFeature
)

// getUpperLimit :: Props -> Number
const getUpperLimit = (props) => {
  const selectedComparison = getSelectedComparison(props)
  const selectedComparisonLabel = selectedComparison && selectedComparison.label
  return R.contains(selectedComparisonLabel, ['Maximum'])
    ? getSelectedFeatureMax(props)
    : 1
}

// getLowerLimit :: Props -> Number
const getLowerLimit = (props) => {
  const selectedComparison = getSelectedComparison(props)
  const selectedComparisonLabel = selectedComparison && selectedComparison.label
  return R.contains(selectedComparisonLabel, ['Minimum', 'Exactly'])
    ? 1 - getSelectedFeatureMax(props)
    : 0
}

// getComparisonOptions :: Props -> [Object]
const getComparisonOptions = R.compose(
  R.map(({ id, name }) => ({
    value: id,
    label: name
  })),
  R.prop('comparisons')
)

// getSelectedComparison :: Props -> Object
const getSelectedComparison = R.converge(R.find, [
  R.compose(
    R.propEq('value'),
    R.path(['constraint', 'comparison', 'value'])
  ),
  getComparisonOptions
])

// getSelectedComparisonLabel :: Props -> String
const getSelectedComparisonLabel = R.compose(
  R.defaultTo(emptyText),
  R.prop('label'),
  R.defaultTo({}),
  getSelectedComparison
)

// getConfiguredValue :: Props -> Number
const getConfiguredValue = R.path(['constraint', 'configured_val', 'value'])

// getHeader :: Props -> Node
const getHeader = (_props) => ({ isExpanded, toggle }) => { // eslint-disable-line react/prop-types
  const configuredValue = getConfiguredValue(_props)
  return (
    <Row onClick={toggle}>
      <Col xs={10}>
        <Text>
          {getSelectedComparisonLabel(_props)}{' '}
          {R.is(Number, configuredValue) &&
            <span>
              <FormattedNumber value={configuredValue} format='percent' />{' '}
            </span>}
          {getSelectedFeatureLabel(_props)}
        </Text>
      </Col>
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>
  )
}

export default class Constraint extends Component {
  static propTypes = {
    className: PropTypes.string,
    comparisons: PropTypes.array.isRequired,
    constraint: PropTypes.object.isRequired,
    constraintValue: PropTypes.object.isRequired,
    features: PropTypes.array.isRequired,
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired,
    isExpanded: PropTypes.bool,
    remove: PropTypes.func.isRequired
  }

  shouldComponentUpdate (nextProps) {
    // TODO: `constraintValue` is only used for triggering update on change. Once we migrate
    // to redux-form v6, this will probably not be needed.
    return propsChanged(['comparisons', 'constraint', 'constraintValue', 'features'],
      this.props, nextProps)
  }

  componentWillReceiveProps (nextProps) {
    const { constraint: { configured_val } } = nextProps
    this.maybeChangeComparisonValue(configured_val, nextProps)
  }

  handleSliderValueChange = (value) => {
    const event = {
      target: {
        value: value * 100
      }
    }
    this.handleComparisonInputChange(event)
  }

  handleComparisonInputChange = (event) => {
    const { onChange } = this.props.constraint.configured_val
    const finalValue = round(event.target.value / 100)
    this.maybeChangeComparisonValue({
      value: finalValue,
      onChange,
    },
    this.props,
    () => onChange(finalValue))
  }

  maybeChangeComparisonValue({ value, onChange }, props, whenIsValid = function () {}) {
    const lowerLimit = getLowerLimit(props)
    const upperLimit = getUpperLimit(props)
    const isBelowMin = (value < lowerLimit)
    const isAboveMax = (value > upperLimit)

    if (isBelowMin) {
      onChange(lowerLimit)
    } else if (isAboveMax) {
      onChange(upperLimit)
    } else {
      whenIsValid()
    }
  }

  render () {
    const { props } = this
    const { className, constraint: { comparison, configured_val, feature }, id, isExpanded,
      remove } = props
    const featureOptions = getFeatureOptions(props)
    const comparisonOptions = getComparisonOptions(props)
    const configuredValue = getConfiguredValue(props)
    const lowerLimit = getLowerLimit(props)
    const upperLimit = getUpperLimit(props)

    return (
      <Panel className={className} collapsible header={getHeader(props)} id={id}
        defaultIsExpanded={isExpanded}>
        {({ isExpanded }) => isExpanded && (
          <Form className={classes.form}>
            <InlineList className={classes.featureRow}>
              {feature &&
                <FormGroup className='constraint-select'>
                  <Select value={feature.value} onChange={feature.onChange}
                    placeholder='Select' searchable={false} clearable={false}
                    options={featureOptions} />
                </FormGroup>}
              <Button bsStyle='thick-outline' className={classes.btnDelete} onClick={remove}>
                <BzTrash size='20' />
              </Button>
            </InlineList>
            <Form inline componentClass='div'>
              {comparison &&
                <FormGroup className={classNames('constraint-select', classes.comparisonSelect)}>
                  <Select value={comparison.value} onChange={comparison.onChange}
                    placeholder='Select' searchable={false} clearable={false}
                    options={comparisonOptions} />
                </FormGroup>}
              {R.is(Number, configuredValue) && (
                <FormGroup className={classes.comparisonInputWrapper}>
                  <InputGroup>
                    <FormControl onChange={this.handleComparisonInputChange}
                      value={round(configuredValue * 100, 0)} type='number' step='1' min='0'
                      max='100' />
                    <InputGroup.Addon>%</InputGroup.Addon>
                  </InputGroup>
                </FormGroup>)}
            </Form>
            {configured_val &&
              <FormGroup className={classes.sliderWrapper}>
                <Slider labelMin='0%' labelMax='100%' min={0} max={1} step={0.01}
                  lowerLimit={lowerLimit} upperLimit={upperLimit}
                  value={configured_val.value} onChange={this.handleSliderValueChange} />
              </FormGroup>}
          </Form>)}
      </Panel>
    )
  }
}
