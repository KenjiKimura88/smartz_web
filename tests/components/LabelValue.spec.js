import React from 'react'
import { shallow } from 'enzyme'
import Label from 'components/Label'
import LabelValue from 'components/LabelValue/LabelValue'
import Value from 'components/Value'

describe('(Component) LabelValue', () => {
  const getComponent = (props) =>
    <LabelValue label='foo' value='bar' />

  it('renders as a <dl />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('dl')
  })

  it('renders <Label />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.containsMatchingElement(<Label>foo</Label>)).to.equal(true)
  })

  it('renders <Value />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.containsMatchingElement(<Value>bar</Value>)).to.equal(true)
  })
})
