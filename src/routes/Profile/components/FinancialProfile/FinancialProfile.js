import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, Form, FormControl, FormGroup } from 'react-bootstrap'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import Button from 'components/Button/Button'
import config from 'config'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import NetWorth from 'containers/NetWorth'
import Notification from 'containers/Notification'
import Select from 'components/Select'

const { onboarding: { EMPLOYMENT_STATUS } } = config

const financialFields = [
  'employment_status', 'employer_type', 'employer', 'income', 'industry_sector', 'occupation'
]

const shouldShowOccupationFields = (employmentStatus) => (
  R.contains(employmentStatus, [
    EMPLOYMENT_STATUS.EMPLOYED,
    EMPLOYMENT_STATUS.SELF_EMPLOYED
  ])
)

const getOptions = R.curry((attr, settings) =>
  R.compose(
    R.map(({ id, name }) => ({
      value: id,
      label: name
    })),
    R.defaultTo([]),
    R.path([attr])
  )(settings)
)

const employerTypesOptions = getOptions('employer_types')
const employmentStatusOptions = getOptions('employment_statuses')
const industryTypesOptions = getOptions('industry_types')
const occupationTypesOptions = getOptions('occupation_types')

export default class FinancialProfile extends Component {
  static propTypes = {
    errors: PropTypes.object.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    refreshProfile: PropTypes.func.isRequired,
    requests: PropTypes.object,
    settings: PropTypes.object,
    show: PropTypes.func,
    updateClient: PropTypes.func,
    user: PropTypes.object,
    values: PropTypes.object
  };

  showSecurityQuestionsModal = (values) => {
    const { show } = this.props
    show('securityQuestions', {
      onSubmit: this.save,
      operation: 'update your financial profile',
      submitButtonLabel: 'Update Financial Profile'
    })
  }

  save = (securityQuestions) => {
    const { updateClient, values } = this.props
    updateClient({
      body: {
        ...R.pick(financialFields, values),
        ...securityQuestions
      }
    })
  }

  render () {
    const { fields, errors, handleSubmit, requests: { updateClient }, settings } = this.props
    const hasErrors = !R.compose(R.equals(0), R.length, R.keys)(errors)

    return (
      <Form horizontal onSubmit={handleSubmit(this.showSecurityQuestionsModal)}>
        <Notification request={updateClient} successMessage='Financial Profile has been updated' />
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>
            Employment Status
          </Col>
          <Col xs={9}>
            <Select clearable={false} searchable={false}
              placeholder='Not Selected' options={employmentStatusOptions(settings)}
              {...fields.employment_status} />
          </Col>
        </FormGroup>
        {shouldShowOccupationFields(fields.employment_status.value) && <div>
          <FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Industry</Col>
            <Col xs={9}>
              <Select clearable={false} searchable={false}
                placeholder='Not Selected' options={industryTypesOptions(settings)}
                {...fields.industry_sector} />
              <FieldError for={fields.industry_sector} />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Occupation</Col>
            <Col xs={9}>
              <Select clearable={false} searchable={false}
                placeholder='Not Selected' options={occupationTypesOptions(settings)}
                {...fields.occupation} />
              <FieldError for={fields.occupation} />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Employer Type</Col>
            <Col xs={9}>
              <Select clearable={false} searchable={false}
                placeholder='Not Selected' options={employerTypesOptions(settings)}
                {...fields.employer_type} />
              <FieldError for={fields.employer_type} />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col xs={3} componentClass={ControlLabel}>Employer</Col>
            <Col xs={9}>
              <FormControl {...domOnlyProps(fields.employer)} />
              <FieldError for={fields.employer} />
            </Col>
          </FormGroup>
        </div>}
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Pretax Income</Col>
          <Col xs={9}>
            <CurrencyInput {...domOnlyProps(fields.income)} />
            <FieldError for={fields.income} />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={3} componentClass={ControlLabel}>Net Worth</Col>
          <Col xs={9}>
            <FormControl.Static>
              <NetWorth />
            </FormControl.Static>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col xs={9} xsOffset={3} className='text-right'>
            <Button type='submit' bsStyle='primary' disabled={hasErrors}>
              Update
            </Button>
          </Col>
        </FormGroup>
      </Form>
    )
  }
}
