import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { CONTACT_INFO_STEP_ID, DOCUMENTS_STEP_ID, RESIDENCE_STEP_ID } from '../helpers'
import { contactInfoSchema } from 'schemas/onboarding'
import { create } from 'redux/api/modules/requests'
import ContactInfo from '../components/ContactInfo'
import config from 'config'

const { ibEnabled } = config

const asyncValidate = (values, dispatch) => {
  return new Promise((resolve, reject) => {
    const rejected = () => reject({ phoneNumber: 'Phone Number is Invalid' })
    return dispatch(create({
      type: 'validatePhoneNumber',
      url: '/validate/phonenumber',
      body: {
        number: values.phoneNumber
      },
      success: () => resolve(),
      fail: rejected
    }))
  })
}

const defaultFromTaxTranscriptData = (getValue) => {
  const documents = getValue(['info', 'steps', DOCUMENTS_STEP_ID])
  const residence = getValue(['info', 'steps', RESIDENCE_STEP_ID])
  const address = R.path(['taxTranscriptData', 'address'], documents)
  return R.equals(residence.country, 'US') && address ? {
    address1: address.address1,
    address2: address.address2,
    city: address.city,
    state: address.state,
    zipCode: address.post_code
  } : {}
}

const defaultFromResidenceData = (getValue) => {
  const residence = getValue(['info', 'steps', RESIDENCE_STEP_ID])
  return R.compose(
    R.reject(R.isNil),
    R.pick(['state', 'country'])
  )(residence)
}

export default R.compose(
  reduxForm({
    form: 'onboardingContactInfo',
    ...contactInfoSchema(ibEnabled),
    asyncValidate,
    asyncBlurFields: ['phoneNumber'],
  }, (state, { getValue }) => ({
    initialValues: R.mergeAll([
      defaultFromTaxTranscriptData(getValue),
      defaultFromResidenceData(getValue),
      R.defaultTo({}, getValue(['info', 'steps', CONTACT_INFO_STEP_ID]))
    ])
  })),
  withRouter
)(ContactInfo)
