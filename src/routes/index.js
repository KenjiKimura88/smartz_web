import React from 'react'
import { Route, IndexRedirect, IndexRoute, Redirect } from 'react-router'
import AccountSettings from './AccountSettings'
import Activity from './Activity'
import Allocation from './Allocation'
import App from './App'
import Client from './Client'
import EmptyPage from './EmptyPage'
import FourOhFour from './FourOhFour/FourOhFour'
import NewAccountTypes from './NewAccountTypes'
import NewGoalComplete from './NewGoalComplete'
import NewGoalEmployerPlan from './NewGoalEmployerPlan'
import NewGoalForm from './NewGoalForm'
import NewGoalRolloverApprovals from './NewGoalRolloverApprovals'
import NewGoalRolloverForm from './NewGoalRolloverForm'
import NewGoalRolloverSignature from './NewGoalRolloverSignature'
import NewGoalRolloverTypes from './NewGoalRolloverTypes'
import NewGoalTypes from './NewGoalTypes'
import NewJointAccount from './NewJointAccount'
import NewTrustAccount from './NewTrustAccount'
import OnBoarding from './OnBoarding'
import onEnterWithInvitation from 'helpers/onEnterInvitation'
import Overview from './Overview'
import PassThrough from 'components/PassThrough'
import PassThroughFrame from 'components/PassThroughFrame'
import Performance from './Performance'
import Portfolio from './Portfolio'
import Profile from './Profile'
import redirectToClient from 'helpers/redirectToClient'
import redirectToSettings from 'helpers/redirectToSettings'
import redirectWhenNoAccounts from 'helpers/redirectWhenNoAccounts'
import requireConfirmedAccount from 'helpers/requireConfirmedAccount'
import RetiresmartzAgreement from './RetiresmartzAgreement'
import RetiresmartzPlan from './RetiresmartzPlan'
import RetiresmartzWelcome from './RetiresmartzWelcome'
import RetiresmartzWizard from './RetiresmartzWizard'
import RiskProfileWizard from './RiskProfileWizard'
import setupOnErrorHandler from 'helpers/setupOnErrorHandler'
import Spinner from 'components/Spinner'
import Transfer from './Transfer'

const profileRoute =
  <Route path='profile' component={Profile} />

const riskProfileRoute =
  <Route path='risk-profile' component={RiskProfileWizard}>
    <IndexRedirect to='0' />
    <Route path=':questionIndex' />
  </Route>

export default (props) =>
  <Route path='/' component={App} onEnter={setupOnErrorHandler(props)}>
    <IndexRoute component={Spinner} onEnter={redirectToClient(props)} />
    <Route path='error/404' component={FourOhFour} />
    <Redirect from='invitation/:invitationKey' to='onboarding/:invitationKey' />
    <Route path='onboarding/:invitationKey(/:step)(/:stepId)' component={OnBoarding}
      onEnter={onEnterWithInvitation(props)} />
    <Route path=':clientId' component={Client}>
      <IndexRedirect to='overview' />
      <Route path='account/:accountId/settings' component={AccountSettings}
        onEnter={requireConfirmedAccount(props)} />
      <Route path='activity' component={PassThrough}>
        <IndexRoute component={Activity} />
        <Route path='account/:accountId' component={Activity} />
        <Route path='goal/:goalId' component={Activity} />
      </Route>
      <Route path='allocation' component={PassThrough}>
        <IndexRoute for='allocation' component={EmptyPage} />
        <Route path=':goalId' component={PassThrough}>
          <IndexRoute onEnter={redirectToSettings({ defaultTo: 'new', ...props })} />
          <Route path=':viewedSettings' component={Allocation} />
        </Route>
      </Route>
      <Route path='overview' component={Overview} onEnter={redirectWhenNoAccounts(props)} />
      <Route path='performance' component={Performance} />
      <Route path='portfolio' component={PassThrough}>
        <IndexRoute for='portfolio' component={EmptyPage} />
        <Route path=':goalId' component={PassThrough}>
          <IndexRoute onEnter={redirectToSettings({ defaultTo: 'current', ...props })} />
          <Route path=':viewedSettings' component={Portfolio} />
        </Route>
      </Route>
      {profileRoute}
      {riskProfileRoute}
      <Route path='transfer' component={PassThrough}>
        <IndexRoute for='transfer' component={EmptyPage} />
        <Route path=':goalId' component={Transfer} />
      </Route>
      <Route path='accounts/new' component={PassThroughFrame}>
        <IndexRoute component={NewAccountTypes} />
        <Route path='joint' component={NewJointAccount} />
        <Route path='trust' component={NewTrustAccount} />
      </Route>
      <Route path='new-goal' component={PassThroughFrame}>
        <IndexRedirect to='account-type' />
        <Route path='account-type' component={NewAccountTypes} />
        <Route path='account/:accountId' component={NewGoalTypes} />
        <Route path='account/:accountId/:goalType' component={NewGoalForm} />
        <Route path='account/:accountId/complete/:goalId' component={NewGoalComplete} />
        <Route path='rollover-type' component={NewGoalRolloverTypes} />
        <Route path='employer-plan' component={NewGoalEmployerPlan} />
        <Route path='rollover/:rolloverType' component={NewGoalRolloverForm} />
        <Route path='rollover/:rolloverType/approvals' component={NewGoalRolloverApprovals} />
        <Route path='rollover/:rolloverType/signature' component={NewGoalRolloverSignature} />
      </Route>
      {profileRoute}
      <Route path='retiresmartz' component={PassThrough}>
        <IndexRedirect to='welcome' />
        <Route path='welcome' component={RetiresmartzWelcome} />
        <Route path='wizard' component={RetiresmartzWizard}>
          <Route path=':id' />
        </Route>
        <Route path=':retirementPlanId' component={PassThrough}>
          <IndexRedirect to='wizard' />
          <Route path='agreement' component={RetiresmartzAgreement} />
          <Route path='plan' component={RetiresmartzPlan} />
          <Route path='wizard' component={RetiresmartzWizard}>
            <Route path=':id' />
          </Route>
        </Route>
      </Route>
      {riskProfileRoute}
    </Route>
    <Redirect from='*' to='error/404' />
  </Route>
