import React, { Component, PropTypes } from 'react'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import P from 'components/P'
import schema from 'schemas/cashManagement'
import SelectPlaidAccount from 'containers/SelectPlaidAccount'
import Switch from 'components/Switch'
import Text from 'components/Text'
import Tip from 'components/Tip'

const description = (goalName) => (
  <div>
    <P>
      <Text bold>
        Invest excess cash held in your bank account into you {goalName} automatically.
      </Text>
    </P>
    <P>
      We will monitor your bank balance and calculate an amount to invest. If we see that
      your bank balance is above the maximum you've set, we'll automatically invest the
      excess cash.
    </P>
  </div>
)

export class CashManagementModal extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    goal: PropTypes.object,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  save = () => {
    window.alert('Feature not activated yet.')
  }

  render () {
    const { fields, goal, handleHide, handleSubmit, show } = this.props
    const { cashManagementEnabled } = fields
    const isEnabled = cashManagementEnabled.value

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header>
          <Modal.Title>
            Cash Management Deposits
            <div className='pull-right'>
              <Switch {...cashManagementEnabled} />
            </div>
          </Modal.Title>
        </Modal.Header>
        <Form horizontal onSubmit={handleSubmit(this.save)}>
          <Modal.Body>
            {isEnabled
              ? <div>
                {description(goal.name)}
                <P>
                  Start by telling us the maximum balance you want in your bank account:
                </P>
                <FormGroup>
                  <Col componentClass={ControlLabel} xs={5}>Maximum bank account balance:</Col>
                  <Col xs={7}>
                    <CurrencyInput {...fields.maximumAccountBalance} />
                    <FieldError for={fields.maximumAccountBalance} />
                  </Col>
                </FormGroup>
                <P>
                  Set the maximum amount of money we invest each time we check your bank balance.
                  We'll never invest more than you want us to:
                </P>
                <FormGroup>
                  <Col componentClass={ControlLabel} xs={5}>Maximum deposit amount:</Col>
                  <Col xs={7}>
                    <CurrencyInput {...fields.maximumDeposit} />
                    <FieldError for={fields.maximumDeposit} />
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} xs={5}>To:</Col>
                  <Col xs={7}>
                    <FormControl.Static>
                      {goal.name}
                    </FormControl.Static>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Col componentClass={ControlLabel} xs={5}>From:</Col>
                  <Col xs={7}>
                    <SelectPlaidAccount value={fields.from.value} onChange={fields.from.onChange}
                      placeholder='Select bank account' />
                    <FieldError for={fields.from} />
                  </Col>
                </FormGroup>
                <Text className='text-center' bold>
                  By clicking "Confirm", you grant us permission to periodically view your bank
                  balance and determine if you have excess cash.
                </Text>
              </div>
              : <div>
                Cash management deposits is currently off.
                <Tip arrow>
                  {description(goal.name)}
                  <P>
                    <a onClick={() => cashManagementEnabled.onChange(true)}>
                      Enable cash management deposits
                    </a>
                  </P>
                </Tip>
              </div>}
          </Modal.Body>
          <Modal.Footer>
            <Button type='submit' bsStyle='primary'>
              Confirm
            </Button>
            <Button onClick={handleHide}>
              Cancel
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'cashManagement' }),
  reduxForm({
    form: 'cashManagement',
    ...schema
  }, () => ({
    initialValues: {
      cashManagementEnabled: false,
      maximumAccountBalance: 1000,
      maximumDeposit: 0
    }
  }))
)(CashManagementModal)
