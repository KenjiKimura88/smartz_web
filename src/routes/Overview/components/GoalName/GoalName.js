import React, { Component, PropTypes } from 'react'
import { MdEdit } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './GoalName.scss'
import InlineEdit from 'components/InlineEdit'

export default class GoalName extends Component {
  static propTypes = {
    name: PropTypes.string,
    isEditing: PropTypes.bool,
    onChange: PropTypes.func,
    onEnd: PropTypes.func,
    setIsEditing: PropTypes.func
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['name', 'isEditing'], this.props, nextProps)
  }

  handleOnCancel = () => {
    const { setIsEditing } = this.props
    setIsEditing(false)
  }

  handleOnEnd = (value) => {
    const { onEnd, setIsEditing } = this.props
    onEnd(value)
    setIsEditing(false)
  }

  render () {
    const { name, isEditing, onChange, setIsEditing } = this.props
    return (
      <InlineEdit value={name} isEditing={isEditing} onChange={onChange}
        onCancel={this.handleOnCancel} onEnd={this.handleOnEnd}>
        <span className={classes.text} onClick={function () { setIsEditing(true) }}>
          {name}
          <MdEdit className={classes.editIcon} size={16} />
        </span>
      </InlineEdit>
    )
  }
}
