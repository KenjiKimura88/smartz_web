import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import classes from './AccountVerifiedModal.scss'
import H2 from 'components/H2'
import P from 'components/P'

class AccountVerifiedModal extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    handleHide: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  handleOk = () => {
    const { clientId, replace } = this.props
    replace(`/${clientId}`)
  }

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show} onHide={handleHide} className={classes.wrapper}>
        <Modal.Body className='text-center'>
          <H2>Thank you</H2>
          <P>
            Your trust has been verified and a trust account has been created.<br />
            We have notified your advisor.<br />
            You can now proceed with the trust's goal creation.
          </P>
          <div>
            <Button bsStyle='primary' onClick={this.handleOk}>
              OK
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'trustAccountVerifiedModal' })(AccountVerifiedModal)
