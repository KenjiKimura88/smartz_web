import React from 'react'
import { mount, shallow } from 'enzyme'
import { Row } from 'react-bootstrap'
import { ActivityHeader } from 'routes/Activity/components/ActivityHeader/ActivityHeader'
import GoalNavigation from 'components/GoalNavigation'
import SelectActivityType from 'routes/Activity/components/SelectActivityType'
import SelectDateRange from 'routes/Activity/components/SelectDateRange'

describe('(Route) Activity', () => {
  describe('(Component) ActivityHeader', () => {
    const params = {
      accountId: '33',
      clientId: '2'
    }
    const activityTypes = [
      {
        id: 1,
        name: 'Foo'
      },
      {
        id: 2,
        name: 'Bar'
      }
    ]
    const goals = [
      {
        id: 1,
        name: 'AAA',
        state: 0
      },
      {
        id: 41,
        name: 'BBB',
        state: 0
      }
    ]
    const accounts = [
      {
        id: 33,
        account_name: 'The Account',
        goals
      }
    ]
    const activeType = activityTypes[0]

    const getComponent = (props) =>
      <ActivityHeader router={{ params, push: function () {} }} activeType={activeType}
        activityTypes={activityTypes} accounts={accounts} goals={goals}
        set={function () {}} {...props} />

    it('renders as a <Row />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Row)
    })

    it('passes the right props to <GoalNavigation />', () => {
      let wrapper = shallow(getComponent())
      expect(wrapper.find(GoalNavigation).prop('accounts')).to.equal(accounts)
      expect(wrapper.find(GoalNavigation).prop('selectedItem'))
        .to.deep.equal({ type: 'account', id: '33' })

      wrapper = shallow(getComponent({
        router: {
          params: {
            clientId: '2',
            goalId: '41'
          }
        }
      }))
      expect(wrapper.find(GoalNavigation).prop('selectedItem'))
        .to.deep.equal({ id: '41', type: 'goal' })
    })

    it('renders <SelectActivityType />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(SelectActivityType)).to.have.length(1)
    })

    it('renders <SelectDateRange />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.find(SelectDateRange)).to.have.length(1)
    })

    it(`navigates to All Accounts on selecting first item`, () => {
      const push = sinon.spy()
      const wrapper = mount(getComponent({ router: { params, push } }))
      const { clientId } = params
      wrapper.find('.dropdown-menu li a').first().simulate('click')
      push.should.have.been.called
      push.should.have.been.calledWith(`/${clientId}/activity`)
    })

    xit('updates on activeDateRange', () => {

    })

    xit('updates on activeType', () => {

    })

    xit('updates on activityTypes', () => {

    })

    xit('updates on allGoals', () => {

    })

    xit('updates on goals', () => {

    })

    xit('updates on params', () => {

    })
  })
})
