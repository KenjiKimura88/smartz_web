import React, { Component, PropTypes } from 'react'
import { OverlayTrigger } from 'react-bootstrap'
import { MdInfoOutline } from 'helpers/icons'
import classes from './OverlayTooltip.scss'
import Tooltip from 'components/Tooltip'

const getTooltip = ({ children, className, id }) => // eslint-disable-line react/prop-types
  <Tooltip id={id}>
    {children}
  </Tooltip>

const defaultTrigger =
  <span className={classes.triggerIcon}>
    <MdInfoOutline size={20} />
  </span>

export default class OverlayTooltip extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    id: PropTypes.string.isRequired,
    trigger: PropTypes.node
  };

  static defaultProps = {
    trigger: defaultTrigger
  };

  render () {
    const { children, id, trigger, ...otherProps } = this.props

    return (
      <OverlayTrigger {...otherProps} overlay={getTooltip({ children, id })}>
        {trigger}
      </OverlayTrigger>
    )
  }
}
