import R from 'ramda'

export default (props, option, fallback) => {
  const { accounts, params: { clientId }, router: { push } } = props
  const account = R.find(R.propEq('account_type', option.value), accounts)

  account
  ? push(`/${clientId}/new-goal/account/${account.id}`)
  : fallback && fallback()
}
