import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import SecurityQuestionsForm, { serializeSecurityQuestions } from 'containers/SecurityQuestionsForm'
import schema from 'schemas/securityQuestions'
import Text from 'components/Text'

export class SecurityQuestionsModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    operation: PropTypes.string.isRequired,
    securityQuestionsForm: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired,
    submitButtonLabel: PropTypes.string.isRequired,
  };

  handleClick = () => {
    const { handleHide, onSubmit, securityQuestionsForm } = this.props
    if (securityQuestionsForm.valid) {
      handleHide()
      onSubmit(serializeSecurityQuestions(securityQuestionsForm))
    }
  }

  render () {
    const { handleHide, operation, securityQuestionsForm: { handleSubmit }, show,
      submitButtonLabel } = this.props

    return (
      <Modal show={show} onHide={handleHide} backdrop='static' keyboard={false}>
        <Modal.Header closeButton>
          <Modal.Title>
            Answer Security Questions
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Text bold>Please answer the following security questions to {operation}.</Text>
          <br />
          <SecurityQuestionsForm />
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={handleSubmit(this.handleClick)}>
            {submitButtonLabel}
          </Button>
          <Button onClick={handleHide}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  reduxForm({
    form: 'securityQuestions',
    propNamespace: 'securityQuestionsForm',
    ...schema
  }),
  connectModal({ name: 'securityQuestions' })
)(SecurityQuestionsModal)
