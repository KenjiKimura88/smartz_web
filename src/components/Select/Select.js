import React, { Component } from 'react'
import 'react-select-plus/dist/react-select-plus.css'
import classes from './Select.scss'
import ReactSelect from 'react-select-plus'

export default class Select extends Component {
  render () {
    return <ReactSelect className={classes.select} {...this.props} />
  }
}
