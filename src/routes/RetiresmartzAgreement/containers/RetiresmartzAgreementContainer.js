import { reduxForm } from 'redux-form'
import { hide, show } from 'redux-modal'
import R from 'ramda'
import { connect } from 'redux/api'
import { getAccounts, getClient } from 'helpers/requests'
import { initialize } from 'routes/RetiresmartzWizard/helpers'
import RetiresmartzAgreement from '../components/RetiresmartzAgreement'

const requests = ({ params: { clientId, retirementPlanId } }) => ({
  accounts: getAccounts(clientId),
  calculatedData: ({ findSingle }) => findSingle({
    type: 'retirementPlanCalculate',
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}/calculated-data`,
    propKey: 'calculatedData'
  }),
  client: getClient(clientId),
  incomes: ({ findAll }) => findAll({
    type: 'retirementIncomes',
    url: `/clients/${clientId}/retirement-incomes`
  }),
  retirementPlan: retirementPlanId && (({ findOne }) => findOne({
    type: 'retirementPlans',
    id: retirementPlanId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
  })),
  retirementPlans: ({ findAll }) => findAll({
    type: 'retirementPlans',
    url: `/clients/${clientId}/retirement-plans`
  }),
  settings: (({ findSingle }) => findSingle({
    type: 'globalSettings',
    url: '/settings'
  })),
  updateRetirementPlan: ({ update }) => update({
    type: 'retirementPlans',
    id: retirementPlanId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
  })
})

const actions = {
  hide,
  show
}

export default R.compose(
  connect(requests, null, actions),
  reduxForm({
    form: 'retirementAgreement',
    destroyOnUnmount: false,
    fields: ['accept']
  }, (state, props) => ({
    initialValues: initialize(props)
  }))
)(RetiresmartzAgreement)
