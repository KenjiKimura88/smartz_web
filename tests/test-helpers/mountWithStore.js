import { PropTypes } from 'react'
import mountWithIntl from './mountWithIntl'

export default function mountWithStore (node, store) {
  return mountWithIntl(node, {
    context: {
      store
    },
    childContextTypes: {
      store: PropTypes.object.isRequired
    }
  })
}
