/* eslint-disable max-len */
import config from 'config'
import { BzJoinedProfile, BzRolloverIra, BzRolloverProfile, BzSingleProfile, BzTrustProfile } from 'icons'
const { accountTypes } = config

const accountTypesOptions = (isRetiresmartzEnabled) => [
  {
    accountName: 'Individual Account',
    title: 'Individual',
    icon: BzSingleProfile,
    text: 'A personal investing account for use with any goal',
    value: accountTypes.ACCOUNT_TYPE_PERSONAL
  },
  {
    accountName: 'IRA',
    title: 'IRA',
    icon: BzRolloverIra,
    text: 'Includes Traditional, Roth, and SEP IRA',
    value: 'ira',
    disabled: !isRetiresmartzEnabled
  },
  {
    accountName: 'Trust Account',
    title: 'Trust Account',
    icon: BzTrustProfile,
    text: 'An account in the name of an existing trust',
    value: accountTypes.ACCOUNT_TYPE_TRUST
  },
  {
    accountName: 'Joint Account',
    title: 'Joint Account',
    icon: BzJoinedProfile,
    text: 'A jointly managed investing account for use with any goal',
    value: accountTypes.ACCOUNT_TYPE_JOINT
  },
  {
    accountName: 'Rollover',
    title: 'Rollover',
    icon: BzRolloverProfile,
    text: 'Transfer an existing IRA or roll over an emplyer sponsored plan including a 401(k)',
    value: 'rollover',
    disabled: !isRetiresmartzEnabled
  }
]

export default accountTypesOptions
