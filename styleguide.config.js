const glob = require('glob')
const path = require('path')
const webpack = require('webpack')
const config = require('./config')
const webpackConfig = require('./build/webpack.config')

const BASE_CSS_LOADER = 'css?sourceMap&-minimize'

const cssModulesLoader = [
  BASE_CSS_LOADER,
  'modules',
  'importLoaders=1',
  'localIdentName=[name]__[local]___[hash:base64:5]'
].join('&')

module.exports = {
  title: 'Betasmartz Style Guide',
  serverPort: 3001,
  components() {
    return glob.sync(path.resolve(__dirname, 'src/components/**/*.js')).filter(function(module) {
      return /\/[A-Z]\w*\.js$/.test(module)
    })
  },
  updateWebpackConfig(defaultConfig) {
    const dir = path.resolve(__dirname, 'src')

    defaultConfig.resolve = {
      root: [defaultConfig.resolve.root, webpackConfig.resolve.root],
      extensions: ['', '.js', '.jsx', '.json']
    }

    defaultConfig.plugins.push(
      new webpack.DefinePlugin(config.globals)
    )

    defaultConfig.sassLoader = webpackConfig.sassLoader
    defaultConfig.postcss = webpackConfig.postcss

    defaultConfig.module.loaders.push(
      {
        test: /\.jsx?$/,
        include: dir,
        loader: 'babel',
      },
      {
        test: /\.css$/,
        include: dir,
        loaders: [
          'style',
          cssModulesLoader,
          'postcss'
        ]
      },
      {
        test: /\.scss$/,
        include: dir,
        loaders: [
          'style',
          cssModulesLoader,
          'postcss',
          'sass?sourceMap'
        ]
      },
      {
        test: /\.scss$/,
        exclude: [dir, /highlight/, /react-styleguidist/, /codemirror/],
        loaders: [
          'style',
          BASE_CSS_LOADER,
          'postcss',
          'sass?sourceMap'
        ]
      },
      {
        test: /\.css$/,
        exclude: [dir, /highlight/, /react-styleguidist/, /codemirror/],
        loaders: [
          'style',
          BASE_CSS_LOADER,
          'postcss'
        ]
      },
      {
        test: /\.woff(\?.*)?$/,
        include: /./,
        loader: 'url?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.woff2(\?.*)?$/,
        include: /./,
        loader: 'url?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff2'
      },
      {
        test: /\.otf(\?.*)?$/,
        include: /./,
        loader: 'file?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=font/opentype'
      },
      {
        test: /\.ttf(\?.*)?$/,
        include: /./,
        loader: 'url?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?.*)?$/,
        include: /./,
        loader: 'file?prefix=fonts/&name=[path][name].[ext]'
      },
      {
        test: /\.svg(\?.*)?$/,
        include: /./,
        loader: 'url?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.(png|jpg)$/,
        include: /./,
        loader: 'url?limit=8192'
      }
    )

    return defaultConfig
  }
}
