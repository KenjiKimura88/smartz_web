import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { LinkContainer } from 'react-router-bootstrap'
import R from 'ramda'

export class NewOrViewExistingRetirementPlan extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    handleHide: PropTypes.func.isRequired,
    retirementPlanId: PropTypes.number.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { clientId, handleHide, retirementPlanId, show } = this.props

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>Confirm next action</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          You have an agreed retirement plan in place.
          Continue to view your existing retirement plan or create a new plan.
        </Modal.Body>
        <Modal.Footer>
          <LinkContainer to={`/${clientId}/retiresmartz/${retirementPlanId}/agreement`}>
            <Button bsStyle='primary'>
              View your existing retirement plan
            </Button>
          </LinkContainer>
          <LinkContainer to={`/${clientId}/retiresmartz`}>
            <Button onClick={handleHide}>
              Create a new plan
            </Button>
          </LinkContainer>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'newOrViewExistingRetirementPlan' })
)(NewOrViewExistingRetirementPlan)
