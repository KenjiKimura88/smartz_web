import { Children, Component, PropTypes } from 'react'

export default class ProviderMock extends Component {
  static propTypes = {
    children: PropTypes.object,
    store: PropTypes.object
  };

  static childContextTypes = {
    store: PropTypes.object.isRequired
  };

  getChildContext () {
    return { store: this.props.store }
  }

  render () {
    return Children.only(this.props.children)
  }
}
