import React, { Component, PropTypes } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { connectModal } from 'redux-modal'

class TaxEfficiencyModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show} animation={false} bsSize='large' onHide={handleHide}
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>Tax Efficiency</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            In a low interest rate environment every bit of return counts. One way to enhance
            returns is to manage tax consequences more efficiently. This may increase your post-tax
            returns (the return you effectively receive in your bank account).
          </p>
          <p>
            BetaSmartz constructs portfolios from the ground up to be maximally tax efficient.
            This begins with asset selection.
          </p>
          <p>
            The tax impact of BetaSmartz portfolios is also made more efficient through a number of
            automated day-to-day management features:
          </p>
          <ul>
            <li>
              Cash-flows are used to reduce the portfolio drift, which reduces future taxable
              rebalances. For example, a deposit from a dividend or automatic transfer will always
              buy the most underweight asset classes first. Similarly, a withdrawal will sell the
              most overweight assets first.
            </li>
            <li>
              Tax lot selection is optimized to reduce taxes. Lots with losses are always sold
              before lots with gains. Short term losses are preferred over long term losses, and
              long term gains are preferred over short term gains. Additionally, lots with the
              highest cost basis, and therefore the lowest embedded potential tax burden, are sold
              first.
            </li>
            <li>
              Rebalancing transactions and portfolio updates are tax aware, and always avoid
              short-term gains which are typically taxed at the highest rates.
            </li>
          </ul>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={handleHide}>OK</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default connectModal({ name: 'taxEfficiency' })(TaxEfficiencyModal)
