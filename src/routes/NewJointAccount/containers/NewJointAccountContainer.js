import { reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import NewJointAccount from '../components/NewJointAccount'
import schema from 'schemas/createJointAccount'

const requests = () => ({
  addAccount: ({ create }) => create({
    type: 'accounts',
    url: `/accounts/joint`
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter,
  reduxForm({
    form: 'createJointAccount',
    ...schema
  })
)(NewJointAccount)
