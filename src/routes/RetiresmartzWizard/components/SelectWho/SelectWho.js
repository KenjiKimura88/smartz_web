import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { isMarried } from '../../helpers'
import Select from 'components/Select'

// getClientName :: Props -> String
const getClientName = R.compose(
  (user) => `${user.first_name} ${user.last_name}`,
  R.defaultTo({}),
  R.prop('user')
)

// clientOption :: Props -> { label: String, value: String }
const clientOption = R.compose(
  R.assoc('label', R.__, { value: 'self' }),
  getClientName
)

// partnerOption :: Props -> { label: String, value: String }
const partnerOption = R.compose(
  R.assoc('label', R.__, { value: 'partner' }),
  R.defaultTo('Your spouse'),
  R.when(R.isEmpty, R.always(null)),
  R.path(['partnerName', 'value'])
)

// getOptions :: Props -> [{ label: String, value: String }]
const getOptions = R.ifElse(
  R.compose(
    isMarried,
    R.path(['maritalStatus', 'value'])
  ),
  R.converge(R.unapply(R.identity), [
    clientOption,
    partnerOption,
    R.always({ label: 'Joint', value: 'joint' })
  ]),
  R.compose(
    R.of,
    clientOption
  )
)

export default class SelectWho extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    user: PropTypes.object
  };

  render () {
    const { props } = this
    const { field } = props
    const options = getOptions(props)

    return (
      <Select placeholder='Select a user' options={options} onChange={field.onChange}
        value={field.value} />
    )
  }
}
