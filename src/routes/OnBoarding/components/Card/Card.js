import React, { PropTypes, Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './Card.scss'
import Spinner from 'components/Spinner'

export default class Card extends Component {
  static propTypes = {
    bgImage: PropTypes.string,
    children: PropTypes.node.isRequired,
    isLoading: PropTypes.bool,
    sidebar: PropTypes.node,
    sidebarStyle: PropTypes.oneOf([
      'white', 'grey'
    ]),
    sidebarBgImage: PropTypes.string
  };

  static defaultProps = {
    isLoading: false,
    sidebarStyle: 'grey'
  };

  render() {
    const { bgImage, children, isLoading, sidebar, sidebarBgImage, sidebarStyle } = this.props
    const contentCols = sidebar ? 8 : 12
    const sidebarClass = classNames({
      [classes.sidebar]: true,
      [classes[sidebarStyle]]: true
    })
    const cssStyle = bgImage ? { backgroundImage: `url(${bgImage})` } : {}
    const sidebarCssStyle = sidebarBgImage ? { backgroundImage: `url(${sidebarBgImage})` } : {}
    return (
      <Grid fluid className={classes.container}>
        <Row className={classes.row}>
          {sidebar && <Col xs={4} className={sidebarClass}>
            <div className={classes.sidebarInner} style={sidebarCssStyle}>{sidebar}</div>
          </Col>}
          <Col xs={contentCols} className={classes.detail}>
            <div className={classes.detailInner} style={cssStyle}>
              {children}
              {isLoading &&
                <div className={classes.loadingWrapper}>
                  <div className={classes.loadingInner}>
                    <Spinner />
                  </div>
                </div>
              }
            </div>
          </Col>
        </Row>
      </Grid>
    )
  }
}
