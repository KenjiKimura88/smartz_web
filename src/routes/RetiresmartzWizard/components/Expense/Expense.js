import React, { Component, PropTypes } from 'react'
import { Col, FormControl, FormGroup, Row } from 'react-bootstrap'
import { BzTrash } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from './Expense.scss'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import Select from 'components/Select/Select'
import SelectWho from '../SelectWho'

export default class Expense extends Component {
  static propTypes = {
    categoryOptions: PropTypes.array.isRequired,
    expense: PropTypes.object.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired,
    user: PropTypes.object
  };

  render () {
    const { props } = this
    const { categoryOptions, expense: { desc, cat, who, amt }, maritalStatus, partnerName, remove,
      user } = props

    return (
      <Row>
        <Col xs={3}>
          <FormGroup className={classes.formGroup}>
            <FormControl placeholder='Something about this' {...domOnlyProps(desc)} />
            <FieldError for={desc} />
          </FormGroup>
        </Col>
        <Col xs={3}>
          <FormGroup className={classes.formGroup}>
            <div>
              <Select placeholder='Select a category' options={categoryOptions}
                onChange={cat.onChange} value={cat.value} />
            </div>
            <FieldError for={cat} />
          </FormGroup>
        </Col>
        <Col xs={3}>
          <FormGroup className={classes.formGroup}>
            <SelectWho field={who} maritalStatus={maritalStatus} partnerName={partnerName}
              user={user} />
            <FieldError for={who} />
          </FormGroup>
        </Col>
        <Col xs={2}>
          <FormGroup className={classes.formGroup}>
            <CurrencyInput {...domOnlyProps(amt)} />
            <FieldError for={amt} />
          </FormGroup>
        </Col>
        <Col xs={1}>
          <div>
            <Button onClick={remove} style={{ padding: '4px 8px 6px 8px' }}>
              <BzTrash size={21} />
            </Button>
          </div>
        </Col>
      </Row>
    )
  }
}
