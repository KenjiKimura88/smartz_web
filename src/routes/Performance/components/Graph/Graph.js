import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, Row } from 'react-bootstrap'
import d3 from 'd3'
import DateTime from 'react-datetime'
import moment from 'moment'
import nv from 'nvd3'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import { allStartDate, ranges } from '../../helpers'
import classes from './Graph.scss'
import Selector from '../../components/Selector'

const xAxisTickFormat = d => d3.time.format('%b %d, %Y')(new Date(d))

const yAxisTickFormat = {
  returns: d => Math.round(parseFloat(d) * 100) + '%',
  values: d => '$ ' + Math.round(parseFloat(d))
}

const x2AxisTickFormat = d => d3.time.format('%b %Y')(new Date(d))

export default class Graph extends Component {
  static propTypes = {
    activeRangeKey: PropTypes.string,
    datum: PropTypes.array,
    type: PropTypes.string,
    setActiveRange: PropTypes.func,
    startDate: PropTypes.number,
    endDate: PropTypes.number,
    set: PropTypes.func.isRequired
  }

  componentDidMount () {
    const { datum, set } = this.props
    !R.isEmpty(datum) && nv.addGraph(this.renderGraph)
    set({
      startDate: allStartDate,
      endDate: moment().endOf('day').valueOf()
    })
  }

  componentDidUpdate () {
    const { datum } = this.props
    !R.isEmpty(datum) && this.renderGraph()
  }

  shouldComponentUpdate(nextProps) {
    return propsChanged([
      'datum', 'startDate', 'endDate', 'type', 'activeRangeKey'
    ], nextProps, this.props)
  }

  componentWillUnmount () {
    this.removeTooltips()
  }

  removeTooltips () {
    // Remove ghost tooltips
    d3.selectAll('.nvtooltip').remove()
  }

  handleActiveRangeChange = (key) => {
    const { datum, setActiveRange, set } = this.props
    const allDatum = datum
    if (R.equals(key, 'all')) {
      set({
        startDate: allStartDate,
        endDate: moment().endOf('day').valueOf()
      })
    } else if (!R.equals(key, 'custom')) {
      set({
        startDate: ranges[key].value(moment(), allDatum).valueOf(),
        endDate: moment().endOf('day').valueOf()
      })
    }
    setActiveRange(key)
  }

  renderGraph = () => {
    const { type, datum } = this.props
    const startX = R.compose(
      R.reduce(R.min, Infinity),
      R.map(R.compose(R.prop('x'), R.defaultTo({}), R.head, R.prop('values')))
    )(datum)
    const endX = R.compose(
      R.reduce(R.max, -Infinity),
      R.map(R.compose(R.prop('x'), R.defaultTo({}), R.last, R.prop('values')))
    )(datum)

    // We try to reuse the current chart instance.
    // If not possible then lets instantiate again
    if (!this.chart || this.rendering) {
      this.chart = nv.models.lineWithFocusChart()
        // .interpolate('cardinal')
        .margin({ left: 30, right: 50 })
        .showLegend(false)
        .duration(350)
        .rightAlignYAxis(true)
        .useInteractiveGuideline(true)
        .focusHeight(90)
    }

    this.chart.yAxis.tickFormat(yAxisTickFormat[type])
    this.chart.xAxis.tickFormat(xAxisTickFormat)
    this.chart.x2Axis.tickFormat(x2AxisTickFormat)
    this.chart.brushExtent([startX, endX])
    // Render chart using d3
    this.selection = d3.select(this.refs.svg).datum(datum).call(this.chart)

    this.chart.dispatch.on('renderEnd', this.renderEnd)
    this.rendering = true
    return this.chart
  }

  renderEnd = (e) => {
    // Once renders end then we set rendering to false to allow to
    // reuse the chart instance.
    this.rendering = false
  }

  render () {
    const { activeRangeKey, startDate, endDate, set } = this.props
    const validStartDate = (current) => current.isSameOrBefore(moment())
    const validEndDate = (current) =>
      current.isSameOrBefore(moment()) && current.isSameOrAfter(startDate)
    return (
      <div className={classes.graph}>
        <Row>
          <Col xs={6}>
            <Selector activeOptionKey={activeRangeKey} options={ranges}
              setActiveOption={this.handleActiveRangeChange} />
          </Col>
          {R.equals(activeRangeKey, 'custom') &&
            <Col xs={6}>
              <Row className='form-horizontal'>
                <Col xs={6}>
                  <Row>
                    <Col componentClass={ControlLabel} xs={3}>
                      From:
                    </Col>
                    <Col xs={9}>
                      <DateTime isValidDate={validStartDate}
                        value={startDate}
                        timeFormat={false}
                        onChange={function (value) {
                          set({ startDate: value.valueOf() })
                        }} />
                    </Col>
                  </Row>
                </Col>
                <Col xs={6}>
                  <Row>
                    <Col componentClass={ControlLabel} xs={3}>
                      To:
                    </Col>
                    <Col xs={9}>
                      <DateTime isValidDate={validEndDate}
                        value={endDate}
                        timeFormat={false}
                        onChange={function (value) {
                          set({ endDate: value.valueOf() })
                        }} />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          }
        </Row>
        <svg ref='svg' />
      </div>
    )
  }
}
