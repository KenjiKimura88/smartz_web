import R from 'ramda'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import { connect } from 'redux/api'
import { findOneSelector } from 'redux/api/selectors'
import { findSingle } from 'redux/api/modules/requests'
import { getClient } from 'helpers/requests'
import { show } from 'redux-modal'
import RiskProfileWizard from '../components/RiskProfileWizard'

const findQuestions = (riskProfileGroups, client) => R.compose(
  R.defaultTo({}),
  R.find(R.propEq('id', R.path(['risk_profile_group'], client))),
  R.defaultTo([])
)(riskProfileGroups).questions

const getAnswers = ({ questions, client }) => {
  const responses = R.compose(
    R.defaultTo([]),
    R.path(['risk_profile_responses'])
  )(client)
  return questions
    ? R.map((question) => {
      const answerValues = R.map(answer => answer.id, question.answers)
      const realAnswers = R.intersection(answerValues, responses)
      return !R.isEmpty(realAnswers) ? realAnswers[0] : null
    }, questions)
    : []
}

const actions = {
  refreshProfile: () => findSingle({
    type: 'me',
    force: true
  }),
  show
}

const selector = (state, { riskProfileGroups, client }) => ({
  questions: findQuestions(riskProfileGroups, client)
})

const requests = ({ params: { clientId } }) => ({
  client: getClient(clientId),
  updateRiskProfileResponses: ({ update }) => update({
    type: 'clients',
    url: `/clients/${clientId}/risk-profile-responses`,
    id: clientId,
    deserialize: (values, getState) => R.merge(
      findOneSelector({ type: 'clients', id: clientId })(getState()),
      { risk_profile_responses: values }
    )
  }),
  riskProfileGroups: ({ findAll }) => findAll({
    type: 'riskProfileGroups',
    url: '/settings/risk-profile-groups'
  })
})

export default R.compose(
  connect(requests),
  connect(null, selector, actions),
  withRouter,
  reduxForm({
    form: 'riskProfile',
    fields: ['answers[]']
  }, (state, props) => ({
    initialValues: { answers: getAnswers(props) }
  }))
)(RiskProfileWizard)
