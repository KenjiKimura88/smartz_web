import React, { Component, PropTypes } from 'react'
import { ControlLabel, Form, FormGroup } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import { BzCloudReupload, BzTrash } from 'icons'
import { deduction, exemptions, gross, income, payment, taxable,
  totalTax, totalTaxCredits } from './options'
import { DOCUMENTS_STEP_ID, serializeTaxTranscriptData, validateTaxTranscriptData }
  from '../../helpers'
import { STATUS_REJECTED } from 'redux/api/modules/requests'
import bgImage from './onboarding_doc-upload-icon.svg'
import Button from 'components/Button'
import Card from '../Card'
import classes from './Documents.scss'
import Dropzone from 'components/Dropzone'
import Notification from 'containers/Notification'
import NavHeader from '../NavHeader'
import Sidebar from '../Sidebar'
import TaxInputPanel from '../TaxInputPanel'

const getFileNameFromFile = R.compose(
  R.prop('name'),
  R.defaultTo({}),
  R.head,
  R.defaultTo([]),
  R.path(['fields', 'taxTranscriptFile', 'value'])
)

const getFileNameFromPath = R.compose(
  R.when(R.is(String), (path) => path.replace(/^.*[\\\/]/, '')),
  R.path(['fields', 'taxTranscript', 'value'])
)

const getFileName = R.either(getFileNameFromFile, getFileNameFromPath)

const isUploadFailed = R.compose(
  R.equals(STATUS_REJECTED),
  R.path(['requests', 'saveFile', 'status'])
)

export default class Documents extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invitation: PropTypes.object,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    saveFile: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props)
  }

  handleReset = () => {
    const { fields: { taxTranscript, taxTranscriptData, taxTranscriptFile } } = this.props
    taxTranscript.onChange(null)
    taxTranscriptFile.onChange(null)
    taxTranscriptData.onChange(null)
  }

  handleSubmitDocSuccess = ({ value }) => {
    const { fields: { adjustedGrossIncome, additionalChildTaxCredit, earnedIncomeCredit,
      excessSocialSecurity, isBlind, isSpouseBlind, netPremiumCredit, nonTaxableCombatPay,
      otherRefundableCredits, selfEmploymentTax, standardDeduction, taxableIncome,
      taxTranscript, taxTranscriptData, totalAdjustments } } = this.props
    const taxTranscriptValidated = validateTaxTranscriptData(value.tax_transcript_data)

    taxTranscript.onChange(value.tax_transcript)
    taxTranscriptData.onChange(taxTranscriptValidated)

    additionalChildTaxCredit.onChange(taxTranscriptValidated['add_child_tax_credit'])
    adjustedGrossIncome.onChange(taxTranscriptValidated['adjusted_gross_income'])
    earnedIncomeCredit.onChange(taxTranscriptValidated['earned_income_credit'])
    excessSocialSecurity.onChange(taxTranscriptValidated['excess_ss_credit'])
    nonTaxableCombatPay.onChange(taxTranscriptValidated['combat_credit'])
    otherRefundableCredits.onChange(taxTranscriptValidated['refundable_credit'])
    standardDeduction.onChange(taxTranscriptValidated['std_deduction'])
    taxableIncome.onChange(taxTranscriptValidated['taxable_income'])
    totalAdjustments.onChange(taxTranscriptValidated['total_adjustments'])

    // TODO: needs to check with other possible values
    isBlind.onChange(taxTranscriptValidated['blind'])
    // TODO: needs to check with other possible values
    isSpouseBlind.onChange(taxTranscriptValidated['blind_spouse'])
    // TODO: verify field name
    netPremiumCredit.onChange(taxTranscriptValidated['premium_tax_credit'])
    // TODO: verify field name
    selfEmploymentTax.onChange(taxTranscriptValidated['se_tax'])
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const onboardingSuccess = () => push('/progress-saved')

    this.submitOnboarding(values, onboardingSuccess)
  }

  handleSubmitOnboarding = (values) => {
    const { push } = this.props
    const onboardingSuccess = () => push(`info/${DOCUMENTS_STEP_ID + 1}`)

    this.submitOnboarding(values, onboardingSuccess)
  }

  handleSubmitDocuments = (files) => {
    const { fields: { taxTranscriptFile } } = this.props
    taxTranscriptFile.onChange(files)
    this.submitDocuments(files)
  }

  handleReupload = () => {
    const { fields: { taxTranscriptFile } } = this.props
    this.submitDocuments(taxTranscriptFile.value)
  }

  submitDocuments = (files) => {
    const { saveFile } = this.props
    const data = new FormData()
    data.append('tax_transcript', R.head(files))

    saveFile({
      headers: {
        'Content-Disposition': `form-data; filename="${getFileName(files)}"`
      },
      body: data,
      multipart: true,
      success: this.handleSubmitDocSuccess
    })
  }

  submitOnboarding = (values, success) => {
    const { save, setValue } = this.props
    const taxTranscriptData = serializeTaxTranscriptData(values)
    const docValue = R.merge(values, { taxTranscriptData, completed: true })

    setValue(['info', 'steps', DOCUMENTS_STEP_ID], docValue, (onboardingValue) => {
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success
      })
    })
  }

  get hasValue() {
    const { fields: { taxTranscriptFile, taxTranscript } } = this.props
    return taxTranscriptFile.value || (taxTranscript.value)
  }

  get shouldShowWarning() {
    const { props } = this
    const { fields: { taxTranscript, taxTranscriptData } } = props
    return isUploadFailed(props) || (taxTranscript.value && !taxTranscriptData.value)
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    return (
      <Sidebar title='Documents'>
        <p>
          We read the documents you provide us to gain a deeper understanding of your{' '}
          financial position. Using our smart technology we can provide deeper insights{' '}
          to your own financial plans.
        </p>
        <p>Remember, we don't share this information with anyone.</p>
      </Sidebar>
    )
  }

  render () {
    const { hasValue, props, shouldShowWarning } = this
    const { fields, handleSubmit, isLoading, requests } = props
    const { taxTranscriptFile, taxTranscriptData } = fields
    const taxTranscriptClass = classNames(classes.taxTranscriptWrapper, {
      [classes.hasFile]: hasValue
    })
    const dropzoneStatus = (!hasValue && 'pending') ||
      (taxTranscriptData.value && 'success') ||
      (shouldShowWarning && 'fail') ||
      null

    return (
      <Form onSubmit={handleSubmit(this.handleSubmitOnboarding)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
          isLoading={isLoading}>
          {requests && <div>
            <Notification request={requests.save} />
            <Notification request={requests.saveFile} />
          </div>}
          <FormGroup>
            <ControlLabel>Tax Transcript</ControlLabel>
            <div className={taxTranscriptClass}>
              <Dropzone disablePreview multiple={false} accept='application/pdf'
                onDrop={this.handleSubmitDocuments} status={dropzoneStatus}
                filename={getFileName(props)}
                prompt='Drag & Drop your document here' />
              {taxTranscriptFile.value &&
                <div className={classes.action}>
                  <Button bsStyle='thick-outline' onClick={this.handleReupload}>
                    <BzCloudReupload size={30} />
                  </Button>
                </div>}
              {hasValue &&
                <div className={classes.action}>
                  <Button bsStyle='thick-outline' onClick={this.handleReset}>
                    <BzTrash size={30} />
                  </Button>
                </div>
              }
            </div>
          </FormGroup>
          {!hasValue &&
            <p className='text-center'>
              The easiest way to get your Tax Transcript is to visit{' '}
              <a href='https://www.irs.gov/individuals/get-transcript'>
                {'https://www.irs.gov/individuals/get-transcript'}
              </a> to download it immediately.{' '}
              Alternatively, you can attach a scan of your current paper copy.
            </p>
          }
          <TaxInputPanel groupId='all-exemptions' items={exemptions(fields)}
            title={'Exemptions'} />
          <TaxInputPanel groupId='all-income' items={income(fields)}
            title={'Income'} />
          <TaxInputPanel groupId='all-gross' items={gross(fields)}
            title={'Adjusted gross income'} />
          <TaxInputPanel groupId='all-deduction' items={deduction(fields)}
            title={'Standard or itemized deduction'} />
          <TaxInputPanel groupId='all-taxable' items={taxable(fields)}
            title={'Taxable income'} />
          <TaxInputPanel groupId='all-totalTaxCredits' items={totalTaxCredits(fields)}
            title={'Total tax credits'} />
          <TaxInputPanel groupId='all-totalTax' items={totalTax(fields)}
            title={'Total tax'} />
          <TaxInputPanel groupId='all-payment' items={payment(fields)}
            title={'Total payments & credits'} />

          <div className={classes.submitWrapper}>
            <Button type='submit' bsStyle='thick-outline'>Continue</Button>
          </div>
        </Card>
      </Form>
    )
  }
}
