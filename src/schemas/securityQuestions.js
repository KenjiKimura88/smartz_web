import buildSchema from 'redux-form-schema'
import R from 'ramda'

export const schema = buildSchema({
  'questions[].id': {
    label: 'Question ID',
    required: true
  },
  'questions[].question': {
    label: 'Question',
    required: true
  },
  'questions[].answer': {
    label: 'Answer',
    required: true
  }
})

export const validateQuestion = question => {
  const errors = {}
  if (R.isEmpty(question.answer) || R.isNil(question.answer)) {
    errors.answer = 'Answer is required'
  }
  return errors
}

const validate = R.compose(
  R.zipObj(['questions']),
  R.of,
  R.map(validateQuestion),
  R.prop('questions')
)

export default R.merge(schema, { validate })
