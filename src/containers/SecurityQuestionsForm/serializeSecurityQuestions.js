import R from 'ramda'

export default (securityQuestionsForm) => {
  const securityQuestions = securityQuestionsForm.values.questions
  const questionIds = R.pluck('id', securityQuestions)
  const answers = R.pluck('answer', securityQuestions)
  return {
    question_one: questionIds[0],
    question_two: questionIds[1],
    answer_one: answers[0],
    answer_two: answers[1]
  }
}
