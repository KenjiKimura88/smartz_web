import React, { Component } from 'react'
import bgImage from './plan-bg.jpg'
import Slide from '../Slide'

export default class SlidePlan extends Component {
  render () {
    return (
      <Slide title='Plan' bgImage={bgImage}
        headline="Make sure you're headed toward your ideal retirement outcome">
        Dreams without a plan are just wishes. Your unique retirement dreams need a plan
        that just for you - a personal ongoing plan that changes as your life does.
      </Slide>
    )
  }
}
