import React, { Component, PropTypes } from 'react'
import Button from 'components/Button'
import Card from '../Card'
import classes from './InvitationExpired.scss'
import LogoHeader from '../LogoHeader'

export default class InvitationExpired extends Component {
  static propTypes = {
    invitation: PropTypes.object.isRequired
  }

  handleDone = () => {
    location.replace('https://www.betasmartz.com')
  }

  render () {
    const { invitation } = this.props
    return (
      <div>
        <LogoHeader invitation={invitation} />
        <Card>
          <div className={classes.wrapper}>
            <div className={classes.wrapperInner}>
              <h2 className={classes.title}>
                Invitation Expired
              </h2>
              <div className={classes.description}>
                <p>
                  Your invitation has now expired. Please get in touch with{' '}
                  your advisor to resend the invitation.
                </p>
                <p>
                  <Button bsSize='large' bsStyle='primary' onClick={this.handleDone}>
                    Okay
                  </Button>
                </p>
              </div>
            </div>
          </div>
        </Card>
      </div>
    )
  }
}
