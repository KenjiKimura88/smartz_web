import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, Row } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { getPendingSettingsId, getTargetSettingsId } from 'routes/Allocation/helpers'
import { MdChevronRight } from 'helpers/icons'
import Button from 'components/Button'
import classes from './PortfolioHeader.scss'
import InlineList from 'components/InlineList'
import Text from 'components/Text'
import ViewGoalSettingsButton from 'components/ViewGoalSettingsButton'

export default class PortfolioHeader extends Component {
  static propTypes = {
    goal: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired
  };

  render () {
    const { goal, params: { clientId, goalId, viewedSettings } } = this.props
    const pendingSettingsId = getPendingSettingsId(goal)
    const targetSettingsId = getTargetSettingsId(goal)
    const basePath = `/${clientId}/portfolio/${goalId}`

    return (
      <Row className={classes.header}>
        <Col xs={8}>
          <ControlLabel>
            <Text size='small' light>View</Text>
          </ControlLabel>
          <InlineList>
            <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel={viewedSettings}
              settingsId='current' settingsLabel='current'>
              Current
            </ViewGoalSettingsButton>
            <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel={viewedSettings}
              settingsId={targetSettingsId} settingsLabel='target'>
              Target
            </ViewGoalSettingsButton>
            {pendingSettingsId &&
              <ViewGoalSettingsButton basePath={basePath} currentSettingsLabel={viewedSettings}
                settingsId={pendingSettingsId} settingsLabel='pending'>
                Pending
              </ViewGoalSettingsButton>}
          </InlineList>
        </Col>
        <Col xs={4} className='text-right'>
          <LinkContainer to={`/${clientId}/allocation/${goalId}`}>
            <Button>
              Modify goal <MdChevronRight />
            </Button>
          </LinkContainer>
        </Col>
      </Row>
    )
  }
}
