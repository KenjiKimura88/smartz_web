import moment from 'moment'
import momentParseFormat from 'moment-parseformat'
import R from 'ramda'
import config from 'config'

const { accountTypes, expensesCategories, maritalStatuses } = config

const getMaritalStatus = (client) => client ? client.civil_status : 0

export const isMarried = (maritalStatus) => R.contains(maritalStatus, [
  maritalStatuses.MARRIED_FILING_JOINTLY,
  maritalStatuses.MARRIED_FILING_SEPARATELY_LIVED_TOGETHER
])

export const partnerNameFromClient =
  R.path(['regional_data', 'tax_transcript_data', 'name_spouse'])

export const partnerSsnFromClient =
  R.path(['regional_data', 'tax_transcript_data', 'SSN_spouse'])

export const parseFloatFromString = R.compose(
  R.defaultTo(undefined),
  parseFloat,
  R.replace(/[$,]/g, ''),
  R.defaultTo(''),
)

export const ssFraTodaysFromClient = R.compose(
  parseFloatFromString,
  R.path(['regional_data', 'social_security_statement_data', 'RetirementAtFull'])
)

export const parseDate = (dateString) => {
  if (dateString) {
    return moment(dateString, momentParseFormat(dateString))
  } else {
    return undefined
  }
}

export const dateOfEstimateFromClient = R.compose(
  parseDate,
  R.path(['regional_data', 'social_security_statement_data', 'date_of_estimate'])
)

// getLatestRetirementPlan :: Props -> Object
export const getLatestRetirementPlan = R.compose(
  R.when(R.prop('dummyyy'), R.always(undefined)),
  R.reduce((acc, retirementPlan) =>
    moment(retirementPlan.updated_at).isAfter(acc.updated_at) ? retirementPlan : acc
  , { updated_at: 0, dummyyy: true }),
  R.prop('retirementPlans')
)

const hasNotAgreenOn = R.compose(
  R.isNil,
  R.prop('agreed_on'),
  R.defaultTo({})
)

// getHasAgreedRetirementPlan :: Props -> Boolean
export const getHasNonAgreedRetirementPlan = R.compose(
  R.converge(R.and, [
    R.is(Object),
    hasNotAgreenOn
  ]),
  getLatestRetirementPlan
)

// isRetirementPlanAgreed :: Props -> Boolean
const isRetirementPlanAgreed = R.compose(
  R.not,
  hasNotAgreenOn
)

// getHasAgreedRetirementPlan :: Props -> Boolean
export const getHasAgreedRetirementPlan = R.compose(
  isRetirementPlanAgreed,
  getLatestRetirementPlan
)

// getHasAnyAgreedRetirementPlan :: Props -> Boolean
export const getHasAnyAgreedRetirementPlan = R.compose(
  R.any(isRetirementPlanAgreed),
  R.prop('retirementPlans')
)

export const initialize = (props) => {
  const { client, params: { retirementPlanId }, retirementPlan } = props
  return retirementPlanId
    ? client && retirementPlan && deserializeRetirementPlan(props)
    : newFromExistingRetirementPlan(props)
}

export const toExpenseAmount = (income, taxRate, rate) =>
  parseInt(income * (1 - taxRate) * rate)

export const toMonthlyIncome = (income) => R.defaultTo(0, income) / 12

const initExpenses = ({ client, peerGroupExpenses }) => {
  const taxItem = R.find(R.propEq('cat', expensesCategories.TAXES))(peerGroupExpenses)

  const taxRate = R.compose(
    R.defaultTo(0),
    R.path(['rate'])
  )(taxItem)

  const income = R.defaultTo(0, R.path(['income'], client))

  return R.map(
    item => R.merge(R.omit(['rate'], item), {
      amt: R.propEq('cat', expensesCategories.TAXES)
      ? R.compose(
        toMonthlyIncome,
        toExpenseAmount
      )(income, taxRate, R.prop('rate')(item))
      : parseInt(income * taxRate / 12)
    })
  )(peerGroupExpenses)
}

const newFromExistingRetirementPlan = (props) => {
  const retirementPlan = getLatestRetirementPlan(props)
  return R.merge(
    R.path(['id'], retirementPlan)
      ? deserializeRetirementPlan(R.merge(props, { retirementPlan }))
      : brandNewRetirementPlan(props),
    {
      expenses: initExpenses(props),
      estimateDate: dateOfEstimateFromClient(props.client),
      managePartnerAccounts: undefined,
      ssFraTodays: ssFraTodaysFromClient(props.client)
    }
  )
}

const brandNewRetirementPlan = ({ client, incomes = [], peerGroupExpenses }) => ({
  financialConfidence: 90,
  incomes: R.map(deserializeRetirementIncome, incomes),
  maritalStatus: getMaritalStatus(client),
  maxEmployerMatch: 0,
  partnerMaxEmployerMatch: 0,
  projectedIncomeGrowth: 1,
  clientIncome: R.path(['income'], client),
  partnerName: partnerNameFromClient(client),
  partnerSsn: partnerSsnFromClient(client)
})

export const serializeRetirementPlan = (values) => ({
  beta_partner: isMarried(values.maritalStatus) && values.managePartnerAccounts,
  btc: values.clientBtc ? values.clientBtc * 12 : values.clientBtc, // convert monthly to annual
  desired_income: values.retirementIncome,
  desired_risk: values.risk,
  expected_return_confidence: values.financialConfidence / 100,
  expenses: values.expenses,
  date_of_estimate: values.estimateDate && moment(values.estimateDate).format('YYYY-MM-DD'),
  income_growth: values.projectedIncomeGrowth,
  income: values.clientIncome,
  lifestyle: values.lifestyleCategory,
  paid_days: values.paidWork,
  partner_data: isMarried(values.maritalStatus) ? serialisePartnerData(values) : undefined,
  retirement_accounts: values.retirementAccounts,
  retirement_age: values.retirementAge,
  retirement_home_price: values.ballparkEstimate,
  retirement_postal_code: values.remainInSameHome || R.isEmpty(values.dreamLocation)
    ? undefined
    : values.dreamLocation,
  reverse_mortgage: values.reverseMortgage,
  same_home: values.remainInSameHome,
  same_location: values.sameLocation,
  selected_life_expectancy: values.lifeExpectancy,
  volunteer_days: values.volunteerWork,
  // These client fields can be updated via retirementPlan api without checking security questions
  civil_status: values.maritalStatus,
  daily_exercise: values.dailyExercise || 0,
  drinks: values.drinks || 0,
  height: values.height,
  home_value: values.homeValue,
  smoker: values.smoker,
  ss_fra_todays: values.ssFraTodays,
  weight: values.weight
})

export const deserializeRetirementPlan = ({ client, incomes = [], retirementPlan }) =>
  R.compose(
    (rp) => ({
      ...deserialisePartnerData(retirementPlan),
      accept: !!retirementPlan.agreed_on,
      ballparkEstimate: rp.retirement_home_price,
      clientBtc: rp.btc ? rp.btc / 12 : rp.btc, // convert annual to monthly btc
      clientIncome: rp.income,
      dailyExercise: rp.daily_exercise || 0,
      estimateDate: rp.date_of_estimate && moment(rp.date_of_estimate).format('MM/DD/YYYY'),
      dreamLocation: rp.retirement_postal_code,
      drinks: rp.drinks || 0,
      expenses: rp.expenses,
      financialConfidence: rp.expected_return_confidence * 100,
      height: rp.height,
      homeValue: rp.home_value,
      incomes: R.map(deserializeRetirementIncome, incomes),
      lifeExpectancy: rp.selected_life_expectancy,
      lifestyleCategory: rp.lifestyle,
      managePartnerAccounts: rp.beta_partner,
      maritalStatus: getMaritalStatus(client),
      paidWork: rp.paid_days,
      projectedIncomeGrowth: rp.income_growth,
      remainInSameHome: rp.same_home,
      retirementAccounts: rp.retirement_accounts,
      retirementAge: rp.retirement_age,
      retirementIncome: rp.desired_income,
      reverseMortgage: rp.reverse_mortgage,
      risk: rp.desired_risk,
      sameLocation: rp.same_location,
      smoker: R.isNil(rp.smoker) ? undefined : rp.smoker,
      socialSecurityClient: rp.social_security_statement,
      socialSecurityPartner: rp.partner_social_security_statement,
      ssFraTodays: rp.ss_fra_todays,
      volunteerWork: rp.volunteer_days,
      weight: rp.weight,
    }),
    R.reject(R.isNil)
  )(retirementPlan)

const serialisePartnerData = (values) => ({
  btc: values.partnerBtc ? values.partnerBtc * 12 : values.partnerBtc, // convert monthly to annual
  daily_exercise: values.partnerDailyExercise || 0,
  dob: values.partnerBirthdate && moment(values.partnerBirthdate).format('YYYY-MM-DD'),
  drinks: values.drinks || 0,
  height: values.partnerHeight,
  income: values.partnerIncome,
  name: values.partnerName,
  retirement_age: values.partnerRetirementAge,
  selected_life_expectancy: values.partnerLifeExpectancy,
  smoker: values.partnerSmoker,
  ssn: values.partnerSsn,
  weight: values.partnerWeight
})

const deserialisePartnerData = R.compose(
  (partner) => ({
    partnerBtc: partner.btc ? partner.btc / 12 : partner.btc, // convert annual to monthly btc
    partnerDailyExercise: partner.daily_exercise || 0,
    partnerBirthdate: partner.dob && moment(partner.dob).format('MM/DD/YYYY'),
    partnerDrinks: partner.drinks || 0,
    partnerHeight: partner.height,
    partnerIncome: partner.income,
    partnerLifeExpectancy: partner.selected_life_expectancy,
    partnerName: partner.name,
    partnerRetirementAge: partner.retirement_age,
    partnerSsn: partner.ssn,
    partnerWeight: partner.weight
  }),
  R.defaultTo({}),
  R.prop('partner_data')
)

export const newRetirementIncome = {
  growth: {
    value: 0
  },
  schedule: {
    value: 1
  }
}

export const serializeRetirementIncome = (values, retirementPlanId) => ({
  account_type: values.accountType,
  amount: values.amount,
  begin_date: moment(values.beginDate).format('YYYY-MM-DD'),
  growth: values.growth / 100,
  name: values.name,
  plan: retirementPlanId,
  schedule: values.schedule
})

export const deserializeRetirementIncome = (retirementIncome) => ({
  accountType: retirementIncome.account_type,
  amount: retirementIncome.amount,
  beginDate: moment(retirementIncome.begin_date).format('MM/DD/YYYY'),
  growth: retirementIncome.growth * 100,
  id: retirementIncome.id,
  name: retirementIncome.name,
  schedule: retirementIncome.schedule
})

export const newExternalAsset = (clientId) => ({
  growth: 0,
  owner: clientId
})

export const serializeExternalAsset = (values) => ({
  acquisition_date: moment(values.acquiredOn).format('YYYY-MM-DD'),
  description: values.description,
  growth: values.growth / 100,
  name: values.name,
  owner: values.owner,
  type: values.type,
  valuation_date: moment(values.valuedOn).format('YYYY-MM-DD'),
  valuation: values.valuation
})

export const deserializeExternalAsset = (externalAsset) => ({
  acquiredOn: externalAsset.acquisition_date &&
    moment(externalAsset.acquisition_date).format('MM/DD/YYYY'),
  description: externalAsset.description,
  growth: externalAsset.growth * 100,
  id: externalAsset.id,
  name: externalAsset.name,
  owner: externalAsset.owner,
  type: externalAsset.type,
  valuation: externalAsset.valuation,
  valuedOn: externalAsset.valuation_date &&
    moment(externalAsset.valuation_date).format('MM/DD/YYYY')
})

export const assetOrGoalObjectToString = ({ id, type }) => id && type && `${id},${type}`

export const assetOrGoalStringToObject = R.compose(
  R.zipObj(['id', 'type']),
  R.split(',')
)

// getHasPartner :: { client, retirementPlan } -> Boolean
export const getHasPartner = R.converge(R.and, [
  R.compose(
    isMarried,
    R.path(['client', 'civil_status'])
  ),
  R.compose(
    R.is(Object),
    R.path(['retirementPlan', 'partner_data'])
  )
])

export const getEpoch = (years, birthdate) => moment(birthdate).add(years, 'years').valueOf()

const valuesSinceRetirement = (retirementDate) => R.compose(
  R.map(R.propOr(0, 'y')),
  R.filter(R.compose(
    R.lte(retirementDate),
    R.prop('x')
  ))
)

export const getOnTrack = (retirementDate, income, desiredIncome) => R.compose(
  R.all(R.apply(R.gte)),
  R.zip
)(
  valuesSinceRetirement(retirementDate)(income),
  valuesSinceRetirement(retirementDate)(desiredIncome)
)

export const getJointAccount = R.find(R.propEq('account_type', accountTypes.ACCOUNT_TYPE_JOINT))

export const hasPendingJointAccount = R.compose(
  R.equals(false),
  R.prop('confirmed'),
  R.defaultTo({}),
  getJointAccount
)

export const hasConfirmedJointAccount = R.compose(
  R.equals(true),
  R.prop('confirmed'),
  R.defaultTo({}),
  getJointAccount
)

export const sumArrayBy = (propName) => R.compose(
  R.sum,
  R.map(R.prop(propName))
)

export const sumAmount = sumArrayBy('amt')

export const sumIPSAmount = R.compose(
  R.defaultTo(0),
  R.sum,
  R.map(R.prop('amt')),
  R.filter(R.propEq('cat', expensesCategories.INSURANCE_PENSIONS_SOCIAL_SECURITY))
)

export const getExpenditureItems = R.compose(
  R.reject(R.propEq('cat', expensesCategories.INSURANCE_PENSIONS_SOCIAL_SECURITY)),
  R.reject(R.propEq('cat', expensesCategories.SAVINGS)),
  R.reject(R.propEq('cat', expensesCategories.TAXES))
)

export const sumExpendituresAmount = R.compose(
  Math.round,
  R.sum,
  R.map(R.prop('amt')),
  getExpenditureItems
)

export const sumSavingsAmount = R.compose(
  R.defaultTo(0),
  R.sum,
  R.map(R.prop('amt')),
  R.filter(R.propEq('cat', expensesCategories.SAVINGS))
)

export const sumTaxesAmount = R.compose(
  R.defaultTo(0),
  R.sum,
  R.map(R.prop('amt')),
  R.filter(R.propEq('cat', expensesCategories.TAXES))
)

export const sumTaxesRate = R.compose(
  R.defaultTo(0),
  R.sum,
  R.map(R.prop('rate')),
  R.filter(R.propEq('cat', expensesCategories.TAXES))
)

export const getMonthlyContribAmt = (item) => R.equals(R.prop('contrib_period', item), 'yearly')
  ? R.prop('contrib_amt', item) / 12
  : R.prop('contrib_amt', item)

export const sumContributionsAmount = (retirementAccounts, who) =>
  R.compose(
    R.sum,
    R.map(getMonthlyContribAmt),
    R.filter(R.propEq('owner', who)),
    R.defaultTo([])
  )(retirementAccounts)

export const getPeerGroupExpenseRate = (cat) => R.compose(
  R.prop('rate'),
  R.defaultTo({ rate: 0 }),
  R.find(R.propEq('cat', cat))
)

export const getAge = R.memoize((birthdate) => moment().diff(moment(birthdate), 'years'))
