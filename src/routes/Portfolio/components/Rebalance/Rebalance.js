import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import { getDriftScore, getRebalance, getRebalanceThreshold } from 'routes/Allocation/helpers'
import DriftBar from 'components/DriftBar'
import LabelValue from 'components/LabelValue'
import Well from 'components/Well'

export default class Rebalance extends Component {
  static propTypes = {
    goal: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
  };

  render () {
    const { goal, settings } = this.props
    const rebalance = getRebalance(settings)
    const threshold = getRebalanceThreshold(settings)
    const driftScore = getDriftScore(goal)

    return (
      <Well bsSize='sm'>
        <Row>
          <Col xs={5}>
            <LabelValue label='Rebalancing' value={rebalance ? 'Enabled' : 'Disabled'} />
          </Col>
          <Col xs={7} className='text-right'>
            <LabelValue label='When Drift Reaches'
              value={<FormattedNumber value={threshold} format='percent' />} />
          </Col>
        </Row>
        <DriftBar driftScore={driftScore} />
      </Well>
    )
  }

}
