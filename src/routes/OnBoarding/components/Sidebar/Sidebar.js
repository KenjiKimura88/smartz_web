import React, { Component, PropTypes } from 'react'
import classes from './Sidebar.scss'

export default class Sidebar extends Component {

  static propTypes = {
    children: PropTypes.node.isRequired,
    footer: PropTypes.node,
    title: PropTypes.string
  };

  render () {
    const { title, children, footer } = this.props
    return (
      <div className={classes.sidebar}>
        <h2 className={classes.sidebarTitle}>{title}</h2>
        <div className={classes.sidebarContent}>
          {children}
        </div>
        {footer &&
          <div className={classes.sidebarBottom}>
            {footer}
          </div>}
      </div>
    )
  }
}
