import React, { Component, PropTypes } from 'react'
import 'slick-carousel/slick/slick.css'
import { Grid } from 'react-bootstrap'
import R from 'ramda'
import { BzArrowLeft, BzArrowRight } from 'icons'
import { propsChanged } from 'helpers/pureFunctions'
import { requestIsFulFilled } from 'helpers/requests'
import Button from 'components/Button'
import classes from './RetiresmartzWelcome.scss'
import SlideAchieve from '../SlideAchieve'
import SlideDream from '../SlideDream'
import SlidePlan from '../SlidePlan'
import Slider from 'react-slick'
import WelcomeRetiresmartzModal from '../../containers/WelcomeRetiresmartzModal'

const accountsRequestLoaded = requestIsFulFilled('accounts')
const LeftNavButton = (props) => // eslint-disable-line
  <Button bsStyle='thick-outline' {...props}
    className={classes.leftNav}>
    <BzArrowLeft />
  </Button>

const RightNavButton = (props) => // eslint-disable-line
  <Button bsStyle='thick-outline' {...props}
    className={classes.rightNav}>
    <BzArrowRight />
  </Button>

export default class RetiresmartzWelcome extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    requests: PropTypes.object
  };

  constructor(props) {
    super(props)
    this.state = {
      slideIndex: 0
    }
  }

  componentDidMount() {
    const { props } = this
    const { accounts, show } = props
    if (accountsRequestLoaded(props) && R.isEmpty(accounts)) {
      // If request was fulfilled and no accounts added, show welcome modal
      show('welcomeRetiresmartzModal')
    }
  }

  componentDidUpdate (prevProps) {
    const { props } = this
    const { accounts, requests, show } = props
    if (propsChanged(['accounts'], requests, prevProps.requests) &&
      accountsRequestLoaded(props) && R.isEmpty(accounts)) {
      // If request was fulfilled and no accounts added, show welcome modal
      show('welcomeRetiresmartzModal')
    }
  }

  handleSlideChanged = (slideIndex) => {
    this.setState({ slideIndex })
  }

  render () {
    const { params: { clientId } } = this.props
    const { slideIndex } = this.state
    const settings = {
      afterChange: this.handleSlideChanged,
      autoplay: true,
      autoplaySpeed: 10000,
      dots: true,
      infinite: false,
      nextArrow: <RightNavButton disabled={R.equals(slideIndex, 2)} />,
      prevArrow: <LeftNavButton disabled={R.equals(slideIndex, 0)} />,
      slidesToScroll: 1,
      slidesToShow: 1,
      speed: 1000
    }

    return (
      <Grid className={classes.wrapper}>
        <Slider {...settings}>
          <div><SlideDream /></div>
          <div><SlidePlan /></div>
          <div><SlideAchieve clientId={clientId} /></div>
        </Slider>
        <WelcomeRetiresmartzModal />
      </Grid>
    )
  }
}
