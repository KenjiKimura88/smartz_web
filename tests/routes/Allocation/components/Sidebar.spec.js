import React from 'react'
import { shallow } from 'enzyme'
import Sidebar from 'routes/Allocation/components/Sidebar'
import Tabs from 'components/Tabs'

describe('(Route) Allocation', () => {
  describe('(Component) Sidebar', () => {
    const goal = {
      drift_score: 0.3,
      selected_settings: {
        target: 155000
      }
    }

    const getComponent = (props) =>
      <Sidebar activeTabKey='foobar' constraints={[]} fields={{}} goal={goal} set={function () {}}
        settings={{}} toggle={function () {}} values={{}} viewedSettings='pending' {...props} />

    it('renders as a <Tabs />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Tabs)
    })

    it('it passes activeKey to <Tabs />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.prop('activeKey')).to.equal('foobar')
    })

    it('sets activeKey on calling handleTabSelect', () => {
      const set = sinon.spy()
      const wrapper = shallow(getComponent({ set }))
      set.should.not.have.been.called
      wrapper.instance().handleTabSelect('FOO')
      set.should.have.been.calledOnce
      set.should.have.been.calledWith({
        id: 'tab',
        activeKey: 'FOO'
      })
    })
  })
})
