import React from 'react'
import { shallow } from 'enzyme'
import RecommendedValue from 'routes/Allocation/components/RecommendedValue/RecommendedValue'
import Well from 'components/Well'

describe('(Component) RecommendedValue', () => {
  const getComponent = (props) =>
    <RecommendedValue onChange={function () {}} value={0} display='$0.00' />

  it('renders as a <Well />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(Well)
  })
})
