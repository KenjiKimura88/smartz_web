import { connect } from 'redux/api'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import PersonalProfile from '../components/PersonalProfile'
import personalProfileSchema from 'schemas/personalProfileSchema'

const requests =  ({ client }) => ({
  updateUser: client && (({ update }) => update({
    type: 'user',
    id: client.user.id
  })),
  updateClient: client && (({ update }) => update({
    type: 'clients',
    id: client.id,
    url: `/clients/${client.id}?personal-profile`
  }))
})

const addressFields = (client) => {
  if (client) {
    const residentialAddress = R.path(['residential_address'], client)
    const addressArray = R.split('\n', residentialAddress.address)
    return {
      address1: R.defaultTo('', addressArray[0]),
      address2: R.defaultTo('', addressArray[1]),
      city: R.defaultTo('', addressArray[2]),
      post_code: residentialAddress.post_code,
      state: R.path(['region', 'code'], residentialAddress),
      country: R.path(['region', 'country'], residentialAddress)
    }
  } else {
    return {}
  }
}

const initialValuesFromClient = (client) => R.reduce(
  (initialValues, fieldName) => (
    R.merge(initialValues, {[fieldName]: R.defaultTo('', initialValues[fieldName])})
  ),
  R.pick(
    personalProfileSchema.fields,
    (client ? R.mergeAll([client, client.user, addressFields(client), client.regional_data]) : {})
  ),
  personalProfileSchema.fields
)

export default R.compose(
  connect(requests),
  reduxForm({
    form: 'userProfileSettings',
    ...personalProfileSchema
  }, (state, props) => ({
    initialValues: initialValuesFromClient(props.client)
  }))
)(PersonalProfile)
