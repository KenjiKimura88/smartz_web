import React, { Component, PropTypes } from 'react'
import R from 'ramda'

// getAdvisorEmail :: Props -> String | undefined
export const getAdvisorEmail = R.path(['advisor', 'user', 'email'])

export default class AdvisorEmail extends Component {
  static propTypes = {
    tagName: PropTypes.string,
    advisor: PropTypes.object.isRequired
  };

  static defaultProps = {
    tagName: 'a'
  }

  render () {
    const { props } = this
    const { tagName: TagName } = props
    const advisorEmail = getAdvisorEmail(props)
    return (
      <span>
        {advisorEmail
          ? <TagName href={`mailto:${advisorEmail}`}>{advisorEmail}</TagName>
          : 'N/A'}
      </span>
    )
  }
}
