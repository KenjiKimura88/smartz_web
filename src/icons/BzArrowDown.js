/* eslint-disable max-len */
import React, { Component } from 'react'
import BzArrowRight from './BzArrowRight'

export default class BzArrowDown extends Component {
  render() {
    return (
      <BzArrowRight style={{ transform: 'rotate(90deg)' }} />
    )
  }
}
