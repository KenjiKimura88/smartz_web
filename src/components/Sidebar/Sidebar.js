import React, { Component, PropTypes } from 'react'
import { Panel } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './Sidebar.scss'
import Col from 'components/Col'

export default class Sidebar extends Component {
  static propTypes = {
    header: PropTypes.node,
    children: PropTypes.node,
    className: PropTypes.string,
    rounded: PropTypes.bool
  };

  render () {
    const { children, className, header, rounded, ...otherProps } = this.props
    const finalClassName = classNames({
      [classes.sidebar]: true,
      [classes.rounded]: rounded,
      [className]: !!className
    })

    return (
      <Col xs={3} className={finalClassName} noGutterRight {...otherProps}>
        <div className={classes.shadow} />
        <div className={classes.sidebarInner}>
          <Panel header={header && <div className={classes.panelHeading}>{header}</div>}
            className={classes.panel}>
            {children}
          </Panel>
        </div>
      </Col>
    )
  }
}
