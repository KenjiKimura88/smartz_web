import React, { Component, PropTypes } from 'react'
import { connectModal } from 'redux-modal'
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Modal }
  from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import { connect } from 'redux/api'
import { findOne } from 'redux/api/modules/requests'
import classes from './ConfirmDepositModal.scss'
import Notification from 'containers/Notification'
import SelectPlaidAccount from '../SelectPlaidAccount'

class ConfirmDepositModal extends Component {
  static propTypes = {
    amount: PropTypes.number,
    goal: PropTypes.object,
    handleHide: PropTypes.func.isRequired,
    plaidAccounts: PropTypes.array,
    requests: PropTypes.object.isRequired,
    resetForm: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  save = () => {
    const { amount, save } = this.props
    const body = { amount }
    save({ body })
  }

  render () {
    const { amount, goal, handleHide, requests: { save }, show } = this.props

    return (
      <Modal animation={false} show={show} onHide={handleHide}
        aria-labelledby='ModalHeader'
        dialogClassName={classes.confirmDepositModal}>
        <Modal.Header closeButton>
          <Modal.Title id='ModalHeader'>Please Confirm Deposit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Notification request={save} />
          <Form horizontal>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>Deposit:</Col>
              <Col xs={8}>
                <FormControl.Static>
                  <FormattedNumber value={amount} format='currency' />
                </FormControl.Static>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>To:</Col>
              <Col xs={8}>
                <FormControl.Static>
                  {goal.name}
                </FormControl.Static>
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={4}>From:</Col>
              <Col xs={8}>
                <SelectPlaidAccount placeholder='Select bank account' />
              </Col>
            </FormGroup>
          </Form>
          <div className='text-center'>
            By confirming you shall be depositing funds from your linked bank account and
            applying them to this goal. If you do not wish to execute a transfer, click Cancel.
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button bsStyle='primary' onClick={this.save}>
            Deposit
          </Button>
          <Button onClick={handleHide}>Cancel</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

const requests = ({ goal: { id }, handleHide, resetForm }) => ({
  save: ({ create }) => create({
    type: 'deposits',
    url: `/goals/${id}/deposit`,
    success: [
      handleHide,
      resetForm,
      () => findOne({
        type: 'goals',
        id,
        force: true
      })
    ]
  })
})

export default R.compose(
  connectModal({ name: 'confirmDeposit' }),
  connect(requests)
)(ConfirmDepositModal)
