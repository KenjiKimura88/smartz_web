import React, { Component, PropTypes } from 'react'
import { shallow } from 'enzyme'
import mount from '../../../test-helpers/mountWithIntl.js'
import HoldingListItem from 'routes/Portfolio/components/HoldingListItem'

describe('(Route) Portfolio', () => {
  class Table extends Component {
    static propTypes = {
      children: PropTypes.node
    };

    render() {
      return (
        <table className='parent'>
          {this.props.children}
        </table>
      )
    }
  }

  const holding = {
    value: 25000,
    percent: 35,
    assetClass: {
      display_name: 'foo',
      primary_color: 'red',
    },
    tickers: []
  }

  const getComponent = (props) =>
    <Table>
      <HoldingListItem isActive holding={holding} {...props} />
    </Table>

  describe('(Component) HoldingListItem', () => {
    it(`renders as a <tbody>`, () => {
      const wrapper = shallow(
        <HoldingListItem isActive holding={holding} />
      )
      expect(wrapper.type()).to.equal('tbody')
    })

    it('renders the value in currency format', () => {
      const wrapper = mount(getComponent())
      expect(wrapper.containsMatchingElement(<span>$25,000.00</span>)).to.equal(true)
    })
  })
})
