import React, { Component, PropTypes } from 'react'
import { Form, FormGroup } from 'react-bootstrap'
import R from 'ramda'
import { CONTACT_INFO_STEP_ID, MISC_INFO_STEP_ID } from '../../helpers'
import bgImage from './onboarding_agreement-icon.svg'
import Button from 'components/Button'
import Card from '../Card'
import Checkbox from 'components/Checkbox'
import classes from './Agreements.scss'
import DataField from '../DataField'
import FieldError from 'components/FieldError'
import IBAgreements from './IBAgreements'
import NavHeader from '../NavHeader'
import Text from 'components/Text'
import Sidebar from '../Sidebar'
import Well from 'components/Well'

const getFullName = (user) => (
  R.join(' ')([
    user.first_name,
    user.middle_name,
    user.last_name
  ])
)

const getFullAddress = ({ getValue }) => {
  const contactInfo = getValue(['info', 'steps', CONTACT_INFO_STEP_ID])
  return R.join(', ')([
    contactInfo.address1,
    contactInfo.address2,
    contactInfo.city,
    contactInfo.state,
    contactInfo.zipCode
  ])
}

const getSSN = ({ getValue }) => getValue(['info', 'steps', MISC_INFO_STEP_ID, 'ssn'])

export default class Agreements extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    getValue: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    ibEnabled: PropTypes.bool,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired,
    user: PropTypes.object
  };

  handleChangeClick = () => {
    const { push } = this.props
    push('/info/1')
  }

  submitAgreement(values, success, fail) {
    const { save, setValue } = this.props
    const handleSave = (onboardingValue) => {
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success,
        fail
      })
    }
    const setCompleted = () => setValue(['agreements', 'completed'], true, handleSave)
    setValue(['agreements', 'steps', 0], R.merge(values, { completed: true }), setCompleted)
  }

  handleSubmitAgreemnt = (values) => {
    const { push } = this.props
    const callback = () => {
      push('third-party')
    }
    this.submitAgreement(values, callback, callback)
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitAgreement(values, callback, callback)
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    const { user } = this.props
    const footer = (
      <div>
        <hr className={classes.hr} />
        <Button bsStyle='thick-outline' onClick={this.handleChangeClick}>Change</Button>
      </div>
    )
    return (
      <Sidebar title='About You' footer={footer}>
        <DataField label='Name' value={user && getFullName(user)} />
        <DataField label='Email' value={user && user.email} />
        <DataField label='Social Security Number' value={getSSN(this.props) || '(Not specified)'} />
        <DataField label='Address' value={getFullAddress(this.props)} />
      </Sidebar>
    )
  }

  render () {
    const { fields, fields: { advisorAgreement }, handleSubmit, invitation,
      ibEnabled, isLoading } = this.props

    return (
      <Form onSubmit={handleSubmit(this.handleSubmitAgreemnt)}>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} isLoading={isLoading}>
          <Text size='medium' bold>Your advisor's agreement</Text>
          <FormGroup>
            <Checkbox checked={advisorAgreement.value} onChange={advisorAgreement.onChange}>
              By checking this box, you acknowledge that you have read and agree to{' '}
              the terms below.
            </Checkbox>
            <FieldError for={advisorAgreement} />
          </FormGroup>
          <Well>
            <div className={classes.description}>
              <p>
                By checking the above box and clicking submit below, you agree to
                be bound by your <a href={invitation.client_agreement_url} target='_blank'>
                  advisor’s advisory agreement
                </a> and acknowledge receiving and reviewing a copy of your advisor’s Form ADV
                Part 2 and initial privacy disclosure also contained therein (should your
                advisor have included their Form ADV Part 2 and initial privacy disclosure)
              </p>
              <p>
                You understand and agree that this is solely an agreement between you and
                your advisor, and that BetaSmartz and its affiliates are not by any means
                acquiring any obligation or legal duties by permitting you and your advisor
                to execute the advisory agreement between you and your advisor.
                You understand that BetaSmartz has not reviewed the advisory agreement
                between you and your advisor. You further agree to hold harmless and
                indemnify BetaSmartz and its affiliates in connection with any content
                in your advisory agreement with your advisor.
              </p>
              <p>
                You consent to receive these documents in electronic form via this webpage.
                If you are unable to download and view these PDF documents, or do not consent
                to electronic delivery, you cannot submit your application using this
                application form. BetaSmartz will not charge you a fee to use this website,
                but you could incur expenses from your Internet service provider when you
                access information online. Your consent applies only to this transaction.
              </p>
              <p>
                By checking the above box and clicking submit below, you agree to be legally
                bound by your advisor’s advisory agreement’s terms and conditions.
                You understand that checking the above box and clicking submit below is the
                legal equivalent of signing your advisor’s advisory agreement by hand.
              </p>
            </div>
          </Well>
          {ibEnabled && <IBAgreements fields={fields} />}
          <hr className={classes.hr} />
          <div className='text-right'>
            <Button bsStyle='thick-outline' type='submit'>Continue</Button>
          </div>
        </Card>
      </Form>
    )
  }
}
