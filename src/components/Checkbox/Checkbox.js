import React, { Component, PropTypes } from 'react'
import { Checkbox as BsCheckbox } from 'react-bootstrap'
import classNames from 'classnames'
import { BzCheck } from 'icons'
import classes from './Checkbox.scss'

export default class Checkbox extends Component {
  static propTypes = {
    checked: PropTypes.bool,
    children: PropTypes.node,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
  };

  static defaultProps = {
    checked: false
  }

  render () {
    const { props } = this
    const { children, checked, disabled, className, inline } = props
    const checkboxClass = classNames(classes.checkbox, {
      [className]: !!className,
      [classes.disabled]: disabled,
      [classes.inline]: inline
    })
    const checkboxIconClass = classNames(classes.checkIcon, {
      ['hide']: !checked
    })

    return (
      <BsCheckbox {...props} className={checkboxClass}>
        <span className={classes.checkboxInner}>
          <BzCheck className={checkboxIconClass} size={12} />
        </span>
        {children}
      </BsCheckbox>
    )
  }
}
