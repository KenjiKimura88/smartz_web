import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import { LinkContainer } from 'react-router-bootstrap'
import classNames from 'classnames'
import moment from 'moment'
import R from 'ramda'
import { BzArrowLeft, BzArrowRight, BzAssets, BzDocUpload, BzFemale, BzFinances,
  BzHouse, BzJoinedProfile, BzMale, BzSavings, BzSteps, BzWork } from 'icons'
import { getHasAnyAgreedRetirementPlan, getHasPartner, getMonthlyContribAmt,
  hasConfirmedJointAccount, hasPendingJointAccount, sumContributionsAmount }
  from 'routes/RetiresmartzWizard/helpers'
import { mapIndexed } from 'helpers/pureFunctions'
import { requestIsPending, requestIsFulFilled } from 'helpers/requests'
import AllCaps from 'components/AllCaps'
import Button from 'components/Button'
import Checkbox from 'components/Checkbox'
import classes from './RetiresmartzAgreement.scss'
import ConfirmReplaceRetirementPlan from '../ConfirmReplaceRetirementPlan'
import ContentPane from 'components/ContentPane'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import InlineList from 'components/InlineList'
import MainFrame from 'components/MainFrame'
import P from 'components/P'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Well from 'components/Well'

const Box = ({ icon, children, ...props }) => // eslint-disable-line react/prop-types
  <Well noMargin className={classes.box} {...props}>
    <InlineList doubleMargin>
      <Text light primary>
        {icon}
      </Text>
      <Text size='medium' light>
        {children}
      </Text>
    </InlineList>
  </Well>

const getPlanProp = (propName, retirementPlan, defaultValue = false) =>
  R.compose(
    R.defaultTo(defaultValue),
    R.path(R.is(Array, propName) ? propName : [propName])
  )(retirementPlan)

const cautiousInvestor = ({ settings, retirementPlan }) => // eslint-disable-line
  R.compose(
    R.prop('name'),
    R.defaultTo({}),
    R.head,
    R.sortBy(R.prop('upper_bound')),
    R.filter(cat => cat.upper_bound >= getPlanProp('desired_risk', retirementPlan, 1)),
    R.defaultTo([]),
    R.path('investor_risk_categories')
  )(settings)

const getLifeStyle = ({ settings, retirementPlan }) => // eslint-disable-line
  R.compose(
    R.prop('title'),
    R.defaultTo({}),
    R.find(R.propEq('id', retirementPlan && retirementPlan.lifestyle)),
    R.defaultTo([]),
    R.path('retirement_lifestyle_categories')
  )(settings)

export const getEmployerAmt = R.curry((income, item) => (
  R.equals(item.employer_match_type, 'contributions')
    ? getMonthlyContribAmt(item) * R.prop('employer_match', item)
    : R.equals(item.employer_match_type, 'income')
      ? income * R.prop('employer_match', item)
      : 0
))

const getEmployerContributions = ({ retirementPlan }, who, income) => // eslint-disable-line
  R.compose(
    R.sum,
    R.map(getEmployerAmt(income)),
    R.filter(R.propEq('owner', who)),
    R.defaultTo([])
  )(retirementPlan ? retirementPlan.retirement_accounts : [])

const getContributions = ({ retirementPlan }, who, income) => // eslint-disable-line
  sumContributionsAmount(R.path(['retirement_accounts'], retirementPlan), who)

const getRetirementAccountsBalance = ({ retirementPlan }) => // eslint-disable-line
  R.compose(
    R.sum,
    R.map(R.prop('balance')),
    R.defaultTo([])
  )(retirementPlan ? retirementPlan.retirement_accounts : null)

const boxItems = (props) => { // eslint-disable-line react/prop-types
  const { client, retirementPlan } = props

  const lifeStyle = getLifeStyle(props)
  const retirementAge = getPlanProp('retirement_age', retirementPlan)
  const hasPartnerData = getHasPartner(props)
  const lifeExpectancy = getPlanProp('selected_life_expectancy', retirementPlan)
  const income = getPlanProp('income', retirementPlan, 1) / 12
  const contributions = getContributions(props, 'self', income)
  const contributionsRatio = contributions / income
  const employerContributions = getEmployerContributions(props, 'self', income)

  const partnerName = getPlanProp(['partner_data', 'name'], retirementPlan)
  const partnerRetirementAge = getPlanProp(['partner_data', 'retirement_age'], retirementPlan)
  const partnerLifeExpectancy = getPlanProp(['partner_data', 'calculated_life_expectancy'],
    retirementPlan)
  const partnerIncome = getPlanProp(['partner_data', 'income'], retirementPlan, 1) / 12
  const partnerContributions = getContributions(props, 'partner', partnerIncome)
  const partnerContributionsRatio = partnerContributions / partnerIncome
  const partnerEmplContributions = getEmployerContributions(props, 'partner', partnerIncome)

  let items = []

  items = R.append({
    icon: <BzAssets size={33} />,
    content: (
      <div>
        Your risk profile puts you as a <Text size='medium' primary>
          {cautiousInvestor(props)}
        </Text>. Your unique retirement goal includes assets suited to your situation.
      </div>
    )
  }, items)

  hasPartnerData && (items = R.append({
    icon: <BzJoinedProfile size={33} />,
    content: (
      <div>
        Your joint retirement is with <Text size='medium' primary>{partnerName}</Text> and
        she is included in this plan.
      </div>
    )
  }, items))

  items = R.append({
    icon: <BzHouse size={33} />,
    content: (
      <div>
        You have told us that you want to retire with a <Text size='medium' primary>
          {lifeStyle} Lifestyle
        </Text> in your <Text size='medium' primary>
          {R.equals(true, getPlanProp('same_home', retirementPlan)) && 'current'}
          {R.equals(false, getPlanProp('same_home', retirementPlan)) && 'new'}
          {' '}home
        </Text>.
      </div>
    )
  }, items)

  items = R.append({
    icon: <BzMale size={33} />,
    content: (
      <div>
        You want to retire at <Text size='medium' primary>{retirementAge}
        </Text> and your life expectancy is <Text size='medium' primary>
          {lifeExpectancy}
        </Text>. This means <Text size='medium' primary>{
          R.defaultTo(false, lifeExpectancy - retirementAge)
        } years</Text> of funding your retirement
        income.
      </div>
    )
  }, items)

  items = R.append({
    icon: <BzWork size={33} />,
    content: (
      <div>
        You currently work for <Text size='medium' primary>{getPlanProp('employer', client)}
        </Text> and your current monthly before tax income is <Text size='medium' primary>
          <FormattedNumber format='currency' value={income} />
        </Text>. You plan to contribute <Text size='medium' primary>
          <FormattedNumber format='currency' value={contributions} />
        </Text> per month. Your employer will match it with a contribution of{' '}
        <Text size='medium' primary>
          <FormattedNumber format='currency' value={employerContributions} />
        </Text> each month. Your combined monthly contributions represents{' '}
        <Text size='medium' primary>
          <FormattedNumber format='percent' value={contributionsRatio} />
        </Text> of your pre-tax income.
      </div>
    )
  }, items)

  hasPartnerData && (items = R.append({
    icon: <BzFemale size={33} />,
    content: (
      <div>
        {partnerName} wants to retire at <Text size='medium' primary>{partnerRetirementAge}
        </Text> and her life expectancy is <Text size='medium' primary>{partnerLifeExpectancy}
        </Text>. This means <Text size='medium' primary>{
          R.defaultTo(false, partnerLifeExpectancy - partnerRetirementAge)
        } year</Text> of funding her retirement income.
      </div>
    )
  }, items))

  hasPartnerData && (items = R.append({
    icon: <BzFinances size={33} />,
    content: (
      <div>
        ${partnerName} has a current pre-tax monthly income of <Text size='medium' primary>
          <FormattedNumber value={partnerIncome} format='currency' />
        </Text>. She and her employer plan to contribute <Text size='medium' primary>
          <FormattedNumber value={partnerContributions} format='currency' />
        </Text> per month. Her employer will match it with a contribution of{' '}
        <Text size='medium' primary>
          <FormattedNumber value={partnerEmplContributions} format='currency' />
        </Text> each month. Their combined monthly contributions represents{' '}
        <Text size='medium' primary>
          <FormattedNumber value={partnerContributionsRatio} format='percent' />
        </Text> of their pre-tax income.
      </div>
    )
  }, items))

  items = R.append({
    icon: <BzSavings size={33} />,
    content: (
      <div>
        We can see that your current balance of retirement accounts is <Text size='medium' primary>
          <FormattedNumber value={getRetirementAccountsBalance(props)} format='currency' />
        </Text>.
      </div>
    )
  }, items)

  items = R.append({
    icon: <BzSteps size={33} />,
    content: (
      <div>
        We can see that <Text size='medium' primary>you {hasPartnerData && `and ${partnerName}`}
        </Text> are on track towards your <Text size='medium' primary>{lifeStyle} Lifestyle</Text>.
      </div>
    )
  }, items)

  return items
}

export default class RetiresmartzAgreement extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    calculatedData: PropTypes.object,
    client: PropTypes.object,
    fields: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    hide: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    retirementPlan: PropTypes.object,
    retirementPlans: PropTypes.array,
    settings: PropTypes.object,
    show: PropTypes.func.isRequired,
    updateRetirementPlan: PropTypes.func.isRequired
  };

  done = () => {
    const { props } = this
    const { hide, show, updateRetirementPlan } = props
    const agreedOn = moment().toISOString()
    const body = {
      agreed_on: agreedOn
    }
    const hasAgreedRetirementPlan = getHasAnyAgreedRetirementPlan(props)
    if (hasAgreedRetirementPlan) {
      show('confirmReplaceRetirementPlan', {
        onClick: () => {
          hide('confirmReplaceRetirementPlan')
          updateRetirementPlan({ body })
        }
      })
    } else {
      updateRetirementPlan({ body })
    }
  }

  render () {
    const { props } = this
    const { accounts, calculatedData, fields: { accept }, params: { clientId, retirementPlanId },
      retirementPlan } = props
    const agreed = retirementPlan && !!retirementPlan.agreed_on
    const isOffTrack = !R.path(['on_track'], calculatedData)
    const doneDisabled = !accept.value || agreed || isOffTrack
    const isPending = requestIsPending('updateRetirementPlan')(props)
    const isCalculatedDataLoaded = requestIsFulFilled('calculatedData')(props)

    return (
      <MainFrame className={classes.retiresmartzAgreement} rounded>
        <ContentPane xs={12}>
          <div className={classes.content}>
            <P>
              <H2 bold>
                <AllCaps>
                  Retirement Planning Overview
                </AllCaps>
              </H2>
            </P>
            <P>
              <Text size='medium' light>
                Thank you for using RetireSmartz. Using RetireSmartz helps us to provide you with
                the best advice to dream, plan and achieve your retirement goals.
              </Text>
            </P>
            <P doubleMargin>
              <Text size='medium' light>
                To prepare our advice we have considered your individual financial objectives and
                goals.
              </Text>
            </P>
            {mapIndexed((item, index) =>
              <Box bsStyle={index % 2 ? 'none' : 'default'} key={index} icon={item.icon}>
                {item.content}
              </Box>
            , boxItems(props))}
            <H2 bold className={classes.statementTitle}>Full Statement of Advice</H2>
            <Well className={classNames(classes.box, classes.downloadBox)}>
              <InlineList doubleMargin>
                <Text light primary>
                  <BzDocUpload size={33} />
                </Text>
                <Text size='medium' light>
                  Download and read your full statement of advice:
                </Text>
                <Button href={retirementPlan && retirementPlan.statement_of_advice_url}
                  bsStyle='primary' target='_blank' disabled={isOffTrack}>
                  Download now
                </Button>
              </InlineList>
            </Well>
            <P>
              <Row>
                <Col xs={2} className={classes.accept}>
                  <Checkbox checked={!!accept.value} disabled={agreed || isOffTrack}
                    onChange={accept.onChange}>
                    <Text size='medium' primary className={classes.acceptText}>
                      <AllCaps>
                        I Accept
                      </AllCaps>
                    </Text>
                  </Checkbox>
                </Col>
                <Col xs={10}>
                  <Well className={classes.box}>
                    {hasConfirmedJointAccount(accounts)
                    ? <Text size='medium' light>
                      By clicking "I accept", you and your spouse agree to the full
                      Statement of Advice provided here.
                    </Text>
                    : hasPendingJointAccount(accounts)
                      ? <Text size='medium' light>
                        By clicking "I accept", you agree to the full Statement of Advice
                        provided here to you individually and not as a joint plan with
                        your spouse, though they may be named.
                      </Text>
                      : <Text size='medium' light>
                        By clicking "I accept", you agree to the full Statement of Advice
                        provided here.
                      </Text>
                    }
                  </Well>
                </Col>
              </Row>
            </P>
            <P>
              {isCalculatedDataLoaded && isOffTrack
              ? <Well bsStyle='danger'>
                Your retirement plan is currently off track. Please go back to the previous screens
                and change your retirement details to ensure your plan on track.
              </Well>
              : <Text light>
                If you would like to change any of these details you can do so by
                going back to the previous screens and editing your profile.
              </Text>}
            </P>
            <Hr />

            <Row>
              <Col xs={5}>
                <LinkContainer
                  to={`/${clientId}/retiresmartz/${retirementPlanId}/plan`}>
                  <Button disabled={agreed}>
                    <BzArrowLeft size={9} />
                    &nbsp;&nbsp;Go back & change
                  </Button>
                </LinkContainer>
              </Col>
              <Col xs={7} className='text-right'>
                <Button bsStyle='default' type='submit' disabled={doneDisabled}
                  onClick={this.done}>
                  {agreed ? 'Agreed' : 'Done'}&nbsp;&nbsp;
                  <BzArrowRight size={9} />
                </Button>
              </Col>
            </Row>
          </div>
          {isPending && <div className={classes.spinnerWrapper}>
            <Spinner className={classes.spinner}>
              <Text size='large' bold primary>Saving ...</Text>
            </Spinner>
          </div>}
        </ContentPane>
        <ConfirmReplaceRetirementPlan />
      </MainFrame>
    )
  }
}
