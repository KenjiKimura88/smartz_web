import R from 'ramda'
import ReactUpdates from 'react-dom/lib/ReactUpdates'
import ReactDefaultBatchingStrategy from 'react-dom/lib/ReactDefaultBatchingStrategy'
import { create } from 'redux/api/modules/requests'

const saveError = (store, error) => {
  // FIXME: Not dispatching request without timeout
  setTimeout(() => {
    const body = {
      header: error.toString(),
      message: error.stack,
      url: document.location.href
    }
    store.dispatch(
      create({
        type: 'errors',
        url: '/error',
        body,
        lazy: false
      })
    )
  }, 1)
}

export default R.curry(({ store }, nextState, replace) => {
  if (__DEV__) {
    return
  }

  let isHandlingError = false
  const ReactTryCatchBatchingStrategy = {
    // this is part of the BatchingStrategy API. simply pass along
    // what the default batching strategy would do.
    get isBatchingUpdates () {
      return ReactDefaultBatchingStrategy.isBatchingUpdates
    },

    batchedUpdates (...args) {
      try {
        ReactDefaultBatchingStrategy.batchedUpdates(...args)
      } catch (e) {
        if (isHandlingError) {
          // our error handling code threw an error. just throw now
          throw e
        }

        isHandlingError = true
        try {
          // dispatch redux action notifying the app that an error occurred.
          // replace this with whatever error handling logic you like.
          saveError(store, e)
        } finally {
          isHandlingError = false
        }
      }
    }
  }

  ReactUpdates.injection.injectBatchingStrategy(ReactTryCatchBatchingStrategy)
})
