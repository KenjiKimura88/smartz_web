import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import PieChart from 'react-simple-pie-chart'
import { getHoldingName } from '../../helpers'
import { MdChevronRight } from 'helpers/icons'
import classes from './HoldingListItem.scss'
import HoldingItemDetails from '../HoldingItemDetails'
import Text from 'components/Text'

// getColor :: Props -> String | undefined
const getColor = R.pathOr('#666666', ['holding', 'assetClass', 'primary_color'])

export default class HoldingListItem extends Component {
  static propTypes = {
    compact: PropTypes.bool,
    hideDetails: PropTypes.bool,
    holding: PropTypes.object.isRequired,
    isActive: PropTypes.bool,
    resetActiveHolding: PropTypes.func,
    setActiveHolding: PropTypes.func
  };

  handleRowItemClick = () => {
    const { isActive, holding, resetActiveHolding, setActiveHolding } = this.props
    isActive
      ? R.is(Function, resetActiveHolding) && resetActiveHolding()
      : R.is(Function, setActiveHolding) && setActiveHolding(holding.id)
  }

  render () {
    const { props } = this
    const { compact, hideDetails, holding, holding: { offset = 0, percent, value },
      isActive } = props
    const name = getHoldingName(holding)
    const color = getColor(props)
    const rowClass = classNames({
      [classes.active]: isActive,
      [classes.compact]: compact,
      [classes.expandable]: !hideDetails
    })
    const textSize = compact ? 'normal' : 'large'
    const pieSlices = [
      { color: '#DFDFDF', value: 100 - percent - offset },
      { color, value: percent },
      { color: '#DFDFDF', value: offset }
    ]

    return (
      <tbody>
        <tr className={rowClass} onClick={this.handleRowItemClick}>
          <td>
            {!hideDetails &&
              <span className={classes.itemBullet}>
                <MdChevronRight size={20} />
              </span>}
            {name
              ? <Text bold>{name}</Text>
              : <Text light>(Holding name not available)</Text>}
          </td>
          <td className={classes.allocated}>
            <div className={classes.pieChartWrapper}>
              <PieChart slices={pieSlices} />
            </div>
            <Text size={textSize} className={classes.allocatedText}>
              <FormattedNumber value={percent / 100} format='percent' />
            </Text>
          </td>
          {!compact &&
            <td className={classes.value}>
              <Text size={textSize}>
                <FormattedNumber value={value} format='currency' />
              </Text>
            </td>}
        </tr>
        {!hideDetails &&
          <HoldingItemDetails compact={compact} holding={holding} isActive={isActive} />
        }
      </tbody>
    )
  }
}
