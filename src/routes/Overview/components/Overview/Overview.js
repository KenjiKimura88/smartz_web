import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import Button from 'react-bootstrap/lib/Button'
import Infinite from 'react-infinite'
import R from 'ramda'
import { BzPlusCircle, BzRefresh } from 'icons'
import { isRetiresmartzEnabled } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import { requestIsFulFilled, requestIsPending } from 'helpers/requests'
import ExternalAccounts from 'components/ExternalAccounts'
import FeatureNotActivatedModal from 'components/FeatureNotActivatedModal'
import Goal from '../Goal'
import NewGoal from '../NewGoal'
import Summary from '../Summary'
import InlineList from 'components/InlineList'
import QuovoFrame from 'containers/QuovoFrame'
import RetiresmartzLink from 'containers/RetiresmartzLink'
import SectionTitle from '../SectionTitle'
import Spinner from 'components/Spinner'

const getOrderedGoals = ({ order }, { goals }) => R.map(
  ({ id }) => R.find(R.propEq('id', id), goals),
  order
)

const getGoalIndex = R.curry(({ order }, id) =>
  R.findIndex(R.propEq('id', id), order)
)

const getActiveGoals = R.filter(R.propEq('state', 0))

const onEndDrop = R.curry(({ order }, { goals, updateGoal }) => {
  R.addIndex(R.forEach)((goal, index) => {
    const body = { order: index }
    updateGoal({
      id: goal.id,
      url: `/goals/${goal.id}`,
      body
    })
  }, getOrderedGoals({ order }, { goals }))
})

// goalsRequestIsPending :: Props -> Boolean
const goalsRequestIsPending = requestIsPending('goals')

const renderGoals = _props => {
  const { connectDropTarget, goals, goalsState, params: { clientId } } = _props
  return R.isEmpty(goals)
  ? <NewGoal clientId={clientId} />
  : connectDropTarget(
    <div>
      <Infinite elementHeight={194} useWindowAsScrollContainer>
        {R.map(
          goal => goal ? renderGoal(goal, _props) : false,
          getOrderedGoals(goalsState, _props)
        )}
      </Infinite>
    </div>
  )
}

const renderGoal = (goal, _props) => {
  const { goalsState, moveGoal, set, show, toggle, updateGoal, user } = _props
  const { id } = goal
  return (
    <Goal key={id} getGoalIndex={getGoalIndex(goalsState)} goal={goal} goalState={goalsState[id]}
      moveGoal={moveGoal} onEndDrop={function () { onEndDrop(goalsState, _props) }} set={set}
      show={show} toggle={toggle}
      updateGoal={function (params) {
        updateGoal({ ...params, url: `/goals/${id}` })
      }} user={user} />
  )
}

export default class Overview extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    client: PropTypes.object,
    connectDropTarget: PropTypes.func.isRequired,
    externalAccounts: PropTypes.array.isRequired,
    fetchExternalAccounts: PropTypes.func.isRequired,
    goals: PropTypes.array.isRequired,
    goalsState: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    requests: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    set: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
    toggle: PropTypes.func.isRequired,
    updateGoal: PropTypes.func.isRequired,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['accounts', 'client', 'externalAccounts', 'goals', 'goalsState', 'params',
      'requests', 'user'], this.props, nextProps)
  }

  componentWillMount () {
    this.syncExternalAccounts()
  }

  syncExternalAccounts = () => {
    const { fetchExternalAccounts } = this.props
    fetchExternalAccounts()
  }

  render () {
    const { props } = this
    const { accounts, connectDropTarget, externalAccounts, goals, goalsState: { all },
    router: { push }, params: { clientId }, set, show, toggle } = props
    const isLoadingExternalAccounts = requestIsPending('fetchExternalAccounts')(props)
    const didFetchExternalAccounts = requestIsFulFilled('fetchExternalAccounts')(props)
    const retiresmartzEnabled = isRetiresmartzEnabled(props)
    return goalsRequestIsPending(props) ? <Spinner /> : connectDropTarget(
      <div className='container'>
        {retiresmartzEnabled && <Row>
          <Col xs={12}>
            <RetiresmartzLink clientId={clientId} />
          </Col>
        </Row>}
        <Row>
          <Col xs={6}>
            <SectionTitle title='Account Summary' />
            <Summary accounts={accounts} allState={all} goals={getActiveGoals(goals)}
              set={set} show={show} toggle={toggle} />
            <Row>
              <Col lg={6}>
                <SectionTitle title='External Accounts' />
              </Col>
              <Col lg={6}>
                <div className='pull-right'>
                  <InlineList>
                    <Button onClick={() => show('quovoFrame')}>
                      <BzPlusCircle /> Manage External Accounts
                    </Button>
                    <Button onClick={this.syncExternalAccounts}
                      disabled={!didFetchExternalAccounts}>
                      <BzRefresh /> Sync
                    </Button>
                  </InlineList>
                </div>
              </Col>
            </Row>
            <ExternalAccounts externalAccounts={externalAccounts}
              isLoading={isLoadingExternalAccounts} />
          </Col>
          <Col xs={6}>
            <Row>
              <Col xs={6}>
                <SectionTitle title='Your Goals' />
              </Col>
              <Col xs={6}>
                <div className='pull-right'>
                  <InlineList>
                    {retiresmartzEnabled &&
                      <Button onClick={function () { push(`/${clientId}/new-goal/rollover-type`) }}>
                        <BzPlusCircle /> Rollover
                      </Button>}
                    <Button onClick={function () { push(`/${clientId}/new-goal`) }}>
                      <BzPlusCircle /> Add Goal
                    </Button>
                    {retiresmartzEnabled && <RetiresmartzLink clientId={clientId} button />}
                  </InlineList>
                </div>
              </Col>
            </Row>
            {renderGoals(this.props)}
          </Col>
        </Row>
        <FeatureNotActivatedModal />
        <QuovoFrame onClose={this.syncExternalAccounts} />
      </div>
    )
  }
}
