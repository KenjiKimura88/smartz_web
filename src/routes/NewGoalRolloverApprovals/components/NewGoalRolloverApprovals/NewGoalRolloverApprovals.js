import React, { Component, PropTypes } from 'react'
import { Col, Form, FormGroup, Row } from 'react-bootstrap'
import { BzArrowLeft, BzArrowRight } from 'icons'
import Button from 'components/Button'
import Checkbox from 'components/Checkbox'
import classes from './NewGoalRolloverApprovals.scss'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Well from 'components/Well'

export default class NewGoalRolloverApprovals extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    rolloverApprovals: PropTypes.object.isRequired,
    rolloverRequest: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { params: { clientId, rolloverType }, router: { replace }, rolloverRequest } = this.props
    if (rolloverRequest.invalid) {
      replace(`/${clientId}/new-goal/rollover/${rolloverType}`)
    }
  }

  handleBack = () => {
    const { params: { clientId, rolloverType }, router: { replace } } = this.props
    replace(`/${clientId}/new-goal/rollover/${rolloverType}`)
  }

  handleContinue = (values) => {
    const { params: { clientId, rolloverType }, router: { replace } } = this.props
    replace(`/${clientId}/new-goal/rollover/${rolloverType}/signature`)
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  handleNotificationHide = () => {
    this.setState({ notification: null })
  }

  render () {
    const { props } = this
    const { rolloverApprovals: { fields: { agree1, agree2 }, handleSubmit, invalid } } = props
    return (
      <Form onSubmit={handleSubmit(this.handleContinue)} className={classes.wrapper}>
        <H2 bold>To add an IRA, you need to agree to the following terms:</H2>
        <FormGroup>
          <Checkbox checked={agree1.checked} onChange={agree1.onBlur}>
            By clicking this box, you elect not to have federal and state withholding taken from
            your IRA distributions.
          </Checkbox>
          <FieldError for={agree1} />
        </FormGroup>
        <FormGroup>
          <Checkbox checked={agree2.checked} onChange={agree2.onBlur}>
            By clicking this box, you acknowledge that you have read and agree to the terms below.
          </Checkbox>
          <FieldError for={agree2} />
          <Well>
            BetaSmartz's <a href='/privacy-policy' target='_blank'>
              privacy policy
            </a> and <a href='/product-brochure' target='_blank'>
              Betasmartz Product Brochure
            </a>.
          </Well>
        </FormGroup>
        <H2 bold>By proceeding you:</H2>
        <ul>
          <li>
            Direct BetaSmartz and the delivery firm to act on all instructions given on this form,
            including transfering assets to BetaSmartz
          </li>
          <li>
            Accept that any assets not readily transferable might not be transferred within the
            time frames set by FINRA or other applicable authorities.
          </li>
          <li>
            Accept that BetaSmartz is not responsible for changes in the value of assets that
            may occur during the transfer process.
          </li>
          <li>
            Affirm that you are aware of any tax or financial implications that may arise in
            connection with this transfer or with the sale or liquidation of any assets prior
            to transfer, including penalties, fees, financial losses, or losses of product
            features or benefits.
          </li>
        </ul>
        <Hr />
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button onClick={this.handleBack}>
              <BzArrowLeft /> Back
            </Button>
            {' '}
            <Button bsStyle='primary' type='submit' disabled={invalid}>
              Continue <BzArrowRight />
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }
}
