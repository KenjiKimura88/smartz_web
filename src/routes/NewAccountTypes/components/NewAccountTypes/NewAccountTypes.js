import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { isRetiresmartzEnabled } from 'routes/Overview/helpers'
import { propsChanged } from 'helpers/pureFunctions'
import { requestIsFulFilled } from 'helpers/requests'
import accountTypesOptions from './accountTypesOptions'
import CircleSelect from 'components/CircleSelect'
import classes from './NewAccountTypes.scss'
import config from 'config'
import goToNewGoalMenu from '../../helpers'
import SelectIraModal from '../../containers/SelectIraModal'
import WelcomeNewAccountModal from '../../containers/WelcomeNewAccountModal'

const { accountTypes } = config

export default class NewAccountTypes extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    client: PropTypes.object,
    params: PropTypes.object.isRequired,
    requests: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { props } = this
    const { accounts, show } = props
    if (requestIsFulFilled('accounts')(props) && R.isEmpty(accounts)) {
      // If request was fulfilled and no accounts added, show welcome modal
      show('welcomeNewAccountModal')
    }
  }

  componentDidUpdate (prevProps) {
    const { props } = this
    const { accounts, requests, show } = props
    if (propsChanged(['accounts'], requests, prevProps.requests) &&
      requestIsFulFilled('accounts')(props) && R.isEmpty(accounts)) {
      // If request was fulfilled and no accounts added, show welcome modal
      show('welcomeNewAccountModal')
    }
  }

  handleChange = (option) => {
    const { props } = this
    const { params: { clientId }, router: { push }, show } = props
    switch (option.value) {
      case accountTypes.ACCOUNT_TYPE_PERSONAL:
        goToNewGoalMenu(props, option, () => {
          push(`/${clientId}`)
        })
        break
      case accountTypes.ACCOUNT_TYPE_JOINT:
        goToNewGoalMenu(props, option, () => {
          push(`/${clientId}/accounts/new/joint`)
        })
        break
      case accountTypes.ACCOUNT_TYPE_TRUST:
        goToNewGoalMenu(props, option, () => {
          push(`/${clientId}/accounts/new/trust`)
        })
        break
      case 'ira':
        show('selectIraModal')
        break
      case 'rollover':
        push(`/${clientId}/new-goal/rollover-type`)
        break
    }
  }

  render () {
    const { props } = this
    const { client, params: { clientId } } = this.props
    const retiresmartzEnabled = isRetiresmartzEnabled(props)
    return (
      <div className={classes.wrapper}>
        {client && <div>
          <CircleSelect options={accountTypesOptions(retiresmartzEnabled)}
            onChange={this.handleChange}
            title='New Goal Account'
            text='Select the type of account for your new goal' />
          <SelectIraModal clientId={clientId} />
        </div>}
        <WelcomeNewAccountModal />
      </div>
    )
  }
}
