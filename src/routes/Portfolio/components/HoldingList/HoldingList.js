import React, { Component, PropTypes } from 'react'
import { Table } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import R from 'ramda'
import { getGoalBalance, sortHoldings, unallocatedForGoal } from '../../helpers'
import AllCaps from 'components/AllCaps'
import classes from './HoldingList.scss'
import HoldingListItem from '../HoldingListItem'
import Text from 'components/Text'

const getOffset = R.compose(
  R.sum,
  R.values,
  R.pick(['offset', 'percent']),
  R.defaultTo({ offset: 0, percent: 0 }),
  R.last
)

const appendOffsetToHoldings = R.compose(
  R.reduce((acc, item) => R.append({
    ...item,
    offset: getOffset(acc),
  }, acc), []),
  sortHoldings
)

export default class HoldingList extends Component {
  static propTypes = {
    activeHolding: PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]).isRequired,
    compact: PropTypes.bool,
    goal: PropTypes.object.isRequired,
    holdings: PropTypes.array.isRequired,
    resetActiveHolding: PropTypes.func.isRequired,
    setActiveHolding: PropTypes.func.isRequired
  };

  static defaultProps = {
    holdings: []
  };

  isActiveHolding = (id) => {
    const { activeHolding } = this.props
    return R.equals(activeHolding, id)
  }

  render () {
    const { compact, holdings, goal, setActiveHolding, resetActiveHolding } = this.props
    const unallocated = unallocatedForGoal(goal, holdings)
    const balance = getGoalBalance(goal)
    const finalClassName = classNames(classes.holdingList, {
      [classes.compact]: compact
    })

    return (
      <div className={finalClassName}>
        <Table bordered striped>
          <thead>
            <tr>
              <th width='60%'>Holding</th>
              <th className={classes.allocated}>Allocated</th>
              {!compact && <th className={classes.value}>Value</th>}
            </tr>
          </thead>
          {!compact &&
            <tfoot>
              <tr>
                <td colSpan={compact ? 1 : 2}>
                  <AllCaps>Total</AllCaps>
                </td>
                <td className={classes.value}>
                  <Text size='large'>
                    <FormattedNumber value={balance} format='currency' />
                  </Text>
                </td>
              </tr>
            </tfoot>}
          {R.map(h => (
            <HoldingListItem key={h.id} compact={compact} holding={h}
              isActive={this.isActiveHolding(h.id)} resetActiveHolding={resetActiveHolding}
              setActiveHolding={setActiveHolding} />
          ), appendOffsetToHoldings(holdings))}
          <HoldingListItem compact={compact} hideDetails holding={unallocated} />
        </Table>
      </div>
    )
  }
}
