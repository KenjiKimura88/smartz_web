import React from 'react'
import { FormGroup, FormControl } from 'react-bootstrap'
import { mount } from 'enzyme'
import AdvisorPanel from 'routes/Profile/components/AdvisorPanel/AdvisorPanel'

describe('(Route) Profile', () => {
  describe('(Component) AdvisorPanel', () => {

    const client = {
      advisor: {
        id: 2,
        gender: 'Male',
        work_phone_num: '1234567890',
        user: {
          id: 3,
          first_name: 'Lachlan',
          middle_name: 'M',
          last_name: 'Macquarie',
          email: 'advisor@example.org'
        }
      },
      secondary_advisors: [
        {
          id: 4,
          gender: 'Male',
          work_phone_num: '1122334455',
          user: {
            id: 5,
            first_name: 'Jimmy',
            middle_name: '',
            last_name: 'Doe',
            email: 'advisor2@example.org'
          }
        }
      ],
      user: {
        id: 3,
        first_name: 'Lachlan',
        middle_name: 'M',
        last_name: 'Macquarie',
        email: 'advisor@example.org'
      }
    }

    const client2 = {
      advisor: {
        id: 2,
        gender: 'Male',
        work_phone_num: '1234567890',
        email: 'advisor@example.org',
        user: {
          id: 3,
          first_name: 'Lachlan',
          middle_name: 'M',
          last_name: 'Macquarie'
        }
      },
      user: {
        id: 3,
        first_name: 'Lachlan',
        middle_name: 'M',
        last_name: 'Macquarie'
      }
    }

    const getComponent = (props) =>
      <AdvisorPanel client={client} {...props} />

    it('renders primary and secondary advisors information', () => {
      const wrapper = mount(getComponent())
      expect(wrapper.containsMatchingElement(<span>Lachlan M Macquarie</span>)).to.equal(true)
      expect(wrapper.containsMatchingElement(<span>1234567890</span>)).to.equal(true)
      expect(wrapper.containsMatchingElement(
        <a href='mailto:advisor@example.org'>advisor@example.org</a>
      )).to.equal(true)
      expect(wrapper.containsMatchingElement(<span>Jimmy Doe</span>)).to.equal(true)
      expect(wrapper.containsMatchingElement(<span>1122334455</span>)).to.equal(true)
      expect(wrapper.containsMatchingElement(
        <a href='mailto:advisor2@example.org'>advisor2@example.org</a>
      )).to.equal(true)
    })

    it('renders N/A if secondary advisors are not available.', () => {
      const wrapper = mount(getComponent({ client: client2 }))
      const advisorsWrapper = wrapper.find(FormGroup).at(1).find(FormControl.Static)
      expect(advisorsWrapper.text()).to.equal('N/A')
    })
  })
})
