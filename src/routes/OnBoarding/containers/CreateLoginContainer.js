import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { findSingle, touchRequest } from 'redux/api/modules/requests'
import { validateOnboardingCreateLogin } from '../helpers'
import CreateLogin from '../components/CreateLogin'

const actions = {
  findSingle,
  touchRequest
}

export default R.compose(
  reduxForm({
    form: 'onboardingCreateLogin',
    fields: ['password', 'passwordConfirmation', 'primarySecurityQuestion',
      'primarySecurityAnswer', 'secondarySecurityQuestion', 'secondarySecurityAnswer'],
    validate: validateOnboardingCreateLogin
  }, (state, { getValue }) => ({
    initialValues: getValue(['login', 'steps', 0])
  })),
  connect(null, actions),
  withRouter
)(CreateLogin)
