export default [
  {
    label: '1st Mariner Bank',
    value: '1st Mariner Bank'
  },
  {
    label: '1st Source Bank',
    value: '1st Source Bank'
  },
  {
    label: '3Rivers Federal Credit Union',
    value: '3Rivers Federal Credit Union'
  },
  {
    label: '4Front Credit Union',
    value: '4Front Credit Union'
  },
  {
    label: 'Achieva Credit Union',
    value: 'Achieva Credit Union'
  },
  {
    label: 'ACMG Federal Credit Union',
    value: 'ACMG Federal Credit Union'
  },
  {
    label: 'Advantis Credit Union',
    value: 'Advantis Credit Union'
  },
  {
    label: 'Affinity Federal Credit Union',
    value: 'Affinity Federal Credit Union'
  },
  {
    label: 'Alamosa State Bank',
    value: 'Alamosa State Bank'
  },
  {
    label: 'ALEC',
    value: 'ALEC'
  },
  {
    label: 'Alliant Credit Union',
    value: 'Alliant Credit Union'
  },
  {
    label: 'Alpine Bank',
    value: 'Alpine Bank'
  },
  {
    label: 'Altier Credit Union',
    value: 'Altier Credit Union'
  },
  {
    label: 'Altra Federal Credit Union',
    value: 'Altra Federal Credit Union'
  },
  {
    label: 'America First Credit Union',
    value: 'America First Credit Union'
  },
  {
    label: 'American Founders Bank',
    value: 'American Founders Bank'
  },
  {
    label: 'American Health Value',
    value: 'American Health Value'
  },
  {
    label: 'American Trust',
    value: 'American Trust'
  },
  {
    label: 'Americana Community Bank',
    value: 'Americana Community Bank'
  },
  {
    label: 'AmeriCU',
    value: 'AmeriCU'
  },
  {
    label: 'Anchor Bank',
    value: 'Anchor Bank'
  },
  {
    label: 'Andigo Credit Union',
    value: 'Andigo Credit Union'
  },
  {
    label: 'Anheuser-Busch Employees Credit Union',
    value: 'Anheuser-Busch Employees Credit Union'
  },
  {
    label: 'Arbor Bank',
    value: 'Arbor Bank'
  },
  {
    label: 'Arvest',
    value: 'Arvest'
  },
  {
    label: 'Associated Bank',
    value: 'Associated Bank'
  },
  {
    label: 'Astera Credit Union',
    value: 'Astera Credit Union'
  },
  {
    label: 'Atlantic Financial Federal Credit Union',
    value: 'Atlantic Financial Federal Credit Union'
  },
  {
    label: 'Atlantic Stewardship Bank',
    value: 'Atlantic Stewardship Bank'
  },
  {
    label: 'Auburn Savings Bank',
    value: 'Auburn Savings Bank'
  },
  {
    label: 'Auburn State Bank',
    value: 'Auburn State Bank'
  },
  {
    label: 'Austin Bank',
    value: 'Austin Bank'
  },
  {
    label: 'Aventa Credit Union',
    value: 'Aventa Credit Union'
  },
  {
    label: 'Avidia Bank',
    value: 'Avidia Bank'
  },
  {
    label: 'Bank Forward',
    value: 'Bank Forward'
  },
  {
    label: 'Bank Mutual',
    value: 'Bank Mutual'
  },
  {
    label: 'Bank of America',
    value: 'Bank of America'
  },
  {
    label: 'Bank of Ann Arbor',
    value: 'Bank of Ann Arbor'
  },
  {
    label: 'Bank of Bennington',
    value: 'Bank of Bennington'
  },
  {
    label: 'Bank of Cashton',
    value: 'Bank of Cashton'
  },
  {
    label: 'Bank of Little Rock',
    value: 'Bank of Little Rock'
  },
  {
    label: 'Bank of New Hampshire',
    value: 'Bank of New Hampshire'
  },
  {
    label: 'Bank of the Cascades',
    value: 'Bank of the Cascades'
  },
  {
    label: 'Bank of the Ozarks',
    value: 'Bank of the Ozarks'
  },
  {
    label: 'Bank of the Sierra',
    value: 'Bank of the Sierra'
  },
  {
    label: 'Bank of Utah',
    value: 'Bank of Utah'
  },
  {
    label: 'Bankers Trust',
    value: 'Bankers Trust'
  },
  {
    label: 'BankVista',
    value: 'BankVista'
  },
  {
    label: 'Baxter Credit Union',
    value: 'Baxter Credit Union'
  },
  {
    label: 'Bay Commercial Bank',
    value: 'Bay Commercial Bank'
  },
  {
    label: 'BB&T',
    value: 'BB&T'
  },
  {
    label: 'Bell State Bank & Trust',
    value: 'Bell State Bank & Trust'
  },
  {
    label: 'Bellco Credit Union',
    value: 'Bellco Credit Union'
  },
  {
    label: 'Benchmark Bank',
    value: 'Benchmark Bank'
  },
  {
    label: 'BenefitWallet',
    value: 'BenefitWallet'
  },
  {
    label: 'Big Horn Federal',
    value: 'Big Horn Federal'
  },
  {
    label: 'Blackhawk Bank',
    value: 'Blackhawk Bank'
  },
  {
    label: 'BLC Community Bank',
    value: 'BLC Community Bank'
  },
  {
    label: 'Blue Ridge Bank',
    value: 'Blue Ridge Bank'
  },
  {
    label: 'BMO Harris Bank',
    value: 'BMO Harris Bank'
  },
  {
    label: 'BNC National Bank',
    value: 'BNC National Bank'
  },
  {
    label: 'Boone Bank And Trust Co',
    value: 'Boone Bank And Trust Co'
  },
  {
    label: 'Border State Bank',
    value: 'Border State Bank'
  },
  {
    label: 'Bremer Bank',
    value: 'Bremer Bank'
  },
  {
    label: 'Brown County State Bank',
    value: 'Brown County State Bank'
  },
  {
    label: 'Busey Bank',
    value: 'Busey Bank'
  },
  {
    label: 'Capital Bank',
    value: 'Capital Bank'
  },
  {
    label: 'Carolina Alliance Bank',
    value: 'Carolina Alliance Bank'
  },
  {
    label: 'Centennial Bank',
    value: 'Centennial Bank'
  },
  {
    label: 'CenterState Bank',
    value: 'CenterState Bank'
  },
  {
    label: 'Central Valley Community Bank',
    value: 'Central Valley Community Bank'
  },
  {
    label: 'Centric Bank',
    value: 'Centric Bank'
  },
  {
    label: 'Choice Financial Group',
    value: 'Choice Financial Group'
  },
  {
    label: 'Citizens Bank',
    value: 'Citizens Bank'
  },
  {
    label: 'Citizens Bank Minnesota',
    value: 'Citizens Bank Minnesota'
  },
  {
    label: 'Citizens Business Bank',
    value: 'Citizens Business Bank'
  },
  {
    label: 'Citizens Union Bank',
    value: 'Citizens Union Bank'
  },
  {
    label: 'Claremont Savings Bank',
    value: 'Claremont Savings Bank'
  },
  {
    label: 'Clover Community Bank',
    value: 'Clover Community Bank'
  },
  {
    label: 'Coastal Federal Credit Union',
    value: 'Coastal Federal Credit Union'
  },
  {
    label: 'Columbia Bank',
    value: 'Columbia Bank'
  },
  {
    label: 'Columbus Bank and Trust Company',
    value: 'Columbus Bank and Trust Company'
  },
  {
    label: 'Commerce Bank',
    value: 'Commerce Bank'
  },
  {
    label: 'Commercial Bank And Trust Company',
    value: 'Commercial Bank And Trust Company'
  },
  {
    label: 'Community Choice Credit Union',
    value: 'Community Choice Credit Union'
  },
  {
    label: 'Community Financial Services Bank',
    value: 'Community Financial Services Bank'
  },
  {
    label: 'Community Link Federal Credit Union',
    value: 'Community Link Federal Credit Union'
  },
  {
    label: 'Connexus Credit Union',
    value: 'Connexus Credit Union'
  },
  {
    label: 'Consumers National Bank',
    value: 'Consumers National Bank'
  },
  {
    label: 'Core Bank',
    value: 'Core Bank'
  },
  {
    label: 'CornerBank',
    value: 'CornerBank'
  },
  {
    label: 'Cornerstone Community Bank',
    value: 'Cornerstone Community Bank'
  },
  {
    label: 'Cornhusker Bank',
    value: 'Cornhusker Bank'
  },
  {
    label: 'Coulee Bank',
    value: 'Coulee Bank'
  },
  {
    label: 'County Bank',
    value: 'County Bank'
  },
  {
    label: 'CP Federal Credit Union',
    value: 'CP Federal Credit Union'
  },
  {
    label: 'Crossroads Bank',
    value: 'Crossroads Bank'
  },
  {
    label: 'Custer Federal State Bank',
    value: 'Custer Federal State Bank'
  },
  {
    label: 'Dane County Credit Union',
    value: 'Dane County Credit Union'
  },
  {
    label: 'Delta Community Credit Union',
    value: 'Delta Community Credit Union'
  },
  {
    label: 'Denison State Bank',
    value: 'Denison State Bank'
  },
  {
    label: 'Denmark State Bank',
    value: 'Denmark State Bank'
  },
  {
    label: 'Desert Schools Federal Credit Union',
    value: 'Desert Schools Federal Credit Union'
  },
  {
    label: 'Digital Federal Credit Union',
    value: 'Digital Federal Credit Union'
  },
  {
    label: 'Discovery Benefits, Inc.',
    value: 'Discovery Benefits, Inc.'
  },
  {
    label: 'Dow Chemical Employee Credit Union',
    value: 'Dow Chemical Employee Credit Union'
  },
  {
    label: 'Eagle Community Bank',
    value: 'Eagle Community Bank'
  },
  {
    label: 'Education First Credit Union',
    value: 'Education First Credit Union'
  },
  {
    label: 'Elements Financial',
    value: 'Elements Financial'
  },
  {
    label: 'Empower Federal Credit Union',
    value: 'Empower Federal Credit Union'
  },
  {
    label: 'Ent Federal Credit Union',
    value: 'Ent Federal Credit Union'
  },
  {
    label: 'Essex Bank',
    value: 'Essex Bank'
  },
  {
    label: 'Everence Federal Credit Union',
    value: 'Everence Federal Credit Union'
  },
  {
    label: 'Farm Bureau Bank',
    value: 'Farm Bureau Bank'
  },
  {
    label: 'Farmers & Merchants State Bank',
    value: 'Farmers & Merchants State Bank'
  },
  {
    label: 'Farmers Bank',
    value: 'Farmers Bank'
  },
  {
    label: 'Farmers Bank and Trust',
    value: 'Farmers Bank and Trust'
  },
  {
    label: 'Farmers State Bank',
    value: 'Farmers State Bank'
  },
  {
    label: 'Fidelity Bank',
    value: 'Fidelity Bank'
  },
  {
    label: 'Fidelity Bank (Kansas)',
    value: 'Fidelity Bank (Kansas)'
  },
  {
    label: 'Fidelity Investments',
    value: 'Fidelity Investments'
  },
  {
    label: 'Fifth Third Bank',
    value: 'Fifth Third Bank'
  },
  {
    label: 'Financial Center First Credit Union',
    value: 'Financial Center First Credit Union'
  },
  {
    label: 'First American Bank',
    value: 'First American Bank'
  },
  {
    label: 'First Bank of Missouri',
    value: 'First Bank of Missouri'
  },
  {
    label: 'First Citizens National Bank',
    value: 'First Citizens National Bank'
  },
  {
    label: 'First Community Credit Union',
    value: 'First Community Credit Union'
  },
  {
    label: 'First Federal Community Bank',
    value: 'First Federal Community Bank'
  },
  {
    label: 'First Federal Lakewood',
    value: 'First Federal Lakewood'
  },
  {
    label: 'First Federal of Northern Michigan',
    value: 'First Federal of Northern Michigan'
  },
  {
    label: 'First Federal Savings Bank',
    value: 'First Federal Savings Bank'
  },
  {
    label: 'First International Bank & Trust',
    value: 'First International Bank & Trust'
  },
  {
    label: 'First Internet Bank',
    value: 'First Internet Bank'
  },
  {
    label: 'First Interstate Bank',
    value: 'First Interstate Bank'
  },
  {
    label: 'First Minnetonka City Bank',
    value: 'First Minnetonka City Bank'
  },
  {
    label: 'First National Bank',
    value: 'First National Bank'
  },
  {
    label: 'First National Bank (Iowa)',
    value: 'First National Bank (Iowa)'
  },
  {
    label: 'First National Bank - (OH)',
    value: 'First National Bank - (OH)'
  },
  {
    label: 'First National Bank - Hartford',
    value: 'First National Bank - Hartford'
  },
  {
    label: 'First National Bank Alaska',
    value: 'First National Bank Alaska'
  },
  {
    label: 'First National Bank North',
    value: 'First National Bank North'
  },
  {
    label: 'First National Bank of River Falls',
    value: 'First National Bank of River Falls'
  },
  {
    label: 'First Northern Bank',
    value: 'First Northern Bank'
  },
  {
    label: 'First State Bank',
    value: 'First State Bank'
  },
  {
    label: 'First State Bank of Bigfork',
    value: 'First State Bank of Bigfork'
  },
  {
    label: 'First Tech Federal Credit Union',
    value: 'First Tech Federal Credit Union'
  },
  {
    label: 'First United Bank & Trust',
    value: 'First United Bank & Trust'
  },
  {
    label: 'First Virginia Community Bank',
    value: 'First Virginia Community Bank'
  },
  {
    label: 'First Western Bank & Trust',
    value: 'First Western Bank & Trust'
  },
  {
    label: 'First Westroads Bank',
    value: 'First Westroads Bank'
  },
  {
    label: 'FirstBank',
    value: 'FirstBank'
  },
  {
    label: 'FlexHSA',
    value: 'FlexHSA'
  },
  {
    label: 'Florida Parishes Bank',
    value: 'Florida Parishes Bank'
  },
  {
    label: 'Forest Park National Bank & Trust Co.',
    value: 'Forest Park National Bank & Trust Co.'
  },
  {
    label: 'Forreston State Bank',
    value: 'Forreston State Bank'
  },
  {
    label: 'Fort Financial Credit Union',
    value: 'Fort Financial Credit Union'
  },
  {
    label: 'Forum Credit Union',
    value: 'Forum Credit Union'
  },
  {
    label: 'Freedom Credit Union',
    value: 'Freedom Credit Union'
  },
  {
    label: 'Frost Bank',
    value: 'Frost Bank'
  },
  {
    label: 'Generations Bank',
    value: 'Generations Bank'
  },
  {
    label: 'Genisys Credit Union',
    value: 'Genisys Credit Union'
  },
  {
    label: 'Glens Falls National Bank',
    value: 'Glens Falls National Bank'
  },
  {
    label: 'Glenview State Bank',
    value: 'Glenview State Bank'
  },
  {
    label: 'Gorham Savings Bank',
    value: 'Gorham Savings Bank'
  },
  {
    label: 'Granite Credit Union',
    value: 'Granite Credit Union'
  },
  {
    label: 'Great Lakes Credit Union',
    value: 'Great Lakes Credit Union'
  },
  {
    label: 'Great Southern Bank',
    value: 'Great Southern Bank'
  },
  {
    label: 'Greater Iowa Credit Union',
    value: 'Greater Iowa Credit Union'
  },
  {
    label: 'Greenville Federal',
    value: 'Greenville Federal'
  },
  {
    label: 'Guaranty Bank',
    value: 'Guaranty Bank'
  },
  {
    label: 'Guilford Savings Bank',
    value: 'Guilford Savings Bank'
  },
  {
    label: 'Hardin County Savings Bank',
    value: 'Hardin County Savings Bank'
  },
  {
    label: 'Harvest Bank',
    value: 'Harvest Bank'
  },
  {
    label: 'HealthEquity',
    value: 'HealthEquity'
  },
  {
    label: 'HealthSavings Administrators',
    value: 'HealthSavings Administrators'
  },
  {
    label: 'Heartland Credit Union',
    value: 'Heartland Credit Union'
  },
  {
    label: 'Heritage Bank',
    value: 'Heritage Bank'
  },
  {
    label: 'Heritage Valley Federal Credit Union',
    value: 'Heritage Valley Federal Credit Union'
  },
  {
    label: 'Hickory Point Bank',
    value: 'Hickory Point Bank'
  },
  {
    label: 'High Plains Bank',
    value: 'High Plains Bank'
  },
  {
    label: 'Home Federal Bank',
    value: 'Home Federal Bank'
  },
  {
    label: 'Home Savings',
    value: 'Home Savings'
  },
  {
    label: 'Home State Bank',
    value: 'Home State Bank'
  },
  {
    label: 'Hometown Community Bank',
    value: 'Hometown Community Bank'
  },
  {
    label: 'Hoosier Hills Credit Union',
    value: 'Hoosier Hills Credit Union'
  },
  {
    label: 'Horizon Bank',
    value: 'Horizon Bank'
  },
  {
    label: 'Howard Bank',
    value: 'Howard Bank'
  },
  {
    label: 'HSA Bank',
    value: 'HSA Bank'
  },
  {
    label: 'HSA Resources',
    value: 'HSA Resources'
  },
  {
    label: 'Hudson Valley Federal Credit Union',
    value: 'Hudson Valley Federal Credit Union'
  },
  {
    label: 'IBERIABANK',
    value: 'IBERIABANK'
  },
  {
    label: 'IBM Southeast Employees\' Credit Union',
    value: 'IBM Southeast Employees\' Credit Union'
  },
  {
    label: 'Idaho Independent Bank',
    value: 'Idaho Independent Bank'
  },
  {
    label: 'IH Mississippi Valley Credit Union',
    value: 'IH Mississippi Valley Credit Union'
  },
  {
    label: 'Incredible Bank',
    value: 'Incredible Bank'
  },
  {
    label: 'Indiana Members Credit Union',
    value: 'Indiana Members Credit Union'
  },
  {
    label: 'Inland Bank',
    value: 'Inland Bank'
  },
  {
    label: 'Interra Credit Union',
    value: 'Interra Credit Union'
  },
  {
    label: 'Intrust Bank',
    value: 'Intrust Bank'
  },
  {
    label: 'Iowa Falls State Bank',
    value: 'Iowa Falls State Bank'
  },
  {
    label: 'Isabella Bank',
    value: 'Isabella Bank'
  },
  {
    label: 'Itasca Bank & Trust Co.',
    value: 'Itasca Bank & Trust Co.'
  },
  {
    label: 'Jones National Bank & Trust Co.',
    value: 'Jones National Bank & Trust Co.'
  },
  {
    label: 'Juniata Valley Bank',
    value: 'Juniata Valley Bank'
  },
  {
    label: 'KEMBA Financial Credit Union',
    value: 'KEMBA Financial Credit Union'
  },
  {
    label: 'KeyBank',
    value: 'KeyBank'
  },
  {
    label: 'Kingston National Bank',
    value: 'Kingston National Bank'
  },
  {
    label: 'Kitsap Bank',
    value: 'Kitsap Bank'
  },
  {
    label: 'KV Federal Credit Union',
    value: 'KV Federal Credit Union'
  },
  {
    label: 'Lake City Bank',
    value: 'Lake City Bank'
  },
  {
    label: 'Lake Elmo Bank',
    value: 'Lake Elmo Bank'
  },
  {
    label: 'Lake Michigan Credit Union',
    value: 'Lake Michigan Credit Union'
  },
  {
    label: 'Lake Sunapee Bank',
    value: 'Lake Sunapee Bank'
  },
  {
    label: 'Landmark Credit Union',
    value: 'Landmark Credit Union'
  },
  {
    label: 'Liberty Bank',
    value: 'Liberty Bank'
  },
  {
    label: 'Liberty Health Bank',
    value: 'Liberty Health Bank'
  },
  {
    label: 'Linn Area Credit Union',
    value: 'Linn Area Credit Union'
  },
  {
    label: 'Logansport Savings Bank',
    value: 'Logansport Savings Bank'
  },
  {
    label: 'Macatawa Bank',
    value: 'Macatawa Bank'
  },
  {
    label: 'Maine Family Federal Credit Union',
    value: 'Maine Family Federal Credit Union'
  },
  {
    label: 'Marine Bank',
    value: 'Marine Bank'
  },
  {
    label: 'MassMutual Federal Credit Union',
    value: 'MassMutual Federal Credit Union'
  },
  {
    label: 'Meadows Credit Union',
    value: 'Meadows Credit Union'
  },
  {
    label: 'Mercantile Bank of Michigan',
    value: 'Mercantile Bank of Michigan'
  },
  {
    label: 'Meredith Village Savings Bank',
    value: 'Meredith Village Savings Bank'
  },
  {
    label: 'Mid Country Bank',
    value: 'Mid Country Bank'
  },
  {
    label: 'Middlesex Healthcare Federal Credit Union',
    value: 'Middlesex Healthcare Federal Credit Union'
  },
  {
    label: 'MidUSA Credit Union',
    value: 'MidUSA Credit Union'
  },
  {
    label: 'Midwest Bank of Western Illinois',
    value: 'Midwest Bank of Western Illinois'
  },
  {
    label: 'MidWestOne Bank',
    value: 'MidWestOne Bank'
  },
  {
    label: 'Minnco Credit Union',
    value: 'Minnco Credit Union'
  },
  {
    label: 'Miramar Federal Credit Union',
    value: 'Miramar Federal Credit Union'
  },
  {
    label: 'Modern Woodmen Bank',
    value: 'Modern Woodmen Bank'
  },
  {
    label: 'Monona State Bank',
    value: 'Monona State Bank'
  },
  {
    label: 'Monument Bank',
    value: 'Monument Bank'
  },
  {
    label: 'Mountain America Credit Union',
    value: 'Mountain America Credit Union'
  },
  {
    label: 'Mutual 1st Federal Credit Union',
    value: 'Mutual 1st Federal Credit Union'
  },
  {
    label: 'MVB Bank',
    value: 'MVB Bank'
  },
  {
    label: 'NASA Federal Credit Union',
    value: 'NASA Federal Credit Union'
  },
  {
    label: 'NBT Bank',
    value: 'NBT Bank'
  },
  {
    label: 'Newtown Savings Bank',
    value: 'Newtown Savings Bank'
  },
  {
    label: 'North Coast Credit Union',
    value: 'North Coast Credit Union'
  },
  {
    label: 'North Island Credit Union',
    value: 'North Salem State Bank'
  },
  {
    label: 'North Shore Bank',
    value: 'North Shore Bank'
  },
  {
    label: 'Northern Credit Union',
    value: 'Northern Credit Union'
  },
  {
    label: 'Northfield Savings Bank',
    value: 'Northfield Savings Bank'
  },
  {
    label: 'Northstar Bank',
    value: 'Northstar Bank'
  },
  {
    label: 'Northwest Bank',
    value: 'Northwest Bank'
  },
  {
    label: 'Northwest Savings Bank',
    value: 'Northwest Savings Bank'
  },
  {
    label: 'Northwoods Credit Union',
    value: 'Northwoods Credit Union'
  },
  {
    label: 'Norway Savings Bank',
    value: 'Norway Savings Bank'
  },
  {
    label: 'Oak Valley Community Bank',
    value: 'Oak Valley Community Bank'
  },
  {
    label: 'Optum Bank',
    value: 'Optum Bank'
  },
  {
    label: 'Origin Bank',
    value: 'Origin Bank'
  },
  {
    label: 'Ouachita Independent Bank',
    value: 'Ouachita Independent Bank'
  },
  {
    label: 'Pacific Community Credit Union',
    value: 'Pacific Community Credit Union'
  },
  {
    label: 'Park City Credit Union',
    value: 'Park City Credit Union'
  },
  {
    label: 'Park Community Credit Union',
    value: 'Park Community Credit Union'
  },
  {
    label: 'Park View Federal Credit Union',
    value: 'Park View Federal Credit Union'
  },
  {
    label: 'Patelco Credit Union',
    value: 'Patelco Credit Union'
  },
  {
    label: 'Pathways Financial Credit Union',
    value: 'Pathways Financial Credit Union'
  },
  {
    label: 'Pen Air Federal Credit Union',
    value: 'Pen Air Federal Credit Union'
  },
  {
    label: 'Peoples Bank',
    value: 'Peoples Bank'
  },
  {
    label: 'Peoples United Bank',
    value: 'Peoples United Bank'
  },
  {
    label: 'Piedmond Federal Savings Bank',
    value: 'Piedmond Federal Savings Bank'
  },
  {
    label: 'Pinnacle Bank',
    value: 'Pinnacle Bank'
  },
  {
    label: 'Pinnacle Financial Partners',
    value: 'Pinnacle Financial Partners'
  },
  {
    label: 'Pioneer Bank',
    value: 'Pioneer Bank'
  },
  {
    label: 'PNC',
    value: 'PNC'
  },
  {
    label: 'PremierOne Credit Union',
    value: 'PremierOne Credit Union'
  },
  {
    label: 'ProGrowth Bank',
    value: 'ProGrowth Bank'
  },
  {
    label: 'Providence Bank',
    value: 'Providence Bank'
  },
  {
    label: 'Provident Credit Union',
    value: 'Provident Credit Union'
  },
  {
    label: 'Pulaski Bank',
    value: 'Pulaski Bank'
  },
  {
    label: 'PyraMax Bank',
    value: 'PyraMax Bank'
  },
  {
    label: 'Redwood Credit Union',
    value: 'Redwood Credit Union'
  },
  {
    label: 'Reliance Bank',
    value: 'Reliance Bank'
  },
  {
    label: 'Renasant Bank',
    value: 'Renasant Bank'
  },
  {
    label: 'Republic Bank',
    value: 'Republic Bank'
  },
  {
    label: 'Resource Bank',
    value: 'Resource Bank'
  },
  {
    label: 'River Valley Bank',
    value: 'River Valley Bank'
  },
  {
    label: 'Riverland Federal Credit Union',
    value: 'Riverland Federal Credit Union'
  },
  {
    label: 'Rocky Mountain Bank',
    value: 'Rocky Mountain Bank'
  },
  {
    label: 'Royal Bank America',
    value: 'Royal Bank America'
  },
  {
    label: 'Royal Credit Union',
    value: 'Royal Credit Union'
  },
  {
    label: 'Salisbury Bank',
    value: 'Salisbury Bank'
  },
  {
    label: 'Sandy Spring Bank',
    value: 'Sandy Spring Bank'
  },
  {
    label: 'Saturna Capital',
    value: 'Saturna Capital'
  },
  {
    label: 'Sb1 Federal Credit Union',
    value: 'Sb1 Federal Credit Union'
  },
  {
    label: 'Scott Credit Union',
    value: 'Scott Credit Union'
  },
  {
    label: 'Security Bank',
    value: 'Security Bank'
  },
  {
    label: 'Security Bank of Kansas City',
    value: 'Security Bank of Kansas City'
  },
  {
    label: 'Security Financial Bank',
    value: 'Security Financial Bank'
  },
  {
    label: 'Security First Bank',
    value: 'Security First Bank'
  },
  {
    label: 'SelectAccount',
    value: 'SelectAccount'
  },
  {
    label: 'SF Fire Credit Union',
    value: 'SF Fire Credit Union'
  },
  {
    label: 'Simmons Bank',
    value: 'Simmons Bank'
  },
  {
    label: 'Solvay Bank',
    value: 'Solvay Bank'
  },
  {
    label: 'Sooper Credit Union',
    value: 'Sooper Credit Union'
  },
  {
    label: 'Sound Community Bank',
    value: 'Sound Community Bank'
  },
  {
    label: 'South State Bank',
    value: 'South State Bank'
  },
  {
    label: 'SouthPoint Financial Credit Union',
    value: 'SouthPoint Financial Credit Union'
  },
  {
    label: 'SPIRE Credit Union',
    value: 'SPIRE Credit Union'
  },
  {
    label: 'Springs Valley Bank and Trust',
    value: 'Springs Valley Bank and Trust'
  },
  {
    label: 'Stamford Federal Credit Union',
    value: 'Stamford Federal Credit Union'
  },
  {
    label: 'Stanford Federal Credit Union',
    value: 'Stanford Federal Credit Union'
  },
  {
    label: 'State Bank & Trust Co',
    value: 'State Bank & Trust Co'
  },
  {
    label: 'State Bank of Alcester',
    value: 'State Bank of Alcester'
  },
  {
    label: 'State Bank of Richmond',
    value: 'State Bank of Richmond'
  },
  {
    label: 'State Employee\'s Credit Union',
    value: 'State Employee\'s Credit Union'
  },
  {
    label: 'State Farm Bank',
    value: 'State Farm Bank'
  },
  {
    label: 'Sterling Administration',
    value: 'Sterling Administration'
  },
  {
    label: 'Sutton Bank',
    value: 'Sutton Bank'
  },
  {
    label: 'Synovus',
    value: 'Synovus'
  },
  {
    label: 'Talmer Bank & Trust',
    value: 'Talmer Bank & Trust'
  },
  {
    label: 'TD Bank',
    value: 'TD Bank'
  },
  {
    label: 'Teachers Credit Union',
    value: 'Teachers Credit Union'
  },
  {
    label: 'Technology Credit Union',
    value: 'Technology Credit Union'
  },
  {
    label: 'Telcoe Federal Credit Union',
    value: 'Telcoe Federal Credit Union'
  },
  {
    label: 'Telhio Credit Union',
    value: 'Telhio Credit Union'
  },
  {
    label: 'Tennessee Valley Federal Credit Union',
    value: 'Tennessee Valley Federal Credit Union'
  },
  {
    label: 'The Adirondack Trust Company',
    value: 'The Adirondack Trust Company'
  },
  {
    label: 'The Bank of Wisconsin Dells',
    value: 'The Bank of Wisconsin Dells'
  },
  {
    label: 'The Callaway Bank',
    value: 'The Callaway Bank'
  },
  {
    label: 'The Henry County Bank',
    value: 'The Henry County Bank'
  },
  {
    label: 'The HSA Authority',
    value: 'The HSA Authority'
  },
  {
    label: 'The Palmetto Bank',
    value: 'The Palmetto Bank'
  },
  {
    label: 'The State Bank',
    value: 'The State Bank'
  },
  {
    label: 'Thoroughbred Health Bank',
    value: 'Thoroughbred Health Bank'
  },
  {
    label: 'Thrivent Federal Credit Union',
    value: 'Thrivent Federal Credit Union'
  },
  {
    label: 'Tigers Community Credit Union',
    value: 'Tigers Community Credit Union'
  },
  {
    label: 'Timberline Bank',
    value: 'Timberline Bank'
  },
  {
    label: 'Tioga State Bank',
    value: 'Tioga State Bank'
  },
  {
    label: 'Travis Credit Union',
    value: 'Travis Credit Union'
  },
  {
    label: 'Triumph Community Bank',
    value: 'Triumph Community Bank'
  },
  {
    label: 'Tropical Financial Credit Union',
    value: 'Tropical Financial Credit Union'
  },
  {
    label: 'Trustco Bank',
    value: 'Trustco Bank'
  },
  {
    label: 'Trustmark National Bank',
    value: 'Trustmark National Bank'
  },
  {
    label: 'TruStone Financial',
    value: 'TruStone Financial'
  },
  {
    label: 'Two River Community Bank',
    value: 'Two River Community Bank'
  },
  {
    label: 'UMB Bank',
    value: 'UMB Bank'
  },
  {
    label: 'Umpqua Bank',
    value: 'Umpqua Bank'
  },
  {
    label: 'Union Bank & Trust Company',
    value: 'Union Bank & Trust Company'
  },
  {
    label: 'Union Savings Bank',
    value: 'Union Savings Bank'
  },
  {
    label: 'Union State Bank',
    value: 'Union State Bank'
  },
  {
    label: 'United Bank',
    value: 'United Bank'
  },
  {
    label: 'United Bank of Michigan',
    value: 'United Bank of Michigan'
  },
  {
    label: 'United Community Bank',
    value: 'United Community Bank'
  },
  {
    label: 'Unitus Community Credit Union',
    value: 'Unitus Community Credit Union'
  },
  {
    label: 'Universal 1 Credit Union',
    value: 'Universal 1 Credit Union'
  },
  {
    label: 'Univest',
    value: 'Univest'
  },
  {
    label: 'USAlliance Federal Credit Union',
    value: 'USAlliance Federal Credit Union'
  },
  {
    label: 'Valley First Credit Union',
    value: 'Valley First Credit Union'
  },
  {
    label: 'Valley View Bank',
    value: 'Valley View Bank'
  },
  {
    label: 'Veritex Community Bank',
    value: 'Veritex Community Bank'
  },
  {
    label: 'Victory Community Bank',
    value: 'Victory Community Bank'
  },
  {
    label: 'VisionBank of Iowa',
    value: 'VisionBank of Iowa'
  },
  {
    label: 'VyStar Credit Union',
    value: 'VyStar Credit Union'
  },
  {
    label: 'Wasatch Peaks Credit Union',
    value: 'Wasatch Peaks Credit Union'
  },
  {
    label: 'West Virginia Central Credit Union',
    value: 'West Virginia Central Credit Union'
  },
  {
    label: 'West Virginia Central Credit Union',
    value: 'West Virginia Central Credit Union'
  },
  {
    label: 'Wildfire Credit Union',
    value: 'Wildfire Credit Union'
  },
  {
    label: 'Wilson Bank & Trust',
    value: 'Wilson Bank & Trust'
  },
  {
    label: 'Woodsville Guaranty Savings Bank',
    value: 'Woodsville Guaranty Savings Bank'
  },
  {
    label: 'Wright-Patt Credit Union',
    value: 'Wright-Patt Credit Union'
  }
]
