import React, { Component, PropTypes } from 'react'

export default class WizardPage extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    scrollContainer: PropTypes.object
  };

  scroll = (event) => {
    const { scrollContainer } = this.props
    const { scrollTarget } = this.refs
    scrollContainer.scrollTop = scrollTarget.offsetTop - 20
  }

  render () {
    const { children } = this.props

    return (
      <div ref='scrollTarget'>
        {children}
      </div>
    )
  }
}
