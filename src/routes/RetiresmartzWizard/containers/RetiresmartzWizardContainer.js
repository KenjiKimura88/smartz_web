import { initialize as reduxFormInitialize, reduxForm } from 'redux-form'
import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { initialize } from '../helpers'
import { findQuery, findSingle } from 'redux/api/modules/requests'
import { getAccounts, getClient, getGoals } from 'helpers/requests'
import { getProfile } from 'redux/modules/auth'
import { set } from 'redux/modules/retiresmartz'
import RetiresmartzWizard from '../components/RetiresmartzWizard'
import schema from 'schemas/retirementPlan'

const requests = ({ params: { clientId, retirementPlanId } }) => ({
  accounts: getAccounts(clientId),
  client: getClient(clientId),
  createIncome: ({ create }) => create({
    type: 'retirementIncomes',
    url: `/clients/${clientId}/retirement-incomes`
  }),
  externalAccounts: ({ findAll }) => findAll({
    type: 'externalAccounts',
    url: '/quovo/get-accounts',
    propKey: 'externalAccounts'
  }),
  goals: getGoals(clientId),
  goalTypes: ({ findAll }) => findAll({
    type: 'goalTypes',
    url: '/goals/types'
  }),
  incomes: retirementPlanId && (({ findQuery }) => findQuery({
    type: 'retirementIncomes',
    url: `/clients/${clientId}/retirement-incomes`,
    query: {
      plan: parseInt(retirementPlanId, 10)
    }
  })),
  jointResendEmail: ({ create }) => create({
    type: 'jointResendEmail',
    url: `/accounts/joint/resend-email`
  }),
  peerGroupExpenses: ({ findAll }) => findAll({
    type: 'peerGroupExpenses',
    url: `/clients/${clientId}/peer-group-expenses/`
  }),
  retirementPlan: retirementPlanId && (({ findOne }) => findOne({
    type: 'retirementPlans',
    id: retirementPlanId,
    url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
  })),
  retirementPlans: ({ findAll }) => findAll({
    type: 'retirementPlans',
    url: `/clients/${clientId}/retirement-plans`
  }),
  saveFile: ({ update }) => update({
    type: 'retirementPlansUpload',
    id: clientId,
    url: `/clients/${clientId}/retirement-plans/upload`,
    multipart: true,
  }),
  saveRetirementPlan: ({ create, update }) => retirementPlanId
    ? update({
      type: 'retirementPlans',
      id: retirementPlanId,
      url: `/clients/${clientId}/retirement-plans/${retirementPlanId}`
    })
    : create({
      type: 'retirementPlans',
      url: `/clients/${clientId}/retirement-plans`
    }),
  settings: (({ findSingle }) => findSingle({
    type: 'globalSettings',
    url: '/settings'
  })),
  user: getProfile
})

const actions = {
  reduxFormInitialize,
  refreshClient: (clientId, callback) => findSingle({
    type: 'clients',
    id: clientId,
    force: true,
    success: callback
  }),
  reloadPeerGroupExpenses: (clientId, income, success) => findQuery({
    // used different type from `peerGroupExpenses` to prevent redux-form reintialize
    // with old values
    type: 'peerGroupExpensesQuery',
    url: `/clients/${clientId}/peer-group-expenses/`,
    query: { income },
    success
  }),
  set,
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter,
  reduxForm({
    form: 'retirementPlan',
    touchOnChange: true,
    ...schema
  }, (state, props) => ({
    initialValues: initialize(props)
  }))
)(RetiresmartzWizard)
