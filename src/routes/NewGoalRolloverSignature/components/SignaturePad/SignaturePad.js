import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import ReactSignaturePad from 'react-signature-pad'
import Button from 'components/Button'
import classes from './SignaturePad.scss'
import placeholderBg from './signature-placeholder.png'

const shouldHidePlaceholder = ({ value }, { drawing }) =>
  !R.isNil(value) && !R.isEmpty(value) || drawing

export default class SignaturePad extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.string
  };

  constructor(props) {
    super(props)
    this.state = {
      drawing: false
    }
  }

  componentDidMount() {
    this.resizeSignaturePad()
  }

  componentWillReceiveProps(nextProps) {
    const { signaturePad } = this.refs
    this.resizeSignaturePad()
    if (signaturePad && !R.equals(this.props.value, nextProps.value))
      signaturePad.fromDataURL(nextProps.value)
  }

  handleClear = () => {
    const { onChange } = this.props
    this.refs.signaturePad.clear()
    onChange()
  }

  handleSignatureBegin = () => {
    this.setState({ drawing: true })
  }

  handleSignatureChange = () => {
    const signaturePad = this.refs.signaturePad
    const { onChange } = this.props
    onChange(signaturePad.toDataURL())
    this.setState({ drawing: false })
  }

  resizeSignaturePad() {
    const { signaturePad, signatureWrapper } = this.refs
    const signatureCanvas = signaturePad.refs.cv
    if (!R.equals(signatureCanvas.width, signatureWrapper.offsetWidth)) {
      signatureCanvas.width = signatureWrapper.offsetWidth
      signatureCanvas.height = signatureWrapper.offsetHeight
    }
  }

  render () {
    const { props, state } = this
    const placeholderClass = classNames(classes.placeholder, {
      'hide': shouldHidePlaceholder(props, state)
    })
    return (
      <div className={classes.signatureBoard}>
        <Row className={classes.row}>
          <Col xs={10} md={11} className={classes.col}>
            <div className={classes.signatureWrapper} ref='signatureWrapper'>
              <div className={placeholderClass}>
                <img src={placeholderBg} alt='Sign here' />
              </div>
              <ReactSignaturePad ref='signaturePad'
                onBegin={this.handleSignatureBegin}
                onEnd={this.handleSignatureChange} />
            </div>
          </Col>
          <Col xs={2} md={1} className={classNames(classes.col, 'text-right')}>
            <Button onClick={this.handleClear}>Clear</Button>
          </Col>
        </Row>
      </div>
    )
  }
}
