import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import R from 'ramda'
import { MdWarning } from 'helpers/icons'
import classes from './AccountNotConfirmed.scss'
import Text from 'components/Text'

export class AccountNotConfirmed extends Component {
  static propTypes = {
    account: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  render () {
    const { account, handleHide, show } = this.props

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>Account setup not completed</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className={classes.panelBody}>
            <MdWarning size={80} className={classes.icon} />
            <Text size='large' className={classes.text}>
              <Text size='large' bold>{account.account_name}</Text> setup is not yet complete.<br />
              <a href='http://www.betasmartz.com/todo-labgroup-link' target='_blank'>
                Click here to complete it
              </a>.
            </Text>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'accountNotConfirmed' })
)(AccountNotConfirmed)
