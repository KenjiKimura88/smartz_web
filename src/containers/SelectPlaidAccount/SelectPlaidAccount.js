import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import { connect } from 'redux/api'
import Select from 'components/Select'

class SelectPlaidAccount extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    plaidAccounts: PropTypes.array.isRequired,
    value: PropTypes.string,
  };

  state = {
    selectedBankAccount: null
  };

  setSelectedBankAccount = (value) => {
    this.setState({
      selectedBankAccount: value
    })
  }

  render () {
    const { plaidAccounts, ...selectProps } = this.props
    const { selectedBankAccount } = this.state
    const bankAccountOptions = R.map(account => ({
      label: `${account.meta.name} - ${account.numbers.account || account.meta.number}`,
      value: `${account._id}`
    }), plaidAccounts)

    return (
      <Select options={bankAccountOptions} value={selectedBankAccount}
        onChange={this.setSelectedBankAccount} {...selectProps} />
    )
  }
}

const requests = {
  plaidAccounts: ({ findAll }) => findAll({
    type: 'plaidAccounts',
    url: '/plaid/get-accounts',
    footprint: R.prop('_id')
  })
}

export default R.compose(
  connect(requests)
)(SelectPlaidAccount)
