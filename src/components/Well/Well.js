import React, { Component, PropTypes } from 'react'
import { Well as BSWell } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './Well.scss'
import OverlayTooltip from 'components/OverlayTooltip'

/**
 * Custom wrapper around Bootstrap's Well component.  Accepts various
 * bsStyles not present in the original Bootstrap implementation, optional
 * tooltip (which will be accessible via an info icon in the top
 * right of the component), and a title.  All of these are styled according to
 * the BetaSmartz Style Guide.
 */
export default class Well extends Component {
  static propTypes = {
    bsSize: PropTypes.oneOf(['sm', 'small', 'lg', 'large']),
    bsStyle: PropTypes.oneOf(['none', 'default', 'white', 'success', 'danger']),
    children: PropTypes.node,
    className: PropTypes.string,
    noMargin: PropTypes.bool,
    title: PropTypes.string,
    tooltip: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    tooltipPlacement: PropTypes.oneOf(['left', 'top', 'bottom', 'right'])
  };

  static defaultProps = {
    bsSize: 'lg',
    bsStyle: 'default',
    tooltipPlacement: 'top'
  };

  render () {
    const { bsStyle, title, tooltip, tooltipPlacement, className, children, noMargin,
       ...props } = this.props
    const finalClassName = classNames(className, classes.well, {
      [classes[bsStyle]]: true,
      [classes.noMargin]: noMargin
    })

    return (
      <BSWell className={finalClassName} {...props}>
        {title && <h1 className={classes.title}>{title}</h1>}
        {children}
        {tooltip &&
          <div className={classes.tooltipWrapper}>
            <OverlayTooltip placement={tooltipPlacement} id={`well-info-${title}`}>
              {tooltip}
            </OverlayTooltip>
          </div>}
      </BSWell>
    )
  }
}
