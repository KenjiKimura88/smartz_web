import React, { Component, PropTypes } from 'react'
import { Col, Collapse, Grid, Panel, Row } from 'react-bootstrap'
import R from 'ramda'
import { isAnyTrue } from 'helpers/pureFunctions'
import { convertToTimestampPercentPairs, convertToTimestampValuePairs, ranges }
  from '../../helpers'
import { requestIsPending } from 'helpers/requests'
import Button from 'components/Button'
import classes from './Performance.scss'
import Graph from '../Graph'
import PageTitle from 'components/PageTitle'
import Selector from '../Selector'
import Spinner from 'components/Spinner'
import Topbar from '../../containers/Topbar'
import Well from 'components/Well'

const PLOT_LIMIT = 6

const canDisablePlot = enabledPlots => R.length(enabledPlots) > 1

const canEnablePlot = enabledPlots => R.length(enabledPlots) < PLOT_LIMIT

const getEnabledPlots = (key, enabledPlots, allPlots) =>
  R.map(
    plot => R.merge(
      plot,
      R.defaultTo({}, R.find(R.propEq(key, plot[key]), allPlots))
    ),
    enabledPlots
  )

const types = {
  values: {
    label: '$ Values',
    getData: ({ performanceState: { enabledBenchmarks, enabledGoals, startDate, endDate },
      selectedBenchmarks, selectedGoals }) =>
      R.concat(
        R.reduce((acc, benchmark) => R.concat(acc, R.length(benchmark.returns) ? [{
          ...(R.find(R.propEq('id', benchmark.id), enabledBenchmarks)),
          key: benchmark.description || benchmark.display_name,
          type: 'balance',
          values: convertToTimestampValuePairs(benchmark.returns, startDate, endDate)
        }] : []), [], selectedBenchmarks),
        R.reduce((acc, goal) => R.concat(acc, R.length(goal.performanceHistory) ? [{
          ...(R.find(R.propEq('id', goal.id), enabledGoals)),
          key: goal.name,
          type: 'balance',
          values: convertToTimestampValuePairs(goal.performanceHistory, startDate, endDate)
        }] : []), [], selectedGoals)
      )
  },
  returns: {
    label: '% Time-weighted Returns',
    getData: ({ performanceState: { enabledBenchmarks, enabledGoals, startDate, endDate },
      selectedBenchmarks, selectedGoals, trailing }) =>
      R.concat(
        R.reduce((acc, benchmark) => R.concat(acc, R.length(benchmark.returns) ? [{
          ...(R.find(R.propEq('id', benchmark.id), enabledBenchmarks)),
          key: benchmark.description || benchmark.display_name,
          type: 'balance',
          values: convertToTimestampPercentPairs(benchmark.returns, startDate, endDate)
        }] : []), [], selectedBenchmarks),
        R.reduce((acc, goal) => R.concat(acc, R.length(goal.performanceHistory) ? [{
          ...(R.find(R.propEq('id', goal.id), enabledGoals)),
          key: goal.name,
          type: 'balance',
          values: convertToTimestampPercentPairs(goal.performanceHistory, startDate, endDate)
        }] : []), [], selectedGoals)
      )
  }
}

// getIsPending :: Props -> Boolean
const getIsPending = R.converge(isAnyTrue, [
  requestIsPending('benchmarks'),
  requestIsPending('goals')
])

export default class Performance extends Component {
  static propTypes = {
    goals: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    performanceState: PropTypes.object,
    benchmarks: PropTypes.array.isRequired,
    selectedGoals: PropTypes.array,
    selectedBenchmarks: PropTypes.array,
    set: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired
  }

  render () {
    const { props } = this
    const { performanceState: { aboutOpened, activeRangeKey, activeTypeKey, enabledBenchmarks,
      enabledGoals, startDate, endDate }, set, benchmarks, goals } = props
    const trailing = ranges[activeRangeKey].trailing
    const dataProps = {
      ...R.pick(['performanceState', 'selectedBenchmarks', 'selectedGoals'], props),
      trailing
    }
    const data = types[activeTypeKey].getData(dataProps)
    const enabledItems = R.concat(enabledBenchmarks, enabledGoals)
    const canEnable = canEnablePlot(enabledItems)
    const canDisable = canDisablePlot(enabledItems)

    return (
      <Grid className={classes.performance}>
        <PageTitle title='Review and compare performance' />
        <Panel>
          {!canEnable && <Well bsStyle='danger'>
            Maximum of {PLOT_LIMIT} comparisons allowed. Remove some comparisons in
            order to add new ones.
          </Well>}
          <Row>
            <Col md={4}>
              <div className={classes.selectorWrapper}>
                <Selector options={types} activeOptionKey={activeTypeKey}
                  setActiveOption={function (key) {
                    set({ activeTypeKey: key })
                  }} />
              </div>
            </Col>
            <Col md={8}>
              <Topbar benchmarks={benchmarks} canDisable={canDisable} canEnable={canEnable}
                getEnabled={getEnabledPlots} goals={goals} />
            </Col>
          </Row>
          <div className={classes.graph}>
            {getIsPending(props) || R.isEmpty(data)
            ? <Spinner />
            : <div className={classes.graphInner}>
              <Graph datum={data} type={activeTypeKey} activeRangeKey={activeRangeKey}
                setActiveRange={function (key) { set({ activeRangeKey: key }) }}
                startDate={startDate} endDate={endDate} set={set} />
              <div className='text-right'>
                {aboutOpened
                  ? <Button onClick={function () { set({ aboutOpened: false }) }}
                    bsStyle='link'>Hide</Button>
                  : <Button onClick={function () { set({ aboutOpened: true }) }}
                    bsStyle='link'>Graph info</Button>}
              </div>
              <Collapse in={aboutOpened}>
                <Well>
                  <p><strong>Graph info</strong></p>
                  {R.equals(activeTypeKey, 'returns') &&
                    `This graph shows the time-weighted return of a goal with
                    benchmarks and model portfolios to compare against over a
                    selected period of time. A time-weighted return takes into
                    account external cash flows, these might include such things
                    as dividends, deposits and withdrawals, all of which are key
                    attributes of a goal portfolio. By showing the portfolio in
                    such a way, we can demonstrate performance against other
                    benchmarks and model allocations in a like-for-like manner.`
                  }
                  {R.equals(activeTypeKey, 'values') &&
                    `This graph shows your goal value over time, along with
                    contributions made to the goal. The line indicates values
                    at each point of time.`
                  }
                </Well>
              </Collapse>
            </div>}
          </div>
        </Panel>
      </Grid>
    )
  }
}
