import { createStructuredSelector } from 'reselect'
import R from 'ramda'
import { connect } from 'redux/api'
import { enabledBenchmarksSelector, enabledGoalsSelector, performanceSelector }
  from 'redux/selectors'
import { findManySelector } from 'redux/api/selectors'
import { getGoals } from 'helpers/requests'
import { set } from 'redux/modules/performance'
import { show } from 'redux-modal'
import Performance from '../components/Performance'

const requests = ({ params: { clientId } }) => ({
  benchmarks: ({ findAll }) => findAll({
    type: 'benchmarks'
  }),
  goals: getGoals(clientId)
})

const selector = createStructuredSelector({
  performanceState: performanceSelector,
  selectedBenchmarks: state =>
    findManySelector({
      type: 'benchmarks',
      ids: R.map(R.prop('id'), enabledBenchmarksSelector(state))
    })(state),
  selectedGoals: state =>
    findManySelector({
      type: 'goals',
      ids: R.map(R.prop('id'), enabledGoalsSelector(state))
    })(state)
})

const actions = { set, show }

export default connect(requests, selector, actions)(Performance)
