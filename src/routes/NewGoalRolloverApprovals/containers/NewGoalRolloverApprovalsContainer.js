import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import { rolloverApprovalsSchema, rolloverRequestSchema } from 'schemas/rolloverRequest'
import { show } from 'redux-modal'
import NewGoalRolloverApprovals from '../components/NewGoalRolloverApprovals'
import R from 'ramda'

const actions = {
  show
}

export default R.compose(
  connect(null, actions),
  withRouter,
  reduxForm({
    form: 'rolloverApprovals',
    destroyOnUnmount: false,
    propNamespace: 'rolloverApprovals',
    ...rolloverApprovalsSchema
  }),
  reduxForm({
    form: 'rolloverRequest',
    destroyOnUnmount: false,
    propNamespace: 'rolloverRequest',
    ...rolloverRequestSchema
  })
)(NewGoalRolloverApprovals)
