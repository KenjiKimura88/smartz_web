import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './Option.scss'
import { FaCheck } from 'helpers/icons'
import Text from 'components/Text'

export default class Option extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    selected: PropTypes.bool,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]).isRequired
  };

  render() {
    const { label, value, onClick, selected } = this.props
    return (
      <div
        className={classNames(classes.option, {[classes.selected]: selected})}
        onClick={onClick(value)}>
        <Text>{label}</Text>
        <span className={classes.checkmark}>
          {selected && <FaCheck size={14} />}
        </span>
      </div>
    )
  }
}
