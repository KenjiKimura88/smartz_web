import React, { Component, PropTypes } from 'react'
import { FormGroup } from 'react-bootstrap'
import { BzPlusCircle } from 'icons'
import { requestIsPending } from 'helpers/requests'
import Button from 'components/Button'
import Card from '../Card'
import classes from './ThirdParty.scss'
import ExternalAccounts from 'components/ExternalAccounts'
import Hr from 'components/Hr'
import NavHeader from '../NavHeader'
import QuovoFrame from 'containers/QuovoFrame'
import Sidebar from '../Sidebar'
import Well from 'components/Well'

export default class ThirdParty extends Component {
  static propTypes = {
    externalAccounts: PropTypes.array,
    fetchExternalAccounts: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func,
    invitation: PropTypes.object.isRequired,
    isLoading: PropTypes.bool,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    requests: PropTypes.object,
    save: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  componentWillMount () {
    this.syncExternalAccounts()
  }

  syncExternalAccounts = () => {
    const { fetchExternalAccounts } = this.props
    fetchExternalAccounts()
  }

  handleContinue = () => {
    const { push } = this.props
    const callback = () => push('almost-there')
    const { save, setValue } = this.props
    const handleSave = (onboardingValue) => {
      save({
        body: {
          onboarding_data: onboardingValue
        },
        success: callback,
        fail: callback
      })
    }
    setValue(['thirdparty'], { completed: true }, handleSave)
  }

  renderHeader() {
    return (
      <NavHeader {...this.props} onSaveAndQuit={function() {}} />
    )
  }

  renderSidebar() {
    const { invitation } = this.props

    return (
      <Sidebar title={`Sync Non-${invitation.firm_name} Accounts`}>
        <p>
          When you sync a non-{invitation.firm_name} account we do not store your
          credentials and our trusted partner for account aggregation protects the
          credentials using bank-level encryption both when transmitted and when stored.
        </p>
      </Sidebar>
    )
  }

  render () {
    const { props } = this
    const { isLoading, externalAccounts, show } = props
    const isLoadingExternalAccounts = requestIsPending('fetchExternalAccounts')(props)

    return (
      <div>
        {this.renderHeader()}
        <Card sidebar={this.renderSidebar()} isLoading={isLoading || isLoadingExternalAccounts}>
          <div className={classes.flexBox}>
            <Well>
                When you securely sync the accounts you hold with other institutions
                we can provide you with an overview of your whole wealth in one place
                and use this information to help you more accurately plan your finances.
                Don't worry, you can come back and do this at a later stage.
            </Well>
            <div className={classes.flexRow}>
              <FormGroup className='text-center'>
                <Button onClick={() => show('quovoFrame')}>
                  <BzPlusCircle /> Click here to sync accounts
                </Button>
              </FormGroup>
              <ExternalAccounts externalAccounts={externalAccounts} />
              <QuovoFrame onClose={this.syncExternalAccounts} />
            </div>
            <div>
              <Hr thick />
              <div className='text-right'>
                <Button bsStyle='thick-outline' onClick={this.handleContinue}>Continue</Button>
              </div>
            </div>
          </div>
        </Card>
      </div>
    )
  }
}
