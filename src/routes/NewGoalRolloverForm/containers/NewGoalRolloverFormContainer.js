import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import { rolloverRequestSchema as schema } from 'schemas/rolloverRequest'
import { show } from 'redux-modal'
import NewGoalRolloverForm from '../components/NewGoalRolloverForm'
import R from 'ramda'

const actions = {
  show
}

export default R.compose(
  connect(null, actions),
  withRouter,
  reduxForm({
    form: 'rolloverRequest',
    destroyOnUnmount: false,
    ...schema
  })
)(NewGoalRolloverForm)
