/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzCheck extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 75 64' {...this.props}>
        <g>
          <path d='M59.57,3a3.34,3.34,0,0,0-.42.56l0,.05L30.67,41.77l-9.5-9.57c-3.74-3.77-8.06-10-14.13-9-8.36,1.44-8.76,10.68-3.86,15.84,4.36,4.6,8.94,9,13.4,13.49,3.28,3.3,7,8.6,11.35,10.58,5.66,2.56,9.55-1.13,12.66-5.3L64.71,25.47c2.89-3.88,6.2-7.67,8.77-11.77C79.27,4.48,67.1-5,59.57,3Z' />
        </g>
      </IconBase>
    )
  }
}
