import React from 'react'
import { shallow } from 'enzyme'
import { BzTrash } from 'icons'
import configureStore from 'redux/createStore'
import Constraint from 'routes/Allocation/components/Constraint/Constraint'
import mountWithStore from '../../../test-helpers/mountWithStore'
import Panel from 'containers/Panel'
import Select from 'components/Select/Select'

describe('(Route) Allocation', () => {
  describe('(Component) Constraint', () => {
    const comparisons = [
      {
        id: 1,
        name: 'foo-comparison'
      },
      {
        id: 2,
        name: 'bar-comparison'
      }
    ]

    const features = [
      {
        id: 1,
        name: 'Group1',
        upper_limit: 0.2,
        values: [
          {
            id: 1,
            name: 'foo-feature-group1',
          },
          {
            id: 2,
            name: 'bar-feature-group1'
          }
        ]
      },
      {
        id: 2,
        name: 'Group2',
        upper_limit: 0.55,
        values: [
          {
            id: 3,
            name: 'foo-feature-group2'
          },
          {
            id: 4,
            name: 'bar-feature-group2'
          }
        ]
      },
      {
        id: 3,
        name: 'Group3-with-no-values'
      }
    ]

    const constraint = {
      comparison: {},
      configured_val: {},
      feature: {},
      id: {}
    }

    const getComponent = (props) =>
      <Constraint comparisons={comparisons} constraint={constraint} constraintValue={{}}
        features={features} id='constraint-test' remove={function () {}} toggle={function () {}}
        {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Panel)
    })

    it('renders description for currently selected values', () => {
      let wrapper = mountWithStore(getComponent(), configureStore())
      expect(wrapper.find('.panel-heading').text()).to.equal('–– ––')

      wrapper = mountWithStore(getComponent({
        constraint: {
          comparison: {
            value: 1
          }
        }
      }), configureStore())
      expect(wrapper.find('.panel-heading').text()).to.equal('foo-comparison ––')

      wrapper = mountWithStore(getComponent({
        constraint: {
          configured_val: {
            value: 0.15529230
          }
        }
      }), configureStore())
      // TODO: no idea why it renders {percentSign}, instead of %
      expect(wrapper.find('.panel-heading').text()).to.equal('–– 15.53{percentSign} ––')

      wrapper = mountWithStore(getComponent({
        constraint: {
          feature: {
            value: 4
          }
        }
      }), configureStore())
      expect(wrapper.find('.panel-heading').text()).to.equal('–– bar-feature-group2')
    })

    it('calls \'remove\' on remove button click', () => {
      const remove = sinon.spy()
      const wrapper = mountWithStore(getComponent({
        isExpanded: true,
        remove
      }), configureStore())
      remove.should.not.have.been.called
      wrapper.find(BzTrash).simulate('click')
      remove.should.have.been.calledOnce
      remove.should.have.been.calledWith()
    })

    it('renders feature dropdown with correct options', () => {
      const wrapper = mountWithStore(getComponent({
        isExpanded: true
      }), configureStore())
      const featureOptions = [
        {
          label: 'Group1',
          options: [
            {
              value: 1,
              label: 'foo-feature-group1',
              upperLimit: 0.2
            },
            {
              value: 2,
              label: 'bar-feature-group1',
              upperLimit: 0.2
            }
          ]
        },
        {
          label: 'Group2',
          options: [
            {
              value: 3,
              label: 'foo-feature-group2',
              upperLimit: 0.55
            },
            {
              value: 4,
              label: 'bar-feature-group2',
              upperLimit: 0.55
            }
          ]
        }
      ]
      expect(wrapper.find(Select).first().prop('options')).to.deep.equal(featureOptions)
    })

    it('renders comparison dropdown with correct options', () => {
      const wrapper = mountWithStore(getComponent({
        isExpanded: true
      }), configureStore())
      const comparisonOptions = [
        {
          value: 1,
          label: 'foo-comparison'
        },
        {
          value: 2,
          label: 'bar-comparison'
        }
      ]
      expect(wrapper.find(Select).last().prop('options')).to.deep.equal(comparisonOptions)
    })

    it(`formats the value for comparison input field, by converting to percentage and rounding to
      integer`, () => {
      let wrapper = mountWithStore(getComponent({
        isExpanded: true,
        constraint: {
          configured_val: {
            value: 0.3292
          }
        }
      }), configureStore())
      expect(wrapper.find('input[type="number"]').prop('value')).to.equal(33)
      wrapper = mountWithStore(getComponent({
        isExpanded: true,
        constraint: {
          configured_val: {
            value: 0.3249
          }
        }
      }), configureStore())
      expect(wrapper.find('input[type="number"]').prop('value')).to.equal(32)
    })

    it('updates the comparison value, on comparison input change', () => {
      const onChange = sinon.spy()
      const wrapper = mountWithStore(getComponent({
        isExpanded: true,
        constraint: {
          configured_val: {
            onChange,
            value: 0.09
          }
        }
      }), configureStore())
      onChange.should.not.have.been.called
      wrapper.find('input[type="number"]').simulate('change', { target: { value: 10 } })
      onChange.should.have.been.calledOnce
      onChange.should.have.been.calledWith(0.1)
      wrapper.find('input[type="number"]').simulate('change', { target: { value: 25.6327 } })
      onChange.should.have.been.calledWith(0.26)
    })

    // TODO
    xit('allows to input values within 0 and 100, for comparison value')

    // TODO
    xit('allows to input whole numbers only, for comparison value')
  })
})
