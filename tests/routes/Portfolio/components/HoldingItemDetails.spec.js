import React, { Component, PropTypes } from 'react'
import { Collapse } from 'react-bootstrap'
import HoldingItemDetails from 'routes/Portfolio/components/HoldingItemDetails'
import mountWithIntl from '../../../test-helpers/mountWithIntl'

describe('(Route) Portfolio', () => {
  describe('(Component) HoldingItemDetails', () => {
    class Table extends Component {
      static propTypes = {
        children: PropTypes.node
      };

      render() {
        return (
          <table>
            <tbody className='parent'>
              {this.props.children}
            </tbody>
          </table>
        )
      }
    }

    const holding = {
      value: 25000,
      percent: 35,
      tickers: [{
        id: 1,
        symbol: 'AAXJ',
        display_name: 'Stock Markets',
        url: 'http://example.com'
      }],
      shares: 3
    }

    const getComponent = (props) =>
      <Table>
        <HoldingItemDetails holding={holding} isActive {...props} />
      </Table>

    it('renders', () => {
      const wrapper = mountWithIntl(getComponent())
      expect(wrapper.find('.parent').children().first().type()).to.equal(HoldingItemDetails)
    })

    it('is shown when isActive is set', () => {
      const wrapper = mountWithIntl(getComponent({ isActive: true }))
      expect(wrapper.find(Collapse).prop('in')).to.equal(true)
    })

    it('is hidden when isActive is not set', () => {
      const wrapper = mountWithIntl(getComponent({ isActive: false }))
      expect(wrapper.find(Collapse).prop('in')).to.equal(false)
    })
  })
})
