import React, { Component, PropTypes } from 'react'
import classes from './LabelValue.scss'
import Label from 'components/Label'
import Value from 'components/Value'

/**
 * Render a label-value pair, vertically aligned, with capitalized label.
 * Example:
 *
 *   FULL NAME
 *   John doe
 */
export default class LabelValue extends Component {
  static propTypes = {
    children: PropTypes.node,
    label: PropTypes.any.isRequired,
    value: PropTypes.any
  };

  render () {
    const { children, label, value } = this.props

    return (
      <dl className={classes.labelValue}>
        <Label>{label}</Label>
        <Value>{children || value}</Value>
      </dl>
    )
  }
}
