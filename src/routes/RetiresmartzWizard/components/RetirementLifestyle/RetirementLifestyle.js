import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import classNames from 'classnames'
import R from 'ramda'
import { BzEat, BzHealth, BzHolidays, BzInterests, BzLeisure, BzWork } from 'icons'
import { domOnlyProps, round } from 'helpers/pureFunctions'
import { MdStar, MdStarOutline } from 'helpers/icons'
import Button from 'components/Button'
import classes from './RetirementLifestyle.scss'
import comfortableImage from './assets/comfortable.jpg'
import CurrencyInput from 'components/CurrencyInput'
import doingOkImage from './assets/doing_ok.jpg'
import doingWellImage from './assets/doing_well.jpg'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import luxuryImage from './assets/luxury.jpg'
import Panel from 'containers/Panel'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Select from 'components/Select'
import Text from 'components/Text'

const defaultLifestyleDescription = 'No description available'

const lifestyleAspects = [
  {
    id: 'holidays',
    label: 'Holidays',
    icon: BzHolidays
  },
  {
    id: 'eating_out',
    label: 'Eating Out',
    icon: BzEat
  },
  {
    id: 'health',
    label: 'Health',
    icon: BzHealth
  },
  {
    id: 'interests',
    label: 'Interests',
    icon: BzInterests
  },
  {
    id: 'leisure',
    label: 'Leisure',
    icon: BzLeisure
  },
  {
    id: 'work',
    label: 'Work',
    icon: BzWork,
    getValue: ({ paidWork: { value: paidValue }, volunteerWork: { value: volunteerValue } }) =>
      `${volunteerValue || 0} day's volunteer; ${paidValue || 0} day's paid`
  }
]

const lifestyleCategoriesInfo = {
  1: {
    label: 'doingOk',
    multiplier: 0.66,
    buttonImage: doingOkImage,
    stars: 3,
    className: classes.doingOk
  },
  2: {
    label: 'comfortable',
    multiplier: 0.81,
    buttonImage: comfortableImage,
    stars: 4,
    className: classes.comfortable
  },
  3: {
    label: 'doingWell',
    multiplier: 1.0,
    buttonImage: doingWellImage,
    stars: 5,
    className: classes.doingWell
  },
  4: {
    label: 'luxury',
    multiplier: 1.5,
    buttonImage: luxuryImage,
    stars: 6,
    className: classes.luxury
  }
}

const volunteerWorkOptions = [
  {
    value: 0,
    label: '0 Days'
  },
  {
    value: 1,
    label: '1 Days'
  },
  {
    value: 2,
    label: '2 Days'
  },
  {
    value: 3,
    label: '3 Days'
  },
  {
    value: 4,
    label: '4 Days'
  },
  {
    value: 5,
    label: '5 Days'
  },
  {
    value: 6,
    label: '6 Days'
  },
  {
    value: 7,
    label: '7 Days'
  }
]

const paidWorkOptions = volunteerWorkOptions

// getLifestyleCategories :: Props -> [Object]
const getLifestyleCategories = R.compose(
  R.map(R.converge(R.merge, [
    R.compose(R.flip(R.prop)(lifestyleCategoriesInfo), R.prop('id')),
    R.identity
  ])),
  R.prop('lifestyleCategories')
)

// getCurrentLifestyleCategory :: Props -> Object
const getCurrentLifestyleCategory = R.converge(R.find, [
  R.compose(R.propEq('id'), R.path(['lifestyleCategory', 'value'])),
  getLifestyleCategories
])

// getMinimumRetirementIncome :: Prop -> Category -> Number
const getMinimumRetirementIncome = R.curry(({ householdIncome }, { multiplier }) =>
  multiplier * householdIncome
)

// getLifestyleCategoryForRetirementIncome :: Number -> Props -> Object
const getLifestyleCategoryForRetirementIncome = (retirementIncome, props) =>
  R.compose(
    R.prop('id'),
    R.reduce(R.maxBy(getMinimumRetirementIncome(props)), { multiplier: 0 }),
    R.filter(R.compose(R.gte(retirementIncome), getMinimumRetirementIncome(props))),
    getLifestyleCategories
  )(props)

const getLifestyleForRetirementIncome = (retirementIncome, { lifestyles }) =>
  R.compose(
    R.reduce(R.maxBy(R.prop('cost')), { cost: -1 }),
    R.filter(R.compose(R.gte(retirementIncome), R.prop('cost')))
  )(lifestyles)

export default class RetirementLifestyle extends Component {
  static propTypes = {
    householdIncome: PropTypes.number.isRequired,
    lifestyleCategories: PropTypes.array.isRequired,
    lifestyleCategory: PropTypes.object.isRequired,
    lifestyles: PropTypes.array.isRequired,
    paidWork: PropTypes.object.isRequired,
    retirementIncome: PropTypes.object.isRequired,
    volunteerWork: PropTypes.object.isRequired
  };

  handleCategoryChange = (id) => {
    const { props } = this
    const { householdIncome, lifestyleCategory, paidWork, retirementIncome, volunteerWork } = props
    const category = lifestyleCategoriesInfo[id]
    if (category) {
      const newRetirementIncome = round(category.multiplier * householdIncome, 0)
      const newLifestyle = getLifestyleForRetirementIncome(newRetirementIncome, props)
      lifestyleCategory.onChange(id)
      paidWork.onChange(newLifestyle.default_paid_days)
      retirementIncome.onChange(newRetirementIncome)
      volunteerWork.onChange(newLifestyle.default_volunteer_days)
    }
  }

  handleRetirementIncomeChange = (value) => {
    const { props } = this
    const { lifestyleCategory, retirementIncome } = props
    const currentLifestyleCategory = getCurrentLifestyleCategory(props)
    const newLifestyleCategoryId =
      getLifestyleCategoryForRetirementIncome(value, props) ||
      R.compose(R.prop('id'), R.head, getLifestyleCategories)(props)

    if (newLifestyleCategoryId && !R.equals(currentLifestyleCategory.id, newLifestyleCategoryId)) {
      lifestyleCategory.onChange(newLifestyleCategoryId)
    }
    retirementIncome.onChange(value)
  }

  render() {
    const { handleCategoryChange, handleRetirementIncomeChange, props } = this
    const { lifestyleCategory, paidWork, retirementIncome, volunteerWork } = props
    const lifestyleCategories = getLifestyleCategories(props)
    const currentLifestyleCategory = getCurrentLifestyleCategory(props)
    const currentLifestyle = getLifestyleForRetirementIncome(retirementIncome.value, props)

    return (
      <div className={classes.retirementLifestyle}>
        <H2 noMargin>Select your retirement lifestyle</H2>
        <Row>
          <Col xs={8}>
            <Text light>
              If you don't have a picture of what the future looks like it's hard to work towards
              it.<br /> We encourage you to picture yourself in relation to one of four retirement
              lifestyles.
            </Text>
          </Col>
        </Row>
        <FormGroup id='lifestyleCategory' className={classes.lifestyleFormGroup}>
          <RadioButtonGroup className={classes.lifestyleButtonGroup}
            {...domOnlyProps(lifestyleCategory)} onChange={handleCategoryChange}>
            {R.map(({ buttonImage, className, id, stars, title }) =>
              <Button key={id} value={id}
                className={classNames(classes.lifestyleButton, className)}>
                <div className={classes.lifestyleContent}>
                  {R.map(i => <MdStarOutline key={i} size={20} />
                    , R.range(0, 6 - stars))}
                  {R.map(i => <MdStar key={i} size={20} />, R.range(0, stars))}
                  <div className={classes.lifestyleText}>
                    <Text size='normal'>{title}</Text>
                  </div>
                </div>
                <div className={classes.lifestyleBg}
                  style={{ backgroundImage: `url(${buttonImage})` }} />
              </Button>
            , lifestyleCategories)}
          </RadioButtonGroup>
          <FieldError for={lifestyleCategory} />
        </FormGroup>
        {currentLifestyleCategory &&
          <FormGroup>
            <Panel id='lifestyleAspect'
              className={classNames(classes.panel, classes[currentLifestyleCategory.label])}>
              <Row>
                <Col xs={6} className={classes.panelColumn}>
                  {R.map(({ icon: Icon, id, label, getValue }) =>
                    <div key={id} className={classes.lifestyleAspect}>
                      <Text light className={classes.lifestyleAspectIcon}>
                        <Icon size={21} />
                      </Text>
                      <div className={classes.lifestyleAspectDetails}>
                        <div className={classes.lifestyleAspectLabel}>
                          <Text>{label}</Text>
                        </div>
                        {getValue
                          ? <Text size='small'>{getValue(props)}</Text>
                          : currentLifestyle && currentLifestyle[id]
                            ? <Text size='small'>{currentLifestyle[id]}</Text>
                            : <Text size='small' light>{defaultLifestyleDescription}</Text>}
                      </div>
                    </div>
                  , lifestyleAspects)}
                </Col>
                <Col xs={6} className={classes.panelColumn}>
                  <FormGroup className={classes.replacementRatioFormGroup}>
                    <ControlLabel className={classes.replacementRatioLabel}>
                      Replacement Ratio (Pre-retirement income in retirement)
                    </ControlLabel>
                    <FormControl.Static>
                      <Text size='large'>
                        <FormattedNumber value={currentLifestyleCategory.multiplier}
                          format='percent' />
                      </Text>
                    </FormControl.Static>
                  </FormGroup>
                  <FormGroup controlId='retirement-income'>
                    <ControlLabel>
                      Retirement Income (In today's $)
                    </ControlLabel>
                    <CurrencyInput {...domOnlyProps(retirementIncome)}
                      onChange={handleRetirementIncomeChange} precision={0} />
                    <FieldError for={retirementIncome} />
                  </FormGroup>
                  <Row>
                    <Col xs={6}>
                      <FormGroup id='volunteerWork'>
                        <ControlLabel>Volunteer Work</ControlLabel>
                        <Select options={volunteerWorkOptions} onChange={volunteerWork.onChange}
                          placeholder='Select days' value={volunteerWork.value} />
                        <FieldError for={volunteerWork} />
                      </FormGroup>
                    </Col>
                    <Col xs={6}>
                      <FormGroup id='paidWork'>
                        <ControlLabel>Paid Work</ControlLabel>
                        <Select options={paidWorkOptions} onChange={paidWork.onChange}
                          placeholder='Select days' value={paidWork.value} />
                        <FieldError for={paidWork} />
                      </FormGroup>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Panel>
          </FormGroup>}
      </div>
    )
  }
}
