import R from 'ramda'
import { findSingleSelector } from 'redux/api/selectors'

export default R.curry(({ store }, nextState, replace, callback) => {
  // We only ever request one client (the current user)
  // FIXME: Need a safer way to get the current client
  const client = findSingleSelector({
    type: 'clients'
  })(store.getState())

  if (client) {
    replace(`/${client.id}`)
  }

  callback()
})
