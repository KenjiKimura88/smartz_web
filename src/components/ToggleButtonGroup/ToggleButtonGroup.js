import React, { Component, PropTypes } from 'react'
import { ButtonGroup } from 'react-bootstrap'
import R from 'ramda'
import { mapIndexed } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from './ToggleButtonGroup.scss'

export default class ToggleButtonGroup extends Component {
  static propTypes = {
    activeKey: PropTypes.string.isRequired,
    bsSize: PropTypes.oneOf([
      'xsmall', 'small', 'large'
    ]),
    justified: PropTypes.bool,
    onSelect: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired
  };

  render () {
    const { activeKey, bsSize, justified, onSelect, options } = this.props

    return (
      <ButtonGroup className={classes.buttonGroup} bsSize={bsSize} justified={justified}>
        {mapIndexed(({ key, label, disabled }, index) =>
          <Button href='#' key={index} active={R.equals(activeKey, key)} disabled={disabled}
            onClick={function () { onSelect(key) }}>
            {label}
          </Button>
        , options)}
      </ButtonGroup>
    )
  }
}
