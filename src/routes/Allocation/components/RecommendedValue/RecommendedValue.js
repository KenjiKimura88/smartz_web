import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classNames from 'classnames'
import classes from './RecommendedValue.scss'
import LabelValue from 'components/LabelValue'
import Well from 'components/Well'

export default class RecommendedValue extends Component {
  static propTypes = {
    display: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired
  };

  render () {
    const { display, onChange, value } = this.props

    return (
      <Well className={classes.recommendedValue}>
        <Row>
          <Col xs={7}>
            <LabelValue label='Recommended'>
              {display}
            </LabelValue>
          </Col>
          <Col xs={5} className={classNames('text-right', classes.applyButton)}>
            <a onClick={function () { onChange(value) }}>Apply this</a>
          </Col>
        </Row>
      </Well>
    )
  }
}
