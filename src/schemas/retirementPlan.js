import buildSchema from 'redux-form-schema'
import R from 'ramda'
import { isMarried as _isMarried } from 'routes/RetiresmartzWizard/helpers'
import { mapKeys } from 'helpers/pureFunctions'
import config from 'config'

const { maritalStatuses } = config

const validateCurrency = {
  validate: {
    float: {
      min: 0
    }
  }
}

// isMarried :: Values -> Boolean
const isMarried = R.compose(
  _isMarried,
  R.prop('maritalStatus')
)

// ------------------------------------
// Objects Fields
// ------------------------------------
export const expenseFields = {
  id: {
    label: 'Id',
    required: true
  },
  amt: {
    label: 'Amount',
    required: true,
    ...validateCurrency
  },
  cat: {
    label: 'Category',
    required: true
  },
  who: {
    label: 'User',
    required: true
  },
  desc: {
    label: 'Description'
  }
}

export const retirementAccountFields = {
  id: {
    label: 'Id',
    required: true
  },
  name: {
    label: 'Name',
    required: true
  },
  cat: {
    label: 'Category',
    required: true
  },
  acc_type: {
    label: 'Account Type',
    required: true
  },
  owner: {
    label: 'Owner',
    required: true
  },
  balance: {
    label: 'Account Balance',
    required: true,
    ...validateCurrency
  },
  balance_efdt: {
    label: 'Balance Effective Date',
    required: true
  },
  contrib_amt: {
    label: 'Contribution Amount',
    required: true,
    ...validateCurrency
  },
  contrib_period: {
    label: 'Contribution Period',
    required: true,
  },
  employer_match: {
    label: 'Employer Matching Percentage',
    required: ({ employer_match_type: emt }) => R.contains(emt, ['income', 'contributions']),
    validate: {
      validateEmployerMatch: (formValues, fieldValue) =>
        R.equals(formValues.employer_match_type, 'income')
        ? (fieldValue >= 0 && fieldValue <= 0.05)
        : (fieldValue >= 0 && fieldValue <= 1)
    }
  },
  employer_match_type: {
    label: 'Employer Matching Type',
    required: true
  }
}

export const incomeFields = {
  id: {
    label: 'ID'
  },
  name: {
    label: 'name',
    required: true
  },
  beginDate: {
    label: 'Start date',
    required: true,
    type: 'date'
  },
  amount: {
    label: 'Amount',
    required: true,
    ...validateCurrency
  },
  accountType: {
    label: 'Account type',
    required: true,
  },
  growth: {
    label: 'growth',
    required: true,
    type: 'decimal'
  },
  schedule: {
    label: 'Schedule',
    required: true
  }
}

export const depositFields = {
  goalOrAsset: {
    label: 'External Goal/Asset',
    required: true
  },
  amount: {
    label: 'Amount',
    required: true,
    ...validateCurrency
  }
}

// ------------------------------------
// Per-page Fields
// ------------------------------------
const assumptionsAndSettingsFields = {
  financialConfidence: {
    label: 'Planned Financial confidence',
    required: true,
    validate: {
      int: {
        min: 50,
        max: 99
      }
    }
  },
  projectedIncomeGrowth: {
    label: 'Projected Income Growth',
    required: true,
    validate: {
      int: {
        min: 0,
        max: 100
      }
    }
  }
}

const cashFlowFields = {
  ...mapKeys(R.concat('expenses[].'), expenseFields)
}

const financesFields = {
  ...mapKeys(R.concat('incomes[].'), incomeFields),
  ...mapKeys(R.concat('retirementAccounts[].'), retirementAccountFields),
  homeValue: {
    label: 'Home value',
    required: true
  },
  retirementGoals: {
    label: 'Goals for retirement spending'
  },
  socialSecurityClient: {
    label: 'Your social security statement'
  },
  socialSecurityPartner: {
    label: 'Your partner social security statement'
  },
  ssFraTodays: {
    label: 'SS FRA Todays',
    required: true,
    ...validateCurrency
  },
  estimateDate: {
    label: 'Date of Estimate',
    required: true,
    type: 'date'
  }
}

const personalDetailsFields = {
  clientIncome: {
    label: 'Your income',
    required: true,
    ...validateCurrency
  },
  managePartnerAccounts: {
    label: 'Manage partner accounts',
    type: 'boolean'
  },
  maritalStatus: {
    label: 'Marital Status',
    required: true,
    validate: {
      validateMaritalStatus: (formValues, fieldValue) =>
        R.contains(fieldValue, R.values(maritalStatuses))
    }
  },
  partnerBirthdate: {
    label: 'Your spouse date of birth',
    required: isMarried,
    type: 'date'
  },
  partnerName: {
    label: 'Your spouse name',
    required: isMarried
  },
  partnerIncome: {
    label: 'Your spouse income',
    required: isMarried,
    ...validateCurrency
  },
  partnerSsn: {
    label: 'Your spouse SSN',
    required: isMarried
  }
}

const yourRetirementFields = {
  lifestyleCategory: {
    label: 'Lifestyle',
    required: true
  },
  paidWork: {
    label: 'Paid work',
    required: true,
    validate: {
      int: {
        min: 0,
        max: 7
      }
    }
  },
  retirementIncome: {
    label: 'Retirement income',
    required: true,
    ...validateCurrency
  },
  volunteerWork: {
    label: 'Volunteer work',
    required: true,
    validate: {
      int: {
        min: 0,
        max: 7
      }
    }
  },
  ballparkEstimate: {
    label: 'Ballpark estimate',
    ...validateCurrency
  },
  downsizeOrCondo: {
    label: 'Downsize',
    type: 'boolean'
  },
  dreamLocation: {
    label: 'Dream location'
  },
  remainInSameHome: {
    label: 'Remain in same home',
    required: true,
    type: 'boolean'
  },
  reverseMortgage: {
    label: 'Reverse mortgage',
    required: true,
    type: 'boolean'
  },
  sameLocation: {
    label: 'Same location',
    type: 'boolean'
  }
}

const planningFields = {
  clientBtc: {
    label: 'Before-Tax Retirement Contribution',
    type: 'decimal'
  },
  dailyExercise: {
    label: 'Daily Exercise'
  },
  drinks: {
    label: 'Alcoholic Drinks Consumption'
  },
  height: {
    label: 'Height'
  },
  lifeExpectancy: {
    label: 'Life Expectancy',
    type: 'decimal'
  },
  partnerBtc: {
    label: 'Partner\'s Before-Tax Retirement Contribution',
    type: 'decimal'
  },
  partnerDailyExercise: {
    label: 'Partners Daily Exercise'
  },
  partnerDrinks: {
    label: 'Partner\'s Alcoholic Drinks Consumption'
  },
  partnerHeight: {
    label: 'Partner\'s Height'
  },
  partnerLifeExpectancy: {
    label: 'Partner\'s Life Expectancy'
  },
  partnerRetirementAge: {
    label: 'Partner\'s Retirement Age'
  },
  partnerSmoker: {
    label: 'Partner\'s Smoker'
  },
  partnerWeight: {
    label: 'Partner\'s Weight'
  },
  retirementAge: {
    label: 'Retirement Age',
    validate: {
      int: {
        min: 0
      }
    }
  },
  risk: {
    label: 'Risk',
    validate: {
      float: {
        min: 0,
        max: 1
      }
    }
  },
  smoker: {
    label: 'Smoker',
    type: 'boolean'
  },
  weight: {
    label: 'Weight'
  }
}

// -------------------------------------------
// Schema (includes the fields for all pages)
// -------------------------------------------
const schema = buildSchema({
  ...assumptionsAndSettingsFields,
  ...cashFlowFields,
  ...financesFields,
  ...personalDetailsFields,
  ...yourRetirementFields,
  ...planningFields
})

// Extend validation to validate fields collections like expenses
const collections = ['expenses', 'incomes', 'retirementAccounts']
const collectionSchemas = {
  expenses: buildSchema(expenseFields),
  incomes: buildSchema(incomeFields),
  retirementAccounts: buildSchema(retirementAccountFields)
}
const collectionValidations = values => R.reduce((acc, collectionName) =>
  R.assoc(
    collectionName,
    R.map(collectionSchemas[collectionName].validate, values[collectionName]),
    acc
  )
, {}, collections)

const isNotCollectionField = (value, key) => R.compose(
  R.none(R.identity),
  R.map((searchKey) => R.test(new RegExp(`^${searchKey}`), key)),
  R.map((c) => `${c}\\[\\]\\.`)
)(collections)

const validate = values => R.compose(
  R.reject(R.either(R.isEmpty, R.isNil)),
  R.merge(R.pickBy(isNotCollectionField, schema.validate(values))),
  collectionValidations
)(values)

export default R.merge(schema, { validate })
