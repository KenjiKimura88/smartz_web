import React, { Component, PropTypes } from 'react'
import { ProgressBar as BsProgressBar } from 'react-bootstrap'
import classes from './ProgressBar.scss'

export default class ProgressBar extends Component {
  static propTypes = {
    className: PropTypes.string
  };

  render () {
    const { className, ...props } = this.props

    return (
      <div className={classes.progressBarWrapper}>
        <BsProgressBar className={className} {...props} />
      </div>
    )
  }
}
