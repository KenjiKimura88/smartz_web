import React, { Component, PropTypes } from 'react'
import CircleSelect from 'components/CircleSelect'
import classes from './NewGoalEmployerPlan.scss'
import employerPlanOptions from '../../employerPlanOptions'
import goToNewGoalMenu from 'routes/NewAccountTypes/helpers'

export default class NewGoalEmployerPlan extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    createAccount: PropTypes.func,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  handleChange = (option) => {
    const { props } = this
    const { params: { clientId }, router: { push } } = props

    goToNewGoalMenu(props, option, () => {
      push(`/${clientId}/new-goal/rollover/${option.value}`)
    })
  }

  render () {
    return (
      <div className={classes.wrapper}>
        <CircleSelect options={employerPlanOptions}
          onChange={this.handleChange}
          title='Select an Employer Plan' />
      </div>
    )
  }
}
