import React from 'react'
import { shallow } from 'enzyme'
import InlineEdit from 'components/InlineEdit/InlineEdit'

describe('(Component) InlineEdit', () => {
  const getComponent = (props) =>
    <InlineEdit />

  it('renders as a <span />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('span')
  })
})
