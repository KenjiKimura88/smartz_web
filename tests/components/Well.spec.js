import React from 'react'
import { shallow, mount } from 'enzyme'
import { Well as BSWell } from 'react-bootstrap'
import classes from 'components/Well/Well.scss'
import OverlayTooltip from 'components/OverlayTooltip'
import Well from 'components/Well'

describe('(Component) Well', () => {
  const getComponent = props => <Well {...props} />

  it(`renders a Bootstrap <Well />`, () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(BSWell)
  })

  it('can have a custom class', () => {
    const wrapper = shallow(getComponent({className: 'class'}))
    expect(wrapper.hasClass('class')).to.be.true
    expect(wrapper.hasClass(classes.well)).to.be.true
  })

  it('renders a tooltip when provided', () => {
    const withTooltip = mount(getComponent({ tooltip: 'tooltip' }))
    const withoutTooltip = mount(getComponent())
    expect(withTooltip.find(OverlayTooltip).length).to.equal(1)
    expect(withoutTooltip.find(OverlayTooltip).length).to.equal(0)
  })

  it('accepts a either a string or element as a tooltip', () => {
    const spy = sinon.spy(console, 'error')
    mount(getComponent({ tooltip: 'tooltip' }))
    mount(getComponent({ tooltip: <tooltip /> }))

    expect(spy.called).to.be.false
    console.error.restore() // eslint-disable-line
  })
})
