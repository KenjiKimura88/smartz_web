import React, { Component, PropTypes } from 'react'
import { Col, FormControl, FormGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { BzArrowDown, BzArrowUp } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import classes from './AssumptionsAndSettings.scss'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import PercentageInput from 'components/PercentageInput'
import Slider from 'components/Slider/Slider'

const renderToggle = (key, value, toggleExpanded) =>
  <a className={classes.toggle} onClick={() => toggleExpanded(key)}>
    {value ? <BzArrowUp /> : <BzArrowDown />}
  </a>

export default class AssumptionsAndSettings extends Component {
  static propTypes = {
    financialConfidence: PropTypes.object.isRequired,
    projectedIncomeGrowth: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)

    this.state = {
      expandedIncomeGrowth: false,
      expandedFinancialConfidence: false
    }
  }

  toggleExpanded = (key) => {
    const value = this.state[key]
    this.setState({
      [key]: !value
    })
  }

  render() {
    const { financialConfidence, projectedIncomeGrowth } = this.props
    const { expandedIncomeGrowth, expandedFinancialConfidence } = this.state

    return (
      <div>
        <Row>
          <Col xs={6}>
            <H2>
              Projected Income Growth
              {renderToggle('expandedIncomeGrowth', expandedIncomeGrowth, this.toggleExpanded)}
            </H2>
            {expandedIncomeGrowth && <Row>
              <Col xs={6}>
                <FormGroup controlId='projectedIncomeGrowth'>
                  <PercentageInput type='percent' min={0} max={100} step={1}
                    {...domOnlyProps(projectedIncomeGrowth)} />
                  <FieldError for={projectedIncomeGrowth} />
                </FormGroup>
              </Col>
            </Row>}
          </Col>
          <Col xs={6}>
            <H2>
              Planned Financial confidence
              {renderToggle('expandedFinancialConfidence', expandedFinancialConfidence,
                this.toggleExpanded)}
            </H2>
            {expandedFinancialConfidence && <Row>
              <Col xs={6}>
                <FormGroup>
                  {R.is(Number, financialConfidence.value) &&
                    <Slider className={classes.financialConfidenceSlider} min={50} max={99}
                      {...domOnlyProps(financialConfidence)} />}
                  <FieldError for={financialConfidence} />
                </FormGroup>
              </Col>
              <Col xs={6}>
                <FormGroup>
                  <FormControl type='number' min={50} max={99} step={1}
                    {...domOnlyProps(financialConfidence)} />
                </FormGroup>
              </Col>
            </Row>}
          </Col>
        </Row>
        <Hr />
      </div>
    )
  }
}
