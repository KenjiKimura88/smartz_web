import React, { Component } from 'react'
import { Col, Grid, Row } from 'react-bootstrap'
import { MdLocalPhone, MdMailOutline } from 'helpers/icons'
import classes from './Footer.scss'
import logo from './betasmartz-logo.png'
import P from 'components/P'
import Text from 'components/Text'

export default class Footer extends Component {
  shouldComponentUpdate () {
    return false
  }

  render () {
    return (
      <footer className={classes.footer}>
        <div className={classes.lightArea}>
          <Grid>
            <ul className={classes.links}>
              <li className={classes.link}>
                <Text bold>Technical support</Text>
              </li>
              <li className={classes.link}>
                <MdMailOutline size='18' className={classes.linkIcon} />
                <a href='mailto:support@betasmartz.com'>Email us</a>
              </li>
              <li className={classes.link}>
                <MdLocalPhone size='18' className={classes.icon} />
                Call 212‑634‑4832
              </li>
            </ul>
          </Grid>
        </div>
        <div className={classes.darkArea}>
          <Grid>
            <Row className={classes.disclosureRow}>
              <Col xs={4}>
                <P>
                  <Text className={classes.bold}>
                    BetaSmartz<sup>®</sup> is made available through independent
                    investment advisors and is sponsored by BetaSmartz Advisory
                    LLC ("BetaSmartz"). BetaSmartz (SEC# 801-108797) is an investment
                    adviser registered the with Securities and Exchange Commission
                    (the “SEC”) pursuant to the Investment Advisers Act of 1940.
                    BetaSmartz is a subsidiary of BetaSmartz Global Pte. Ltd.
                    ("BetaSmartz Global").
                  </Text>
                </P>
                <P>
                  <Text>
                    Unless otherwise specified, all return figures shown are for illustrative
                    purposes only, and are not actual customer or model returns. Actual returns
                    will vary greatly and depend on personal and market circumstances.
                    No representation, warranty or undertaking, express or implied, is given as
                    to the accuracy or completeness of the information herein.
                  </Text>
                </P>
              </Col>
              <Col xs={4}>
                <P>
                  <Text>
                    Brokerage and custody services are provided by participating
                    broker-dealers registered with the SEC pursuant to Section 15
                    of the Securities Exchange Act of 1934 and are members of the
                    Financial Industry Regulatory Authority and the Securities
                    Investor Protection Corporation.
                  </Text>
                </P>
                <P>
                  <Text>
                    Independent investment advisors and BetaSmartz are independent
                    of each other and are not affiliated with, sponsored by,
                    endorsed by, or supervised by each other.
                  </Text>
                </P>
                <P>
                  <Text>
                    Any links are provided purely as a convenience and not as an
                    endorsement of any linked website or its sponsors or contributors.
                  </Text>
                </P>
                <P>
                  <Text>
                    Investing in securities involves risks, and there is always the
                    potential of losing money when you invest in securities.
                  </Text>
                </P>
              </Col>
              <Col xs={4}>
                <P>
                  <Text>
                    Before investing, consider your investment objectives and your
                    independent investment advisor and BetaSmartz’s charges and
                    expenses. Past performance does not guarantee future results,
                    and the likelihood of investment outcomes are hypothetical
                    in nature. See full disclosures for more information.
                    Nothing herein constitutes an offer, solicitation of an offer,
                    or advice to buy or sell securities in jurisdictions where
                    BetaSmartz is not registered.
                  </Text>
                </P>
                <P>
                  <Text>
                    Unauthorized access is prohibited. Usage is monitored.
                  </Text>
                </P>
                <P>
                  <Text>
                    ©2017 BetaSmartz Global Pte. Ltd. All rights reserved.
                    BetaSmartz is a trademark of BetaSmartz Global Pte. Ltd.
                  </Text>
                </P>
                <P>
                  <Text>
                    <a href='/privacy-notice' target='_blank'>
                      BetaSmartz Privacy Notice
                    </a>&nbsp;|&nbsp;
                    <a href='/terms-of-use' target='_blank'>
                      Terms of Use
                    </a>
                  </Text>
                </P>
                <P>
                  <Text>
                    Contact: BetaSmartz, 375 Park Avenue, Suite 2607, New York, NY, 10152.
                    Tel: 212‑634‑4832
                  </Text>
                </P>
              </Col>
            </Row>
            <Col xs={12} className={classes.logoRow}>
              <div className={classes.poweredBy}>Powered by</div>
              <figure className={classes.logo}>
                <img src={logo} alt='BetaSmartz' />
              </figure>
              <figcaption>©2016 BetaSmartz Global Pte. Ltd.</figcaption>
            </Col>
          </Grid>
        </div>
      </footer>
    )
  }
}
