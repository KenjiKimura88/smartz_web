import React, { Component } from 'react'
import R from 'ramda'
import uuid from 'uuid'
import { domOnlyProps } from 'helpers/pureFunctions'
import classes from './Switch.scss'

export default class Switch extends Component {
  componentWillMount () {
    this.id = uuid.v4()
  }

  render () {
    const { props } = this
    const { checked } = props
    const finalProps = R.compose(
      R.assoc('checked', !!checked),
      domOnlyProps
    )(props)

    return (
      <div className={classes.switch}>
        <input id={this.id} type='checkbox' className={classes.toggle} {...finalProps} />
        <label htmlFor={this.id} />
      </div>
    )
  }
}
