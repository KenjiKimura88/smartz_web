import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classes from './FormContainer.scss'

export default class FormContainer extends Component {
  static propTypes = {
    sidebar: PropTypes.node,
    children: PropTypes.node
  };

  render () {
    const { children, sidebar } = this.props
    return (
      <Row className={classes.wrapper}>
        <Col xs={4} md={3} className={classes.sidebar}>{sidebar}</Col>
        <Col xs={8} md={9} className={classes.formContent}>{children}</Col>
      </Row>
    )
  }
}
