import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import Label from 'components/Label'
import LabelValue from 'components/LabelValue'
import PortfolioDonut from '../PortfolioDonut'
import Well from 'components/Well'

const tooltip = `Your allocation chart shows the composition of your portfolio. Select pie segment
  to see which fund it represents`

export default class PortfolioDonutWell extends Component {
  static propTypes = {
    holdings: PropTypes.array.isRequired
  };

  render () {
    const { props } = this
    const { holdings } = props
    const hasHoldings = R.length(holdings) > 0

    return (
      <Well tooltip={tooltip}>
        {hasHoldings
          ? <Label>
            <div>Portfolio</div>
          </Label>
          : <LabelValue label={<div>Portfolio</div>}
            value={<div>No allocated holdings</div>} />}
        <PortfolioDonut {...props} />
      </Well>
    )
  }
}
