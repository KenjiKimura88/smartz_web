import React, { Component, PropTypes } from 'react'
import { FormattedNumber } from 'react-intl'
import { Col, FormGroup, Row, Table } from 'react-bootstrap'
import R from 'ramda'
import { BzPlusCircle } from 'icons'
import { connect } from 'redux/api'
import { mapIndexed } from 'helpers/pureFunctions'
import { show } from 'redux-modal'
import Button from 'components/Button'
import CurrencyInput from 'components/CurrencyInput'
import classes from './Assets.scss'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import QuovoFrame from 'containers/QuovoFrame'

export class Assets extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    externalAccounts: PropTypes.array.isRequired,
    fetchExternalAccounts: PropTypes.func.isRequired,
    homeValue: PropTypes.object.isRequired,
    retirementPlanId: PropTypes.string,
    show: PropTypes.func.isRequired,
  };

  componentWillMount () {
    this.syncExternalAccounts()
  }

  syncExternalAccounts = () => {
    const { fetchExternalAccounts } = this.props
    fetchExternalAccounts()
  }

  render () {
    const { externalAccounts, homeValue, show } = this.props

    return (
      <div className={classes.assets}>
        <H2>External Assets</H2>
        {!R.isEmpty(externalAccounts) && (
          <Table className={classes.table} bordered striped>
            <thead>
              <tr>
                <th className={classes.name}>Name</th>
                <th className={classes.accountType}>Account Type</th>
                <th className={classes.currentValue}>Current Value</th>
              </tr>
            </thead>
            <tbody>
              {mapIndexed((account, index) =>
                <tr key={index}>
                  <td>
                    {account.brokerage_name}
                  </td>
                  <td>
                    -
                  </td>
                  <td>
                    {account.value && <FormattedNumber value={account.value} format='currency' />}
                  </td>
                </tr>
              , externalAccounts)}
            </tbody>
          </Table>)}
        <FormGroup>
          <Button onClick={() => show('quovoFrame')}>
            <BzPlusCircle size={18} /> Manage assets
          </Button>
        </FormGroup>
        <H2>Home Value</H2>
        <Row>
          <Col xs={4}>
            <CurrencyInput {...homeValue} />
            <FieldError for={homeValue} />
          </Col>
        </Row>
        <QuovoFrame onClose={this.syncExternalAccounts} />
      </div>
    )
  }
}

const requests = ({ clientId }) => ({
  fetchExternalAccounts: ({ findAll }) => findAll({
    type: 'externalAccounts',
    url: '/quovo/get-accounts',
    lazy: true,
    force: true,
    propKey: 'externalAccounts'
  })
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions)
)(Assets)
