import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import moment from 'moment'
import R from 'ramda'
import { accountTypeOptions, contribPeriodOptions, employerMatchTypeOptions } from './options'
import { getMonthlyContribAmt, sumContributionsAmount } from '../../helpers'
import { BzTrash } from 'icons'
import { domOnlyProps, round } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from './RetirementAccount.scss'
import config from 'config'
import CurrencyInput from 'components/CurrencyInput'
import DateTime from 'react-datetime'
import FieldError from 'components/FieldError'
import PercentageInput from 'components/PercentageInput'
import Select from 'components/Select/Select'
import SelectWho from '../SelectWho'

const { retirementAccountCategories: { DORMANT_ACCOUNT_NO_CONTRIB } } = config

const getCategoryOptions = (accType, categoryOptions) =>
  R.compose(
    R.filter((item) => R.contains(
      item.value,
      R.compose(
        R.defaultTo([]),
        R.path(['categories']),
        R.find(R.propEq('value', accType))
      )(accountTypeOptions)
    )),
  )(categoryOptions)

export default class RetirementAccount extends Component {
  static propTypes = {
    categoryOptions: PropTypes.array.isRequired,
    expenses: PropTypes.array.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    retirementAccount: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired,
    updateExpensesFields: PropTypes.func.isRequired,
    user: PropTypes.object,
    values: PropTypes.object.isRequired
  };

  handleAccountTypeChange = (option) => {
    const { props } = this
    const { categoryOptions, retirementAccount: { acc_type: accType, cat } } = props
    accType.onChange(option)
    const selectedCategoryOptions = getCategoryOptions(option.value, categoryOptions)
    !R.isEmpty(selectedCategoryOptions) && cat.onChange(selectedCategoryOptions[0])
  }

  handleDateChange = (value) => {
    const { retirementAccount: { balance_efdt: balanceEfdt } } = this.props
    balanceEfdt.onChange(moment(value).format('YYYY-MM-DD'))
  }

  handleMatchTypeChange = (option) => {
    const { retirementAccount: { employer_match_type: employerMatchType,
      employer_match: employerMatch } } = this.props
    R.equals(option.value, 'none') && employerMatch.onChange(0)
    employerMatchType.onChange(option)
  }

  handleEmployerMatchChange = (event) => {
    const { retirementAccount: { employer_match: employerMatch } } = this.props
    employerMatch.onChange(event.target.value / 100)
  }

  handleAmtChange = (value) => {
    const { props } = this
    const { retirementAccount: { contrib_amt: contribAmt, contrib_period: contribPeriod },
      updateExpensesFields, values: { clientIncome, retirementAccounts } } = props
    const prevMonthlyBtc = sumContributionsAmount(retirementAccounts, 'self')
    const nextMonthlyBtc = prevMonthlyBtc + getMonthlyContribAmt({
      contrib_amt: value - contribAmt.value,
      contrib_period: contribPeriod.value
    })
    const monthlyClientIncome = clientIncome / 12
    const prevAvailableExpenses = monthlyClientIncome - prevMonthlyBtc
    const newAvailableExpenses = monthlyClientIncome - nextMonthlyBtc
    contribAmt.onChange(value)
    updateExpensesFields(prevAvailableExpenses, newAvailableExpenses)
  }

  sliderDisplayedValue = (value) => {
    return `${round(value * 100, 1)}%`
  }

  render () {
    const { props } = this
    const { categoryOptions, retirementAccount, maritalStatus, partnerName, remove, user } = props
    const { name, cat, acc_type: accType, owner, balance, balance_efdt: balanceEfdt,
      contrib_amt: contribAmt, contrib_period: contribPeriod, employer_match: employerMatch,
      employer_match_type: employerMatchType } = retirementAccount

    return (
      <FormGroup>
        <Row>
          <Col xs={3}>
            <FormGroup className={classes.formGroup}>
              <ControlLabel>Description</ControlLabel>
              <FormControl placeholder='Something about this' {...domOnlyProps(name)} />
              <FieldError for={name} />
            </FormGroup>
          </Col>
          <Col xs={3}>
            <FormGroup className={classes.formGroup}>
              <ControlLabel>Account Type</ControlLabel>
              <Select placeholder='Account Type' options={accountTypeOptions}
                onChange={this.handleAccountTypeChange} value={accType.value} />
              <FieldError for={accType} />
            </FormGroup>
          </Col>
          <Col xs={3}>
            <FormGroup className={classes.formGroup}>
              <ControlLabel>Category</ControlLabel>
              <Select placeholder='Select a category' onChange={cat.onChange} value={cat.value}
                options={getCategoryOptions(accType.value, categoryOptions)} />
              <FieldError for={cat} />
            </FormGroup>
          </Col>
          <Col xs={3}>
            <FormGroup className={classes.formGroup}>
              <ControlLabel>Owner</ControlLabel>
              <SelectWho field={owner} maritalStatus={maritalStatus} partnerName={partnerName}
                user={user} />
              <FieldError for={owner} />
            </FormGroup>
          </Col>
          <Col xs={12} className='form-horizontal'>
            <FormGroup className={classes.formGroup}>
              <Col xs={3} componentClass={ControlLabel}>Account Balance</Col>
              <Col xs={3}>
                <CurrencyInput {...domOnlyProps(balance)} />
                <FieldError for={balance} />
              </Col>
              <Col xs={1} componentClass={ControlLabel}>As at</Col>
              <Col xs={2}>
                <DateTime dateFormat='MM/DD/YYYY' onChange={this.handleDateChange}
                  timeFormat={false} value={moment(balanceEfdt.value, 'YYYY-MM-DD')} />
                <FieldError for={balanceEfdt} />
              </Col>
            </FormGroup>
            {!R.equals(cat.value, DORMANT_ACCOUNT_NO_CONTRIB) &&
              <FormGroup className={classes.formGroup}>
                <Col xs={3} componentClass={ControlLabel}>Contribution Amount</Col>
                <Col xs={3}>
                  <CurrencyInput {...domOnlyProps(contribAmt)} onChange={this.handleAmtChange} />
                  <FieldError for={contribAmt} />
                </Col>
                <Col xs={1} componentClass={ControlLabel}>Every</Col>
                <Col xs={2}>
                  <Select placeholder='Contribution Period' options={contribPeriodOptions}
                    onChange={contribPeriod.onChange} value={contribPeriod.value}
                    searchable={false} clearable={false} />
                  <FieldError for={contribPeriod} />
                </Col>
              </FormGroup>}
            {!R.equals(cat.value, DORMANT_ACCOUNT_NO_CONTRIB) &&
              <FormGroup className={classes.formGroup}>
                <Col xs={3} componentClass={ControlLabel}>Employer Matching</Col>
                <Col xs={3}>
                  <Select placeholder='Matching Type' options={employerMatchTypeOptions}
                    onChange={this.handleMatchTypeChange} value={employerMatchType.value}
                    searchable={false} clearable={false} />
                  <FieldError for={employerMatchType} />
                </Col>
                <Col xs={1} componentClass={ControlLabel}>
                  {R.contains(employerMatchType.value, ['income', 'contributions']) &&
                    (R.equals(employerMatchType.value, 'income') ? '(0-5)' : '(0-100)')
                  }
                </Col>
                <Col xs={2}>
                  {R.contains(employerMatchType.value, ['income', 'contributions']) && <div>
                    {R.equals(employerMatchType.value, 'income')
                    ? <PercentageInput type='rate' min={0} max={0.05}
                      {...domOnlyProps(employerMatch)} />
                    : <PercentageInput type='rate' min={0} max={1}
                      {...domOnlyProps(employerMatch)} />
                    }
                    <FieldError for={employerMatch} />
                  </div>}
                </Col>
              </FormGroup>}
            <Button onClick={remove} className={classes.removeButton}>
              <BzTrash size={21} />
            </Button>
          </Col>
        </Row>
      </FormGroup>
    )
  }
}
