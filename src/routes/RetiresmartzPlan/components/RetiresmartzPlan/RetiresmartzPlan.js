import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classNames from 'classnames'
import R from 'ramda'
import { BzArrowLeft, BzArrowRight, BzBot } from 'icons'
import { debounce } from 'lodash'
import { LinkContainer } from 'react-router-bootstrap'
import { getHoldings } from 'routes/Portfolio/helpers'
import { getEpoch, getHasPartner, getOnTrack, serializeRetirementPlan }
  from 'routes/RetiresmartzWizard/helpers'
import { isAnyTrue } from 'helpers/pureFunctions'
import { requestIsPending } from 'helpers/requests'
import { MdLightbulbOutline } from 'helpers/icons'
import AdviceFeed from '../AdviceFeed'
import AllCaps from 'components/AllCaps'
import Button from 'components/Button'
import classes from './RetiresmartzPlan.scss'
import ContentPane from 'components/ContentPane'
import IncomeAndAssetsChart from '../IncomeAndAssetsChart'
import Label from 'components/Label'
import MainFrame from 'components/MainFrame'
import Notification from 'containers/Notification'
import Panel from 'containers/Panel'
import PieChart from '../PieChart'
import PortfolioDonut from 'components/PortfolioDonut'
import RequestAdvisorSupportModal from '../../containers/RequestAdvisorSupportModal'
import RiskDescription from 'components/RiskDescription'
import RiskSlider from 'components/RiskSlider'
import Sidebar from 'components/Sidebar'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Well from 'components/Well'

const monthlyPanelHeader =
  <div className={classNames(classes.monthlyPanelHeader, 'text-center')}>
    <Text bold>
      <AllCaps value='Monthly View' />
    </Text>
  </div>

// getPortfolio :: Props -> Array
const getPortfolio = R.path(['calculatedData', 'portfolio'])

// getProjection :: Props -> Array
const getProjection = R.pathOr([], ['calculatedData', 'projection'])

const getBirthDate = R.path(['client', 'date_of_birth'])

const getMaxRisk = R.pathOr(1, ['retirementPlan', 'max_risk'])

const getRecommendedRisk = R.pathOr(1, ['retirementPlan', 'recommended_risk'])

// getIsPending :: Props -> Boolean
const getIsPending = R.converge(isAnyTrue, [
  requestIsPending('updateRetirementPlan'),
  requestIsPending('calculate'),
  requestIsPending('calculatedData')
])

const getIsLoadingPiechart = R.converge(isAnyTrue, [
  requestIsPending('retirementPlan'),
  requestIsPending('expenseCategories'),
  requestIsPending('savingCategories')
])

const getOnTrackStatus = (birthdate, retirementAge, data) => {
  if (R.equals(0, R.length(data))) return undefined
  const retirementDate = getEpoch(retirementAge, birthdate)
  const values = R.map(([time, assets, income]) => ({
    x: time * 8.64e7, // epoch days => epoch ms
    y: income
  }), data)
  const targetValues = R.map(([time, assets, income, desiredIncome]) => ({
    x: time * 8.64e7, // epoch days => epoch ms
    y: desiredIncome
  }), data)
  return getOnTrack(retirementDate, values, targetValues)
}

export default class RetiresmartzPlan extends Component {
  static propTypes = {
    adviceFeeds: PropTypes.array.isRequired,
    calculate: PropTypes.func.isRequired,
    calculatedData: PropTypes.object,
    client: PropTypes.object,
    dirty: PropTypes.bool.isRequired,
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    initializeForm: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    readAdviceFeed: PropTypes.func.isRequired,
    reduxFormInitialize: PropTypes.func.isRequired,
    refreshAdviceFeeds: PropTypes.func.isRequired,
    retirementPlan: PropTypes.object,
    retiresmartz: PropTypes.object.isRequired,
    requests: PropTypes.object.isRequired,
    set: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
    updateApiData: PropTypes.func.isRequired,
    updateRetirementPlan: PropTypes.func.isRequired,
    valid: PropTypes.bool.isRequired,
    values: PropTypes.object.isRequired
  };

  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)
    this.immediateSaveAndCalculate = this.saveAndCalculate
    this.saveAndCalculate = debounce(this.saveAndCalculate, 1500)
    // TODO: Refactor to not use state
    this.state = {
      isSaving: false
    }
  }

  componentWillMount() {
    const { refreshAdviceFeeds, retiresmartz: { shouldCalculate }, set } = this.props
    if (shouldCalculate) {
      refreshAdviceFeeds()
      this.doCalc()
      set({ shouldCalculate: false })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { dirty } = nextProps
    const { isSaving } = this.state
    if (dirty && !isSaving) {
      this.saveAndCalculate(nextProps)
    }
  }

  saveAndCalculate(props) {
    const { handleSaveAndCalculateFail } = this
    const { refreshAdviceFeeds, updateRetirementPlan, values } = props
    const body = serializeRetirementPlan(values)

    this.setState({
      isSaving: true
    })

    // TODO: Refactor to remove deep nesting
    updateRetirementPlan({
      body,
      success: ({ value: retirementPlan }) => {
        refreshAdviceFeeds()
        this.doCalc()
      },
      fail: handleSaveAndCalculateFail
    })
  }

  doCalc() {
    const { handleSaveAndCalculateFail, props } = this
    const { calculate, refreshAdviceFeeds } = props
    calculate({
      success: ({ value }) => {
        value.reload_feed && refreshAdviceFeeds()
        this.setState({
          isSaving: false
        })
      },
      fail: handleSaveAndCalculateFail
    })
  }

  handleSaveAndCalculateFail = ({ error }) => {
    const { initializeForm, values } = this.props
    initializeForm(values)
    this.setState({
      isSaving: false
    })
  }

  handleRiskChange = (value) => {
    const { props } = this
    const { fields: { risk }, retirementPlan: { max_risk } } = props
    const max = R.defaultTo(1, max_risk)
    if (value <= max) {
      risk.onChange(value)
    }
  }

  handleGetAssist = () => {
    const { show } = this.props
    show('requestAdvisorSupportModal')
  }

  handleReadAdviceFeed = (feedId, read) => {
    const { adviceFeeds, params: { clientId, retirementPlanId }, readAdviceFeed,
      updateApiData } = this.props
    const index = R.findIndex(R.propEq('id', feedId))(adviceFeeds)
    if (R.gte(index, 0)) {
      updateApiData({
        path: ['retirementAdviceFeeds', index, 'read'],
        value: read
      })
      readAdviceFeed(clientId, retirementPlanId, feedId, read)
    }
  }

  renderCalculationError = (error) => {
    const { props } = this
    return (
      <span>
        Retirement calculation has failed.{' '}
        <a onClick={() => this.immediateSaveAndCalculate(props)}>
          Retry
        </a> or{' '}
        <a onClick={() => window.alert('Feature not activated')}>
          Revert to the last-working settings
        </a>.
      </span>
    )
  }

  render () {
    const { props, state } = this
    const { adviceFeeds, client, fields, fields: { risk, retirementAge },
      params: { clientId, retirementPlanId }, reduxFormInitialize, requests,
      requests: { calculate, calculatedData, updateRetirementPlan }, retirementPlan,
      retiresmartz: { rcFormVisible }, savingCategories, set, settings, values } = props
    const { asset_classes: assetsClasses = [], health_devices: healthDevicesConfig,
      retirement_expense_categories: expenseCategories = [],
      tickers = [] } = R.defaultTo({}, settings)
    const { isSaving } = state
    const portfolio = getPortfolio(props)
    const projection = getProjection(props)
    const holdings = retirementPlan && getHoldings({
      assetsClasses,
      goal: retirementPlan,
      portfolio,
      tickers
    })
    const maxRisk = getMaxRisk(props)
    const recommendedRisk = getRecommendedRisk(props)
    const basePath = `/${clientId}/retiresmartz/${retirementPlanId}`
    const birthdate = getBirthDate(props)
    const hasRisk = R.is(Number, risk.value)
    const hasPartner = getHasPartner(props)
    const isFinalSaving = getIsPending(props) || isSaving
    const isLoadingFeeds = requestIsPending('adviceFeeds')(props)
    const isLoadingPieChart = getIsLoadingPiechart(props)
    const desiredIncome = retirementPlan && retirementPlan.desired_income
    const calculatedLifeExpectancy = retirementPlan && retirementPlan.calculated_life_expectancy
    const isOnTrack = getOnTrackStatus(birthdate, retirementAge.value, projection)

    return (
      <div ref='plan' className={classes.retirementPlan}>
        <MainFrame rounded>
          <Sidebar xs={4} md={3}>
            <div className={classNames(classes.main, classes.sidebarMain)}>
              <div className='text-center'>
                <div className={classes.sidebarTitle}>
                  <Text bold>
                    <AllCaps value='Advice' />
                  </Text>
                </div>
                <Button className={classNames('btn-circle', 'btn-default', classes.bot)}>
                  <BzBot size={18} />
                </Button>
              </div>
              <div className={classes.sidebarContent}>
                <AdviceFeed client={client} feeds={adviceFeeds} isLoading={isLoadingFeeds}
                  isOnTrack={isOnTrack} readAdviceFeed={this.handleReadAdviceFeed}
                  retirementPlan={retirementPlan} requests={requests} />
              </div>
            </div>
            <div className={classNames(classes.footer, 'text-center')}>
              <Button className={classes.assistanceButton} onClick={this.handleGetAssist}>
                <span className='hidden-md hidden-sm hidden-xs'>Get further assistance&nbsp;</span>
                <span className='hidden-lg'>Get further assist&nbsp;</span>
                <MdLightbulbOutline />
              </Button>
            </div>
          </Sidebar>
          <ContentPane xs={8} md={9}>
            <div className={classNames(classes.main, classes.contentMain)}>
              <Row>
                <Col xs={12}>
                  <div className={classes.errorNotificationWrapper}>
                    <Notification show isRetrying={isSaving}
                      request={calculate || calculatedData || updateRetirementPlan}
                      errorMessage={this.renderCalculationError}
                      onRetry={() => this.immediateSaveAndCalculate(props)} showTimestamp />
                  </div>
                  <Panel id='incomeChart' className={classNames(classes.panel, classes.chartPanel)}>
                    {birthdate &&
                      <IncomeAndAssetsChart birthdate={birthdate}
                        calculatedLifeExpectancy={calculatedLifeExpectancy} client={client}
                        data={projection} desiredIncome={desiredIncome} fields={fields}
                        hasPartner={hasPartner} healthDevicesConfig={healthDevicesConfig}>
                        {isFinalSaving && <Spinner size='small' />}
                      </IncomeAndAssetsChart>}
                  </Panel>
                </Col>
              </Row>
              <Row>
                <Col md={6} className={classes.mbForSmall}>
                  <Panel id='retirementRisk' className={classes.panel}>
                    <Row>
                      <Col xs={6}>
                        <Label>Risk</Label>
                      </Col>
                      <Col xs={6} className='text-right'>
                        <Text primary>
                          {hasRisk && <RiskDescription riskValue={risk.value}
                            recommendedValue={recommendedRisk} noTooltip />}
                        </Text>
                      </Col>
                    </Row>
                    {hasRisk && <RiskSlider max={maxRisk} {...risk} after />}
                    {portfolio && holdings &&
                      <Well className={classes.donutWell}>
                        <PortfolioDonut holdings={holdings} small />
                      </Well>}
                  </Panel>
                </Col>
                <Col md={6}>
                  <Panel id='retirementPieChart' header={monthlyPanelHeader}
                    className={classes.panel}>
                    <div className={classes.monthlyView}>
                      <PieChart client={client} expenseCategories={expenseCategories}
                        fields={fields} hasPartner={hasPartner} isLoading={isLoadingPieChart}
                        rcFormVisible={rcFormVisible} reduxFormInitialize={reduxFormInitialize}
                        savingCategories={savingCategories} set={set} values={values} />
                    </div>
                  </Panel>
                </Col>
              </Row>
            </div>
            <div className={classes.footer}>
              <Row>
                <Col xs={5}>
                  <LinkContainer to={`${basePath}/wizard`}>
                    <Button>
                      <BzArrowLeft /> Back to details
                    </Button>
                  </LinkContainer>
                </Col>
                <Col xs={7} className='text-right'>
                  <LinkContainer to={`${basePath}/agreement`}>
                    <Button>
                      Proceed <BzArrowRight size={9} />
                    </Button>
                  </LinkContainer>
                </Col>
              </Row>
            </div>
            <RequestAdvisorSupportModal />
          </ContentPane>
        </MainFrame>
      </div>
    )
  }
}
