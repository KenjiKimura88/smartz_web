/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class BzArrowLeft extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 9 18' {...this.props}>
        <g>
          <path d='M7.47,0a1.54,1.54,0,0,1,1,.34,1.48,1.48,0,0,1,.22,2.11L3.21,9l5.45,6.55a1.48,1.48,0,0,1-.22,2.11,1.55,1.55,0,0,1-2.15-.21L.57,10.57a2.46,2.46,0,0,1,0-3.15L6.28.55A1.54,1.54,0,0,1,7.47,0ZM2.94,8.68h0Z' />
        </g>
      </IconBase>
    )
  }
}
