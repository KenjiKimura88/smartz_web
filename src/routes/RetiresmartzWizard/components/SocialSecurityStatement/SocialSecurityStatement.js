import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormGroup, Row } from 'react-bootstrap'
import DateTime from 'react-datetime'
import { isMarried } from '../../helpers'
import { requestIsPending } from 'helpers/requests'
import classes from './SocialSecurityStatement.scss'
import ClientName from 'components/ClientName'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import SocialSecurityStatementUpload from '../SocialSecurityStatementUpload'
import Well from 'components/Well'
import Frassbentod from '../Frassbentod'
import Text from 'components/Text'

const ssfraTooltip = `You can obtain this information from your Social Security Statement
  or to obtain a new Social Security Statement setup an online account with the Social Security
  Administration at https://secure.ssa.gov/RIL/SiView.do.`

export default class SocialSecurityStatement extends Component {
  static propTypes = {
    client: PropTypes.object,
    maritalStatus: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    requests: PropTypes.object.isRequired,
    saveFile: PropTypes.func,
    socialSecurityClient: PropTypes.object.isRequired,
    socialSecurityPartner: PropTypes.object.isRequired,
    ssFraTodays: PropTypes.object.isRequired,
    estimateDate: PropTypes.object.isRequired
  };

  render () {
    const { props } = this
    const { client, maritalStatus, partnerName, saveFile, socialSecurityClient,
      socialSecurityPartner, ssFraTodays, estimateDate } = props
    const partnerNameValue = partnerName.value
    const finalPartnerName = partnerNameValue || 'Your Spouse'
    const isSSUploading = requestIsPending('saveFile')(props)

    return (
      <div>
        <H2 className={classes.title}>Social Security Statement</H2>
        <Well>
          Enter the following information from your most recent Social Security Statement
          (SSA-7005) or upload a copy.
          If you don't have a scanned copy you can download one immediately from{' '}
          <a href='https://myaccount.socialsecurity.gov' target='_blank'>
            https://myaccount.socialsecurity.gov
          </a>.
        </Well>
        <Row>
          <Col xs={12}>
            <Text tagName='div' className={classes.ssLabel}>
              According to your Social Security Statement (Form SSA-7005, see pg.2),
              what is the retirement benefit payment you will receive at your full
              retirement age (67 years)?
            </Text>
          </Col>
          <Col xs={4}>
            <FormGroup controlId='ssFraTodays'>
              <Frassbentod field={ssFraTodays} tooltipText={ssfraTooltip} />
              <FieldError for={ssFraTodays} />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Text tagName='div' className={classes.ssLabel}>
              Date of estimate (i.e. the date at top of pg.1 of your
              Social Security Statement (Form SSA-7005))
            </Text>
          </Col>
          <Col xs={4}>
            <FormGroup>
              <DateTime dateFormat='MM-DD-YYYY' timeFormat={false}
                onChange={estimateDate.onChange} value={estimateDate.value} />
              <FieldError for={estimateDate} />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup>
          <Row>
            <Col xs={6}>
              <ControlLabel>
                <ClientName client={client} />
              </ControlLabel>
              <SocialSecurityStatementUpload estimateDate={estimateDate}
                field={socialSecurityClient} fieldName='social_security_statement'
                id='clientSocialSecurityUpload' isUploading={isSSUploading} saveFile={saveFile}
                ssFraTodays={ssFraTodays} />
            </Col>
            {isMarried(maritalStatus.value) && (
              <Col xs={6}>
                <ControlLabel>{finalPartnerName}</ControlLabel>
                <SocialSecurityStatementUpload estimateDate={estimateDate}
                  field={socialSecurityPartner} fieldName='partner_social_security_statement'
                  id='partnerSocialSecurityUpload' isUploading={isSSUploading} saveFile={saveFile}
                  ssFraTodays={ssFraTodays} />
              </Col>)}
          </Row>
        </FormGroup>
      </div>
    )
  }
}
