import React, { Component, PropTypes } from 'react'
import { FormGroup, FormControl, Row, Col } from 'react-bootstrap'
import Checkbox from 'components/Checkbox'
import classes from './Agreements.scss'
import FieldError from 'components/FieldError'
import Text from 'components/Text'
import Well from 'components/Well'

const Form6105 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form6105.pdf'
const Form6108 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form6108.pdf'
const Form2109 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form2109.pdf'
const Form3007 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3007.pdf'
const Form3002 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3002.pdf'
const Form3070 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3070.pdf'
const Form4035 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form4035.pdf'
const Form4036 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form4036.pdf'
const Form3071 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3071.pdf'
const Form2192 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form2192.pdf'
const Form3024 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3024.pdf'
const Form3094 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3094.pdf'
const Form3074 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form3074.pdf'
const Form4016 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form4016.pdf'
const Form9130 = 'ftp://ftp.interactivebrokers.com/outgoing/Forms/Form9130.pdf'

class IBAgreements extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired
  };

  render() {
    const { fields: { ibAgreement, signature } } = this.props
    return (
      <div>
        <Text size='medium' bold>Interactive Brokers agreement</Text>
        <FormGroup>
          <Checkbox checked={ibAgreement.value}
            onChange={ibAgreement.onChange}>
            By checking this box, you acknowledge that you have read and agree to{' '}
            the terms below.
          </Checkbox>
          <FieldError for={ibAgreement} />
        </FormGroup>
        <Well>
          <div className={classes.description}>
            <p>
              By checking the above box, signing and clicking continue below, you agree to
              be bound by the following agreements:
            </p>
            <p>
              - <a href={Form6105} target='_blank'>
                Form 6105 (Client FA Agreement)
              </a>,{' '}
              <a href={Form6108} target='_blank'>
                Form 6108 (Agreement Limiting Interactive Brokers Liability)
              </a>,{' '}
              <a href={Form2109} target='_blank'>
                Form 2109 (Legal Acknowledgement)
              </a>,{' '}
              <a href={Form3007} target='_blank'>
                Form 3007 (Indiv/Joint/Trust: Customer Agreement)
              </a>,{' '}
              <a href={Form3002} target='_blank'>
                Form 3002 (IRA:  Customer Agreement)
              </a>,{' '}
              <a href={Form3070} target='_blank'>
                Form 3070 (Business Continuity Plan Disclosure)
              </a>,{' '}
              <a href={Form4035} target='_blank'>
                Form 4035 (Mutual Fund Agreement)
              </a>,{' '}
              <a href={Form4036} target='_blank'>
                Form 4036 (Mutual Fund Disclosure)
              </a>,{' '}
              <a href={Form3071} target='_blank'>
                Form 3071 (Day Trading Risk Disclosure)
              </a>,{' '}
              <a href={Form2192} target='_blank'>
                Form 2192 (Privacy Statement)
              </a>,{' '}
              <a href={Form3024} target='_blank'>
                Form 3024 (Forex and Multi-Currency Risk Disclosure)
              </a>,{' '}
              <a href={Form3094} target='_blank'>
                Form 3094 (Notice Regarding NFA's BASIC System)
              </a>,{' '}
              <a href={Form3074} target='_blank'>
                Form 3074 (Order Routing and Payment for Order Flow Disclosure)
              </a>,{' '}
              <a href={Form4016} target='_blank'>
                Form 4016 (After-hours Trading Risk Disclosure)
              </a>,{' '}
              <a href={Form9130} target='_blank'>
                Form 9130 (US Stock Stop Order Disclosure)
              </a>. You acknowledge receiving and reviewing copies of the agreements.
            </p>
            <p>
              You understand and agree that these are solely agreements between you and
              Interactive Brokers.
            </p>
          </div>
        </Well>
        <Row>
          <Col xs={6} className='text-right'>
            <Text size='medium' bold>Signature</Text>
          </Col>
          <Col xs={6}>
            <FormControl
              value={signature.value}
              placeholder='Type Full Name'
              onChange={signature.onChange} />
            <FieldError for={signature} />
          </Col>
        </Row>
      </div>
    )
  }
}

export default IBAgreements
