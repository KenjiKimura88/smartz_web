import buildSchema from 'redux-form-schema'
import R from 'ramda'
import config from 'config'
const { EMPLOYMENT_STATUS } = config.onboarding

const schemaProperties = {
  'employment_status': {
    label: 'Employment Status',
    required: true
  },
  'industry_sector': {
    label: 'Industry Sector',
    required: ({ employment_status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  'occupation': {
    label: 'Occupation',
    required: ({ employment_status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  'employer': {
    label: 'Employer',
    required: ({ employment_status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  'employer_type': {
    label: 'Employer Type',
    required: ({ employment_status }) => R.contains(status,
      [EMPLOYMENT_STATUS.EMPLOYED, EMPLOYMENT_STATUS.SELF_EMPLOYED])
  },
  'income': {
    label: 'Annual Income',
    required: true
  }
}

export default buildSchema(schemaProperties)
