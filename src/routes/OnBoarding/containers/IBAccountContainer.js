import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { findSingle, touchRequest } from 'redux/api/modules/requests'
import { IBAccountSchema } from 'schemas/onboarding'
import IBAccount from '../components/IBAccount'

const actions = {
  findSingle,
  touchRequest
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const asyncValidate = (values) => {
  return sleep(1)
    .then(() => {
      if (values.accountNumber !== values.accountNumberConfirmation) {
        throw { accountNumberConfirmation: 'Account Number do not match' }
      }
    })
}

export default R.compose(
  reduxForm({
    form: 'onboardingAccountInfo',
    ...IBAccountSchema,
    asyncValidate,
    asyncBlurFields: ['accountNumberConfirmation'],
  }, (state, { getValue }) => ({
    initialValues: getValue(['login', 'steps', 1])
  })),
  connect(null, actions),
  withRouter
)(IBAccount)
