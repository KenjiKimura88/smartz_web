import R from 'ramda'
import d3 from 'd3'
import nv from 'nvd3'

export default function() {
    'use strict'

    //============================================================
    // Public letiables with Default Settings
    //------------------------------------------------------------

    let margin = {top: 30, right: 20, bottom: 50, left: 60},
        color = nv.utils.defaultColor(),
        width = null,
        height = null,
        guidelineHeight = null,
        yDomain2,
        forceY = [0],
        getX = function(d) { return d.x },
        getY = function(d) { return d.y },
        interpolate = 'monotone',
        interactiveLayer = nv.interactiveGuideline(),
        legendRightAxisHint = ' (right axis)',
        duration = 250

    //============================================================
    // Private letiables
    //------------------------------------------------------------

    let x = d3.scale.linear(),
        yScale2 = d3.scale.linear(),
        lines2 = nv.models.line().yScale(yScale2).duration(duration),
        stack2 = nv.models.stackedArea().yScale(yScale2).duration(duration),
        yAxis2 = nv.models.axis().scale(yScale2).orient('right').duration(duration),
        dispatch = d3.dispatch()

    let charts = [lines2, stack2]

    lines2.interactive(false)
    stack2.interactive(false)
    lines2.useVoronoi(false)
    stack2.useVoronoi(false)

    //============================================================
    // Markers
    //------------------------------------------------------------
    const markersVisibility = []

    const updateMarkerTooltip = (d, xValue, i) => {
      if (markersVisibility[i]) {
          let tooltipEl = R.last(markersVisibility[i])
          let d3TooltipEl = d3.select(tooltipEl)
          let target = d.target
          let pointIndex = nv.interactiveBisect(target, xValue, x())
          let point = target[pointIndex]
          let box = tooltipEl.getBoundingClientRect()
          let left = nv.utils.NaNtoZero(x(point.x)) - box.width / 2
          let top = nv.utils.NaNtoZero(yScale2(point.y)) + 16
          d3TooltipEl.style('left', left + 'px').style('top', top + 'px')
      }
    }

    const maybeClickTooltip = (context, d, i, marker, tooltip) => {
      if (!tooltip.contains(d3.event.target) && !marker.contains(d3.event.target)) {
        markerClick.call(context, d, i)
      }
    }

    const handleBodyClick = R.compose(
      R.forEach(R.apply(maybeClickTooltip)),
      R.reject(R.isNil)
    )

    const addBodyClickEventListener = function () {
      const body = d3.select('body')
      body.on('click', () => handleBodyClick(markersVisibility))
    }

    const markerDrag = function (d, i) {
        let marker = d3.select(this)
        let data = marker.data()[0]
        let target = data.target
        let onDrag = data.onDrag
        let pointIndex = nv.interactiveBisect(target, chart._pointXValue, x())
        let point = target[pointIndex]
        onDrag(point.x)

        if (!markersVisibility[i]) {
          markerClick.bind(this)(d, i)
        }
    }

    const markerDragStart = function (d, i) {
        chart._disableGuideline = true
        chart.interactiveLayer.tooltip.enabled(false)
        d3.selectAll('.nvtooltip').style('visibility', 'hidden')
        d3.select('body').attr('style', 'cursor:move')
    }

    const markerDragEnd = function () {
        chart._disableGuideline = false
        chart.interactiveLayer.tooltip.enabled(true)
        d3.selectAll('.nvtooltip').style('visibility', 'visible')
        d3.select('body').attr('style', 'cursor:auto')

        let marker = d3.select(this)
        let data = marker.data()[0]
        data.onDragEnd()
        return true
    }

    const markerClick = function (d, i) {
      let marker = d3.select(this)
      let data = marker.data()[0]
      let visible = markersVisibility[i]
      let tooltipEl = data.tooltipEl
      let d3TooltipEl = d3.select(data.tooltipEl)

      if (!tooltipEl) {
        return
      }

      visible = !visible
      markersVisibility[i] = visible ? [this, d, i, marker.node(), tooltipEl] : null
      let anyMarkerVisible = R.any(R.compose(R.not, R.isNil), markersVisibility)
      d3TooltipEl.style('display', visible ? 'block' : 'none')

      chart._disableGuideline = anyMarkerVisible
      chart.interactiveLayer.tooltip.hidden(true).enabled(!anyMarkerVisible)
      d3.selectAll('.nvtooltip').style('visibility', anyMarkerVisible ? 'hidden' : 'visible')

      updateMarkerTooltip(d, getX(d, i), i)
    }

    let renderWatch = nv.utils.renderWatch(d3.dispatch('renderEnd'), duration)
    let markers = function (selection) {
      renderWatch.reset()

      selection.each(function (data) {
          let container = d3.select(this)

          nv.utils.initSVG(container)
          container.selectAll('g.nv-wrap.nv-marker').remove()

          let wrap = container.selectAll('g.nv-wrap.nv-marker').data([data])
          let wrapEnter = wrap.enter().append('g').attr('class', 'nvd3 nv-wrap nv-marker')
          let defsEnter = wrapEnter.append('defs')
          let gEnter = wrapEnter.append('g')

          let filter = defsEnter.append('filter')
            .attr('id', 'drop-shadow')
            .attr('x', '-20%')
            .attr('y', '-20%')
            .attr('height', '200%')
            .attr('width', '200%')

          filter.append('feGaussianBlur')
            .attr('in', 'SourceAlpha')
            .attr('stdDeviation', 3)

          filter.append('feOffset')
            .attr('dx', 2)
            .attr('dy', 4)

          let feComponentTransfer = filter.append('feComponentTransfer')

          feComponentTransfer.append('feFuncA')
            .attr('type', 'linear')
            .attr('slope', '0.3')

          let feMerge = filter.append('feMerge')

          feMerge.append('feMergeNode')
          feMerge.append('feMergeNode')
            .attr('in', 'SourceGraphic')

          gEnter.append('g').attr('class', 'nv-groups')
          let groups = wrap.select('.nv-groups').selectAll('.nv-group')
            .data(function(d) { return d }, function(d) { return d.key })
          groups.enter().append('g')
          groups.exit().remove()
          groups.attr('class', function(d,i) {
            return (d.classed || '') + ' nv-group nv-series-' + i
          })
          let group = groups.selectAll('g.nv-marker').data(function(d) { return d.values })
          let circlesG = group.enter().append('g')
            .attr('transform', function (d, i) {
              let target = d.target
              if (target.length > 0) {
                  let pointIndex = nv.interactiveBisect(target, getX(d, i), x())
                  let point = target[pointIndex]
                  let xValue = nv.utils.NaNtoZero(x(point.x))
                  let yValue = nv.utils.NaNtoZero(yScale2(point.y))
                  updateMarkerTooltip(d, getX(d, i), i)
                  return `translate(${xValue},${yValue})`
              } else {
                this.remove()
              }
            })
            .attr('class', function(d) {
              return d.classed
            })

          group.append('text')
            .text(function(d) { return d.key })
            .attr('text-anchor', 'middle')
            .attr('transform', 'translate(0, -20)')

          circlesG.append('circle')
            .attr('r', 12)
            .style('filter', 'url(#drop-shadow)')
          circlesG.append('circle')
            .attr('r', 2)
          circlesG.call(d3.behavior.drag()
            .on('drag', markerDrag)
            .on('dragstart', markerDragStart)
            .on('dragend', markerDragEnd)
          )
          circlesG.on('click', markerClick)
      })
      addBodyClickEventListener()

      renderWatch.renderEnd('markers immediate')
      return markers
    }

    function chart(selection) {
        selection.each(function(data) {
            let container = d3.select(this)
            nv.utils.initSVG(container)

            chart.update = function() {
                if( duration === 0 ) {
                    container.call( chart )
                } else {
                    container.transition().duration(duration).call(chart)
                }
            }

            chart.container = this

            let availableWidth = nv.utils.availableWidth(width, container, margin),
                availableHeight = nv.utils.availableHeight(height, container, margin)

            let dataLines2 = data.filter(function(d) {return d.type == 'line'})
            let dataStack2 = data.filter(function(d) {return d.type == 'area'})
            let dataMarkers = data.filter(function(d) {return d.type == 'marker'})

            // Return if there's nothing to show.
            if (!data || !data.length ||
              !data.filter(function(d) { return d.values.length }).length) {
                return chart
            }

            let series2 = data.filter(function(d) {return !d.disabled})
                .map(function(d) {
                    return d.values.map(function(d,i) {
                        return { x: getX(d), y: getY(d) }
                    })
                })

            x.domain(d3.extent(d3.merge(series2), function(d) { return getX(d) }))
              .range([0, availableWidth])

            let wrap = container.selectAll('g.wrap.multiChart').data([data])
            let gEnter = wrap.enter().append('g').attr('class', 'wrap nvd3 multiChart').append('g')

            gEnter.append('g').attr('class', 'nv-x nv-axis')
            gEnter.append('g').attr('class', 'nv-y2 nv-axis')
            gEnter.append('g').attr('class', 'stack2Wrap')
            gEnter.append('g').attr('class', 'lines2Wrap')
            gEnter.append('g').attr('class', 'nv-interactive')
            gEnter.append('circle')
                .style('display', 'none')
                .attr('class', 'mouseover-point')
                .attr('r', 3)

            let g = wrap.select('g')

            let color_array = data.map(function(d,i) {
                return data[i].color || color(d, i)
            })

            lines2
                .width(availableWidth)
                .height(availableHeight)
                .interpolate(interpolate)
                .color(color_array.filter(function(d,i) {
                  return !data[i].disabled && data[i].type == 'line'
                }))
            stack2
                .width(availableWidth)
                .height(availableHeight)
                .color(color_array.filter(function(d,i) {
                  return !data[i].disabled && data[i].type == 'area'
                }))

            g.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

            let lines2Wrap = g.select('.lines2Wrap')
                .datum(dataLines2.filter(function(d){return !d.disabled}))
            let stack2Wrap = g.select('.stack2Wrap')
                .datum(dataStack2.filter(function(d){return !d.disabled}))

            yScale2
              .domain(forceY)
              .range([availableHeight, 0])

            lines2.yDomain(yScale2.domain())
            stack2.yDomain(yScale2.domain())

            if(dataStack2.length){d3.transition(stack2Wrap).call(stack2)}
            if(dataLines2.length){d3.transition(lines2Wrap).call(lines2)}

            g.select('.nv-x.nv-axis')
                .attr('transform', 'translate(0,' + availableHeight + ')')

            yAxis2
                ._ticks( nv.utils.calcTicksY(availableHeight/36, data) )
                .tickSize( -availableWidth, 0)

            g.select('.nv-y2.nv-axis').transition()
                .duration(duration)
                .call(yAxis2)

            g.select('.nv-y2.nv-axis')
                .classed('nv-disabled', series2.length ? false : true)
                .attr('transform', 'translate(' + x.range()[1] + ',0)')

            interactiveLayer
                .width(availableWidth)
                .height(availableHeight)
                .margin({left:margin.left, top:margin.top})
                .svgContainer(container)
                .xScale(x)
            wrap.select('.nv-interactive').call(interactiveLayer)

            gEnter.append('g').attr('class', 'markersWrap')
            let markersWrap = g.select('.markersWrap')
              .datum(dataMarkers.filter(function(d){return !d.disabled}))
            if(dataMarkers.length){d3.transition(markersWrap).call(markers)}

            //============================================================
            // Event Handling/Dispatching
            //------------------------------------------------------------

            function clearHighlights() {
              for(let i=0, il=charts.length; i < il; i++){
                let chart = charts[i]
                try {
                  chart.clearHighlights()
                } catch (e) {
                  // Do nothing
                }
              }
            }

            interactiveLayer.dispatch.on('elementMousemove', function(e) {
                clearHighlights()
                let extent = x.domain()
                let mouseY = lines2.yScale().invert(e.mouseY)
                chart._pointXValue = e.pointXValue

                const hasLineData = R.compose(
                    R.any(({ values }) =>
                        values.length > 0
                    ),
                    R.filter(R.propEq('type', 'line'))
                )(data)

                if (chart._disableGuideline || !hasLineData) {
                    return
                }

                let line = R.compose(
                    R.addIndex(R.reduce)((acc, series, index) => {
                        let currentValues = series.values.filter(function(d, i) {
                            return chart.x()(d,i) >= extent[0] && chart.x()(d,i) <= extent[1]
                        })
                        let pointIndex = nv.interactiveBisect(
                          currentValues,
                          e.pointXValue,
                          chart.x()
                        )
                        let point = currentValues[pointIndex]
                        let pointYValue = chart.y()(point, pointIndex)
                        let distance = Math.abs(pointYValue - mouseY)
                        return !series.disabled && !series.disableTooltip &&
                            R.lt(distance, acc.distance)
                            ? {
                                series,
                                index,
                                point,
                                pointIndex,
                                distance
                            }
                            : acc
                    }, { distance: Infinity })
                )(data)

                let position = {
                    left: x(line.point.x),
                    top: yScale2(line.point.y)
                }

                wrap.select('.mouseover-point')
                    .style('display', 'inline')
                    .attr('cx', position.left)
                    .attr('cy', position.top)

                interactiveLayer.tooltip
                .data({
                    value: chart.x()(line.point, line.pointIndex),
                    index: line.pointIndex,
                    series: {
                        key: line.series.key,
                        value: chart.y()(line.point, line.pointIndex),
                        data: line.point
                    }
                })
                .position(() => {
                    let offset = chart.container.getBoundingClientRect()
                    let client = document.body.getBoundingClientRect()
                    return {
                        left: position.left + offset.left - client.left,
                        top: position.top + offset.top - client.top
                    }
                })()

                d3.selectAll('.nvtooltip').classed('right', true)
                interactiveLayer.renderGuideLine(x(chart.x()(line.point, line.pointIndex)))

                if (guidelineHeight) {
                  wrap.select('.nv-interactiveGuideLine line').attr('y1', guidelineHeight)
                }
            })

            interactiveLayer.dispatch.on('elementMouseout', function(e) {
                clearHighlights()
                d3.selectAll('.nvtooltip').classed('right', false)
                wrap.select('.mouseover-point').style('display', 'none')
            })
        })

        return chart
    }

    //============================================================
    // Global getters and setters
    //------------------------------------------------------------

    chart.dispatch = dispatch
    chart.lines2 = lines2
    chart.stack2 = stack2
    chart.markers = markers
    chart.yAxis2 = yAxis2
    chart.interactiveLayer = interactiveLayer

    chart.options = nv.utils.optionsFunc.bind(chart)

    chart._options = Object.create({}, {
        // simple options, just get/set the necessary values
        width: {get: function(){return width}, set: function(_){width=_}},
        height:{get: function(){return height}, set: function(_){height=_}},
        guidelineHeight: {
          get: function(){return guidelineHeight},
          set: function(_){guidelineHeight=_}
        },
        yDomain2: {get: function(){return yDomain2}, set: function(_){yDomain2=_}},
        interpolate: {get: function(){return interpolate}, set: function(_){interpolate=_}},
        legendRightAxisHint: {
          get: function(){return legendRightAxisHint},
          set: function(_){legendRightAxisHint=_}
        },

        // options that require extra logic in the setter
        margin: {get: function(){return margin}, set: function(_){
            margin.top    = _.top    !== undefined ? _.top    : margin.top
            margin.right  = _.right  !== undefined ? _.right  : margin.right
            margin.bottom = _.bottom !== undefined ? _.bottom : margin.bottom
            margin.left   = _.left   !== undefined ? _.left   : margin.left
        }},
        color:  {get: function(){return color}, set: function(_){
            color = nv.utils.getColor(_)
        }},
        duration: {get: function(){return duration}, set: function(_){
            duration = _
            lines2.duration(duration)
            stack2.duration(duration)
            yAxis2.duration(duration)
        }},
        x: {get: function(){return getX}, set: function(_){
            getX = _
            lines2.x(_)
            stack2.x(_)
        }},
        y: {get: function(){return getY}, set: function(_){
            getY = _
            lines2.y(_)
            stack2.y(_)
        }},
        forceY: {get: function(){return forceY}, set: function(_){forceY=_}}
    })

    nv.utils.initOptions(chart)

    return chart
}
