import React, { Component, PropTypes } from 'react'
import { ButtonGroup } from 'react-bootstrap'
import R from 'ramda'

const propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['checkbox', 'radio']).isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired
}

// getButtonGroupProps :: Props -> Object
const getButtonGroupProps = R.compose(
  R.omit(['onBlur', 'onDragStart', 'onDrop', 'onFocus']),
  R.omit(R.keys(propTypes))
)

export default class RadioButtonGroup extends Component {
  static propTypes = propTypes;

  static defaultProps = {
    type: 'radio'
  };

  handleClick(child) {
    const { onChange, type, value } = this.props
    const { value: childValue } = child.props
    if (R.equals(type, 'radio')) {
      !R.equals(childValue, value) && onChange(childValue)
    } else if (R.contains(childValue, value)) {
      onChange(R.reject(R.equals(childValue), value))
    } else {
      onChange(R.append(childValue, value))
    }
  }

  renderChildren() {
    const { children, type, value } = this.props
    const isRadio = R.equals(type, 'radio')

    return React.Children.map(children, (child) => {
      const { value: childValue } = child.props
      const isActive = isRadio
        ? R.equals(childValue, value)
        : R.contains(childValue, value)

      return React.cloneElement(child, {
        active: isActive,
        onClick: this.handleClick.bind(this, child)
      })
    })
  }

  render() {
    return (
      <ButtonGroup {...getButtonGroupProps(this.props)}>
        {this.renderChildren()}
      </ButtonGroup>
    )
  }
}
