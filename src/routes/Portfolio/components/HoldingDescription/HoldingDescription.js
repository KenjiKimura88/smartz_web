import React, { Component, PropTypes } from 'react'
import { Collapse } from 'react-bootstrap/lib'
import { MdInfoOutline } from 'helpers/icons'
import classes from './HoldingDescription.scss'
import InlineList from 'components/InlineList'
import P from 'components/P'
import Text from 'components/Text'

export default class HoldingDescription extends Component {
  static propTypes = {
    assetClassExplanation: PropTypes.string,
    tickersExplanation: PropTypes.string
  };

  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
  }

  handleClick = (e) => {
    e.stopPropagation()
    this.setState({ open: !this.state.open })
  }

  render () {
    const { assetClassExplanation, tickersExplanation } = this.props
    const { open } = this.state

    return (
      <div>
        <InlineList>
          <Text primary>
            <MdInfoOutline size={16} />
          </Text>
          <a onClick={this.handleClick}>
            Why these assets?
          </a>
        </InlineList>
        <Collapse in={open}>
          <InlineList>
            <MdInfoOutline size={16} style={{ opacity: 0 }} />
            <div className={classes.description}>
              {assetClassExplanation && <P>{assetClassExplanation}</P>}
              {tickersExplanation && <P>{tickersExplanation}</P>}
            </div>
          </InlineList>
        </Collapse>
      </div>
    )
  }
}
