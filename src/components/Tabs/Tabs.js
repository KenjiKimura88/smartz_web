import React, { Component, PropTypes } from 'react'
import { Tab } from 'react-bootstrap'
import { mapIndexed } from 'helpers/pureFunctions'
import classes from './Tabs.scss'
import ToggleButtonGroup from 'components/ToggleButtonGroup'

export default class Tabs extends Component {
  static propTypes = {
    activeKey: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,
    tabs: PropTypes.array.isRequired
  };

  render () {
    const { activeKey, children, className, id, onSelect, tabs } = this.props

    return (
      <Tab.Container activeKey={activeKey} id={id} onSelect={onSelect}>
        <div className={className}>
          <div className={classes.tabWrapper}>
            <ToggleButtonGroup activeKey={activeKey} justified options={tabs}
              onSelect={onSelect} />
          </div>
          <Tab.Content animation>
            {mapIndexed((child, index) =>
              <Tab.Pane key={tabs[index].key} className={classes.tabPane}
                eventKey={tabs[index].key}>
                {child}
              </Tab.Pane>
            , children)}
          </Tab.Content>
        </div>
      </Tab.Container>
    )
  }
}
