import { createStructuredSelector } from 'reselect'
import R from 'ramda'
import uuid from 'uuid'
import { activitySelector } from 'redux/selectors'
import { connect } from 'redux/api'
import { getAccounts, getGoals } from 'helpers/requests'
import { set } from 'redux/modules/activity'
import Activity from '../components/Activity'

const requests = ({ params: { accountId, clientId, goalId } }) => ({
  accountActivity: accountId && (({ findQuery }) => findQuery({
    type: 'activity',
    url: `/accounts/${accountId}/activity`,
    query: {
      account: parseInt(accountId, 10)
    },
    mergeParams: () => ({
      account: parseInt(accountId, 10),
      id: uuid.v4()
    })
  })),
  accounts: getAccounts(clientId),
  activityTypes: ({ findAll }) => findAll({
    type: 'activityTypes',
    url: '/settings/activity-types'
  }),
  clientActivity: (!accountId && !goalId) && (({ findAll }) => findAll({
    type: 'activity',
    url: `/clients/${clientId}/activity`,
    mergeParams: () => ({
      id: uuid.v4()
    })
  })),
  goalActivity: goalId && (({ findQuery }) => findQuery({
    type: 'activity',
    url: `/goals/${goalId}/activity`,
    query: {
      goal: parseInt(goalId, 10)
    },
    mergeParams: () => ({
      goal: parseInt(goalId, 10),
      id: uuid.v4()
    })
  })),
  goals: getGoals(clientId)
})

const selector = createStructuredSelector({
  activityState: activitySelector
})

const actions = {
  set
}

export default R.compose(
  connect(requests, selector, actions)
)(Activity)
