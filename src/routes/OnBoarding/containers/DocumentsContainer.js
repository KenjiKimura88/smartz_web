import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { DOCUMENTS_STEP_ID } from '../helpers'
import { documentsSchema } from 'schemas/onboarding'
import Documents from '../components/Documents'

export default R.compose(
  withRouter,
  reduxForm({
    form: 'onboardingDocuments',
    ...documentsSchema
  }, (state, { getValue }) => ({
    initialValues: R.defaultTo({}, getValue(['info', 'steps', DOCUMENTS_STEP_ID]))
  }))
)(Documents)
