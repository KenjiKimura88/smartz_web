import React, { PropTypes } from 'react'
import classes from './DataField.scss'

const DataField = ({ label, value }) => {
  return (
    <dl className={classes.dataField}>
      <dt className={classes.label}>{label}</dt>
      <dd className={classes.value}>{value}</dd>
    </dl>
  )
}

DataField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.node
}

export default DataField
