import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import classes from './ExternalAccounts.scss'
import H2 from 'components/H2'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Well from 'components/Well'

export default class ExternalAccounts extends Component {
  static propTypes = {
    externalAccounts: PropTypes.array.isRequired,
    isLoading: PropTypes.bool
  };

  render () {
    const { externalAccounts, isLoading } = this.props
    return (
      <div className={classes.externalAccounts}>
        {isLoading
        ? <Spinner />
        : R.map(externalAccount =>
          <Well key={externalAccount.id} className={classes.well}>
            <H2 size='medium' bold>
              {externalAccount.brokerage_name} - Account {externalAccount.id}
            </H2>
            <Row className={classes.balance}>
              <Col xs={3}>
                <Text size='medium' bold>
                  Balance
                </Text>
              </Col>
              <Col xs={9} className='text-right'>
                <Text bold>
                  {externalAccount.value &&
                    <FormattedNumber value={externalAccount.value} format='currency' />}
                </Text>
              </Col>
            </Row>
          </Well>
        , externalAccounts)}
      </div>
    )
  }
}
