import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classNames from 'classnames'
import Dropzone from 'react-dropzone'
import R from 'ramda'
import { BzCheck, BzRefresh, BzTrash, BzUpload } from 'icons'
import { parseDate, parseFloatFromString } from '../../helpers'
import Button from 'components/Button'
import classes from './SocialSecurityStatementUpload.scss'
import FieldError from 'components/FieldError'
import Panel from 'containers/Panel'

const getHasFile = R.compose(
  R.not,
  R.converge(R.or, [
    R.isNil,
    R.isEmpty
  ]),
  R.path(['field', 'value'])
)

const getFileNameFromFile = R.compose(
  R.prop('name'),
  R.defaultTo({}),
  R.head,
  R.defaultTo([]),
  R.path(['field', 'value'])
)

const getFileNameFromPath = R.compose(
  R.when(R.is(String), (path) => path.replace(/^.*[\\\/]/, '')),
  R.path(['field', 'value'])
)

const getFileName = R.either(getFileNameFromFile, getFileNameFromPath)

export default class SocialSecurityStatementUpload extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    estimateDate: PropTypes.object,
    fieldName: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    saveFile: PropTypes.func,
    isUploading: PropTypes.bool,
    ssFraTodays: PropTypes.object
  };

  handleChange = (files) => {
    const { onChange } = this.props.field
    onChange(files)
    files && !R.isEmpty(files) && this.submitStatement(files)
  }

  parseAndUpdateFormValues = ({ value }) => {
    const { estimateDate, ssFraTodays } = this.props
    if (estimateDate) {
      const estimateDateValue = R.compose(
        parseDate,
        R.path(['social_security_statement_data', 'date_of_estimate'])
      )(value)
      estimateDate.onChange(estimateDateValue)
    }
    if (ssFraTodays) {
      const ssFraTodaysValue = R.compose(
        parseFloatFromString,
        R.path(['social_security_statement_data', 'RetirementAtFull'])
      )(value)
      ssFraTodays.onChange(ssFraTodaysValue)
    }
  }

  submitStatement = (files) => {
    const { fieldName, saveFile } = this.props
    const data = new FormData()
    data.append(fieldName, R.head(files))

    saveFile({
      headers: {
        'Content-Disposition': `form-data; filename="${getFileName(files)}"`
      },
      body: data,
      multipart: true,
      success: this.parseAndUpdateFormValues
    })
  }

  render () {
    const { handleChange, props } = this
    const { field, id, isUploading } = props
    const hasFile = getHasFile(props)
    const className = classNames(classes.panel, {
      [classes.hasFile]: hasFile,
      [classes.isUploading]: isUploading
    })

    return (
      <div>
        <Row className={classes.socialSecurityStatementUpload}>
          <Col xs={hasFile ? 10 : 12}>
            <Panel id={id} className={className} header={<div />}>
              <Dropzone className={classes.dropzone} disablePreview multiple={false}
                onDrop={handleChange}>
                <Row>
                  <Col xs={10} className={classes.filename}>
                    {hasFile
                      ? <span>{getFileName(props)}</span>
                      : <span>Upload Social Security Statement</span>}
                  </Col>
                  <Col xs={2} className='text-right'>
                    {isUploading
                      ? <BzRefresh size={22} className={classes.spin} />
                      : hasFile
                        ? <BzCheck size={22} />
                        : <BzUpload size={22} />}
                  </Col>
                </Row>
              </Dropzone>
            </Panel>
          </Col>
          {hasFile &&
            <Col xs={2}>
              <Button className={classes.iconButton}
                onClick={function () { handleChange(null) }}>
                <BzTrash size={22} />
              </Button>
            </Col>}
        </Row>
        <Row>
          <Col xs={12}>
            <FieldError for={field} />
          </Col>
        </Row>
      </div>
    )
  }
}
