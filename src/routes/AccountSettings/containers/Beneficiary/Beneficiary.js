import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { connect } from 'redux/api'
import { domOnlyProps } from 'helpers/pureFunctions'
import DateTime from 'react-datetime'
import { BzCheck, BzTrash } from 'icons'
import { isBeneficiaryDirty, relationships, serializeBeneficiary } from '../../helpers'
import Button from 'components/Button'
import classes from './Beneficiary.scss'
import FieldError from 'components/FieldError'
import Select from 'components/Select'

export class Beneficiary extends Component {
  static propTypes = {
    beneficiary: PropTypes.object.isRequired,
    deleteBeneficiary: PropTypes.func,
    onDelete: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    saveBeneficiary: PropTypes.func.isRequired
  };

  save = () => {
    const { saveBeneficiary } = this.props
    if (isBeneficiaryDirty(this.props)) {
      saveBeneficiary({
        body: serializeBeneficiary(this.props)
      })
    }
  }

  delete = () => {
    const { beneficiary, deleteBeneficiary, onDelete } = this.props
    onDelete(beneficiary)
    R.is(Function, deleteBeneficiary) && deleteBeneficiary()
  }

  render () {
    const { beneficiary } = this.props
    return (
      <Row>
        <Col xs={3} componentClass={FormGroup} className={classes.formGroup}>
          <ControlLabel>Name</ControlLabel>
          <FormControl {...domOnlyProps(beneficiary.name)} />
          <FieldError for={beneficiary.name} />
        </Col>
        <Col xs={3} componentClass={FormGroup} className={classes.formGroup}>
          <ControlLabel>Relationship</ControlLabel>
          <Select options={relationships} placeholder='Select'
            value={beneficiary.relationship.value} onChange={beneficiary.relationship.onChange} />
          <FieldError for={beneficiary.relationship} />
        </Col>
        <Col xs={3} componentClass={FormGroup} className={classes.formGroup}>
          <ControlLabel>Birthdate</ControlLabel>
          <DateTime dateFormat='MM/DD/YYYY' timeFormat={false}
            onChange={beneficiary.birthdate.onChange} value={beneficiary.birthdate.value} />
          <FieldError for={beneficiary.birthdate} />
        </Col>
        <Col xs={2} componentClass={FormGroup} className={classes.formGroup}>
          <ControlLabel>Share</ControlLabel>
          <FormControl type='number' min={0} max={100} step={0.1}
            {...domOnlyProps(beneficiary.share)} />
          <FieldError for={beneficiary.share} />
        </Col>
        <Col xs={1} className='text-right'>
          <ControlLabel className='text-success'>
            {isBeneficiaryDirty(this.props)
              ? <span>&nbsp;</span>
              : <BzCheck />}
          </ControlLabel>
          <div>
            <Button onClick={this.delete}
              style={{ padding: '4px 8px 6px 8px' }}>
              <BzTrash size={21} />
            </Button>
          </div>
        </Col>
      </Row>
    )
  }
}

const requests = ({ beneficiary, params: { accountId, clientId } }) => ({
  deleteBeneficiary: beneficiary.id.value && (({ deleteRequest }) => deleteRequest({
    type: 'beneficiaries',
    url: `/clients/${clientId}/beneficiaries/${beneficiary.id.value}`,
    id: beneficiary.id.value
  })),
  saveBeneficiary: ({ create, update }) => beneficiary.id.value
    ? update({
      type: 'beneficiaries',
      url: `/clients/${clientId}/beneficiaries/${beneficiary.id.value}`,
      id: beneficiary.id.value,
      mergeParams: () => ({
        account: parseInt(accountId, 10)
      })
    })
    : create({
      type: 'beneficiaries',
      url: `/accounts/${accountId}/beneficiaries`,
      mergeParams: () => ({
        account: parseInt(accountId, 10)
      })
    })
})

const mergeProps = R.compose(
  R.mergeAll,
  R.unapply(R.identity)
)

export default R.compose(
  connect(requests, null, null, mergeProps, { withRef: true })
)(Beneficiary)
