import React from 'react'
import { mount, shallow } from 'enzyme'
import R from 'ramda'
import PendingStatus from 'routes/Allocation/components/PendingStatus'
import Text from 'components/Text'

describe('(Route) Allocation', () => {
  describe('(Component) PendingStatus', () => {
    const goal = {
      id: 1,
      name: 'AAA'
    }

    const getComponent = (props) =>
      <PendingStatus goal={goal} {...props} />

    it('should render as a <div />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal('div')
    })

    it('renders "Pending Approval" when selected_settings is present', () => {
      const wrapper = mount(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 1,
          selected_settings: 2
        })
      }))
      expect(wrapper.find(Text).last().text()).to.equal('Pending Approval')
    })

    it('renders "Pending Activation" when approved_settings is present', () => {
      const wrapper = mount(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 2,
          selected_settings: 2
        })
      }))
      expect(wrapper.find(Text).last().text()).to.equal('Pending Activation')
    })

    it('renders "Activated" when no selected_settings or approved_settings is present', () => {
      const wrapper = mount(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 1,
          selected_settings: 1
        })
      }))
      expect(wrapper.find(Text).last().text()).to.equal('Activated')
    })

    it('renders empty when no settings are present', () => {
      const wrapper2 = mount(getComponent())
      expect(wrapper2.find(Text).last().text()).to.equal('')
    })
  })
})
