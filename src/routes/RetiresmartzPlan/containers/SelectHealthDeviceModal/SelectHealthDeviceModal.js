import React, { Component, PropTypes } from 'react'
import { connectModal } from 'redux-modal'
import { Col, Modal, Row } from 'react-bootstrap'
import { mapIndexed } from 'helpers/pureFunctions'
import classes from './SelectHealthDeviceModal.scss'
import devices from './devices'

export class SelectHealthDeviceModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    show: PropTypes.bool
  };

  handleSelect = (provider) => {
    const { handleHide, onSelect } = this.props
    handleHide()
    onSelect(provider)
  }

  render () {
    const { handleSelect, props } = this
    const { handleHide, show } = props

    return (
      <Modal show={show} onHide={handleHide} backdrop='static' dialogClassName={classes.modal}>
        <Modal.Header closeButton>
          <Modal.Title>Select your health device</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row className={classes.itemWrapper}>
            {
              mapIndexed((device, index) => (
                <Col xs={6} key={index} className={classes.item}>
                  <a className={classes.button} disabled={device.disabled}
                    onClick={function() { !device.disabled && handleSelect(device.provider) }}>
                    <img src={device.image} alt='' />
                  </a>
                </Col>
              ), devices)
            }
          </Row>
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'selectHealthDeviceModal' })(SelectHealthDeviceModal)
