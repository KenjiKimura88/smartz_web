import React from 'react'
import { shallow } from 'enzyme'
import PortfolioDonut from 'components/PortfolioDonut'
import * as helpers from 'components/PortfolioDonut/helpers'

describe('(Component) PortfolioDonut', () => {
  const holdings = [{
    id: 1,
    percent: 10,
    tickers: [{ etf: true }],
    assetClass: {
      investment_type: 1,
      primary_color: 'red',
      display_name: 'foo',
    }
  },
  {
    id: 2,
    percent: 20,
    tickers: [{ etf: true }],
    assetClass: {
      investment_type: 2,
      primary_color: 'orange',
      display_name: 'bar',
    }
  },
  {
    id: 3,
    percent: 30,
    tickers: [{ etf: false }],
    assetClass: {
      investment_type: 3,
      primary_color: 'yellow',
      display_name: 'baz'
    }
  },
  {
    id: 4,
    percent: 40,
    tickers: [{ etf: false }],
    assetClass: {
      investment_type: 3,
      primary_color: 'green',
      display_name: 'qux'
    }
  }]

  const getComponent = props =>
    <PortfolioDonut holdings={holdings} {...props} />

  it('renders as a div', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.name()).to.eq('div')
  })

  describe('(Helper) mapHoldingToDatum', () => {
    const datum = helpers.mapHoldingToDatum(holdings[0])

    it('creates accurate datum for a holding', () => {
      expect(datum).to.deep.eq({id: 1, label: 'foo', value: 10, color: 'red'})
    })
  })

  describe('(Helper) mapPortfolioToDatum', () => {

  })

  describe('(Helper) mapPortfolioToETFDatum', () => {
    const [active, etf] = helpers.mapPortfolioToETFDatum(holdings)

    it('returns "Active" first', () => {
      // Chart is drawn clockwise
      expect(active.label).to.eq('Active')
    })

    it('appropriately splits ETF & active based on the ticker', () => {
      expect(etf.items.length).to.eq(2)
      expect(active.items.length).to.eq(2)
    })
  })

  describe('(Helper) mapDatumToRadii', () => {
    const radii = helpers.mapDatumToRadii(holdings, 1)

    it('highlights the activeHolding', () => {
      expect(radii[0].outer).to.eq('1.15')
    })
  })
})
