import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import DateTime from 'react-datetime'
import MaskedInput from 'react-input-mask'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import { getJointAccount, hasPendingJointAccount, hasConfirmedJointAccount, isMarried,
  partnerNameFromClient, partnerSsnFromClient, sumContributionsAmount } from '../../helpers'
import Button from 'components/Button'
import classes from './PersonalDetails.scss'
import ClientName from 'components/ClientName'
import CreateJointAccountModal from '../../containers/CreateJointAccountModal'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Text from 'components/Text'
import Select from 'components/Select'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Well from 'components/Well'

const defaultPartnerName = 'your spouse'

const civilStatusOptions = (civilStatuses) => (
  civilStatuses && R.compose(
    R.map(({ id, name }) => ({
      value: id,
      label: name
    })),
    R.defaultTo([])
  )(civilStatuses)
)

const getIncomeGroup = (income) => {
    const incomeGroup = parseInt(income / 10000)
    if (incomeGroup === 6) return 5
    if (incomeGroup < 1) return 1
    if (incomeGroup > 7) return 7
    return incomeGroup
}

const inDifferentIncomeGroup = (income1, income2) =>
  !R.equals(getIncomeGroup(income1), getIncomeGroup(income2))

export default class PersonalDetails extends Component {
  static propTypes = {
    accounts: PropTypes.array,
    civilStatuses: PropTypes.array,
    client: PropTypes.object,
    clientIncome: PropTypes.object.isRequired,
    expenses: PropTypes.array.isRequired,
    jointResendEmail: PropTypes.func.isRequired,
    managePartnerAccounts: PropTypes.object.isRequired,
    maritalStatus: PropTypes.object.isRequired,
    partnerBirthdate: PropTypes.object.isRequired,
    partnerName: PropTypes.object.isRequired,
    partnerIncome: PropTypes.object.isRequired,
    partnerSsn: PropTypes.object.isRequired,
    reloadPeerGroupExpenses: PropTypes.func.isRequired,
    show: PropTypes.func.isRequired,
    updateExpensesFields: PropTypes.func.isRequired,
    values: PropTypes.object
  };

  handleCivilStatusChange = (option) => {
    const { client, maritalStatus, partnerName, partnerSsn } = this.props
    maritalStatus.onChange(option)
    if (isMarried(option.value)) {
      R.isEmpty(partnerName.value) && partnerName.onChange(partnerNameFromClient(client))
      R.isEmpty(partnerSsn.value) && partnerSsn.onChange(partnerSsnFromClient(client))
    }
  }

  handleManagePartnerAccountsChange = (value) => {
    const { props } = this
    const { accounts, managePartnerAccounts, show } = props
    if (value && !getJointAccount(accounts)) {
      show('createJointAccountModal')
    }
    managePartnerAccounts.onChange(value)
  }

  handleCancelManagePartnerAccount = () => {
    const { managePartnerAccounts } = this.props
    managePartnerAccounts.onChange(false)
  }

  reinitializeExpenses(prevIncome, newIncome) {
    const { props } = this
    const { updateExpensesFields, values: { expenses: retirementAccounts } } = props
    const prevMontlyIncome = prevIncome / 12
    const newMonthlyIncome = newIncome / 12
    const monthlyBtc = sumContributionsAmount(retirementAccounts, 'self')
    const prevAvailableExpenses = prevMontlyIncome - monthlyBtc
    const newAvailableExpenses = newMonthlyIncome - monthlyBtc
    updateExpensesFields(prevAvailableExpenses, newAvailableExpenses)
  }

  handleClientIncomeChange = (value) => {
    const { client, clientIncome, reloadPeerGroupExpenses } = this.props
    const prevIncome = clientIncome.value
    const newIncome = value
    clientIncome.onChange(value)
    if (inDifferentIncomeGroup(clientIncome.value, value)) {
      reloadPeerGroupExpenses(client.id, newIncome, ({ value }) => {
        this.reinitializeExpenses(prevIncome, newIncome, value)
      })
    } else {
      this.reinitializeExpenses(prevIncome, newIncome)
    }
  }

  handleResendEmail = () => {
    const { jointResendEmail } = this.props
    jointResendEmail()
  }

  render() {
    const { props } = this
    const { accounts, client, clientIncome, civilStatuses, managePartnerAccounts, maritalStatus,
      partnerBirthdate, partnerIncome, partnerName, partnerSsn } = props
    const partnerNameValue = partnerName.value
    const finalPartnerName = partnerNameValue || defaultPartnerName

    return (
      <div>
        <FormGroup id='maritalStatus'>
          <H2>What is your marital status for tax filing?</H2>
          <Row>
            <Col xs={6}>
              <Select
                clearable={false}
                searchable={false}
                placeholder='Not Selected'
                options={civilStatusOptions(civilStatuses)}
                {...domOnlyProps(maritalStatus)}
                onChange={this.handleCivilStatusChange} />
              <FieldError for={maritalStatus} />
            </Col>
          </Row>
        </FormGroup>
        {isMarried(maritalStatus.value) && (
          <div>
            <H2 noMargin>What is your spouse's name and age?</H2>
            <Row>
              <Col xs={4}>
                <FormGroup controlId='partnerName'>
                  <ControlLabel>Your Spouse Name</ControlLabel>
                  <FormControl type='text' {...domOnlyProps(partnerName)} />
                  <FieldError for={partnerName} />
                </FormGroup>
              </Col>
              <Col xs={4}>
                <FormGroup>
                  <ControlLabel>Your Spouse Date of Birth</ControlLabel>
                  <DateTime dateFormat='MM/DD/YYYY' timeFormat={false}
                    onChange={partnerBirthdate.onChange} value={partnerBirthdate.value} />
                  <FieldError for={partnerBirthdate} />
                </FormGroup>
              </Col>
              <Col xs={4}>
                <FormGroup id='partnerSsn'>
                  <ControlLabel>Your Spouse Social Security Number</ControlLabel>
                  <FormControl componentClass={MaskedInput} mask='999-99-9999'
                    {...domOnlyProps(partnerSsn)} />
                  <FieldError for={partnerSsn} />
                </FormGroup>
              </Col>
            </Row>
          </div>)}
        <H2 noMargin>Current Annual Pre-Tax Income</H2>
        <Row>
          <Col xs={4}>
            <FormGroup controlId='annualIncome'>
              <ControlLabel>
                <ClientName client={client} defaultTo='Client' />
              </ControlLabel>
              <CurrencyInput {...clientIncome} onChange={this.handleClientIncomeChange} />
              <FieldError for={clientIncome} />
            </FormGroup>
          </Col>
          {isMarried(maritalStatus.value) && (
            <Col xs={4}>
              <FormGroup controlId='partnerAnnualIncome'>
                <ControlLabel>
                  {finalPartnerName}
                </ControlLabel>
                <CurrencyInput {...partnerIncome} />
                <FieldError for={partnerIncome} />
              </FormGroup>
            </Col>)}
        </Row>
        {isMarried(maritalStatus.value) && (
          <div id='managePartnerAccounts'>
            <H2>Include Spouse in a Joint Retirement Account?</H2>
            <Row>
              <Col xs={9}>
                <Text light tagName='P'>
                  By clicking yes, we will ask for your spouse's email address. They will be sent
                  an email to verify joint retirement account creation and to create a new account
                  if not an exising user.
                </Text>
              </Col>
            </Row>
            <FormGroup>
              <RadioButtonGroup type='radio' {...domOnlyProps(managePartnerAccounts)}
                onChange={this.handleManagePartnerAccountsChange}>
                <Button bsStyle='wide' value>Yes</Button>
                <Button bsStyle='wide' value={false}>No</Button>
              </RadioButtonGroup>
              <Row>
                <Col xs={4}>
                  <FieldError for={managePartnerAccounts} />
                </Col>
              </Row>
            </FormGroup>
            {R.equals(managePartnerAccounts.value, true) && hasPendingJointAccount(accounts) &&
              <FormGroup>
                <Well bsStyle='success'>
                  Your spouse has been sent an email to verify their joint retirement plan.
                  We will notify you when this happens or continue now for an individual plan.
                  If you want to resend verification email, click{' '}
                  <a href='#' onClick={this.handleResendEmail}>here</a>.
                </Well>
              </FormGroup>
            }
            {R.equals(managePartnerAccounts.value, true) && hasConfirmedJointAccount(accounts) &&
              <FormGroup>
                <Well bsStyle='success'>
                  {partnerName.value} verified joint retirement plan.
                </Well>
              </FormGroup>
            }
            {R.equals(managePartnerAccounts.value, false) &&
              <FormGroup>
                <Well bsStyle='danger'>
                  We will make assumptions about your spouse which will reduce the accuracy
                  of your retirement plan. To improve accuracy we recommend creating a joint
                  retirement plan.
                </Well>
              </FormGroup>}
          </div>)}
        <Hr className={classes.hrLine} />
        <CreateJointAccountModal ssn={partnerSsn.value}
          onCancel={this.handleCancelManagePartnerAccount} />
      </div>
    )
  }
}
