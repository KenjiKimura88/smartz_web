import R from 'ramda'
import { connect } from 'redux/api'
import Risk from '../components/Risk'

const findQuestions = (riskProfileGroups, invitation) =>
  R.compose(
    R.defaultTo({}),
    R.find(R.propEq('id', invitation.risk_profile_group)),
    R.defaultTo([])
  )(riskProfileGroups).questions

const selector = (state, { riskProfileGroups, invitation }) => ({
  questions: findQuestions(riskProfileGroups, invitation)
})

const requests = () => ({
  riskProfileGroups: ({ findAll }) => findAll({
    type: 'riskProfileGroups',
    url: '/settings/risk-profile-groups'
  })
})

export default R.compose(
  connect(requests),
  connect(null, selector)
)(Risk)
