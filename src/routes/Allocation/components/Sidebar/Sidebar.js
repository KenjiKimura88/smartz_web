import React, { Component, PropTypes } from 'react'
import Constraints from '../../containers/Constraints'
import Settings from '../../containers/Settings'
import Tabs from 'components/Tabs'

const tabs = [
  {
    key: 'settings',
    label: 'Settings'
  },
  {
    key: 'constraints',
    label: 'Constraints'
  }
]

export default class Sidebar extends Component {
  static propTypes = {
    activeTabKey: PropTypes.string.isRequired,
    constraints: PropTypes.array.isRequired,
    fields: PropTypes.object.isRequired,
    goal: PropTypes.object.isRequired,
    portfolio: PropTypes.object,
    set: PropTypes.func.isRequired,
    settings: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    viewedSettings: PropTypes.string.isRequired
  };

  handleTabSelect = (tabKey) => {
    const { set } = this.props
    set({
      id: 'tab',
      activeKey: tabKey
    })
  }

  render () {
    const { handleTabSelect } = this
    const { activeTabKey, fields, fields: { constraints }, goal, portfolio, settings,
      values, viewedSettings } = this.props

    return (
      <Tabs activeKey={activeTabKey} id='allocation-sidebar-tabs' onSelect={handleTabSelect}
        tabs={tabs}>
        <Settings fields={fields} goal={goal} portfolio={portfolio} settings={settings}
          values={values} viewedSettings={viewedSettings} />
        <Constraints constraints={constraints} constraintsValues={values.constraints} />
      </Tabs>
    )
  }
}
