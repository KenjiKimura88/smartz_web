import { connect } from 'redux/api'
import R from 'ramda'
import { findSingle } from 'redux/api/modules/requests'
import { hide, show } from 'redux-modal'
import { getClient } from 'helpers/requests'
import Profile from '../components/Profile'

const requests = ({ params: { clientId } }) => ({
  changePassword: ({ create }) => create({
    type: 'changePassword',
    url: '/me/password/'
  }),
  client: getClient(clientId),
  plaidAccounts: ({ findAll }) => findAll({
    type: 'plaidAccounts',
    url: '/plaid/get-accounts',
    footprint: R.prop('_id')
  }),
  settings: (({ findSingle }) => findSingle({
    type: 'globalSettings',
    url: '/settings'
  }))
})

const actions = {
  hide,
  show,
  refreshProfile: () => findSingle({
    type: 'me',
    force: true
  })
}

export default R.compose(
  connect(requests, null, actions)
)(Profile)
