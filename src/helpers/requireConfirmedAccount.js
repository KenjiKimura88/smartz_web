import R from 'ramda'
import { show } from 'redux-modal'
import { findOne } from 'redux/api/modules/requests'

const maybeAbortAndShowModal = (store, callback, account) => {
  if (account && !account.confirmed) {
    store.dispatch(show('accountNotConfirmed', { account }))
  } else if (account && account.confirmed) {
    callback()
  }
}

export default R.curry(({ store }, nextState, replace, callback) => {
  const { params: { accountId } } = nextState
  store.dispatch(
    findOne({
      type: 'accounts',
      id: accountId,
      success: ({ value }) => maybeAbortAndShowModal(store, callback, value),
      fail: () => callback()
    })
  )
})
