import { connect } from 'redux/api'
import { reduxForm } from 'redux-form'
import R from 'ramda'
import FinancialProfile from '../components/FinancialProfile'
import financialProfileSchema from 'schemas/financialProfileSchema'

const requests =  ({ client }) => ({
  updateClient: client && (({ update }) => update({
    type: 'clients',
    id: client.id,
    url: `/clients/${client.id}?financial-profile`
  })),
})

const initialValuesFromUser = (client) => R.reduce(
  (initialValues, fieldName) => (
    R.merge(initialValues, {[fieldName]: R.defaultTo('', initialValues[fieldName])})
  ),
  R.pick(financialProfileSchema.fields, client || {}),
  financialProfileSchema.fields
)

export default R.compose(
  connect(requests),
  reduxForm({
    form: 'financialProfileSettings',
    ...financialProfileSchema
  }, (state, props) => ({
    initialValues: initialValuesFromUser(props.client)
  }))
)(FinancialProfile)
