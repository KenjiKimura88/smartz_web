import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './redux/createStore'
import AppContainer from './containers/AppContainer'

// ========================================================
// Store and History Instantiation
// ========================================================
const initialState = JSON.parse(
  window.localStorage.getItem(LOCALSTORAGE_ROOT) || '{}'
)
const store = createStore(initialState)

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  const routes = require('./routes/index')({ store })

  ReactDOM.render(
    <AppContainer routes={routes} store={store} />,
    MOUNT_NODE
  )
}

// Log current tag-commit for debugging
console.log( // eslint-disable-line no-console
  `%c BetaSmartz Web App: ${GIT.tag}@${GIT.commit}`, 'color: #2CA127'
)

// ========================================================
// Developer Tools Setup
// ========================================================

// This code is excluded from production bundle
if (__DEV__) {
  window.perf = require('react-addons-perf')

  if (module.hot) {
    // Development render functions
    const renderApp = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    // Wrap render in try/catch
    render = () => {
      try {
        renderApp()
      } catch (error) {
        renderError(error)
      }
    }

    // Setup hot module replacement
    module.hot.accept('./routes/index', () => {
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    })
  }
}

// ========================================================
// Go!
// ========================================================
render()
