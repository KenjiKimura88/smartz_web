import moment from 'moment'
import momentParseFormat from 'moment-parseformat'
import R from 'ramda'
import config from 'config'
import statesOptions from 'helpers/states'

const { onboarding: { EMPLOYMENT_STATUS }, maritalStatuses } = config

export const CREATELOGIN_STEP_ID = 0
export const IBACCOUNT_STEP_ID = 1

export const RESIDENCE_STEP_ID = 0
export const DOCUMENTS_STEP_ID = 1
export const CONTACT_INFO_STEP_ID = 2
export const MISC_INFO_STEP_ID = 3
export const EMPLOYMENT_STEP_ID = 4
export const TRADING_INFO_STEP_ID = 5

export const validateOnboardingCreateLogin = (values) => {
  const errors = {}
  if (!values.password) {
    errors.password = ['You must enter a password']
  }
  if (!values.passwordConfirmation) {
    errors.passwordConfirmation = ['You must confirm your password']
  }
  if (values.passwordConfirmation && values.password !== values.passwordConfirmation) {
    errors.passwordConfirmation = ['Passwords do not match']
  }
  if (!values.primarySecurityQuestion) {
    errors.primarySecurityQuestion = ['You must enter a primary security question']
  }
  if (!values.primarySecurityAnswer) {
    errors.primarySecurityAnswer = ['You must enter a primary security answer']
  }
  if (!values.secondarySecurityQuestion) {
    errors.secondarySecurityQuestion = ['You must enter a secondary security question']
  }
  if (!values.secondarySecurityAnswer) {
    errors.secondarySecurityAnswer = ['You must enter a secondary security answer']
  }
  return errors
}

export const stateNameFromCode = (code) => R.compose(
  R.defaultTo(code),
  R.prop('label'),
  R.defaultTo({}),
  R.find(R.propEq('value', code))
)(statesOptions)

export const shouldIncludeStudentLoan = (_props, employmentStatus) => {
  const { getValue } = _props
  const dateOfBirth = getValue(['info', 'steps', MISC_INFO_STEP_ID, 'dateOfBirth'])
  return R.lt(moment().diff(dateOfBirth, 'years'), 50) &&
    R.contains(employmentStatus, [
      EMPLOYMENT_STATUS.UNEMPLOYED,
      EMPLOYMENT_STATUS.EMPLOYED,
      EMPLOYMENT_STATUS.SELF_EMPLOYED,
      EMPLOYMENT_STATUS.NOT_LABORFORCE
    ])
}

const riskProfileAnswers = (stepData) =>
  R.reduce((answers, item) => R.append(item.value, answers), [], stepData)

const employmentFields = (fields) => {
  switch (fields.status) {
    case EMPLOYMENT_STATUS.EMPLOYED:
      return {
        employer_type: fields.employerType,
        employer: fields.employer,
        employment_status: fields.status,
        income: parseInt(fields.employmentIncome, 10),
        other_income: parseInt(fields.otherIncome, 10),
        industry_sector: fields.industrySector,
        occupation: fields.occupation
      }
    case EMPLOYMENT_STATUS.SELF_EMPLOYED:
      return {
        employer_type: fields.employerType,
        employer: fields.employer,
        employment_status: fields.status,
        income: parseInt(fields.businessIncome, 10),
        other_income: parseInt(fields.otherIncome, 10),
        industry_sector: fields.industrySector,
        occupation: fields.occupation
      }
    case EMPLOYMENT_STATUS.RETIRED:
    case EMPLOYMENT_STATUS.UNEMPLOYED:
    case EMPLOYMENT_STATUS.NOT_LABORFORCE:
      return {
        employment_status: fields.status,
        income: 0,
        otherIncome: parseInt(fields.otherIncome, 10)
      }
    default:
    return {
      employment_status: fields.status
    }
  }
}

const containsAll = (words, source) => {
  let regexStr = R.reduce((regexStr, word) => (
    regexStr + '(?=.*\\b' + word + ')'
  ), '^', words) + '.*$'
  return RegExp(regexStr, 'i').test(source)
}

export const maritalStatusFromTaxTranscript = (filingStatusText) => {
  if (containsAll(['Married', 'Filing', 'Joint'], filingStatusText)) {
    return maritalStatuses.MARRIED_FILING_JOINTLY
  }
  if (containsAll(['Qualifying', 'Widow'], filingStatusText)) {
    return maritalStatuses.QUALIFYING_WIDOWER
  }
  if (containsAll(['Head', 'Household'], filingStatusText)) {
    return maritalStatuses.HEAD_OF_HOUSEHOLD
  }

  if (containsAll(['Single'], filingStatusText)) {
    return maritalStatuses.SINGLE
  }
  if (containsAll(['Married', 'Filing', 'Separately'], filingStatusText)) {
    return maritalStatuses.MARRIED_FILING_SEPARATELY_LIVED_TOGETHER
  }
  return null
}

const isUS = R.equals('US')

export const serialize = ({ getValue, ibEnabled, salutation, suffix }) => {
  const residence = getValue(['info', 'steps', RESIDENCE_STEP_ID])
  const account = getValue(['login', 'steps', IBACCOUNT_STEP_ID])
  const documents = getValue(['info', 'steps', DOCUMENTS_STEP_ID])
  const contactInfo = getValue(['info', 'steps', CONTACT_INFO_STEP_ID])
  const miscInfo = getValue(['info', 'steps', MISC_INFO_STEP_ID])
  const tradingInfo = getValue(['info', 'steps', TRADING_INFO_STEP_ID])
  const employment = getValue(['info', 'steps', EMPLOYMENT_STEP_ID])
  const agreements = getValue(['agreements', 'steps', 0])
  const risk = getValue(['risk', 'steps'])

  const { country } = residence

  const residentialAddress = {
    address: R.join('\n', [
      contactInfo.address1,
      contactInfo.address2,
      contactInfo.city
    ]),
    post_code: contactInfo.zipCode,
    region: {
      country,
      name: stateNameFromCode(contactInfo.state),
      code: contactInfo.state
    }
  }

  return {
    residential_address: residentialAddress,
    phone_num: contactInfo.phoneNumber,
    date_of_birth: moment(miscInfo.dateOfBirth).format('YYYY-MM-DD'),
    gender: miscInfo.gender,
    civil_status: miscInfo.filingStatus,
    ...employmentFields(employment),
    student_loan: shouldIncludeStudentLoan({ getValue }, employment.status)
      ? employment.studentLoan
      : undefined,
    student_loan_assistance_program: employment.studentLoanOfferLoanRepayment,
    student_loan_graduate_looking: employment.studentLoanGraduateLooking,
    student_loan_parent_looking: employment.studentLoanParentLooking,
    hsa_eligible: employment.healthSavingsAccount,
    hsa_provider_name: employment.HSAProviderName,
    hsa_state: employment.HSAState,
    hsa_coverage_type: employment.HSACoverageType,
    betasmartz_agreement: true,
    advisor_agreement: agreements.advisorAgreement,
    regional_data: {
      politically_exposed: miscInfo.politicalExposure,
      ...( isUS(country) ? {
        ssn: miscInfo.ssn,
        tax_transcript: documents.taxTranscript,
        tax_transcript_data: R.merge(documents.taxTranscriptData, {
          'SSN': miscInfo.ssn
        }),
        tax_transcript_data_ex: R.omit([
          'completed', 'taxTranscript', 'taxTranscriptData', 'taxTranscriptFile'
        ], documents)
      } : {})
    },
    risk_profile_responses: riskProfileAnswers(risk),
    ib_onboard: ibEnabled ? {
      account_number: account.accountNumber,
      tax_address: contactInfo.isMailingAddress ? {
        address: R.join('\n', [
          contactInfo.mailingAddress1,
          contactInfo.mailingAddress2,
          contactInfo.mailingCity
        ]),
        post_code: contactInfo.mailingZipCode,
        region: {
          country: contactInfo.mailingCountry,
          name: contactInfo.mailingState,
          code: contactInfo.mailingState
        }
      } : residentialAddress,
      employer_address: {
        address: R.join('\n', [
          employment.employerAddress1,
          employment.employerAddress2,
          employment.employerCity
        ]),
        post_code: employment.employerZipCode,
        region: {
          country: employment.employerCountry,
          name: employment.employerState,
          code: employment.employerState
        },
      },
      num_dependents: miscInfo.dependents,
      country_of_birth: miscInfo.countryOfBirth,
      identif_leg_citizenship: miscInfo.countryOfCitizenship,
      is_permanent_resident: miscInfo.isUsPermanentResident,
      reg_status_broker_deal: tradingInfo.regulatoryQuestion1,
      reg_status_exch_memb: tradingInfo.regulatoryQuestion2,
      reg_status_disp: tradingInfo.regulatoryQuestion3,
      reg_status_investig: tradingInfo.regulatoryQuestion4,
      reg_status_stk_cont: tradingInfo.regulatoryQuestion5,
      asset_exp_0_yrs: tradingInfo.stockYearsTrading,
      asset_exp_0_trds_per_yr: tradingInfo.stockTradesPerYear,
      asset_exp_0_knowledge: tradingInfo.stockKnowledgeLevel,
      asset_exp_1_yrs: tradingInfo.fundYearsTrading,
      asset_exp_1_trds_per_yr: tradingInfo.fundTradesPerYear,
      asset_exp_1_knowledge: tradingInfo.fundKnowledgeLevel,
      salutation,
      suffix
    } : undefined
  }
}

export const parseNumber = R.compose(
  R.defaultTo(0),
  parseFloat,
  R.replace(/[$,]/, '')
)

export const validateTaxTranscriptData = (taxTranscriptParsed) =>
  R.merge(taxTranscriptParsed, {
    adjusted_gross_income: parseNumber(taxTranscriptParsed['adjusted_gross_income']),
    taxable_income: parseNumber(taxTranscriptParsed['taxable_income']),
    add_child_tax_credit: parseNumber(taxTranscriptParsed['add_child_tax_credit']),
    earned_income_credit: parseNumber(taxTranscriptParsed['earned_income_credit']),
    excess_ss_credit: parseNumber(taxTranscriptParsed['excess_ss_credit']),
    combat_credit: parseNumber(taxTranscriptParsed['combat_credit']),
    refundable_credit: parseNumber(taxTranscriptParsed['refundable_credit']),
    total_adjustments: parseNumber(taxTranscriptParsed['total_adjustments']),
    std_deduction: parseNumber(taxTranscriptParsed['std_deduction']),
    exemption_amount: parseNumber(taxTranscriptParsed['exemption_amount']),
    total_credits: parseNumber(taxTranscriptParsed['total_credits']),
    exemptions: parseNumber(taxTranscriptParsed['exemptions']),
    total_payments: parseNumber(taxTranscriptParsed['total_payments']),
    total_tax: parseNumber(taxTranscriptParsed['total_tax']),
    tentative_tax: parseNumber(taxTranscriptParsed['tentative_tax']),
    total_income: parseNumber(taxTranscriptParsed['total_income']),
    tax_period: moment(taxTranscriptParsed['tax_period'], momentParseFormat(taxTranscriptParsed['tax_period'])).format('YYYY-MM-DD'), // eslint-disable-line
    se_tax: parseNumber(taxTranscriptParsed['se_tax']),
    premium_tax_credit: parseNumber(taxTranscriptParsed['premium_tax_credit']),
    blind: !!parseNumber(taxTranscriptParsed['blind']),
    blind_spouse: !!parseNumber(taxTranscriptParsed['blind_spouse'])
  })

export const serializeTaxTranscriptData = (values) =>
  R.merge(values.taxTranscriptData, {
    adjusted_gross_income: values.adjustedGrossIncome,
    taxable_income: values.taxableIncome,
    add_child_tax_credit: values.additionalChildTaxCredit,
    earned_income_credit: values.earnedIncomeCredit,
    excess_ss_credit: values.excessSocialSecurity,
    combat_credit: values.nonTaxableCombatPay,
    refundable_credit: values.otherRefundableCredits,
    total_adjustments: values.totalAdjustments,
    std_deduction: values.standardDeduction,
    // TODO: verify field name
    se_tax: values.selfEmploymentTax,
    // TODO: verify field name
    premium_tax_credit: values.netPremiumCredit,
    // TODO: needs to check with other possible values
    blind: values.isBlind,
    // TODO: needs to check with other possible values
    blind_spouse: values.isSpouseBlind
  })

export const FILING_STATUS_OPTIONS = [
  {
    value: config.maritalStatuses.MARRIED_FILING_JOINTLY,
    label: 'Married filing jointly'
  },
  {
    value: config.maritalStatuses.SINGLE,
    label: 'Single'
  },
  {
    value: config.maritalStatuses.HEAD_OF_HOUSEHOLD,
    label: 'Head of household'
  },
  {
    value: config.maritalStatuses.MARRIED_FILING_SEPARATELY_NOT_LIVED_TOGETHER,
    label: 'Married filing separately'
  },
  {
    value: config.maritalStatuses.QUALIFYING_WIDOWER,
    label: 'Qualified widow(er)'
  }
]

export const getCountry = ({ getValue }) =>
  getValue(['info', 'steps', RESIDENCE_STEP_ID, 'country'])
