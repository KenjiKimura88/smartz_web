import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { findQuerySelector } from 'redux/api/selectors'
import { getGoal } from 'helpers/requests'
import NewGoalComplete from '../components/NewGoalComplete'

const requests = ({ params: { clientId, goalId } }) => ({
  assetsClasses: ({ findAll }) => findAll({
    type: 'assetsClasses',
    url: '/settings/asset-classes'
  }),
  goal: getGoal({ clientId, goalId }),
  positions: ({ findAll }) => findAll({
    type: 'positions',
    url: `/goals/${goalId}/positions`,
    selector: findQuerySelector({
      type: 'positions',
      query: { goal: parseInt(goalId, 10) }
    })
  }),
  tickers: ({ findAll }) => findAll({
    type: 'tickers',
    url: '/settings/tickers'
  })
})

export default R.compose(
  connect(requests),
  withRouter
)(NewGoalComplete)
