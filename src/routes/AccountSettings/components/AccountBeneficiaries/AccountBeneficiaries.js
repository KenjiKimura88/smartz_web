import React, { Component, PropTypes } from 'react'
import { Col, Form, Modal, Row } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { FormattedNumber } from 'react-intl'
import { reduxForm } from 'redux-form'
import { mapIndexed, removeField } from 'helpers/pureFunctions'
import R from 'ramda'
import { BzPlusCircle } from 'icons'
import { deserializeBeneficiaries, getBeneficiariesByType, getTotalShareForBeneficiariesType,
  isAnyBeneficiaryDirty, types } from '../../helpers'
import Beneficiary from '../../containers/Beneficiary'
import Button from 'components/Button'
import classes from './AccountBeneficiaries.scss'
import createSchema from 'schemas/accountBeneficiaries'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import P from 'components/P'
import Text from 'components/Text'

const getBeneficiariesChildren = R.compose(
  R.flatten,
  R.values,
  R.prop('beneficiariesChildren')
)

const renderTotalShare = (share) => share
  ? <div>
    <Col xs={4} className='text-right'>
      <Text light>Total</Text>
    </Col>
    <Col xs={3}>
      <Text light>
        <FormattedNumber value={share / 100} format='percent' />
      </Text>
    </Col>
  </div>
  : false

export class AccountBeneficiaries extends Component {
  static propTypes = {
    beneficiaries: PropTypes.array.isRequired,
    errors: PropTypes.object.isRequired,
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired
  };

  constructor (props) {
    super(props)
    this.beneficiariesChildren = {
      primary: [],
      contingent: []
    }
  }

  addBeneficiary = (typeLabel) => {
    const { beneficiaries } = this.props.fields
    beneficiaries.addField({
      type: types[typeLabel]
    })
  }

  deleteBeneficiary = (beneficiary) => {
    const { fields: { beneficiaries } } = this.props
    removeField(beneficiary, beneficiaries)
  }

  save = () => {
    R.forEach(
      child => child.getWrappedInstance().getWrappedInstance().save(),
      getBeneficiariesChildren(this)
    )
  }

  render () {
    const { addBeneficiary, deleteBeneficiary, props } = this
    const { errors, fields: { beneficiaries }, handleHide, handleSubmit, params, show } = props
    const primaryBeneficiaries = getBeneficiariesByType('primary')(beneficiaries)
    const contingentBeneficiaries = getBeneficiariesByType('contingent')(beneficiaries)
    const primaryShare = getTotalShareForBeneficiariesType('primary')(beneficiaries)
    const contingentShare = getTotalShareForBeneficiariesType('contingent')(beneficiaries)

    return (
      <Modal className={classes.accountBeneficiaries} show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>
            Transfer On Death Beneficiaries
          </Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit(this.save)}>
          <Modal.Body>
            <P>Register your existing brokerage account as a Transfer on Death (TOD) account and
            designate the beneficiaries to receive the account assets upon your death. Adding a
            beneficiary to this account will supersede any conflicting provisions in any will,
            trust, or agreement, except for joint accounts. For joint accounts, ownership shall
            pass to the surviving joint account holder.</P>
            <H2>Primary Beneficiaries</H2>
            {mapIndexed((beneficiary, index) =>
              <Beneficiary key={index} beneficiary={beneficiary} onDelete={deleteBeneficiary}
                params={params} ref={c => this.beneficiariesChildren.primary[index] = c} />
            , primaryBeneficiaries)}
            <Row className={classes.addButtonRow}>
              <Col xs={5}>
                <Button onClick={function () { addBeneficiary('primary') }}>
                  <BzPlusCircle size={18} />{' '}
                  <span>Add Primary Beneficiary</span>
                </Button>
              </Col>
              {renderTotalShare(primaryShare)}
            </Row>
            <Row className={classes.errors}>
              <Col xs={12}>
                {errors.primary && <span className='text-danger'>{errors.primary}</span>}
              </Col>
            </Row>
            <Hr />
            <H2>Contingent Beneficiaries</H2>
            {mapIndexed((beneficiary, index) =>
              <Beneficiary key={index} beneficiary={beneficiary} onDelete={deleteBeneficiary}
                params={params} ref={c => this.beneficiariesChildren.contingent[index] = c} />
            , contingentBeneficiaries)}
            <Row className={classes.addButtonRow}>
              <Col xs={5}>
                <Button onClick={function () { addBeneficiary('contingent') }}>
                  <BzPlusCircle size={18} />{' '}
                  <span>Add Contingent Beneficiary</span>
                </Button>
              </Col>
              {renderTotalShare(contingentShare)}
            </Row>
            <Row className={classes.errors}>
              <Col xs={12}>
                {errors.contingent && <span className='text-danger'>{errors.contingent}</span>}
              </Col>
            </Row>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle='primary' type='submit' disabled={!isAnyBeneficiaryDirty(props)}>
              Save
            </Button>
            <Button onClick={handleHide}>
              Close
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'accountBeneficiaries' }),
  reduxForm({
    form: 'accountBeneficiaries',
    ...createSchema({ types })
  }, (state, props) => ({
    initialValues: {
      beneficiaries: deserializeBeneficiaries(props)
    }
  }))
)(AccountBeneficiaries)
