import React from 'react'
import { shallow } from 'enzyme'
import HoldingList from 'routes/Portfolio/components/HoldingList'
import mountWithIntl from '../../../test-helpers/mountWithIntl'

describe('(Route) Portfolio', () => {
  describe('(Component) HoldingList', () => {
    const goal = {
      balance: 1500
    }

    const holdings = [
      {
        id: 1,
        value: 2500,
        percent: 25,
        assetClass: {
          color: '#FF0000',
          display_name: 'B'
        },
        tickers: [],
        shares: 2.15
      },
      {
        id: 2,
        value: 4000,
        percent: 40,
        assetClass: {
          color: '#00FF00',
          display_name: 'A'
        },
        tickers: [],
        shares: 4
      }
    ]

    const getComponent = (props) =>
      <HoldingList activeHolding={2} goal={goal} holdings={holdings}
        resetActiveHolding={function () {}} setActiveHolding={function () {}} {...props} />

    it('renders as a <div />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.name()).to.equal('div')
    })

    it('always renders "Unallocated Funds"', () => {
      const unalloc = mountWithIntl(getComponent())
        .find('tbody tr').last().find('td').first()
      const emptyUnalloc = mountWithIntl(getComponent({ holdings: [] }))
        .find('tbody tr').last().find('td').first()
      expect(unalloc.text()).to.eq('Unallocated Funds')
      expect(emptyUnalloc.text()).to.eq('Unallocated Funds')
    })

    it('renders the correct amount in unallocated funds', () => {
      const wrapper = mountWithIntl(getComponent({ goal: { balance: 10000 } }))
        .find('td')
        .filterWhere(td => td.text() === '$3,500.00')
      expect(wrapper.length).to.eq(1)
    })

    it('renders alphabetically with "Unallocated Funds" coming last', () => {
      const wrapper = mountWithIntl(getComponent())
      // TODO: improve (should ignore <td>'s for item details)
      const holdingList = wrapper.find('tbody tr').map(tr => tr.find('td').first().text())
      expect(holdingList[0]).to.equal('A')
      expect(holdingList[2]).to.equal('B')
      expect(holdingList[4]).to.equal('Unallocated Funds')
    })
  })
})
