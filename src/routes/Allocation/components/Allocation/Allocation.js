import React, { Component, PropTypes } from 'react'
import { Collapse, ControlLabel } from 'react-bootstrap'
import R from 'ramda'
import { deserializeSettings, getConstraints, getValidConstraints,
  newSettings } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import AllocationHeader from '../AllocationHeader'
import Button from 'components/Button'
import CalculatePortfolio from '../../containers/CalculatePortfolio'
import classes from './Allocation.scss'
import ConfirmGoalSettingsModal from '../../containers/ConfirmGoalSettingsModal'
import ContentPane from 'components/ContentPane'
import CreateGoalSuccessModal from '../../containers/CreateGoalSuccessModal'
import GoalNavigation from 'components/GoalNavigation'
import Holdings from '../../containers/Holdings'
import MainFrame from 'components/MainFrame'
import ProjectionGraph from '../../containers/ProjectionGraph'
import Sidebar from 'components/Sidebar'
import SidebarContent from '../Sidebar'
import Spinner from 'components/Spinner'
import Text from 'components/Text'
import Well from 'components/Well'

const numberOfConstraintsChanged = (settings, validConstraints) => !R.equals(
  R.length(getConstraints(settings)),
  R.length(validConstraints)
)

const metricChanged = metric =>
  metric.comparison.dirty || metric.feature.dirty || metric.configured_val.dirty

const metricsChanged = (props) => {
  const { fields, settings, values } = props
  const validConstraints = getValidConstraints(values.constraints)
  return (numberOfConstraintsChanged(settings, validConstraints)) ||
    fields.risk.dirty || R.any(metricChanged, fields.constraints)
}

const isDirty = ({ dirty, settings, values }) =>
  R.or(dirty, numberOfConstraintsChanged(settings, getValidConstraints(values.constraints)))

const getActiveTabKey = R.pathOr('settings', ['allocationState', 'tab', 'activeKey'])
const getDisclaimer = R.pathOr(false, ['allocationState', 'disclaimer', 'open'])

const sidebarHeader = (_props, allocationInstance) => {
  const { accounts, params: { clientId, goalId }, router: { push } } = _props
  return (
    <div>
      <ControlLabel>
        <Text size='small' light>Goal</Text>
      </ControlLabel>
      <GoalNavigation accounts={accounts} onSelectGoal={function (goal) {
        push(`/${clientId}/allocation/${goal.id}`)
      }} selectedItem={{ id: goalId, type: 'goal' }} key='goalNavigation' />
    </div>
  )
}

export default class Allocation extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    allocationState: PropTypes.object.isRequired,
    approve: PropTypes.func.isRequired,
    dirty: PropTypes.bool.isRequired,
    fields: PropTypes.object.isRequired,
    goal: PropTypes.object,
    goals: PropTypes.array.isRequired,
    oneTimeDepositForm: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    performance: PropTypes.object,
    resetForm: PropTypes.func.isRequired,
    revert: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    setAllocationVar: PropTypes.func.isRequired,
    setGoalsVar: PropTypes.func.isRequired,
    settings: PropTypes.object,
    show: PropTypes.func.isRequired,
    toggleAllocationVar: PropTypes.func.isRequired,
    user: PropTypes.object,
    values: PropTypes.object.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['allocationState', 'dirty', 'errors', 'fields', 'goal', 'params',
      'performance', 'settings', 'user', 'values'], this.props, nextProps) ||
      propsChanged(['values'], this.props.oneTimeDepositForm, nextProps.oneTimeDepositForm)
  }

  toggleDisclaimer = () => {
    const { toggleAllocationVar } = this.props
    toggleAllocationVar({
      id: 'disclaimer',
      prop: 'open'
    })
  }

  render () {
    const { props } = this
    const { approve, destroyForm, fields, fields: { constraints, performance }, goal,
      initializeForm, oneTimeDepositForm, params, params: { clientId, viewedSettings },
      revert, setAllocationVar, settings, show, user, values } = props
    const validConstraints = getValidConstraints(values.constraints)
    const portfolio = metricsChanged(props)
      ? null
      : (settings && settings.portfolio && R.merge(settings.portfolio, performance))
    const finalValues = R.merge(values, { oneTimeDeposit: oneTimeDepositForm.values.amount })
    const dirty = isDirty(props)
    const activeTabKey = getActiveTabKey(props)
    const finalSettings = R.equals(viewedSettings, 'new') ? newSettings : settings
    const isAdvisor = user && user.role === 'advisor' || false
    const canShow = goal && finalSettings
    const disclaimerOpen = getDisclaimer(props)

    // Workaround for ensuring that all fields,
    // including nested fields like constraints[], are reset.
    // Grabbed from https://git.io/vi0kr
    const resetForm = () => {
      destroyForm()
      initializeForm(R.merge(deserializeSettings(settings), { goalId: goal.id }))
    }

    return (
      <MainFrame>
        <Sidebar header={sidebarHeader(this.props, this)}>
          {canShow &&
            <SidebarContent activeTabKey={activeTabKey} constraints={constraints}
              fields={fields} goal={goal} portfolio={portfolio} set={setAllocationVar}
              settings={finalSettings} values={finalValues} viewedSettings={viewedSettings} />}
        </Sidebar>
        <ContentPane header={goal && <AllocationHeader approve={approve} dirty={dirty} goal={goal}
          isAdvisor={isAdvisor} params={params} resetForm={resetForm} revert={revert}
          show={show} />} className={classes.allocationPanel}>
          {canShow
            ? (
              <div className={classes.main}>
                <div className={classes.stretchBox}>
                  <ProjectionGraph values={finalValues} portfolio={portfolio}
                    balance={goal.balance} set={setAllocationVar}>
                    {!portfolio &&
                      <CalculatePortfolio activeTabKey={activeTabKey} balance={goal.balance}
                        settings={finalSettings}
                        values={R.merge(finalValues, { constraints: validConstraints })}
                        viewedSettings={viewedSettings} />}
                  </ProjectionGraph>
                </div>
                <div className='text-right'>
                  <Button bsStyle='link' onClick={this.toggleDisclaimer}>
                    {disclaimerOpen ? 'Hide' : 'Disclaimer'}
                  </Button>
                </div>
                <Collapse in={disclaimerOpen}>
                  <Well>
                    Nominal values are used to illustrate investment gains and losses and
                    are used for illustrative purposes only. There is no guarantee that
                    any of these assumed rates of return will actually be achieved, and
                    as always, past performance is no guarantee of future results.
                    If you decide to employ the allocation assumed in the graph you should
                    be aware that you are not protected against loss in a declining market,
                    nor does it guarantee profit.
                  </Well>
                </Collapse>
                <Holdings goal={goal} portfolioFromSettings={portfolio} values={finalValues} />
              </div>
            )
            : <Spinner />}
        </ContentPane>
        {canShow &&
          <div>
            <ConfirmGoalSettingsModal clientId={clientId} goal={goal} isAdvisor={isAdvisor}
              settings={finalSettings} viewedSettings={viewedSettings} />
            <CreateGoalSuccessModal goal={goal} values={finalValues} />
          </div>}
      </MainFrame>
    )
  }
}
