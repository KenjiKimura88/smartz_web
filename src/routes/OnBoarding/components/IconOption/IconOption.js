import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './IconOption.scss'
import Text from 'components/Text'

export default class IconOption extends Component {
  static propTypes = {
    cornerIcon: PropTypes.func,
    icon: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    selected: PropTypes.bool,
    value: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.number,
      PropTypes.string
    ]).isRequired
  };

  render() {
    const { cornerIcon, icon, label, onClick, selected, value } = this.props
    return (
      <div
        className={classNames(classes.iconOption, {[classes.selected]: selected})}
        onClick={function () { onClick(value) }} tabIndex={0}>
        <div className={classes.iconOptionInner}>
          <div className={classes.content}>
            {cornerIcon && <div className={classes.cornerIcon}>
              {React.createElement(cornerIcon, { size: 28 })}
            </div>}
            <div className={classes.icon}>{React.createElement(icon, { size: 80 })}</div>
            <Text>{label}</Text>
          </div>
        </div>
      </div>
    )
  }
}
