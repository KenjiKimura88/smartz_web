import React, { Component, PropTypes } from 'react'
import { MenuItem } from 'react-bootstrap'
import R from 'ramda'
import Dropdown from 'components/Dropdown'

export default class SelectActivityType extends Component {
  static propTypes = {
    activeType: PropTypes.object.isRequired,
    activityTypes: PropTypes.array.isRequired,
    set: PropTypes.func.isRequired
  };

  render () {
    const { activeType, activityTypes, set } = this.props

    return (
      <Dropdown id='activityTypeSelector' value={activeType.name}>
        {R.map(({ id, name }) =>
          <MenuItem key={id} onClick={function () { set({ activeTypeId: id }) }}>
            {name}
          </MenuItem>
        , activityTypes)}
      </Dropdown>
    )
  }
}
