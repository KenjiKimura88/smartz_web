import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import classes from './GoalStatusGroup.scss'
import GoalRiskStatus from '../../containers/GoalRiskStatus'
import GoalTargetValue from 'components/GoalTargetValue'
import LabelValue from 'components/LabelValue'
import PerformanceTracking from 'components/PerformanceTracking'
import Well from 'components/Well'

export default class GoalStatusGroup extends Component {
  static propTypes = {
    goal: PropTypes.object
  };

  render () {
    const { goal } = this.props
    return (
      <Row className={classes.goalStatus}>
        <Col xs={4} className={classes.boxPaddingCol}>
          <PerformanceTracking goal={goal} />
        </Col>
        <Col xs={4} className={classes.boxPaddingCol}>
          <GoalRiskStatus goal={goal} />
        </Col>
        <Col xs={4} className={classes.boxPaddingCol}>
          <Well bsSize='sm' bsStyle='white'>
            <LabelValue label={<div>Target</div>} value={<GoalTargetValue goal={goal} />} />
          </Well>
        </Col>
      </Row>
    )
  }
}
