import React from 'react'
import { Checkbox as BsCheckbox } from 'react-bootstrap'
import { mount, shallow } from 'enzyme'
import { BzCheck } from 'icons'
import Checkbox from 'components/Checkbox/Checkbox'
import classes from 'components/Checkbox/Checkbox.scss'

describe('(Component) Checkbox', () => {
  const getComponent = (props) =>
    <Checkbox checked={false} {...props}>Foo</Checkbox>

  it('renders as a <BsCheckbox />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal(BsCheckbox)
  })

  it('renders customized check icon wrapper', () => {
    const wrapper = mount(getComponent())
    expect(wrapper.find(`.${classes.checkboxInner}`)).to.have.length(1)
  })

  it('renders customized check icon when checked', () => {
    const wrapper = shallow(getComponent({ checked: true }))
    expect(wrapper.find(BzCheck)).to.have.length(1)
  })
})
