import React, { PropTypes } from 'react'
import classes from './LogoHeader.scss'
import Header from '../Header'
import Logo from 'containers/Logo'

const LogoHeader = ({ invitation }) => {
  const logo = invitation && (invitation.firm_colored_logo || invitation.firm_logo)
  return (
    <Header>
      <div className={classes.logoWrapper}>
        <Logo className={classes.logo} logo={logo} />
      </div>
    </Header>
  )
}

LogoHeader.propTypes = {
  invitation: PropTypes.object
}

export default LogoHeader
