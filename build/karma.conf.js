const argv = require('yargs').argv
const config = require('../config')
const webpackConfig = require('./webpack.config')
const debug = require('debug')('app:karma')

debug('Create configuration.')

const karmaConfig = {
  basePath: '../', // project root in relation to bin/karma.js
  files: [
    {
      pattern: `./${config.dir_test}/test-bundler.js`,
      watched: false,
      served: true,
      included: true
    },
    './node_modules/promise-polyfill/promise.js'
  ],
  singleRun: !argv.watch,
  frameworks: ['mocha', 'intl-shim'],
  reporters: ['mocha', 'progress', 'junit'],
  junitReporter: {
    // results will be saved as $outputDir/$browserName.xml
    outputDir: 'test_results',
    // if included, results will be saved as $outputDir/$browserName/$outputFile
    outputFile: undefined,
    // suite will become the package name attribute in xml testsuite element
    suite: '',
    // add browser name to report and classes names
    useBrowserName: true,
    // function (browser, result) to customize the name attribute in xml testcase element
    nameFormatter: undefined,
    // function (browser, result) to customize the classname attribute in xml testcase element
    classNameFormatter: undefined,
    // key value pair of properties to add to the <properties> section of the report
    properties: {}
  },
  preprocessors: {
    [`${config.dir_test}/test-bundler.js`]: ['webpack']
  },
  browsers: ['PhantomJS'],
  webpack: {
    devtool: 'cheap-module-source-map',
    resolve: Object.assign({}, webpackConfig.resolve, {
      alias : Object.assign({}, webpackConfig.resolve.alias, {
        sinon : 'sinon/pkg/sinon.js'
      })
    }),
    plugins: webpackConfig.plugins,
    module: {
      noParse: [
        /\/sinon\.js/
      ],
      loaders: webpackConfig.module.loaders.concat([
        {
          test: /sinon(\\|\/)pkg(\\|\/)sinon\.js/,
          loader: 'imports?define=>false,require=>false'
        }
      ])
    },
    // Enzyme fix, see:
    // https://github.com/airbnb/enzyme/issues/47
    externals: Object.assign({}, webpackConfig.externals, {
      'react/addons'                   : true,
      'react/lib/ExecutionEnvironment' : true,
      'react/lib/ReactContext'         : 'window'
    }),
    sassLoader: webpackConfig.sassLoader
  },
  webpackMiddleware: {
    noInfo: true
  },
  coverageReporter: {
    reporters: config.coverage_reporters
  }
}

if (config.globals.__COVERAGE__) {
  karmaConfig.reporters.push('coverage')
  karmaConfig.webpack.module.preLoaders = [{
    test: /\.(js|jsx)$/,
    include: new RegExp(config.dir_client),
    loader: 'babel',
    query: Object.assign({}, config.compiler_babel, {
      plugins: (config.compiler_babel.plugins || []).concat('istanbul')
    })
  }]
}

// cannot use `export default` because of Karma.
module.exports = (cfg) => cfg.set(karmaConfig)
