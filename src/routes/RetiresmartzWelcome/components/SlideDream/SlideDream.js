import React, { Component } from 'react'
import bgImage from './dream-bg.jpg'
import Slide from '../Slide'

export default class SlideDream extends Component {
  render () {
    return (
      <Slide title='Dream' bgImage={bgImage}
        headline="Make sure you're headed toward your ideal retirement outcome">
        It all starts with a dream for life after work.
        Tell us where you want to be in retirement so
        we can find the best way to get you there.
      </Slide>
    )
  }
}
