import React, { PropTypes } from 'react'
import 'intl/locale-data/jsonp/en.js'
import { IntlProvider, intlShape } from 'react-intl'
import R from 'ramda'
import { mount } from 'enzyme'
import config from 'config'

// Helper function from
// https://github.com/yahoo/react-intl/wiki/Testing-with-React-Intl#helper-function-1

// Create the IntlProvider to retrieve context for wrapping around.
const intlProvider = new IntlProvider(config.intl, {})
const { intl } = intlProvider.getChildContext()

const router = {
  params: {},
  push: function () {},
  replace: function () {},
  go: function () {},
  goBack: function () {},
  goForward: function () {},
  setRouteLeaveHook: function () {},
  isActive: function () {},
  createHref: function () {}
}

// When using React-Intl `injectIntl` on components, props.intl is required.
function nodeWithIntlProp (node) {
  return React.cloneElement(node, { intl, router })
}

export default function mountWithIntl (node, { context, childContextTypes } = {}) {
  return mount(nodeWithIntlProp(node), {
    context: R.merge(context, { intl, router }),
    childContextTypes: R.merge(childContextTypes, {
      intl: intlShape,
      router: PropTypes.object
    })
  })
}
