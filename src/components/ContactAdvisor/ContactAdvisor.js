import React, { Component, PropTypes } from 'react'
import { DropdownButton, MenuItem } from 'react-bootstrap'
import classNames from 'classnames'
import { MdChat, MdLocalPhone, MdMailOutline } from 'helpers/icons'
import AdvisorEmail, { getAdvisorEmail } from 'components/AdvisorEmail'
import AdvisorName from 'components/AdvisorName'
import AdvisorPhone from 'components/AdvisorPhone'
import classes from './ContactAdvisor.scss'

export default class ContactAdvisor extends Component {
  static propTypes = {
    client: PropTypes.object.isRequired
  };

  render () {
    const { props } = this
    const { client } = props
    const advisor = client && client.advisor
    const advisorEmail = getAdvisorEmail({ advisor })
    const emailHref = advisorEmail && `mailto:${advisorEmail}`

    return (
      <div>
        <div className={classNames('navbar-text', classes.name)}>
          <span>Your advisor: </span>
          <AdvisorName advisor={advisor} />
        </div>
        <DropdownButton className={classNames(classes.button, 'btn-circle', 'navbar-btn')}
          title={<MdChat size='18' />} id='contact-advisor' pullRight noCaret>
          <MenuItem>
            <span className={classes.circle}>
              <MdLocalPhone size='14' />
            </span>
            <AdvisorPhone advisor={advisor} />
          </MenuItem>
          <MenuItem href={emailHref}>
            <span className={classes.circle}>
              <MdMailOutline size='14' />
            </span>
            <AdvisorEmail advisor={advisor} tagName='span' />
          </MenuItem>
        </DropdownButton>
      </div>
    )
  }
}
