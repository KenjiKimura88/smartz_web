import React, { Component, PropTypes } from 'react'
import { Button, Col, Form, FormGroup, Modal, Row } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import { withRouter } from 'react-router'
import R from 'ramda'
import { BzArrowRight } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import Checkbox from 'components/Checkbox'
import H2 from 'components/H2'
import P from 'components/P'
import schema from 'schemas/ira'
import Radio from 'components/Radio'

class SelectIraModal extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired
  };

  handleContinue = () => {
    const { clientId, router: { push } } = this.props
    push(`/${clientId}/retiresmartz`)
  }

  render () {
    const { handleHide, invalid, show, fields } = this.props
    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Body>
          <H2 className='text-center' bold>Select an IRA</H2>
          <Form>
            <Row>
              <Col sm={4}>
                <FormGroup className='text-center'>
                  <Radio inline {...domOnlyProps(fields.ira)} value='traditional'
                    checked={R.equals(fields.ira.value, 'traditional')} />
                </FormGroup>
                <H2 className='text-center' bold>Traditional</H2>
                <P>
                  You receive a tax deduction now for the contributions you make.
                  Your money grows tax-deferred so you don't have to pay taxes until
                  you begin withdrawing money from your account later. The money is
                  then taxed at your marginal rate.
                </P>
              </Col>
              <Col sm={4}>
                <FormGroup className='text-center'>
                  <Radio inline {...domOnlyProps(fields.ira)} value='roth'
                    checked={R.equals(fields.ira.value, 'roth')} />
                </FormGroup>
                <H2 className='text-center' bold>Roth</H2>
                <P>
                  Contributions are not tax-deductable, but qualified withdrawals
                  and growth are tax free.
                </P>
              </Col>
              <Col sm={4}>
                <FormGroup className='text-center'>
                  <Radio inline {...domOnlyProps(fields.ira)} value='sep'
                    checked={R.equals(fields.ira.value, 'sep')} />
                </FormGroup>
                <H2 className='text-center' bold>SEP</H2>
                <P>
                  Our SEPs are for self-employed people and small-business owners
                  looking for single participant plans.
                </P>
                {R.equals(fields.ira.value, 'sep') && <div>
                  <P>We currently only support IRAs for onl one plan participant</P>
                  <FormGroup>
                    <Checkbox {...domOnlyProps(fields.confirmSep)}>
                      I confirm there is only 1 employee participant (myself)
                    </Checkbox>
                  </FormGroup>
                </div>}
              </Col>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Row>
            <Col xs={6} className='text-left'>
              <Button onClick={handleHide}>Exit</Button>
            </Col>
            <Col xs={6} className='text-right'>
              <Button bsStyle='primary' onClick={this.handleContinue} disabled={invalid}>
                Continue &nbsp; <BzArrowRight />
              </Button>
            </Col>
          </Row>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'selectIraModal' }),
  withRouter,
  reduxForm({
    form: 'selectIra',
    ...schema
  }
))(SelectIraModal)
