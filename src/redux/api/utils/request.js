import R from 'ramda'

// getCSRFToken :: document -> String
export const getCSRFToken = R.compose(
  R.prop('csrftoken'),
  R.fromPairs,
  R.map(R.split('=')),
  R.map(R.trim),
  R.split(';'),
  R.prop('cookie')
)

export const assignDefaults = request => {
  const csrfToken = getCSRFToken(document)

  const baseHeaders = {
    'X-CSRFToken': csrfToken
  }

  let additionalHeaders = {}
  if (request.multipart) {
    additionalHeaders = {
      Accept: 'application/json'
    }
  } else if (!R.equals(request.method, 'DELETE')) {
    additionalHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }
  }

  const requestHeaders = R.defaultTo({}, request.headers)
  const headers = R.mergeAll([
    baseHeaders,
    additionalHeaders,
    requestHeaders
  ])

  const body = R.contains(request.method, ['POST', 'PUT']) && !request.multipart &&
    request.body && !R.is(String, request.body)
    ? JSON.stringify(request.body)
    : request.body

  const other = {
    method: 'GET',
    redirect: 'follow',
    credentials: 'same-origin',
    meta: {}
  }

  return R.mergeAll([
    other,
    request,
    { headers },
    { body }
  ])
}

export const requestFootprint = R.compose(
  ({ headers, ...otherProps }) => ({
    headers : R.omit(['Content-Disposition'], headers),
    ...otherProps
  }),
  R.pick(['url', 'method', 'headers'])
)

export const compareRequests = R.useWith(R.equals, [
  requestFootprint,
  requestFootprint
])
