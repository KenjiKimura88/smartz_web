import React, { Component, PropTypes } from 'react'
import { connectModal } from 'redux-modal'
import { Modal } from 'react-bootstrap'
import R from 'ramda'

class FeatureNotActivatedModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func,
    show: PropTypes.bool
  };

  render () {
    const { handleHide, show } = this.props

    return (
      <Modal animation={false} show={show} onHide={handleHide}
        aria-labelledby='ModalHeader'>
        <Modal.Header closeButton>
          <Modal.Title className='text-center'>Feature Not Activated</Modal.Title>
        </Modal.Header>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'featureNotActivatedModal' })
)(FeatureNotActivatedModal)
