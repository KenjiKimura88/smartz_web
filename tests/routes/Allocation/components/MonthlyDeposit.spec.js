import React from 'react'
import { shallow } from 'enzyme'
import MonthlyDeposit from 'routes/Allocation/components/MonthlyDeposit/MonthlyDeposit'
import Panel from 'containers/Panel'

describe('(Route) Allocation', () => {
  describe('(Container) MonthlyDeposit', () => {

    const getComponent = (props) =>
      <MonthlyDeposit monthlyTransactionAmount={{}} recommendedValue={100} settings={{}}
        {...props} />

    it('renders as a <Panel />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Panel)
    })
  })
})
