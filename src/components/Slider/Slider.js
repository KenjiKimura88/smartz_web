import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import R from 'ramda'
import RcSlider from 'rc-slider'
import classes from './Slider.scss'
import Handle from './Handle'
import Text from 'components/Text'

const belowLowerLimit = ({ lowerLimit, max, min }) => { // eslint-disable-line react/prop-types
  const position = (lowerLimit - min) / (max - min) * 100
  return isNaN(position) || R.equals(position, 0)
    ? false
    : <div className={classes.bound} style={{ left: `${position}%` }} />
}

const aboveUpperLimit = ({ max, min, upperLimit }) => { // eslint-disable-line react/prop-types
  const position = (upperLimit - min) / (max - min) * 100
  return isNaN(position) || R.equals(position, 100)
    ? false
    : <div className={classes.bound} style={{ left: `${position}%` }} />
}

export default class Slider extends Component {
  static propTypes = {
    className: PropTypes.string,
    getDisplayedValue: PropTypes.func,
    hideLabels: PropTypes.bool,
    label: PropTypes.string,
    labelMax: PropTypes.string,
    labelMin: PropTypes.string,
    lowerLimit: PropTypes.number,
    max: PropTypes.number,
    min: PropTypes.number,
    upperLimit: PropTypes.number
  }

  get labelMin () {
    const { labelMin, min } = this.props
    return labelMin || min
  }

  get labelMax () {
    const { labelMax, max } = this.props
    return labelMax || max
  }

  render () {
    const { props } = this
    const { className, getDisplayedValue, hideLabels, label } = props
    const otherProps = R.omit(['children', 'className'], props)
    const finalClassName = classNames(classes.slider, {
      [className]: !!className
    })

    // Disable tooltip
    const tipFormatter = null

    return (
      <div className={finalClassName}>
        <RcSlider className={classes.rcSlider}
          handle={<Handle getDisplayedValue={getDisplayedValue} />}
          tipFormatter={tipFormatter} {...otherProps}>
          {belowLowerLimit(props)}
          {aboveUpperLimit(props)}
        </RcSlider>
        {!hideLabels &&
          <div className={classNames(classes.labels, 'clearfix')}>
            <div className={classes.labelMin}>
              <Text light>{this.labelMin}</Text>
            </div>
            <div className={classes.labelMax}>
              <Text light>{this.labelMax}</Text>
            </div>
            <div className={classes.label}>
              <Text light>{label}</Text>
            </div>
          </div>}
      </div>
    )
  }
}
