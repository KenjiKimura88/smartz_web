import React, { Component, PropTypes } from 'react'
import { Col, Form, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { BzArrowRight } from 'icons'
import { domOnlyProps } from 'helpers/pureFunctions'
import Button from 'components/Button'
import classes from './NewGoalRolloverForm.scss'
import CurrencyInput from 'components/CurrencyInput'
import employerPlanOptions from 'routes/NewGoalEmployerPlan/employerPlanOptions'
import FieldError from 'components/FieldError'
import FormContainer from '../../../NewGoalForm/components/FormContainer'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import RadioButtonGroup from 'components/RadioButtonGroup'
import rolloverTypesOptions from 'routes/NewGoalRolloverTypes/rolloverTypesOptions'

const rolloverIcon = (rolloverType) =>
  R.compose(
    R.prop('icon'),
    R.defaultTo({}),
    R.find(R.propEq('value', parseInt(rolloverType, 10))),
  )(R.concat(employerPlanOptions, rolloverTypesOptions))

export default class NewGoalRolloverForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    show: PropTypes.func.isRequired
  };

  handleContinue = (values) => {
    const { params: { clientId, rolloverType }, router: { push } } = this.props
    push(`/${clientId}/new-goal/rollover/${rolloverType}/approvals`)
  }

  handleExit = () => {
    const { params: { clientId }, router: { push } } = this.props
    push(`/${clientId}`)
  }

  render () {
    const { props } = this
    const { fields, handleSubmit, invalid, params: { rolloverType } } = props
    const Icon = rolloverIcon(rolloverType, 10)
    const sidebar = <Icon size={150} />
    return (
      <Form onSubmit={handleSubmit(this.handleContinue)} className={classes.wrapper}>
        <FormContainer sidebar={sidebar}>
          <H2 bold>Complete this form to prepare your direct transfer request</H2>
          <FormGroup>
            <ControlLabel>Who is your current provider?</ControlLabel>
            <Row>
              <Col xs={4}>
                <FormControl type='text' {...domOnlyProps(fields.provider)} />
                <FieldError for={fields.provider} />
              </Col>
            </Row>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Account Number</ControlLabel>
            <Row>
              <Col xs={4}>
                <FormControl type='text' {...domOnlyProps(fields.accountNumber)} />
                <FieldError for={fields.accountNumber} />
              </Col>
            </Row>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Select type of transfer</ControlLabel>
            <div>
              <RadioButtonGroup type='radio' {...domOnlyProps(fields.transferType)}>
                <Button value='full'>Full Balance</Button>
                <Button value='part'>Part Balance</Button>
              </RadioButtonGroup>
            </div>
            <Row>
              <Col xs={4}>
                <FieldError for={fields.transferType} />
              </Col>
            </Row>
          </FormGroup>
          <FormGroup>
            <ControlLabel>
              {R.equals(fields.transferType.value, 'full')
                ? 'Enter an estimate of your IRA balance'
                : 'Enter an exact amount to transfer'}
            </ControlLabel>
            <Row>
              <Col xs={4}>
                <CurrencyInput {...domOnlyProps(fields.balance)} />
                <FieldError for={fields.balance} />
              </Col>
            </Row>
          </FormGroup>
        </FormContainer>
        <Hr />
        <Row>
          <Col xs={6}>
            <Button onClick={this.handleExit}>Exit</Button>
          </Col>
          <Col xs={6} className='text-right'>
            <Button bsStyle='primary' type='submit' disabled={invalid}>
              Continue <BzArrowRight />
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }
}
