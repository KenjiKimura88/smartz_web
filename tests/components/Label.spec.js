import React from 'react'
import { shallow } from 'enzyme'
import Label from 'components/Label/Label'

describe('(Component) Label', () => {
  const getComponent = (props) =>
    <Label>Foo</Label>

  it('renders as a <dt />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('dt')
  })
})
