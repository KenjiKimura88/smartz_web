import React, { Component, PropTypes } from 'react'
import { Button, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import classes from './EmailSentModal.scss'
import H2 from 'components/H2'
import P from 'components/P'

class EmailSentModal extends Component {
  static propTypes = {
    clientId: PropTypes.string.isRequired,
    handleHide: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired
  };

  handleOk = () => {
    const { clientId, replace } = this.props
    replace(`/${clientId}`)
  }

  render () {
    const { handleHide, show } = this.props
    return (
      <Modal show={show} onHide={handleHide} backdrop='static' className={classes.wrapper}>
        <Modal.Body>
          <H2 className='text-center'>Thank you</H2>
          <P className='text-center'>
            An email has been sent to the secondary account holder to verify
            the creation of a Joint Account. You will be notified when this is
            completed and can proceed with Joint goal creation.
          </P>
          <div className='text-center'>
            <Button bsStyle='primary' onClick={this.handleOk}>
              OK
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

export default connectModal({ name: 'jointAccountEmailSentModal' })(EmailSentModal)
