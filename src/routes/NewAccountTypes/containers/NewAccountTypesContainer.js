import { show } from 'redux-modal'
import { withRouter } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getClient } from 'helpers/requests'
import NewAccountTypes from '../components/NewAccountTypes'

const requests = ({ params: { clientId } }) => ({
  accounts: clientId && (({ findAll }) => findAll({
    type: 'accounts',
    url: `/clients/${clientId}/accounts`
  })),
  accountTypes: ({ findAll }) => findAll({
    type: 'accountTypes',
    url: '/settings/account-types'
  }),
  client: getClient(clientId)
})

const actions = {
  show
}

export default R.compose(
  connect(requests, null, actions),
  withRouter
)(NewAccountTypes)
