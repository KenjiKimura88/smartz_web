import React, { Component, PropTypes } from 'react'
import classNames from 'classnames'
import classes from './Spinner.scss'

export default class Spinner extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    size: PropTypes.oneOf(['small', 'normal'])
  };

  static defaultProps = {
    size: 'normal'
  };

  shouldComponentUpdate () {
    return false
  }

  render () {
    const { children, className, size } = this.props
    const finalClassName = classNames({
      [classes.spinner]: true,
      [classes[size]]: true,
      [className]: !!className
    })

    return (
      <div className={finalClassName}>
        <div className={classes.icon}>
          <div className={classes.rail} />
          <div className={classes.trace} />
        </div>
        <div className={classes.content}>
          {children}
        </div>
      </div>
    )
  }
}
