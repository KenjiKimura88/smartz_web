import React, { Component, PropTypes } from 'react'
import R from 'ramda'
import classes from './Questions.scss'
import Question from '../Question'
import Row from 'react-bootstrap/lib/Row'

const questions = [
  {
    key: 'whyThisPortfolio',
    category: 'investing strategy',
    title: 'Why this Portfolio?'
  },
  {
    key: 'whyTheseFunds',
    category: 'investing strategy',
    title: 'Why these Funds?'
  },
  {
    key: 'taxEfficiency',
    category: 'investing strategy',
    title: 'Tax Efficiency'
  }
]

export default class Questions extends Component {
  static propTypes = {
    show: PropTypes.func.isRequired
  };

  render () {
    const { show } = this.props

    return (
      <Row className={classes.questions}>
        {R.map(question =>
          <Question question={question} key={question.key} show={show} />
        , questions)}
      </Row>
    )
  }
}
