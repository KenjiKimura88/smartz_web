/* eslint-disable max-len */
import R from 'ramda'

export default {
  intl: {
    locale: 'en',
    formats: {
      date: {
        numericDate: {
          day: 'numeric',
          month: 'numeric',
          year: 'numeric'
        },
        monthAndYear: {
          month: 'long',
          year: 'numeric'
        },
        dayMonthAndYear: {
          day: 'numeric',
          month: 'long',
          year: 'numeric'
        },
        year: {
          year: 'numeric'
        }
      },
      number: {
        currency: {
          style: 'currency',
          currency: 'USD',
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        },
        currencyWithoutCents: {
          style: 'currency',
          currency: 'USD',
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        },
        percent: {
          style: 'percent',
          minimumFractionDigits: 1,
          maximumFractionDigits: 2
        },
        percentRounded: {
          style: 'percent',
          minimumFractionDigits: 0,
          maximumFractionDigits: 0
        },
        decimal: {
          style: 'decimal',
          minimumFractionDigits: 0,
          maximumFractionDigits: 2
        }
      }
    }
  },

  riskDescriptions: [
    { when: ({ maxRec, minRec, value }) =>
      (R.lte(value, maxRec) && R.gte(value, minRec)) ||
      (R.gt(value, maxRec) && R.lte(value - maxRec, 0.1)) ||
      (R.gt(minRec, value) && R.lte(minRec - value, 0.1)),
      label: 'Moderate (Growth)', className: 'text-success' },
    { when: ({ maxRec, minRec, value }) =>
      R.gt(value - maxRec, 0.1) && R.lte(value - maxRec, 0.2),
      label: 'Semi-Dynamic', className: 'text-warning' },
    { when: ({ maxRec, minRec, value }) =>
      R.gt(minRec - value, 0.1) && R.lte(minRec - value, 0.2),
      label: 'Semi-Protected', className: 'text-warning' },
    { when: ({ maxRec, minRec, value }) => R.gt(value - maxRec, 0.2),
      label: 'Dynamic', className: 'text-danger' },
    { when: ({ maxRec, minRec, value }) => R.gt(minRec - value, 0.2),
      label: 'Protected', className: 'text-danger' }
  ],

  goalMetric: {
    type: { // or metricType
      values: {
        METRIC_TYPE_PORTFOLIO_MIX: 0,
        METRIC_TYPE_RISK_SCORE: 1
      },
      labels: {
        0: 'Portfolio Mix',
        1: 'RiskScore'
      }
    },
    comparison: {
      values: {
        METRIC_COMPARISON_MINIMUM: 0,
        METRIC_COMPARISON_EXACTLY: 1,
        METRIC_COMPARISON_MAXIMUM: 2
      },
      labels: {
        0: 'Minimum',
        1: 'Exactly',
        2: 'Maximum'
      }
    }
  },

  goalsStates: ['active', 'archiveRequested', 'closing', 'archived'],

  goal: {
    minimumDuration: 1 * 12, // 1 year
    maximumDuration: 50 * 12 // 50 years
  },

  onboarding: {
    INVITE_STATUS: {
      STATUS_SENT: 1,
      STATUS_ACCEPTED: 2,
      STATUS_EXPIRED: 3,
      STATUS_COMPLETED: 4
    },
    INVITE_REASON: {
      RETIREMENT: 1,
      PERSONAL_INVESTING: 2
    },
    EMPLOYMENT_STATUS: {
      EMPLOYED: 0,
      UNEMPLOYED: 1,
      SELF_EMPLOYED: 2,
      RETIRED: 3,
      NOT_LABORFORCE: 4
    }
  },

  accountTypes: {
    ACCOUNT_TYPE_PERSONAL: 0,
    ACCOUNT_TYPE_JOINT: 1,
    ACCOUNT_TYPE_TRUST: 2,
    ACCOUNT_TYPE_SMSF: 3,
    ACCOUNT_TYPE_CORPORATE: 4,
    ACCOUNT_TYPE_401K: 5,
    ACCOUNT_TYPE_ROTH401K: 6,
    ACCOUNT_TYPE_IRA: 7,  // Traditional pre-tax IRA account
    ACCOUNT_TYPE_ROTHIRA: 8,  // Tax-paid IRA account
    ACCOUNT_TYPE_SEPIRA: 9, // IRA account for self-employed
    ACCOUNT_TYPE_403K: 10,
    ACCOUNT_TYPE_SIMPLEIRA: 11,
    ACCOUNT_TYPE_SARSEPIRA: 12,
    ACCOUNT_TYPE_PAYROLLDEDUCTIRA: 13,
    ACCOUNT_TYPE_PROFITSHARING: 14,
    ACCOUNT_TYPE_DEFINEDBENEFIT: 15,
    ACCOUNT_TYPE_MONEYPURCHASE: 16,
    ACCOUNT_TYPE_ESOP: 17,
    ACCOUNT_TYPE_GOVERMENTAL: 18,
    ACCOUNT_TYPE_457: 19,
    ACCOUNT_TYPE_409A: 20,
    ACCOUNT_TYPE_403B: 21,
    ACCOUNT_TYPE_TRUSTTEST: 22,
    ACCOUNT_TYPE_TRUSTNONTEST: 23,
    ACCOUNT_TYPE_INVESTCLUBORGANIZATION: 24,
    ACCOUNT_TYPE_PARTNERSHIPORGANIZATION: 25,
    ACCOUNT_TYPE_SOLEPROPORGANIZATION: 26,
    ACCOUNT_TYPE_LLCORGANIZATION: 27,
    ACCOUNT_TYPE_ASSOCIATIONORGANIZATION: 28,
    ACCOUNT_TYPE_NONCORPORATIONORGANIZATION: 29,
    ACCOUNT_TYPE_PENSION: 30,
    ACCOUNT_TYPE_HSA: 31,
    ACCOUNT_TYPE_529: 32,
    ACCOUNT_TYPE_ESA: 33,
    ACCOUNT_TYPE_UG: 34,
    ACCOUNT_TYPE_GUARDIANSHIP: 35,
    ACCOUNT_TYPE_CUSTODIAL: 36,
    ACCOUNT_TYPE_THRIFTSAVING: 37,
    ACCOUNT_TYPE_401A: 38,
    ACCOUNT_TYPE_QUALIFIEDANNUITY: 39,
    ACCOUNT_TYPE_TAXDEFERRED_ANNUITY: 40,
    ACCOUNT_TYPE_QUALIFIEDNPPLAN: 41,
    ACCOUNT_TYPE_QUALIFIEDNPROTHPLAN: 42,
    ACCOUNT_TYPE_QUALIFIEDPRIV457PLAN: 43,
    ACCOUNT_TYPE_INDIVDUAL401K: 44,
    ACCOUNT_TYPE_INDROTH401K: 45,
    ACCOUNT_TYPE_VARIABLEANNUITY: 46,
    ACCOUNT_TYPE_SINGLELIFEANNUITY: 47,
    ACCOUNT_TYPE_JOINTSURVIVORANNUITY: 48,
    ACCOUNT_TYPE_OTHE: 99,
  },

  maritalStatuses: {
    SINGLE :0,
    MARRIED_FILING_JOINTLY: 1,
    MARRIED_FILING_SEPARATELY_LIVED_TOGETHER: 2,
    MARRIED_FILING_SEPARATELY_NOT_LIVED_TOGETHER: 3,
    HEAD_OF_HOUSEHOLD: 4,
    QUALIFYING_WIDOWER: 5
  },

  expensesCategories: {
    ALCOHOLIC_BEVERAGE: 1,
    APPAREL_SERVICES: 2,
    EDUCATION: 3,
    ENTERTAINMENT: 4,
    FOOD: 5,
    HEALTHCARE: 6,
    HOUSING: 7,
    INSURANCE_PENSIONS_SOCIAL_SECURITY: 8,
    PERSONAL_CARE: 9,
    READING: 10,
    SAVINGS: 11,
    TAXES: 12,
    TOBACCO: 13,
    TRANSPORTATION: 14,
    MISCELLANEOUS: 15
  },

  retirementAccountCategories: {
    EMPLOYER_DESCRETIONARY_CONTRIB: 1,
    EMPLOYER_MATCHING_CONTRIB: 2,
    SALARY_PRE_TAX_ELECTIVE_DEFERRAL: 3,
    AFTER_TAX_ROTH_CONTRIB: 4,
    AFTER_TAX_CONTRIB: 5,
    SELF_EMPLOYED_PRE_TAX_CONTRIB: 6,
    SELF_EMPLOYED_AFTER_TAX_CONTRIB: 7,
    DORMANT_ACCOUNT_NO_CONTRIB: 8
  },

  retirementPlan: {
    MIN_LIFE_EXPECTANCY: 65,
    MAX_LIFE_EXPECTANCY: 100
  },

  healthDevices: {
    GOOGLE_FIT: 1,
    FITBIT: 2,
    SAMSUNG_DIGI_HEALTH: 3,
    MICROSOFT_HEALTH: 4,
    JAWBONE: 5,
    UNDERARMOUR: 6,
    WITHINGS: 7,
    TOMTOM: 8,
    GARMIN: 9,
    APPLE_HEALTHKIT: 10
  },

  ibEnabled: false
}
