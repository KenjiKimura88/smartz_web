import R from 'ramda'
import { connect } from 'redux/api'
import { getClient, getGoals } from 'helpers/requests'
import Client from '../components/Client'

const requests = ({ params: { clientId } }) => ({
  client: getClient(clientId),
  goals: getGoals(clientId)
})

export default R.compose(
  connect(requests)
)(Client)
