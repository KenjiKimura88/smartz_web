import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import PieChart from 'react-simple-pie-chart'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './GoalAnnualizedReturn.scss'

export default class GoalAnnualizedReturn extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['value'], this.props, nextProps)
  }

  render () {
    const { value } = this.props
    const percent = value * 100
    const pieSlices = [
      { color: '#DFDFDF', value: 100 - percent },
      { color: '#6ECDE7', value: percent }
    ]
    return (
      <div className={classes.row}>
        <Row>
          <Col xs={8} className={classes.label}>
            Annualized Return
          </Col>
          <Col xs={4} className='text-right'>
            <div className={classes.pieChartWrapper}>
              <PieChart slices={pieSlices} />
            </div>
            <FormattedNumber value={value} format='percent' />
          </Col>
        </Row>
      </div>
    )
  }
}
