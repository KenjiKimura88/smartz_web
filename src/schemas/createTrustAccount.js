import buildSchema from 'redux-form-schema'
import moment from 'moment'
import R from 'ramda'

const schemaProperties = {
  domestic: {
    label: 'Domestic trust',
    validate: {
      validateDomestic: (formValues, fieldValue) => fieldValue
    },
    error: 'We only accept US.domestic trust. If you have any questions please contact your advisor'
  },
  legalName: {
    label: 'Legal name',
    required: true
  },
  nickName: {
    label: 'Nickname',
    required: true
  },
  establishedDate: {
    label: 'Date of establishment',
    required: true,
    validate: {
      validateEstablishedDate: (formValues, fieldValue) =>
        moment(fieldValue, 'MM/DD/YYYY', true).isValid()
    }
  },
  organizationState: {
    label: 'State of organization',
    required: true
  },
  tin: {
    label: 'Type of TIN',
    required: true
  },
  ssn: {
    label: 'SSN',
    required: (values) => R.equals(R.path(['tin', 'value'], values), 'ssn'),
    validate: {
      validateSsn: (formValues, fieldValue) =>
        /^\d{3}-\d{2}-\d{4}$/.test(fieldValue)
    }
  },
  ein: {
    label: 'EIN',
    required: (values) => R.equals(R.path(['tin', 'value'], values), 'ein'),
    validate: {
      validateEin: (formValues, fieldValue) =>
        /^\d{2}-\d{7}$/.test(fieldValue)
    }
  },
  address: {
    label: 'Address',
    required: true
  },
  city: {
    label: 'City',
    required: true
  },
  state: {
    label: 'State'
  },
  zip: {
    label: 'Zip',
    required: true,
    validate: {
      validateZip: (formValues, fieldValue) =>
        /^\d{5}$/.test(fieldValue)
    }
  },
  agree: {
    label: 'Agree',
    required: true,
    validate: {
      validateAgree: (formValues, fieldValue) => !!fieldValue
    },
    error: 'You must accept the terms and conditions to continue'
  }
}

export default buildSchema(schemaProperties)
