import React, { Component, PropTypes } from 'react'
import classes from './Frassbentod.scss'
import OverlayTooltip from 'components/OverlayTooltip'
import CurrencyInput from 'components/CurrencyInput'

export default class Frassbentod extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    tooltipText: PropTypes.string.isRequired
  };

  render () {
    const { props } = this
    const { field, tooltipText } = props

    return (
      <div>
        <div className={classes.inputWrapper}>
          <CurrencyInput {...field} />
          <div className={classes.tooltipTriggerWrapper}>
            <OverlayTooltip placement='top' id='tooltipMaxEmployerMatch'>
              {tooltipText}
            </OverlayTooltip>
          </div>
        </div>
      </div>
    )
  }
}
