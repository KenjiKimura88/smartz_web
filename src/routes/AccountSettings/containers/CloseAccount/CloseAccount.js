import React, { Component, PropTypes } from 'react'
import { Form, FormGroup, Modal } from 'react-bootstrap'
import { connectModal } from 'redux-modal'
import { reduxForm } from 'redux-form'
import classNames from 'classnames'
import R from 'ramda'
import { BzTrash } from 'icons'
import Button from 'components/Button'
import classes from './CloseAccount.scss'
import closeAccountSchema, { closeOptions, LIQUIDATE, TRANSFER_TO_ACCOUNT, TRANSFER_TO_CUSTODIAN,
  TAKE_CUSTODY } from 'schemas/closeAccount'
import Dropzone from 'components/Dropzone'
import H2 from 'components/H2'
import Hr from 'components/Hr'
import Notification from 'containers/Notification'
import P from 'components/P'
import Radio from 'components/Radio'
import SecurityQuestionsForm, { serializeSecurityQuestions } from 'containers/SecurityQuestionsForm'
import securityQuestionsSchema from 'schemas/securityQuestions'
import Text from 'components/Text'
import Well from 'components/Well'

const getFileName = R.compose(
  R.prop('name'),
  R.defaultTo({}),
  R.head,
  R.defaultTo([]),
  R.path(['fields', 'accountTransferForm', 'value'])
)

const getNeedsFileUpload = R.pathEq(['fields', 'closeOption', 'value'], TRANSFER_TO_ACCOUNT)

const renderInfo = (_props) => {
  const { account: { account_name, account_number }, fields: { closeOption: { value } } } = _props
  if (R.equals(value, LIQUIDATE)) {
    return (
      <Well>
        <P>
          You have chosen to liquidate your assets held in
          your <Text bold>{account_name}</Text> Account (<Text bold>{account_number}</Text>).
        </P>
        <P>
          These assets will be liquidated over a period of up to ninety days.
        </P>
        During the liquidation process, fees will continue to accrue until the account has been
        fully liquidated and closed.
      </Well>
    )
  } else if (R.equals(value, TRANSFER_TO_ACCOUNT)) {
    return (
      <Well>
        Please complete our <a href='/account-transfer-form.pdf' target='_blank'>Internal Account
        Transfer Form</a> and upload the completed form using the box below.
      </Well>
    )
  } else if (R.equals(value, TRANSFER_TO_CUSTODIAN)) {
    return (
      <Well>
        <P>
          You have selected to transfer your assets in your <Text bold>{account_name}</Text> account
          (<Text bold>{account_number}</Text>) to a new custodian.
        </P>
        <P>
          This means that your account will be closed.
        </P>
        <P>
          You can still access your account until the transfer has taken place but will be unable
          to make any changes to your account.
        </P>
        Please ask your new custodian to send a transfer request to us at <a
          href='mailto:support@betasmartz.com'>support@betasmartz.com</a> including your account
        name ({account_name}) and account number ({account_number}).
      </Well>
    )
  } else if (R.equals(value, TAKE_CUSTODY)) {
    return (
      <Well>
        <P>
          By confirming that you wish to take direct control of your assets you confirm that your
          account will be closed and that where possible physical certificates will be sent to you,
          and digital records will be updated accordingly. Where transfer is not possible, assets
          will be liquidated over a period of up to ninety days.
        </P>
        During the transfer and liquidation process fees will continue to accrue until the account
        has been closed.
      </Well>
    )
  }
}

export class CloseAccount extends Component {
  static propTypes = {
    account: PropTypes.object.isRequired,
    closeAccount: PropTypes.func,
    closeAccountWithFileUpload: PropTypes.func,
    fields: PropTypes.object.isRequired,
    handleHide: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    requests: PropTypes.object.isRequired,
    securityQuestionsForm: PropTypes.object.isRequired,
    show: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  };

  handleFileDrop = (files) => {
    const { accountTransferForm } = this.props.fields
    accountTransferForm.onChange(files)
  }

  removeFile = () => {
    const { accountTransferForm } = this.props.fields
    accountTransferForm.onChange()
  }

  save = ({ closeOption }) => {
    const { account: { id }, handleHide, closeAccount, securityQuestionsForm } = this.props
    const body = {
      account: id,
      close_choice: closeOption,
      ...serializeSecurityQuestions(securityQuestionsForm)
    }
    closeAccount({
      body,
      success: handleHide
    })
  }

  saveWithFileUpload = ({ closeOption, accountTransferForm }) => {
    const { account: { id }, handleHide, closeAccountWithFileUpload,
      securityQuestionsForm } = this.props
    const securityQuestions = serializeSecurityQuestions(securityQuestionsForm)
    const filename = getFileName(this.props)
    const body = new FormData()
    body.append('account', id)
    body.append('close_choice', closeOption)
    body.append('account_transfer_form', R.head(accountTransferForm))
    R.forEach(([key, value]) =>
      body.append(key, value)
    , R.toPairs(securityQuestions))

    closeAccountWithFileUpload({
      headers: {
        'Content-Disposition': `form-data; filename="${filename}"`
      },
      body,
      success: handleHide
    })
  }

  render () {
    const { props } = this
    const { fields: { accountTransferForm, closeOption }, handleHide, handleSubmit,
      securityQuestionsForm, requests: { closeAccount, closeAccountWithFileUpload },
      show, valid } = props
    const closeOptionValue = closeOption.value
    const needsFileUpload = getNeedsFileUpload(props)
    const saveCallback = needsFileUpload ? this.saveWithFileUpload : this.save
    const didSelectOption = R.is(Number, closeOptionValue)
    const didAddFile = !!accountTransferForm.value
    const bothFormsValid = valid && securityQuestionsForm.valid
    const dropzoneStatus = accountTransferForm.value ? 'success' : 'pending'
    const dropzoneWrapper = classNames(classes.dropzoneWrapper, {
      [classes.hasFile]: didAddFile
    })

    return (
      <Modal show={show} onHide={handleHide}>
        <Modal.Header closeButton>
          <Modal.Title>Close Account</Modal.Title>
        </Modal.Header>
        <Form onSubmit={handleSubmit(saveCallback)}>
          <Modal.Body>
            <Notification request={closeAccount} />
            <Notification request={closeAccountWithFileUpload} />
            <H2>Would you like to:</H2>
            <FormGroup>
              {R.map(({ label, value }) =>
                <Radio key={value} checked={R.equals(closeOptionValue, value)}
                  onChange={() => closeOption.onChange(value)}>
                  {label}
                </Radio>
              , closeOptions)}
            </FormGroup>
            {didSelectOption && renderInfo(props)}
            {needsFileUpload &&
              <div className={dropzoneWrapper}>
                <Dropzone disablePreview multiple={false} accept='application/pdf'
                  onDrop={this.handleFileDrop} filename={getFileName(props)}
                  status={dropzoneStatus}
                  prompt='Drag & Drop the completed Account Transfer Form here' />
                {accountTransferForm.value &&
                  <div className={classes.action}>
                    <Button bsStyle='thick-outline' onClick={this.removeFile}>
                      <BzTrash size={30} />
                    </Button>
                  </div>}
              </div>}
            {valid &&
              <div>
                <Hr />
                <Text bold>
                  Before proceeding with closing your account, please answer the following security
                  questions.
                </Text>
                <SecurityQuestionsForm />
              </div>}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle='danger' type='submit' disabled={!bothFormsValid}>
              Close Account
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
}

export default R.compose(
  connectModal({ name: 'closeAccount' }),
  reduxForm({
    form: 'securityQuestions',
    propNamespace: 'securityQuestionsForm',
    ...securityQuestionsSchema
  }),
  reduxForm({
    form: 'closeAccountOption',
    ...closeAccountSchema
  })
)(CloseAccount)
