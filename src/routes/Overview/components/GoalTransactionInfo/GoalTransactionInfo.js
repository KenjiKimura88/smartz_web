import React, { Component, PropTypes } from 'react'
import { Col, Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import { MdRefresh } from 'helpers/icons'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './GoalTransactionInfo.scss'
import OverlayTooltip from 'components/OverlayTooltip'

const overlayInner = ({ date, enabled, label }) =>
  <div className={classes.inlineBlock}>
    <span className={classes.iconLeft}>
      <MdRefresh size='18' />
    </span>
    <span className={classes.inlineBlock}>
      {enabled
        ? <span>
          {label} on&nbsp;
          {date}
        </span>
        : <span>{label} off</span>}
    </span>
  </div>

overlayInner.propTypes = {
  date: PropTypes.element,
  enabled: PropTypes.bool,
  label: PropTypes.string
}

export default class GoalTransactionInfo extends Component {
  static propTypes = {
    amount: PropTypes.number,
    date: PropTypes.element.isRequired,
    enabled: PropTypes.bool,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    tooltip: PropTypes.node.isRequired,
    tooltipId: PropTypes.string.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['amount', 'date', 'enabled', 'label', 'tooltip', 'tooltipId'],
      this.props, nextProps)
  }

  render () {
    const { amount, date, enabled, label, onClick, tooltip, tooltipId } = this.props

    return (
      <Row className={classes.transactionInfo} onClick={onClick}>
        <Col xs={9} className={enabled ? classes.on : classes.off}>
          {tooltip
            ? <OverlayTooltip placement='top' id={tooltipId}
              trigger={overlayInner({ date, enabled, label })}>
              {tooltip}
            </OverlayTooltip>
            : <div>
              {overlayInner({ date, enabled, label })}
            </div>
          }
        </Col>
        {enabled && amount &&
          <Col xs={3} className={classes.amount}>
            <FormattedNumber value={amount} format='currency' />
          </Col>}
      </Row>
    )
  }
}
