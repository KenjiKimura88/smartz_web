import React, { Component, PropTypes } from 'react'
import { Form, FormControl, FormGroup, ControlLabel, Row, Col } from 'react-bootstrap'
import R from 'ramda'
import setPath from 'lodash/set'
import { domOnlyProps } from 'helpers/pureFunctions'
import { IBACCOUNT_STEP_ID } from '../../helpers'
import MaskedInput from 'react-input-mask'
import bgImage from './onboarding_login-icon.svg'
import Button from 'components/Button'
import Card from '../Card'
import classes from './IBAccount.scss'
import FieldError from 'components/FieldError'
import NavHeader from '../NavHeader'
import Sidebar from '../Sidebar'
import RadioButtonGroup from 'components/RadioButtonGroup'

export default class IBAccount extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    getValue: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
    invitation: PropTypes.object,
    onboarding: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    registerUser: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    securityQuestions: PropTypes.array,
    setValue: PropTypes.func.isRequired,
    step: PropTypes.string,
    stepId: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props)
  }

  handleSubmitAccountInfo = (values) => {
    const { push } = this.props
    const callback = () => {
      push('info')
    }
    this.submitAccountInfo(values, callback, callback)
  }

  submitAccountInfo(values, success, fail) {
    const { save, setValue } = this.props
    const finalValues = R.merge(values, { completed: true })
    setValue(['login', 'steps', IBACCOUNT_STEP_ID], finalValues, (onboardingValue) => {
      onboardingValue = setPath(onboardingValue, ['login', 'completed'], true)
      save({
        body: {
          onboarding_data: R.assocPath(['login', 'completed'], true, onboardingValue)
        },
        success,
        fail
      })
    })
  }

  handleSaveAndQuit = (values) => {
    const { push } = this.props
    const callback = () => {
      push('/progress-saved')
    }
    this.submitAccountInfo(values, callback, callback)
  }

  renderHeader() {
    const { props } = this
    const { handleSubmit } = props
    return (
      <NavHeader {...props} onSaveAndQuit={handleSubmit(this.handleSaveAndQuit)} />
    )
  }

  renderSidebar() {
    const { invitation } = this.props
    return (
      <Sidebar title='Create your login'>
        <p>
          If you are an existing client of
          Interactive Brokers we can link your
          account directly to{' '}
          <span className={classes.name}>{invitation && invitation.firm_name} </span>
          platform
        </p>
      </Sidebar>
    )
  }

  render () {
    const { props } = this
    const { fields: { existingAccount, accountNumber, accountNumberConfirmation },
      handleSubmit, isLoading } = props
    return (
      <div>
        <Form onSubmit={handleSubmit(this.handleSubmitAccountInfo)}>
          {this.renderHeader()}
          <Card sidebar={this.renderSidebar()} sidebarBgImage={bgImage} sidebarStyle='white'
            isLoading={isLoading}>
            <FormGroup className={classes.select_button}>
                Are you an existing account holder with Interactive Brokers?
            </FormGroup>
            <FormGroup className={classes.text_center}>
              <RadioButtonGroup type='radio' {...domOnlyProps(existingAccount)}>
                <Button bsStyle='wide' value>Yes</Button>
                <Button bsStyle='wide' value={false}>No</Button>
              </RadioButtonGroup>
              <FieldError for={existingAccount} />
            </FormGroup>
            {R.equals(existingAccount.value, true) && (
              <FormGroup className={classes.notes}>
                Please enter your interactive Brokers
                account number below and we will link
                your account automatically
              </FormGroup>
            )}
            {R.equals(existingAccount.value, false) && (
              <FormGroup className={classes.notes}>
                Carry on to sign up for a new account
              </FormGroup>
            )}
            {R.equals(existingAccount.value, true) && (
              <Row>
                <Col xs={2} />
                <Col xs={8}>
                  <FormGroup>
                    <ControlLabel>Your Interactive Brokers Account Number</ControlLabel>
                    <FormControl componentClass={MaskedInput} mask='U9999999'
                      placeholder='e.g U1234567'
                      {...domOnlyProps(accountNumber)} />
                    <FieldError for={accountNumber} />
                  </FormGroup>
                </Col>
                <Col xs={2} />
              </Row>
            )}
            {R.equals(existingAccount.value, true) && (
              <Row>
                <Col xs={2} />
                <Col xs={8}>
                  <FormGroup>
                    <ControlLabel>Confirm Your Interactive Brokers Account Number</ControlLabel>
                    <FormControl componentClass={MaskedInput} mask='U9999999'
                      {...domOnlyProps(accountNumberConfirmation)} />
                    <FieldError for={accountNumberConfirmation} />
                  </FormGroup>
                </Col>
                <Col xs={2} />
              </Row>
            )}
            <hr className={classes.hr} />
            <div className='text-right'>
              <Button disabled={isLoading} bsStyle='thick-outline' type='submit'>
                Continue
              </Button>
            </div>
          </Card>
        </Form>
      </div>
    )
  }
}
