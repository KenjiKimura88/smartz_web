import React, { Component, PropTypes } from 'react'
import { ControlLabel } from 'react-bootstrap'
import R from 'ramda'
import { requestIsPending } from 'helpers/requests'
import classes from './EmptyPage.scss'
import ContentPane from 'components/ContentPane'
import GoalNavigation from 'components/GoalNavigation'
import MainFrame from 'components/MainFrame'
import Sidebar from 'components/Sidebar'
import Spinner from 'components/Spinner'
import Text from 'components/Text'

const getHasNoGoals = R.converge(R.and, [
  R.pathEq(['requests', 'goals', 'status'], 'fulFilled'),
  R.compose(
    R.isEmpty,
    R.prop('goals')
  )
])

const getGoalsPending = requestIsPending('goals')

const sidebarHeader = (_props) => {
  const { accounts, params: { clientId }, replace, route: { for: forPath } } = _props
  const hasNoGoals = getHasNoGoals(_props)
  const label = hasNoGoals ? 'No Goals' : null

  return (
    <div>
      <ControlLabel>
        <Text size='small' light>Goal</Text>
      </ControlLabel>
      <GoalNavigation accounts={accounts} disabled={hasNoGoals} label={label}
        onSelectGoal={function (goal) { replace(`/${clientId}/${forPath}/${goal.id}`) }} />
    </div>
  )
}

export default class EmptyPage extends Component {
  static propTypes = {
    accounts: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    route: PropTypes.shape({
      for: PropTypes.string.isRequired
    }).isRequired,
    router: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.maybeGoToFirstGoal(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.maybeGoToFirstGoal(nextProps)
  }

  maybeGoToFirstGoal(props) {
    const { accounts, params: { clientId }, route: { for: forPath }, router: { replace } } = props
    const firstAccount = R.head(accounts)
    const firstGoal = firstAccount && R.head(firstAccount)
    firstGoal && replace(`/${clientId}/${forPath}/${firstGoal.id}`)
  }

  render () {
    const { props } = this

    return (
      <MainFrame>
        <Sidebar className={classes.sidebar} header={sidebarHeader(props)} />
        <ContentPane className={classes.contentPane} header={<div />}>
          {getGoalsPending(props) && <Spinner />}
        </ContentPane>
      </MainFrame>
    )
  }
}
