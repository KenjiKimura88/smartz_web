import React from 'react'
import R from 'ramda'
import { shallow } from 'enzyme'
import Button from 'components/Button'
import SaveButton from 'routes/Allocation/components/SaveButton'

describe('(Route) Allocation', () => {
  describe('(Component) SaveButton', () => {
    const dirty = true
    const goal = {
      id: 1,
      name: 'AAA'
    }

    const getComponent = (props) =>
      <SaveButton dirty={dirty} goal={goal} show={function () {}} {...props} />

    it('renders as a <Button />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(Button)
    })

    it('is disabled only when not dirty or target is pending activation', () => {
      const wrapper1 = shallow(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 1
        }),
        dirty: false
      }))
      expect(wrapper1.prop('disabled')).to.equal(true)

      const wrapper2 = shallow(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 2
        }),
        dirty: true
      }))
      expect(wrapper2.prop('disabled')).to.equal(true)

      const wrapper3 = shallow(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          approved_settings: 1
        }),
        dirty: true
      }))
      expect(wrapper3.prop('disabled')).to.equal(false)

      const wrapper4 = shallow(getComponent({
        goal: R.merge(goal, {
          active_settings: 1,
          selected_settings: 2
        }),
        dirty: true
      }))
      expect(wrapper4.prop('disabled')).to.equal(false)
    })

    it('shows confirmGoalSettings modal on click', () => {
      const show = sinon.spy()
      const wrapper = shallow(getComponent({ show }))
      show.should.not.have.been.called
      wrapper.simulate('click')
      show.should.have.been.calledOnce
      show.should.have.been.calledWith('confirmGoalSettings')
    })
  })
})
