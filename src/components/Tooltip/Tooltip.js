import React, { PropTypes } from 'react'
import { Tooltip as BsTooltip } from 'react-bootstrap'
import classes from './Tooltip.scss'

const Tooltip = ({ children, ...options }) => {
  return (
    <BsTooltip {...options} title={<span>Test</span>}>
      <div className={classes.wrapper}>
        {children}
      </div>
    </BsTooltip>
  )
}

Tooltip.propTypes = {
  children: PropTypes.node.isRequired
}

export default Tooltip
