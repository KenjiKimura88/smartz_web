import React, { Component, PropTypes } from 'react'
import { FormControl, InputGroup, Row } from 'react-bootstrap'
import R from 'ramda'
import { getDuration, prettyDuration } from '../../helpers'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Duration.scss'
import Col from 'components/Col'
import config from 'config'
import InlineList from 'components/InlineList'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import RecommendedValue from 'routes/Allocation/components/RecommendedValue'
import ResetValueButton from 'routes/Allocation/components/ResetValueButton'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

// getDurationValue :: Props -> Number
const getDurationValue = R.path(['duration', 'value'])

// getYears :: Props -> Number
const getYears = R.compose(
  Math.floor,
  R.flip(R.divide)(12),
  R.defaultTo(0),
  getDurationValue
)

// getMonths :: Props -> Number
const getMonths = R.compose(
  R.flip(R.mathMod)(12),
  getDurationValue
)

// handleChange :: Props -> Object -> undefined
const handleChange = (props, { months, years }) => {
  const { onChange } = props.duration
  const finalYears = parseInt(years || getYears(props), 10)
  const finalMonths = parseInt(months || getMonths(props), 10)
  const duration = finalYears * 12 + finalMonths
  const { maximumDuration, minimumDuration } = config.goal
  if (R.lte(duration, maximumDuration) && R.gte(duration, minimumDuration) ) {
    onChange(finalYears * 12 + finalMonths)
  }
}

const header = ({ duration, settings }) =>
  ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
    <Row onClick={toggle}>
      <Col xs={8}>
        <Label>Time</Label>
      </Col>
      <Col xs={2} className='text-right'>
        {isExpanded && <ResetValueButton field={duration}
          savedValue={getDuration(settings, 'months')} />}
      </Col>
      <Col xs={2} className='text-right' noGutterLeft>
        <TogglePanelButton isExpanded={isExpanded} />
      </Col>
    </Row>

const body = (_props) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { duration: { onChange, value }, recommendedValue } = _props

  return isExpanded
    ? <div>
      <InlineList className={classes.fields}>
        <InputGroup>
          <FormControl type='number'
            onChange={function (event) { handleChange(_props, { years: event.target.value }) }}
            value={getYears(_props)} />
          <InputGroup.Addon>
            years
          </InputGroup.Addon>
        </InputGroup>
        <InputGroup>
          <FormControl type='number' min={0} max={12}
            onChange={function (event) { handleChange(_props, { months: event.target.value }) }}
            value={getMonths(_props)} />
          <InputGroup.Addon>
            months
          </InputGroup.Addon>
        </InputGroup>
      </InlineList>
      {R.is(Number, recommendedValue) &&
        <RecommendedValue onChange={onChange} value={recommendedValue}
          display={prettyDuration(recommendedValue)} />}
    </div>
    : <Value>
      {R.is(Number, value) && prettyDuration(value)}
    </Value>
}

export default class Duration extends Component {
  static propTypes = {
    duration: PropTypes.object,
    recommendedValue: PropTypes.number,
    settings: PropTypes.object
  };

  static defaultProps = {
    settings: {}
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['duration', 'settings', 'recommendedValue'], this.props, nextProps)
  }

  render () {
    const { props } = this

    return (
      <Panel id='durationPanel' collapsible header={header(props)}>
        {body(props)}
      </Panel>
    )
  }
}
