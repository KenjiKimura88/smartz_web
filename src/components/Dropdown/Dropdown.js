import React, { Component, PropTypes } from 'react'
import { Dropdown as BsDropdown } from 'react-bootstrap'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Dropdown.scss'

export default class Dropdown extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    id: PropTypes.string.isRequired,
    onSelect: PropTypes.func,
    value: PropTypes.string.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['children', 'onSelect', 'value'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { children, id, onSelect, value } = props

    return (
      <BsDropdown className={classes.dropdown} id={id} onSelect={onSelect} {...props}>
        <BsDropdown.Toggle className={classes.dropdownToggle}>
          <span className={classes.selectedValue}>{value}</span>
        </BsDropdown.Toggle>
        <BsDropdown.Menu className={classes.dropdownMenu}>
          {children}
        </BsDropdown.Menu>
      </BsDropdown>
    )
  }
}
