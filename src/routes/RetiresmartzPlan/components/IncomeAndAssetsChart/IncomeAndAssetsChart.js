import React, { Component, PropTypes } from 'react'
import { Row } from 'react-bootstrap'
import { debounce } from 'lodash'
import classNames from 'classnames'
import d3 from 'd3'
import moment from 'moment'
import nv from 'nvd3'
import R from 'ramda'
import ReactDOMServer from 'react-dom/server'
import { formatCurrency, getMin, getMax, propsChanged, round } from 'helpers/pureFunctions'
import { getEpoch, getOnTrack, getAge } from 'routes/RetiresmartzWizard/helpers'
import AllCaps from 'components/AllCaps'
import Col from 'components/Col'
import config from 'config'
import classes from './IncomeAndAssetsChart.scss'
import LifeExpectancyForm from '../LifeExpectancyForm'
import nvIncomeChart from './nvIncomeChart'
import nvMultiBarChart from './nvMultiBarChart'
import RetirementAgeForm from '../RetirementAgeForm'
import Text from 'components/Text'

const axisXHeight = 40
const axisYWidth = 65
const incomeGraphHeight = 200
const assetsGraphHeight = 124

const tooltip = R.curry((context, assetsDatum, d) => {
  const elem = d.series[0]
  const x = elem.data.x
  const assetsValue = getAssetsValueOnX(assetsDatum, x)

  return R.isNil(d.series)
    ? false
    :  <div className={classes.tooltip}>
      {R.equals(elem.key, 'Target')
        ? <Row>
          <Col xs={5}>
            <Text bold>{elem.key}</Text>
          </Col>
          <Col xs={7} className='text-right'>
            <Text>{yTickFormat(context, elem.value)}</Text>
          </Col>
        </Row>
        : <div>
          <Row>
            <Col xs={12}>
              <Text bold>{elem.key}</Text>
              <Text> chance of getting</Text>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Text bold>{yTickFormat(context, elem.value)}</Text>
              <Text> of income</Text>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Text bold>{yTickFormat(context, assetsValue)}</Text>
              <Text> of assets</Text>
            </Col>
          </Row>
        </div>}
    </div>
})

const getAssetsValueOnX = (assetsDatum, x) => R.compose(
  R.prop('y'),
  R.defaultTo({}),
  R.find(R.propEq('x', x)),
  R.prop('values'),
  R.head
)(assetsDatum)

const getYears = (epoch, birthdate) => {
  const epochDiff = moment(epoch).diff(moment(birthdate))
  // convert to years
  return epochDiff / 3.154e+10
}

const getAssetsDatum = ({ data }) => ([
  {
    key: 'Assets',
    values: R.map(([time, assets, income]) => ({
      x: time * 8.64e7, // epoch days => epoch ms
      y: assets
    }), data)
  }
])

const xTickFormat = R.curry(({ intl: { formatDate } }, value) =>
  formatDate(value, { format: 'year' })
)

const yTickFormat = R.curry(({ intl: { formatNumber } }, value) =>
  formatCurrency(value, formatNumber)
)

const isValidRetirementAge = (retirementAge, lifeExpectancy, birthdate) =>
  retirementAge < lifeExpectancy &&
  retirementAge > getAge(birthdate)

const isValidLifeExpectancy = (lifeExpectancy, retirementAge) =>
  lifeExpectancy > retirementAge &&
  lifeExpectancy >= config.retirementPlan.MIN_LIFE_EXPECTANCY &&
  lifeExpectancy <= config.retirementPlan.MAX_LIFE_EXPECTANCY

export default class IncomeAndAssetsChart extends Component {
  static propTypes = {
    birthdate: PropTypes.string.isRequired,
    client: PropTypes.object,
    calculatedLifeExpectancy: PropTypes.number,
    children: PropTypes.node.isRequired,
    data: PropTypes.array.isRequired,
    desiredIncome: PropTypes.number.isRequired,
    fields: PropTypes.object.isRequired,
    hasPartner: PropTypes.bool.isRequired,
    healthDevicesConfig: PropTypes.object
  };

  static defaultProps = {
    desiredIncome: 0
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  constructor (props) {
    super(props)
    const { fields: { lifeExpectancy, retirementAge, partnerLifeExpectancy,
      partnerRetirementAge } } = props
    this.renderGraph = debounce(this.renderGraph, 20, { maxWait: 80 })
    this.state = {
      lifeExpectancyValue: lifeExpectancy.value,
      metricSystem: true,
      retirementAgeValue: retirementAge.value,
      partnerLifeExpectancyValue: partnerLifeExpectancy.value,
      partnerRetirementAgeValue: partnerRetirementAge.value
    }
  }

  componentWillReceiveProps (nextProps) {
    const { fields: { lifeExpectancy, retirementAge, partnerLifeExpectancy,
      partnerRetirementAge } } = nextProps
    this.setState({
      lifeExpectancyValue: lifeExpectancy.value,
      retirementAgeValue: retirementAge.value,
      partnerLifeExpectancyValue: partnerLifeExpectancy.value,
      partnerRetirementAgeValue: partnerRetirementAge.value
    })
  }

  componentDidMount () {
    nv.addGraph(this.renderGraph.bind(this))
    window.addEventListener('resize', this.handleResize)
  }

  shouldComponentUpdate (nextProps, nextState) {
    const { props, state } = this
    return propsChanged(['calculatedLifeExpectancy', 'dailyExercise', 'drinks', 'hasPartner',
      'height', 'lifeExpectancy', 'partnerBirthdate', 'partnerDailyExercise', 'partnerDrinks',
      'partnerHeight', 'partnerLifeExpectancy', 'partnerRetirementAge', 'partnerSmoker',
      'partnerWeight', 'retirementAge', 'smoker', 'weight'], props.fields, nextProps.fields) ||
    propsChanged(['birthdate', 'client', 'children', 'data', 'hasPartner', 'healthDevicesConfig'],
      props, nextProps) ||
    propsChanged(['lifeExpectancyValue', 'retirementAgeValue', 'partnerLifeExpectancyValue',
      'partnerRetirementAgeValue', 'metricSystem'], state, nextState)
  }

  componentDidUpdate () {
    this.renderGraph()
  }

  componentWillUnmount () {
    this.removeTooltips()
    window.removeEventListener('resize', this.handleResize)
  }

  handleResize = (event) => {
    this.renderGraph()
  }

  getIncomeDatum() {
    const { birthdate, data, fields: { lifeExpectancy, partnerBirthdate,
      partnerLifeExpectancy, partnerRetirementAge, retirementAge }, hasPartner } = this.props
    const { lifeExpectancyValue, retirementAgeValue, partnerLifeExpectancyValue,
      partnerRetirementAgeValue } = this.state
    const retirementDate = getEpoch(retirementAgeValue, birthdate)
    const partnerRetirementDate = getEpoch(partnerRetirementAgeValue, partnerBirthdate.value)
    const lifeExpectancyDate = getEpoch(lifeExpectancyValue, birthdate)
    const partnerLifeExpectancyDate = getEpoch(partnerLifeExpectancyValue, partnerBirthdate.value)
    const values = R.map(([time, assets, income]) => ({
      x: time * 8.64e7, // epoch days => epoch ms
      y: income
    }), data)
    const targetValues = R.compose(
      R.reject(R.isNil),
      R.map(([time, assets, income, desiredIncome]) => {
        const x = time * 8.64e7
        return x < retirementDate
          ? null
          : { x, y: desiredIncome }
      })
    )(data)
    this.onTrack = getOnTrack(retirementDate, values, targetValues)
    const { lifeExpectancyTooltip, partnerLifeExpectancyTooltip, partnerRetirementTooltip,
      retirementTooltip } = this.refs
    const clientMarkers = [
      {
        x: retirementDate,
        y: 0,
        key: 'Retirement',
        classed: classes.retirementMarker,
        target: values,
        onDrag: (epoch) => {
          const newRetirementAge = round(getYears(epoch, birthdate), 0)
          const { lifeExpectancyValue } = this.state
          if (isValidRetirementAge(newRetirementAge, lifeExpectancyValue, birthdate)) {
            this.setState({
              retirementAgeValue: newRetirementAge
            })
          }
        },
        onDragEnd: () => retirementAge.onChange(this.state.retirementAgeValue),
        tooltipEl: retirementTooltip
      },
      {
        x: lifeExpectancyDate,
        y: 0,
        key: 'Life Expectancy',
        classed: classes.lifeExpectancyMarker,
        target: values,
        onDrag: (epoch) => {
          const newLifeExpectancy = round(getYears(epoch, birthdate), 0)
          const { retirementAgeValue } = this.state
          if (isValidLifeExpectancy(newLifeExpectancy, retirementAgeValue)) {
            this.setState({
              lifeExpectancyValue: newLifeExpectancy
            })
          }
        },
        onDragEnd: () => lifeExpectancy.onChange(this.state.lifeExpectancyValue),
        tooltipEl: lifeExpectancyTooltip
      }
    ]
    const partnerMarkers = [
      {
        x: partnerRetirementDate,
        y: 0,
        key: 'Partner\'s Retirement',
        classed: classes.retirementMarker,
        target: values,
        onDrag: (epoch) => {
          const newRetirementAge = round(getYears(epoch, partnerBirthdate.value), 0)
          const { partnerLifeExpectancyValue } = this.state
          if (isValidRetirementAge(newRetirementAge, partnerLifeExpectancyValue,
            partnerBirthdate.value)) {
            this.setState({
              partnerRetirementAgeValue: newRetirementAge
            })
          }
        },
        onDragEnd: () => partnerRetirementAge.onChange(this.state.partnerRetirementAgeValue),
        tooltipEl: partnerRetirementTooltip
      },
      {
        x: partnerLifeExpectancyDate,
        y: 0,
        key: 'Partner\'s Life Expectancy',
        classed: classes.lifeExpectancyMarker,
        target: values,
        onDrag: (epoch) => {
          const newLifeExpectancy = round(getYears(epoch, partnerBirthdate.value), 0)
          const { partnerRetirementAgeValue } = this.state
          if (isValidLifeExpectancy(newLifeExpectancy, partnerRetirementAgeValue)) {
            this.setState({
              partnerLifeExpectancyValue: newLifeExpectancy
            })
          }
        },
        onDragEnd: () => partnerLifeExpectancy.onChange(this.state.partnerLifeExpectancyValue),
        tooltipEl: partnerLifeExpectancyTooltip
      }
    ]
    const markers = hasPartner ? R.concat(partnerMarkers, clientMarkers) : clientMarkers

    return [
      {
        key: '90%',
        type: 'line',
        classed: classes.incomeLine,
        values
      },
      {
        key: 'Target',
        type: 'line',
        classed: classNames(classes.targetLine, {
          [classes.onTrack]: this.onTrack
        }),
        values: targetValues
      },
      {
        key: 'Markers',
        type: 'marker',
        disableTooltip: true,
        classed: classes.marker,
        values: markers
      }
    ]
  }

  renderGraph () {
    const { context, props } = this
    const incomeDatum = this.getIncomeDatum()
    const assetsDatum = getAssetsDatum(props)
    const income = d3.select(this.refs.income)
    const assets = d3.select(this.refs.assets)
    this.incomeChart = this.incomeChart || nvIncomeChart()
    this.assetsChart = this.assetsChart || nvMultiBarChart()

    const lines = R.filter(R.propEq('type', 'line'), incomeDatum)
    const incomeMinY = getMin('y')(lines)
    const incomeMaxY = getMax('y')(lines)
    const incomeYRange = incomeMaxY - incomeMinY
    const lowerBound = incomeMinY - 0.15 * incomeYRange
    const finalLowerBound = lowerBound < 0 ? 0 : lowerBound
    this.incomeChart
      .margin({ left: 0, right: axisYWidth, top: 0, bottom: 0 })
      .height(incomeGraphHeight)
      .guidelineHeight(incomeGraphHeight + assetsGraphHeight)
      .forceY([finalLowerBound, incomeMaxY + 0.25 * incomeYRange])

    this.incomeChart.yAxis2
      .tickFormat(yTickFormat(context))
      .showMaxMin(false)
      .ticks(5)

    const assetsMinY = getMin('y')(assetsDatum)
    const assetsMaxY = getMax('y')(assetsDatum)
    const assetsYRange = assetsMaxY - assetsMinY

    this.assetsChart
      .margin({ left: 0, right: axisYWidth, top: 20, bottom: axisXHeight + 1 })
      .noData('')
      .showControls(false)
      .showLegend(false)
      .rightAlignYAxis(true)
      .height(assetsGraphHeight)
      .forceY([0, assetsMaxY + 0.1 * assetsYRange])

    this.assetsChart.xAxis
      .tickFormat(xTickFormat(context))

    this.assetsChart.yAxis
      .tickFormat(yTickFormat(context))
      .showMaxMin(false)
      .ticks(4)

    income.datum(incomeDatum).call(this.incomeChart)
    assets.datum(assetsDatum).call(this.assetsChart)

    this.incomeChart.interactiveLayer.tooltip.contentGenerator(
      R.compose(
        ReactDOMServer.renderToStaticMarkup,
        tooltip(context, assetsDatum)
      )
    )

    d3.select(`.${classes.datumText}`).attr('visibility', 'visible')

    return this.incomeChart
  }

  removeTooltips () {
    // Remove ghost tooltips
    d3.selectAll('.nvtooltip').remove()
  }

  setMetricSystem = (metricSystem) => {
    this.setState({ metricSystem })
  }

  render () {
    const { props, setMetricSystem, state } = this
    const { birthdate, calculatedLifeExpectancy, children, client, fields,
      fields: { partnerRetirementAge, partnerDailyExercise, partnerDrinks, partnerHeight,
        partnerLifeExpectancy, partnerSmoker, partnerWeight, partnerBirthdate, retirementAge
      }, hasPartner, healthDevicesConfig } = props
    const { lifeExpectancyValue, metricSystem, partnerLifeExpectancyValue,
      partnerRetirementAgeValue, retirementAgeValue } = state
    const incomeDatum = this.getIncomeDatum()
    const isDatumEmpty = R.either(R.isEmpty, R.isNil)(incomeDatum)
    const className = classNames({
      [classes.projectionGraph]: true,
      [classes.isDatumEmpty]: isDatumEmpty,
      [classes.hasChildren]: !!children,
      [classes.hasPartner]: hasPartner
    })
    const partnerLifeExpectancyFields = {
      dailyExercise: partnerDailyExercise,
      drinks: partnerDrinks,
      height: partnerHeight,
      lifeExpectancy: partnerLifeExpectancy,
      smoker: partnerSmoker,
      weight: partnerWeight,
      title: 'Life Expectancy'
    }

    return (
      <div className={className} onBlur={this.removeTooltips}>
        <div ref='root' className={classes.graph}>
          {children &&
            <div className={classes.childrenWrapper}>
              {children}
            </div>}
          <div className={classes.assetsFillBg} />
          <div className={classes.axisYFillBg} />
          <div className={classes.axisXFillBg} />
          <svg ref='income' className={classes.incomeGraph} style={{ height: incomeGraphHeight }}>
            <svg>
              <g className={classes.datumText} visibility='hidden'>
                <text ref='targetText' x='20' y='30%'>
                  {this.onTrack
                    ? <tspan className={classes.success}>ON TRACK </tspan>
                    : <tspan className={classes.danger}>OFF TRACK </tspan>}
                </text>
              </g>
            </svg>
          </svg>
          <svg ref='assets' className={classes.assetsGraph} style={{ height: assetsGraphHeight }} />
          <div className={classes.incomeTitle}>
            <Text bold>
              <AllCaps>Income</AllCaps>
            </Text>
          </div>
          <div className={classes.assetsTitle}>
            <Text bold>
              <AllCaps>Assets</AllCaps>
            </Text>
          </div>
          <div className={classes.markerTooltip} ref='retirementTooltip'>
            <RetirementAgeForm retirementAge={retirementAge} birthdate={birthdate}
              retirementAgeValue={retirementAgeValue} title='Retirement Age' />
          </div>
          <div className={classes.markerTooltip} ref='lifeExpectancyTooltip'>
            <LifeExpectancyForm calculatedLifeExpectancy={calculatedLifeExpectancy}
              client={client} fields={fields} healthDevicesConfig={healthDevicesConfig}
              lifeExpectancyValue={lifeExpectancyValue} metricSystem={metricSystem}
              setMetricSystem={setMetricSystem} title='Life Expectancy' />
          </div>
          {hasPartner &&
            <div>
              <div className={classes.markerTooltip} ref='partnerRetirementTooltip'>
                <RetirementAgeForm retirementAge={partnerRetirementAge}
                  birthdate={partnerBirthdate.value}
                  retirementAgeValue={partnerRetirementAgeValue} title='Retirement Age' />
              </div>
              <div className={classes.markerTooltip} ref='partnerLifeExpectancyTooltip'>
                <LifeExpectancyForm calculatedLifeExpectancy={calculatedLifeExpectancy}
                  fields={partnerLifeExpectancyFields} healthDevicesConfig={healthDevicesConfig}
                  lifeExpectancyValue={partnerLifeExpectancyValue} setMetricSystem={setMetricSystem}
                  metricSystem={metricSystem} title='Life Expectancy' />
              </div>
            </div>}
        </div>
      </div>
    )
  }
}
