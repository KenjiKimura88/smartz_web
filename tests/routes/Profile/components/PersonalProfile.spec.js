import React from 'react'
import { mount, shallow } from 'enzyme'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import PersonalProfile from 'routes/Profile/components/PersonalProfile/PersonalProfile'

describe('(Route) Profile', () => {
  describe('(Component) PersonalProfile', () => {
    const client = {
      id: 2,
      date_of_birth: '1972-09-22',
      gender: 'Male',
      phone_num: '+61424888888',
      civil_status: null,
      tax_file_number: '85755',
      net_worth: 2000000,
      occupation: 'President',
      employer: 'Government of the United States',
      residential_address: {
        id: 171,
        region: {
          id: 50,
          name: 'NSW',
          code: 'NSW',
          country: 'AU'
        },
        address: '18 Mitchell Street\nPaddington\nSydney',
        post_code: '2021',
        global_id: null
      },
      user: {
        id: 1,
        first_name: 'John',
        last_name: 'Doe',
        username: 'johndoe',
        middle_name: 'm',
        email:'johndoe@example.org',
      }
    }

    const settings = {
      civil_statuses: [
        {
          id: 0,
          name: 'SINGLE'
        },
        {
          id: 1,
          name: "MARRIED"
        }
      ],
      employment_statuses: [
        {
          id: 0,
          name: "Employed (full-time)"
        },
        {
          id: 1,
          name: "Employed (part-time)"
        },
        {
          id: 2,
          name: "Self-employed"
        }
      ]
    }

    const securityQuestions = [{
      id: 245,
      question: "What was the name of your favorite childhood friend?",
      user: 6
    }]

    const data = {
      clients: [client],
      globalSettings: [settings],
      securityQuestions
    }

    const state = {
      api: {
        data
      },
      modal: {

      },
      form: {

      }
    }

    const fields = {
      first_name: {
        name: 'first_name',
        value: 'John',
        onChange: function () {}
      },
      last_name: {
        name: 'last_name',
        value: 'Doe',
        onChange: function () {}
      },
      'middle_name': {
        name: 'middle_name',
        value: 'M',
        onChange: function () {}
      },
      'email': {
        name: 'email',
        value: 'johndoe@example.org',
        onChange: function () {}
      },
      'phone_num': {
        name: 'phone_num',
        value: '+1 (930) 393-0933',
        onChange: function () {}
      },
      'gender': {
        name: 'gender',
        value: 'Male',
        onChange: function () {}
      },
      'date_of_birth': {
        name: 'date_of_birth',
        value: '1972-09-12',
        onChange: function () {}
      },
      'ssn': {
        name: 'ssn',
        value: '55555',
        onChange: function () {}
      },
      'civil_status': {
        name: 'civil_status',
        value: 0,
        onChange: function () {}
      },
      'country': {
        name: 'country',
        value: 'US',
        onChange: function () {}
      },
      'state': {
        name: 'state',
        value: 'NY',
        onChange: function () {}
      }
    }

    const handleSubmit = sinon.spy()

    const getComponent = (props) => {
      const store = configureStore()(state)
      return (
        <Provider store={store}>
          <PersonalProfile settings={settings} updateProfile={function() {}}
            refreshProfile={function () {}} updateUser={function() {}} handleSubmit={handleSubmit}
            errors={{}} fields={fields} show={function() {}} requests={{}} hide={function() {}}
            {...props} />
        </Provider>
      )
    }

    it('renders as a <PersonalProfile />', () => {
      const wrapper = shallow(getComponent())
      expect(wrapper.type()).to.equal(PersonalProfile)
    })

    it('populates initial values from `fields`', () => {
      const wrapper = mount(getComponent())
      expect(wrapper.find('input[name="first_name"]').get(0).value)
        .to.equal(fields.first_name.value)
      expect(wrapper.find('input[name="last_name"]').get(0).value).to.equal(fields.last_name.value)
      expect(wrapper.find('input[name="middle_name"]').get(0).value)
        .to.equal(fields.middle_name.value)
      expect(wrapper.find('input[name="email"]').get(0).value).to.equal(fields.email.value)
      expect(parseInt(wrapper.find({ name: 'civil_status' }).prop('value'), 10))
        .to.equal(fields.civil_status.value)
    })

    it('disables `Update Info` button when field errors exist.', () => {
      const errors = {
        first_name: ["First Name is required."]
      }
      const wrapper = mount(getComponent({ errors }))
      expect(wrapper.find({type: 'submit', disabled: true})).to.have.length(1)
    })

    it('triggers `handleSubmit` on UpdateInfo button click.', () => {
      const wrapper = mount(getComponent())
      wrapper.find({type: 'submit'}).simulate('click')
      handleSubmit.should.have.been.called
    })
  })
})
