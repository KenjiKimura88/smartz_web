import React from 'react'
import { shallow } from 'enzyme'
import Value from 'components/Value/Value'

describe('(Component) Value', () => {
  const getComponent = (props) =>
    <Value>Foo</Value>

  it('renders as a <dt />', () => {
    const wrapper = shallow(getComponent())
    expect(wrapper.type()).to.equal('dd')
  })
})
