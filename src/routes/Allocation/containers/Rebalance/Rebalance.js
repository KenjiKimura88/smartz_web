import React, { Component, PropTypes } from 'react'
import { Row } from 'react-bootstrap'
import { FormattedNumber } from 'react-intl'
import R from 'ramda'
import { propsChanged } from 'helpers/pureFunctions'
import { thresholdOptions } from 'schemas/rebalance'
import Button from 'components/Button'
import Col from 'components/Col'
import DriftBar from 'components/DriftBar'
import Label from 'components/Label'
import Panel from 'containers/Panel'
import Select from 'components/Select/Select'
import TogglePanelButton from 'components/TogglePanelButton'
import Value from 'components/Value'

const getThresholdOptions = formatNumber => (
  R.map((value) => ({
    value: value,
    label: formatNumber(value, { format: 'percent' })
  }), thresholdOptions)
)

const header = ({ values }) => ({ isExpanded, toggle }) => // eslint-disable-line react/prop-types
  <Row onClick={toggle}>
    <Col xs={values.rebalance ? 4 : 10}>
      <Label>Rebalancing</Label>
    </Col>
    {values.rebalance &&
      <Col xs={6} noGutterRight>
        <Label>When Drift Reaches</Label>
      </Col>}
    <Col xs={2} className='text-right' noGutterLeft>
      <TogglePanelButton isExpanded={isExpanded} />
    </Col>
  </Row>

const body = (_props, context) => ({ isExpanded }) => { // eslint-disable-line react/prop-types
  const { driftScore, fields: { rebalance, rebalanceThreshold }, values } = _props
  const { intl: { formatNumber } } = context
  const thresholdOptions = getThresholdOptions(formatNumber)
  const rebalanceValue = values.rebalance
  const onButtonClick = () => rebalance.onChange(!rebalanceValue)

  return (
    <div>
      <Row>
        <Col xs={rebalanceValue ? 4 : 10}>
          {isExpanded
            ? <Button bsStyle='primary' onClick={onButtonClick}>
              {rebalanceValue ? 'Disable': 'Enable'}
            </Button>
            : <Value>{rebalanceValue ? 'Enabled' : 'Disabled'}</Value>}
        </Col>
        {rebalanceValue &&
          <Col xs={6} noGutterRight>
            {isExpanded
              ? <Select {...rebalanceThreshold} options={thresholdOptions} searchable={false}
                clearable={false} />
              : <Value>
                <FormattedNumber value={values.rebalanceThreshold} format='percent' />
              </Value>}
          </Col>}
      </Row>
      <DriftBar driftScore={driftScore} />
    </div>
  )
}

export default class Rebalance extends Component {
  static propTypes = {
    driftScore: PropTypes.number.isRequired,
    fields: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    viewOnly: PropTypes.bool
  };

  static contextTypes = {
    intl: PropTypes.object.isRequired
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['driftScore', 'fields', 'values', 'viewOnly'], this.props, nextProps)
  }

  render () {
    const { context, props } = this

    return (
      <Panel id='rebalance' collapsible header={header(props)}>
        {body(props, context)}
      </Panel>
    )
  }
}
