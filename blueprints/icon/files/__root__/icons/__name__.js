/* eslint-disable max-len */
import React, { Component } from 'react'
import IconBase from 'react-icon-base'

export default class <%= pascalEntityName %> extends Component {
  render() {
    return (
      <IconBase viewBox='0 0 40 40' {...this.props}>
        <g>
          <path d='M16 48 L48 48 L48 16 L16 16 Z' />
        </g>
      </IconBase>
    )
  }
}
