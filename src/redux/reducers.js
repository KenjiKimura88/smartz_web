import { reducer as modal } from 'redux-modal'
import { reducer as form } from 'redux-form'
import { combineReducers } from 'redux/api'
import activity from './modules/activity'
import allocation from './modules/allocation'
import goals from './modules/goals'
import isAuthenticated, { isPartnerAuthenticated } from './modules/auth'
import panel from 'containers/Panel/modules/panel'
import performance from './modules/performance'
import portfolio from './modules/portfolio'
import retiresmartz from './modules/retiresmartz'
import transfer from './modules/transfer'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    // Add sync reducers here
    activity,
    allocation,
    isAuthenticated,
    isPartnerAuthenticated,
    form,
    goals,
    modal,
    panel,
    performance,
    portfolio,
    retiresmartz,
    transfer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
