export default [
  {
    label: 'Andaman and Nicobar Islands',
    value: 'AN'
  },
  {
    label: 'Andhra Pradesh',
    value: 'AP'
  },
  {
    label: 'Arunachal Pradesh',
    value: 'AR'
  },
  {
    label: 'Assam',
    value: 'AS'
  },
  {
    label: 'Bihar',
    value: 'BR'
  },
  {
    label: 'Chandigarh',
    value: 'CH'
  },
  {
    label: 'Chhattisgarh',
    value: 'CT'
  },
  {
    label: 'Dadra and Nagar Haveli',
    value: 'DN'
  },
  {
    label: 'Daman and Diu',
    value: 'DD'
  },
  {
    label: 'Delhi',
    value: 'DL'
  },
  {
    label: 'Goa',
    value: 'GA'
  },
  {
    label: 'Gujarat',
    value: 'GJ'
  },
  {
    label: 'Haryana',
    value: 'HR'
  },
  {
    label: 'Himachal Pradesh',
    value: 'HP'
  },
  {
    label: 'Jammu and Kashmir',
    value: 'JK'
  },
  {
    label: 'Jharkhand',
    value: 'JH'
  },
  {
    label: 'Karnataka',
    value: 'KA'
  },
  {
    label: 'Kerala',
    value: 'KL'
  },
  {
    label: 'Lakshadweep',
    value: 'LD'
  },
  {
    label: 'Madhya Pradesh',
    value: 'MP'
  },
  {
    label: 'Maharashtra',
    value: 'MH'
  },
  {
    label: 'Manipur',
    value: 'MN'
  },
  {
    label: 'Meghalaya',
    value: 'ML'
  },
  {
    label: 'Mizoram',
    value: 'MZ'
  },
  {
    label: 'Nagaland',
    value: 'NL'
  },
  {
    label: 'Odisha',
    value: 'OR'
  },
  {
    label: 'Puducherry',
    value: 'PY'
  },
  {
    label: 'Punjab',
    value: 'PB'
  },
  {
    label: 'Rajasthan',
    value: 'RJ'
  },
  {
    label: 'Sikkim',
    value: 'SK'
  },
  {
    label: 'Telangana',
    value: 'TG'
  },
  {
    label: 'Tamil Nadu',
    value: 'TN'
  },
  {
    label: 'Tripura',
    value: 'TR'
  },
  {
    label: 'Uttar Pradesh',
    value: 'UP'
  },
  {
    label: 'Uttarakhand',
    value: 'UL'
  },
  {
    label: 'West Bengal',
    value: 'WB'
  },
]
