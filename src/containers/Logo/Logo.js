import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import R from 'ramda'
import { connect } from 'redux/api'
import { getProfile } from 'redux/modules/auth'
import { propsChanged } from 'helpers/pureFunctions'
import classes from './Logo.scss'

// getAdvisor :: Props -> Object | undefined
const getAdvisor = R.compose(
  R.either(R.prop('advisor'), R.path(['client', 'advisor'])),
  R.defaultTo({}),
  R.prop('user')
)

// getFirmId :: Props -> Object | undefined
const getFirmId = R.compose(
  R.prop('firm'),
  R.defaultTo({}),
  getAdvisor
)

// getLogoPropKey :: Props -> String
const getLogoPropKey = R.ifElse(
  R.prop('colored'),
  R.always('colored_logo'),
  R.always('logo')
)

// getLogo :: Props -> String | undefined
const getLogo = (props) => {
  const { logo } = props
  const logoPropKey = getLogoPropKey(props)
  return logo || R.path(['firm', logoPropKey])(props)
}

// getTitle :: Props -> String | undefined
const getTitle = R.path(['firm', 'name'])

export class Logo extends Component {
  static propTypes = {
    className: PropTypes.string,
    colored: PropTypes.bool,
    firm: PropTypes.object,
    logo: PropTypes.string,
    user: PropTypes.object
  };

  shouldComponentUpdate (nextProps) {
    return propsChanged(['className', 'colored', 'firm', 'logo', 'user'], this.props, nextProps)
  }

  render () {
    const { props } = this
    const { className } = props
    const logo = getLogo(props)

    return logo
      ? (
        <Link to='/' className={className}>
          <img src={logo} title={getTitle(props)} className={classes.logo} />
        </Link>
      )
      : false
  }
}

const requests1 = ({ logo }) => ({
  user: !logo && getProfile
})

const requests2 = (props) => {
  const firmId = getFirmId(props)
  return !props.logo && firmId
    ? {
      firm: ({ findOne }) => findOne({
        type: 'firms',
        url: `/firm/${firmId}`,
        id: firmId
      })
    }
    : {}
}

export default R.compose(
  connect(requests1),
  connect(requests2)
)(Logo)
