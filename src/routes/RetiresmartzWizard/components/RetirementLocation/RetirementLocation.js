import React, { Component, PropTypes } from 'react'
import { Col, ControlLabel, FormControl, FormGroup, Row } from 'react-bootstrap'
import AddressAutocomplete from 'react-address-autocomplete'
import R from 'ramda'
import { domOnlyProps } from 'helpers/pureFunctions'
import Button from 'components/Button'
import CurrencyInput from 'components/CurrencyInput'
import FieldError from 'components/FieldError'
import H2 from 'components/H2'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Text from 'components/Text'
import Well from 'components/Well'

export default class RetirementLocation extends Component {
  static propTypes = {
    ballparkEstimate: PropTypes.object.isRequired,
    downsizeOrCondo: PropTypes.object.isRequired,
    dreamLocation: PropTypes.object.isRequired,
    remainInSameHome: PropTypes.object.isRequired,
    reverseMortgage: PropTypes.object.isRequired,
    sameLocation: PropTypes.object.isRequired
  };

  constructor(props){
    super(props)
    this.state = {
      autoLocation: {
        search: props.dreamLocation.value,
        latitude: Infinity,
        longitude: Infinity
      }
    }
  }

  render() {
    const { ballparkEstimate, downsizeOrCondo, dreamLocation, remainInSameHome, reverseMortgage,
        sameLocation } = this.props

    return (
      <div>
        <FormGroup id='title'>
          <H2 noMargin>Tell us about where you want to live in retirement</H2>
          <div>
            <Text light>
              Now that you have a picture of what your retirement lifestyle determine where you want
              to enjoy your golden years.
            </Text>
          </div>
        </FormGroup>
        <FormGroup id='remainInSameHome'>
          <H2>Do you want to remain in the same home?</H2>
          <div>
            <RadioButtonGroup type='radio' {...domOnlyProps(remainInSameHome)}>
              <Button bsStyle='wide' value>Yes</Button>
              <Button bsStyle='wide' value={false}>No</Button>
            </RadioButtonGroup>
            <Row>
              <Col xs={4}>
                <FieldError for={remainInSameHome} />
              </Col>
            </Row>
          </div>
        </FormGroup>
        {R.equals(remainInSameHome.value, false) && (
          <FormGroup id='sameLocation'>
            <H2>Do you want to remain in the same location?</H2>
            <div>
              <RadioButtonGroup type='radio' {...domOnlyProps(sameLocation)}>
                <Button bsStyle='wide' value>Yes</Button>
                <Button bsStyle='wide' value={false}>No</Button>
              </RadioButtonGroup>
              <Row>
                <Col xs={4}>
                  <FieldError for={sameLocation} />
                </Col>
              </Row>
            </div>
          </FormGroup>
        )}
        {R.equals(remainInSameHome.value, false) &&
          R.equals(sameLocation.value, false) && (
          <FormGroup>
            <H2>Where is your dream location to live?</H2>
            <Row>
              <Col xs={6}>
                <ControlLabel>City or Zip Code</ControlLabel>
                <FormControl type='hidden' {...domOnlyProps(dreamLocation)} />
                <AddressAutocomplete
                  name='address1'
                  className='form-control'
                  placeholder=''
                  value={this.state.autoLocation}
                  onChange={data => {this.setState({autoLocation:data})
                                     dreamLocation.onChange(data.search)}}
                  apiKey={'mapzen-9pnZ5YT'} />
                <FieldError for={dreamLocation} />
              </Col>
            </Row>
          </FormGroup>
        )}
        {R.equals(remainInSameHome.value, false) && (
          <FormGroup id='downsizeOrCondo'>
            <H2>Would you like to downsize or change lifestyle, like a condo?</H2>
            <div>
              <RadioButtonGroup type='radio' {...domOnlyProps(downsizeOrCondo)}>
                <Button bsStyle='wide' value>Yes</Button>
                <Button bsStyle='wide' value={false}>No</Button>
              </RadioButtonGroup>
              <Row>
                <Col xs={4}>
                  <FieldError for={downsizeOrCondo} />
                </Col>
              </Row>
            </div>
          </FormGroup>
        )}
        {R.equals(downsizeOrCondo.value, true) && (
          <FormGroup controlId='ballparkEstimate'>
            <H2>Give us a ballpark estimate of the cost of your dream property in today's $</H2>
            <Row>
              <Col xs={5}>
                <CurrencyInput {...domOnlyProps(ballparkEstimate)} />
                <FieldError for={ballparkEstimate} />
              </Col>
            </Row>
          </FormGroup>
        )}
        <FormGroup id='reverseMortgage'>
          <H2>Would you consider a reverse mortgage as a source of retirement income?</H2>
          <div>
            <RadioButtonGroup type='radio' {...domOnlyProps(reverseMortgage)}>
              <Button bsStyle='wide' value>Yes</Button>
              <Button bsStyle='wide' value={false}>No</Button>
            </RadioButtonGroup>
            <Row>
              <Col xs={4}>
                <FieldError for={reverseMortgage} />
              </Col>
            </Row>
          </div>
        </FormGroup>
        {R.equals(reverseMortgage.value, true) && (
          <Well>
            We will assume you have used a reverse mortgage over your home in retirement.
          </Well>
        )}
      </div>
    )
  }
}
